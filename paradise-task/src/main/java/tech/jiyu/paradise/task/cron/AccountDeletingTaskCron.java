package tech.jiyu.paradise.task.cron;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;
import tech.jiyu.paradise.core.account.domain.AccountDeletingTask;
import tech.jiyu.paradise.core.account.service.AccountService;
import tech.jiyu.paradise.task.service.AccountDeletingTaskDelegate;

import java.util.List;

@Component
@Slf4j
@ConditionalOnProperty(name = "paradise.account.deleting.task", havingValue = "true")
public class AccountDeletingTaskCron {

    @Autowired
    private AccountService accountService;

    @Autowired
    private AccountDeletingTaskDelegate accountDeletingTaskDelegate;

    @Scheduled(cron = "0 * * * * *")
    public void executeAccountDeletingTask() {
        List<AccountDeletingTask> taskList = accountDeletingTaskDelegate.getTaskToProcess(); // 冷静期较长，不方便测试，因此提供该类去扩展，业务可自行实现以便测试
        if (CollectionUtils.isEmpty(taskList)) {
            log.info("find 0 account deleting task");
            return;
        }
        log.info("find {} account deleting tasks", taskList.size());
        for (AccountDeletingTask task : taskList) {
            accountService.deleteById(task.getAccountId());
            log.info("execute account deleting task completely. taskId: {} accountId: {}", task.getId(), task.getAccountId());
        }
    }

}
