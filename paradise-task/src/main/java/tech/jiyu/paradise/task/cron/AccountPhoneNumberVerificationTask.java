package tech.jiyu.paradise.task.cron;

import com.google.common.collect.Lists;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnExpression;
import org.springframework.stereotype.Component;

import java.util.List;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import tech.jiyu.paradise.core.account.dao.mapper.AccountPhoneNumberVerificationResultMapper;
import tech.jiyu.paradise.core.account.dao.po.AccountPhoneNumber;
import tech.jiyu.paradise.core.account.dao.po.AccountPhoneNumberVerificationResult;
import tech.jiyu.utility.phonenumber.PhoneNumberUtils;
import tech.jiyu.utility.phonenumber.PhoneNumberVerificationResult;

/**
 * @author Simon [simonchan@jinjie.tech]
 */
@Slf4j
@Component
@RequiredArgsConstructor
@ConditionalOnExpression("#{'true'.equals(systemProperties['enabledAccountPhoneNumberVerificationTask'])}")
public class AccountPhoneNumberVerificationTask implements InitializingBean {

    private final AccountPhoneNumberVerificationResultMapper mapper;

    @Override
    public void afterPropertiesSet() {
        new Thread(this::startup, "account-phone-number-verification-task").start();
    }

    public void startup() {
        log.info("AccountPhoneNumberVerificationTask: starting...");
        long minId = -1L;
        int batch = 1;
        int totalPhoneNums = 0;
        int totalWrongNums = 0;
        List<AccountPhoneNumber> accountPhoneNumbers = mapper.selectByIdRange(minId);
        while (!accountPhoneNumbers.isEmpty()) {
            log.info("AccountPhoneNumberVerificationTask: process batch {}, size: {}", batch, accountPhoneNumbers.size());
            totalPhoneNums += accountPhoneNumbers.size();
            List<AccountPhoneNumberVerificationResult> results = Lists.newArrayList();
            for (AccountPhoneNumber accountPhoneNumber : accountPhoneNumbers) {
                log.info("verify for {}", accountPhoneNumber);
                String countryCode = accountPhoneNumber.getCountryCode();
                String phoneNumber = accountPhoneNumber.getPhoneNumber();
                String reason = "unknown";
                PhoneNumberVerificationResult phoneNumberVerificationResult = PhoneNumberUtils.verifyPhoneNumberAndFormat(countryCode, phoneNumber);
                Boolean isValid = phoneNumberVerificationResult.getIsValid();
                if (isValid && StringUtils.equals(phoneNumber, phoneNumberVerificationResult.getPhoneNumber())) {
                    continue;
                }
                // 手机开头包含区号
                if (phoneNumber.startsWith(countryCode) && isValid) {
                    reason = "starts_with_country_code";
                }
                // 手机开头包含0
                else if (phoneNumber.startsWith("0") && isValid) {
                    reason = "starts_with_zero";
                }
                // 国家选错（国家码里包含了手机号）
                else if (!StringUtils.equals(countryCode, phoneNumberVerificationResult.getCountryCode()) && isValid) {
                    reason = "wrong_country_code";
                }
                AccountPhoneNumberVerificationResult checkResult = new AccountPhoneNumberVerificationResult();
                checkResult.setAccountId(accountPhoneNumber.getId());
                checkResult.setOriginalCountryCode(countryCode);
                checkResult.setOriginalPhoneNumber(accountPhoneNumber.getPhoneNumber());
                checkResult.setCorrectCountryCode(isValid ? phoneNumberVerificationResult.getCountryCode() : "");
                checkResult.setCorrectPhoneNumber(isValid ? phoneNumberVerificationResult.getPhoneNumber() : "");
                checkResult.setReason(reason);
                results.add(checkResult);
            }
            if (CollectionUtils.isNotEmpty(results)) {
                log.info("batch {} has {} wrong phone numbers.", batch, results.size());
                mapper.batchInsertResults(results);
                totalWrongNums += results.size();
            }
            minId = accountPhoneNumbers.get(accountPhoneNumbers.size() - 1).getId();
            accountPhoneNumbers = mapper.selectByIdRange(minId);
            batch++;
        }
        log.info("AccountPhoneNumberVerificationTask: finished... totalPhoneNums: {}, totalWrongNums: {}", totalPhoneNums, totalWrongNums);
    }
}
