package tech.jiyu.paradise.task.conf;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import tech.jiyu.paradise.core.account.service.AccountDeletingTaskService;
import tech.jiyu.paradise.task.service.AccountDeletingTaskDelegate;
import tech.jiyu.paradise.task.service.impl.AccountDeletingTaskDelegateImpl;

@Configuration
@EnableScheduling
public class ParadiseTaskConf {

    @Bean
    @Autowired
    @ConditionalOnMissingBean(AccountDeletingTaskDelegate.class)
    public AccountDeletingTaskDelegate accountDeletingTaskDelegate(AccountDeletingTaskService accountDeletingTaskService) {
        return new AccountDeletingTaskDelegateImpl(accountDeletingTaskService);
    }
}
