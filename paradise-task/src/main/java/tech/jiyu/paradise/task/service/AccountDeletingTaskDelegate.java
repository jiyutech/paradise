package tech.jiyu.paradise.task.service;

import tech.jiyu.paradise.core.account.domain.AccountDeletingTask;

import java.util.List;

/**
 * 冷静期较长，不方便测试，因此提供该类去扩展，业务可自行实现以便测试
 */
public interface AccountDeletingTaskDelegate {

    List<AccountDeletingTask> getTaskToProcess();
}
