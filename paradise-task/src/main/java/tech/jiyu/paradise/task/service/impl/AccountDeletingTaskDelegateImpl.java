package tech.jiyu.paradise.task.service.impl;

import org.apache.commons.lang3.time.DateUtils;
import tech.jiyu.paradise.core.account.domain.AccountDeletingTask;
import tech.jiyu.paradise.core.account.service.AccountDeletingTaskService;
import tech.jiyu.paradise.task.service.AccountDeletingTaskDelegate;
import tech.jiyu.utility.web.TimeRange;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;

public class AccountDeletingTaskDelegateImpl implements AccountDeletingTaskDelegate {

    private final AccountDeletingTaskService accountDeletingTaskService;

    public AccountDeletingTaskDelegateImpl(AccountDeletingTaskService accountDeletingTaskService) {
        this.accountDeletingTaskService = accountDeletingTaskService;
    }

    @Override
    public List<AccountDeletingTask> getTaskToProcess() {
        // 找出冷静期外的任务
        TimeRange timeRange = getTimeRange();
        return accountDeletingTaskService.getDeletingTaskByTimeRange(timeRange);
    }

    private TimeRange getTimeRange() {
        long endTime = System.currentTimeMillis();
        long startTime = endTime - DateUtils.MILLIS_PER_DAY; // 只用endTime去扫描会有全表扫描的风险，所以引入startTime，进行范围查找，减少扫描的行数
        TimeRange timeRange = new TimeRange();
        timeRange.setStart(startTime);
        timeRange.setEnd(endTime);
        return timeRange;
    }

    public static void main(String[] args) {
        long time = System.currentTimeMillis();
        long x = new BigDecimal(System.currentTimeMillis()).subtract(new BigDecimal(0.005).multiply(new BigDecimal(DateUtils.MILLIS_PER_DAY)).setScale(2, RoundingMode.HALF_UP)).longValue();
        System.out.println(time);
        System.out.println(x);
        System.out.println(time-x);
        System.out.println(new BigDecimal(0.005).compareTo(BigDecimal.ONE) < 0 ? new BigDecimal(0.005).setScale(2, RoundingMode.HALF_UP) : new BigDecimal(0.005));
        System.out.println(new BigDecimal(7).compareTo(BigDecimal.ONE) < 0 ? new BigDecimal(7).setScale(2, RoundingMode.HALF_UP) : new BigDecimal(7));
    }
}
