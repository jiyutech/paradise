package tech.jiyu.paradise;

import org.springframework.boot.ResourceBanner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.core.io.ClassPathResource;

/**
 * @author <a href="mailto:chenshao@jiyu.tech">shawsmith</a>
 */
@SpringBootApplication
public class Application {

    private static final String BANNER_PATH = "banner.txt";

    public static void main(String[] args) {
        configureApplication().run(args);
    }

    private static SpringApplication configureApplication() {
        SpringApplicationBuilder springApplicationBuilder = new SpringApplicationBuilder();
        springApplicationBuilder.sources(Application.class)
                .headless(true)
                .banner(new ResourceBanner(new ClassPathResource(BANNER_PATH)));
        return springApplicationBuilder.build();
    }
}