package tech.jiyu.paradise.conf;

import com.google.common.collect.Lists;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.session.ExecutorType;
import org.mybatis.spring.SqlSessionFactoryBean;
import org.mybatis.spring.mapper.MapperScannerConfigurer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.Resource;

import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.ServiceLoader;

import javax.sql.DataSource;

import tech.jiyu.utility.mybatis.MyBatisConfigurer;

/**
 * @author <a href="mailto:chenshao@dekeapp.com">shawsmith</a>
 */
@Configuration
public class ParadiseMyBatisConfiguration {

    private static List<MyBatisConfigurer> configurers = loadMyBatisConfigurers("paradiseDataSource");

    @Bean
    @Autowired
    public SqlSessionFactoryBean paradiseSqlSessionFactory(DataSource paradiseDataSource) throws Exception {
        SqlSessionFactoryBean factoryBean = new SqlSessionFactoryBean();
        factoryBean.setDataSource(paradiseDataSource);

        if (CollectionUtils.isNotEmpty(configurers)) {

            List<Resource> totalMapperLocations = Lists.newArrayList();
            List<String> totalTypeAliasPackages = Lists.newArrayList();

            configurers.forEach(configurer -> {
                Resource[] mapperLocations = configurer.getMapperLocations();
                if (mapperLocations != null && mapperLocations.length > 0) {
                    totalMapperLocations.addAll(Arrays.asList(mapperLocations));
                }

                String[] typeAliasPackages = configurer.getAliasPackages();
                if (typeAliasPackages != null && typeAliasPackages.length > 0) {
                    totalTypeAliasPackages.addAll(Arrays.asList(typeAliasPackages));
                }
            });

            factoryBean.setMapperLocations(totalMapperLocations.toArray(new Resource[0]));
            factoryBean.setTypeAliasesPackage(StringUtils.join(totalTypeAliasPackages, ","));
        }

        org.apache.ibatis.session.Configuration configuration = new org.apache.ibatis.session.Configuration();
        configuration.setCacheEnabled(false);
        configuration.setMapUnderscoreToCamelCase(true);
        configuration.setDefaultExecutorType(ExecutorType.REUSE);
        factoryBean.setConfiguration(configuration);

        return factoryBean;
    }

    private static List<MyBatisConfigurer> loadMyBatisConfigurers(String dataSourceName) {
        ServiceLoader<MyBatisConfigurer> providers = ServiceLoader.load(MyBatisConfigurer.class);
        Iterator<MyBatisConfigurer> providerIterator = providers.iterator();

        List<MyBatisConfigurer> configurers = Lists.newArrayList();
        while (providerIterator.hasNext()) {
            MyBatisConfigurer configurer = providerIterator.next();
            if (configurer != null && configurer.supports(dataSourceName)) {
                configurers.add(configurer);
            }
        }
        return configurers;
    }


    @Bean
    public static MapperScannerConfigurer paradiseMapperScannerConfigurer() {
        MapperScannerConfigurer scannerConfigurer = new MapperScannerConfigurer();
        if (CollectionUtils.isNotEmpty(configurers)) {
            List<String> totalBasePackages = Lists.newArrayList();
            configurers.forEach(configurer -> {
                String[] basePackages = configurer.getScannerBasePackages();
                if (basePackages != null && basePackages.length > 0) {
                    totalBasePackages.addAll(Arrays.asList(basePackages));
                }
            });
            String basePackages = StringUtils.join(totalBasePackages, ",");
            scannerConfigurer.setBasePackage(basePackages);
        }
        scannerConfigurer.setAnnotationClass(Mapper.class);
        scannerConfigurer.setSqlSessionFactoryBeanName("paradiseSqlSessionFactory");
        return scannerConfigurer;
    }
}
