<!DOCTYPE html>
<html lang="en">
<body>
	${userName!""},
	<br><br>
	To reset your password, the captcha code is:
	<br><br>
	<font color="red">${captchaCode!""}</font>
	<br><br>
	Thanks!
	<br>
	${signature!""}
</body>
</html>