<!DOCTYPE html>
<html lang="en">
<body>
	${userName!""},
	<br><br>
	You recently registered for ${projectName!""} using this email address. Please complete your registration by following the link below:
	<br><br>
	<a href="${activationUrl!""}">${activationUrl!""}</a>
	<br><br>
	If you did not recently register, or believe you have received this email in error, please disregard this message.
	<br><br>
	Thanks!
	<br>
	${signature!""}
</body>
</html>