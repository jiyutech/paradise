package tech.jiyu.paradise.web.account.vo;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.util.Objects;

import javax.validation.constraints.NotEmpty;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author <a href="mailto:chenshao@jiyu.tech">shawsmith</a>
 */
@NoArgsConstructor
@Getter
@Setter
public class AccountChangePasswordDTO {

    @NotEmpty(message = "{account.change.password.new.password.limit}")
    private String newPassword;

    @NotEmpty(message = "{account.change.password.confirmed.new.password.limit}")
    private String confirmedNewPassword;

    private String oldPassword;

    @JsonIgnore
    public boolean isPasswordMatched() {
        return Objects.equals(newPassword, confirmedNewPassword);
    }
}
