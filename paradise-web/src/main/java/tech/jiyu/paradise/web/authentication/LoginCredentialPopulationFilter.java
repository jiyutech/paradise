package tech.jiyu.paradise.web.authentication;

import com.fasterxml.jackson.databind.ObjectMapper;

import org.springframework.security.web.util.matcher.RequestMatcher;
import org.springframework.web.filter.OncePerRequestFilter;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import tech.jiyu.utility.encryption.RSAKeyRepository;

/**
 * @author <a href="mailto:chenshao@dekeapp.com">shawsmith</a>
 */
public class LoginCredentialPopulationFilter extends OncePerRequestFilter {

    private RequestMatcher loginRequestMatcher;
    private String attributeKey;
    private RSAKeyRepository rsaKeyRepository;

    public LoginCredentialPopulationFilter(RequestMatcher loginRequestMatcher, String attributeKey, RSAKeyRepository rsaKeyRepository) {
        this.loginRequestMatcher = loginRequestMatcher;
        this.attributeKey = attributeKey;
        this.rsaKeyRepository = rsaKeyRepository;
    }

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
        if (loginRequestMatcher.matches(request)) {
            LoginCredential loginCredential = new LoginCredential(request, rsaKeyRepository, new ObjectMapper());
            request.setAttribute(attributeKey, loginCredential);
        }
        filterChain.doFilter(request, response);
    }
}
