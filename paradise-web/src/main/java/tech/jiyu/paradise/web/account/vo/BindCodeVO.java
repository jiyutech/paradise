package tech.jiyu.paradise.web.account.vo;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * CreatedDate 2023-02-21 14:22
 *
 * @author wudawei;emailAddress: wudawei@neyber.tech
 */
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class BindCodeVO {

    private String code;
    private Long ttlSeconds;
}
