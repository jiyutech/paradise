package tech.jiyu.paradise.web.account.controller;

import com.google.common.collect.Collections2;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import tech.jiyu.paradise.core.account.domain.Account;
import tech.jiyu.paradise.core.account.service.AccountAdditionalAttributeService;
import tech.jiyu.paradise.core.account.service.AccountLoginSourceService;
import tech.jiyu.paradise.core.account.service.AccountService;
import tech.jiyu.paradise.core.login.service.LoginService;
import tech.jiyu.paradise.web.account.vo.AccountVO;
import tech.jiyu.parrot.geography.domain.Region;
import tech.jiyu.parrot.geography.service.RegionService;
import tech.jiyu.restful.api.service.ModelViewObjectAssembler;
import tech.jiyu.utility.cache.CacheStrategy;

/**
 * @author <a href="mailto:chenshao@jiyu.tech">shawsmith</a>
 */
@Component("accountModelViewObjectAssembler")
public class AccountModelViewObjectAssembler implements ModelViewObjectAssembler<Account, AccountVO> {

    @Autowired
    private RegionService regionService;

    @Autowired
    private List<AccountAdditionalAttributeService> accountAdditionalAttributeServices;

    @Autowired
    private AccountLoginSourceService accountLoginSourceService;

    @Autowired
    private LoginService loginService;

    @Autowired
    private AccountService accountService;

    @Override
    public Account build(AccountVO accountVO) {
        return (accountVO != null) ? accountVO.buildAccount(regionService) : null;
    }

    @Override
    public List<AccountVO> assemble(List<Account> accounts) {
        if (CollectionUtils.isEmpty(accounts)) {
            return Lists.newArrayListWithCapacity(0);
        }

        Map<String, Boolean> usageInfoMap = getAccountUsageInfo(accounts);
        Map<Long, Account> creatorAccounts = getCreatorAccounts(accounts);
        Map<Long, String> regionMappings = getAccountRegionMappings(accounts);
        Map<Long, Map<String, Object>> accountAdditionalAttributes = getAccountAdditionalAttributes(accounts);
        Map<Long, Collection<String>> accountLoginSourceGroups = getAccountLoginSourceGroups(accounts);

        return accounts.stream()
                .filter(Objects::nonNull)
                .map(account -> {
                    Long accountId = account.getId();
                    Boolean used = usageInfoMap.get(String.valueOf(accountId));
                    String provinceName = regionMappings.get(account.getProvinceId());
                    String cityName = regionMappings.get(account.getCityId());
                    String districtName = regionMappings.get(account.getDistrictId());

                    String imageUrl = account.getAvatar();
                    Account creatorAccount = creatorAccounts.get(account.getCreatorId());

                    Map<String, Object> additionalAttributes = accountAdditionalAttributes.get(accountId);
                    if (additionalAttributes == null) {
                        additionalAttributes = Maps.newHashMap();
                    }

                    Collection<String> loginSources = Optional.ofNullable(accountLoginSourceGroups.get(accountId)).orElse(Lists.newArrayListWithCapacity(0));
                    return new AccountVO(account, used, provinceName, cityName, districtName, imageUrl, creatorAccount, additionalAttributes, loginSources);
                })
                .collect(Collectors.toList());
    }

    private Map<String, Boolean> getAccountUsageInfo(Collection<Account> accounts) {
        if (CollectionUtils.isEmpty(accounts)) {
            return Maps.newHashMap();
        }

        Collection<String> loginIds = accounts.stream()
                .filter(account -> account != null && account.getId() != null)
                .map(account -> String.valueOf(account.getId()))
                .collect(Collectors.toList());

        Collection<String> loggedIds = loginService.findLoggedIds(loginIds);
        Map<String, Boolean> usageInfoMap = Maps.newHashMap();
        loginIds.forEach(loginId -> {
            Boolean used = loggedIds.contains(loginId);
            usageInfoMap.put(loginId, used);
        });
        return usageInfoMap;
    }

    private Map<Long, Account> getCreatorAccounts(List<Account> accounts) {
        if (CollectionUtils.isEmpty(accounts)) {
            return Maps.newHashMap();
        }

        Set<Long> creatorIds = Sets.newHashSet(Collections2.filter(Collections2.transform(accounts, Account::getCreatorId), Objects::nonNull));
        return Maps.uniqueIndex(accountService.findByIds(creatorIds, CacheStrategy.CACHE_PREFER), Account::getId);

    }

    private Map<Long, String> getAccountRegionMappings(List<Account> accounts) {

        Set<Long> regionIds = Sets.newHashSet();
        accounts.forEach(account -> {
            Long provinceId = account.getProvinceId();
            if (provinceId != null) {
                regionIds.add(provinceId);
            }

            Long cityId = account.getCityId();
            if (cityId != null) {
                regionIds.add(cityId);
            }

            Long districtId = account.getDistrictId();
            if (districtId != null) {
                regionIds.add(districtId);
            }
        });

        return regionService.findByIds(regionIds, CacheStrategy.CACHE_PREFER)
                .stream()
                .collect(Collectors.toMap(Region::getId, Region::getName));
    }


    private Map<Long, Map<String, Object>> getAccountAdditionalAttributes(List<Account> accounts) {
        if (CollectionUtils.isEmpty(accountAdditionalAttributeServices)) {
            return Maps.newHashMap();
        }

        if (CollectionUtils.isEmpty(accounts)) {
            return Maps.newHashMap();
        }

        Map<Long, Map<String, Object>> totalAdditionalAttributes = Maps.newHashMap();
        accountAdditionalAttributeServices.forEach(service -> {
            Map<Long, Map<String, Object>> accountAdditionalAttributes = service.getAdditionalAttributes(accounts);
            if (accountAdditionalAttributes != null && !accountAdditionalAttributes.isEmpty()) {
                accountAdditionalAttributes.forEach((accountId, additionalAttributes) -> {
                    if (additionalAttributes != null && !additionalAttributes.isEmpty()) {
                        Map<String, Object> attributes = totalAdditionalAttributes.computeIfAbsent(accountId, key -> Maps.newHashMap());
                        attributes.putAll(additionalAttributes);
                    }
                });
            }
        });

        return totalAdditionalAttributes;
    }

    private Map<Long, Collection<String>> getAccountLoginSourceGroups(List<Account> accounts) {
        if (CollectionUtils.isEmpty(accounts)) {
            return Maps.newHashMap();
        }

        Collection<Long> accountIds = Sets.newHashSet(Collections2.filter(Collections2.transform(accounts, Account::getId), Objects::nonNull));
        return accountLoginSourceService.getAccountLoginSources(accountIds);
    }
}
