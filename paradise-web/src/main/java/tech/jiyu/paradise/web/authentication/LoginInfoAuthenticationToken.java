package tech.jiyu.paradise.web.authentication;

import com.google.common.collect.Lists;

import org.springframework.security.authentication.AbstractAuthenticationToken;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import tech.jiyu.paradise.core.login.domain.LoginInfo;
import tech.jiyu.paradise.core.util.SecurityUtils;


/**
 * @author <a href="mailto:chenshao@jiyu.tech">shawsmith</a>
 */
public class LoginInfoAuthenticationToken extends AbstractAuthenticationToken {

    private LoginInfo loginInfo;

    public LoginInfoAuthenticationToken(LoginInfo loginInfo) {
        super(Lists.newArrayList(new SimpleGrantedAuthority(SecurityUtils.ROLE_PREFIX + loginInfo.getRoleTypeCode())));
        this.loginInfo = loginInfo;
    }

    public LoginInfo getLoginInfo() {
        return loginInfo;
    }

    @Override
    public Object getCredentials() {
        return loginInfo.getLoginToken();
    }

    @Override
    public Object getPrincipal() {
        return loginInfo.getLoginId();
    }
}
