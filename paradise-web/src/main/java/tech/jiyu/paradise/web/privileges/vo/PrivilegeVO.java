package tech.jiyu.paradise.web.privileges.vo;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import tech.jiyu.paradise.core.privileges.domain.Privilege;

/**
 * @author <a href="mailto:chenshao@jinjie.tech">shawsmith</a>
 */
@NoArgsConstructor
@Getter
@Setter
public class PrivilegeVO {

    private Long id;
    private String name;
    private String description;
    private Long orderNumber;

    public PrivilegeVO(Privilege privilege) {
        if (privilege != null) {
            setId(privilege.getId());
            setName(privilege.getName());
            setDescription(privilege.getDescription());
            setOrderNumber(privilege.getOrderNumber());
        }
    }

    public Privilege buildPrivilege() {
        Privilege privilege = new Privilege();

        privilege.setId(getId());
        privilege.setName(getName());
        privilege.setDescription(getDescription());
        privilege.setOrderNumber(getOrderNumber());

        return privilege;
    }
}
