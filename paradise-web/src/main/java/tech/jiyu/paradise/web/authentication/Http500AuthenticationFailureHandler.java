package tech.jiyu.paradise.web.authentication;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.http.server.ServletServerHttpResponse;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import tech.jiyu.paradise.web.ParadiseExceptionHandler;
import tech.jiyu.utility.api.errors.ApiError;
import tech.jiyu.utility.api.errors.ApiErrorRepository;
import tech.jiyu.utility.api.errors.MessageInterpreter;
import tech.jiyu.utility.api.errors.RestfulApiError;

/**
 * @author <a href="mailto:chenshao@jiyu.tech">shawsmith</a>
 */
public class Http500AuthenticationFailureHandler implements AuthenticationFailureHandler {

    private final ApiErrorRepository apiErrorRepository;
    private final MessageInterpreter messageInterpreter;
    private final ParadiseExceptionHandler paradiseExceptionHandler;
    private final MappingJackson2HttpMessageConverter messageConverter;

    public Http500AuthenticationFailureHandler(ApiErrorRepository apiErrorRepository, MessageInterpreter messageInterpreter, ParadiseExceptionHandler paradiseExceptionHandler, MappingJackson2HttpMessageConverter messageConverter) {
        this.apiErrorRepository = apiErrorRepository;
        this.messageInterpreter = messageInterpreter;
        this.paradiseExceptionHandler = paradiseExceptionHandler;
        this.messageConverter = messageConverter;
    }

    @Override
    public void onAuthenticationFailure(HttpServletRequest request, HttpServletResponse response, AuthenticationException exception) throws IOException, ServletException {
        ApiError fallbackError = apiErrorRepository.get("login", "invalid.credential", messageInterpreter);
        RestfulApiError apiError = paradiseExceptionHandler.buildAndNotify(exception, fallbackError, HttpStatus.INTERNAL_SERVER_ERROR);
        response.setStatus(apiError.getResponseStatus().value());
        messageConverter.write(apiError, MediaType.APPLICATION_JSON_UTF8, new ServletServerHttpResponse(response));
    }
}
