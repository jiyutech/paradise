package tech.jiyu.paradise.web.authentication;

import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.security.web.util.matcher.RequestMatcher;

import javax.servlet.Filter;

import tech.jiyu.utility.encryption.RSAKeyRepository;
import tech.jiyu.utility.security.AuthenticationFilterConfigurer;

/**
 * @author <a href="mailto:chenshao@jiyu.tech">shawsmith</a>
 */
public class LoginCredentialPopulationFilterConfigurer implements AuthenticationFilterConfigurer {

    public static final String LOGIN_CREDENTIAL_ATTRIBUTE_KEY = "__paradise_login_credentials_key__";
    public static final Long DEFAULT_ORDER = 100L;

    @Override
    public Long getOrder() {
        return DEFAULT_ORDER;
    }

    @Override
    public Class<? extends Filter> getTargetFilterClass() {
        return LoginCredentialPopulationFilter.class;
    }

    @Override
    public void configure(HttpSecurity http, RequestMatcher loginRequestMatcher, AuthenticationSuccessHandler ash, AuthenticationFailureHandler afh, AuthenticationManager authenticationManager, Class<? extends Filter> afterFilter) {
        this.configure(http, loginRequestMatcher, null, ash, afh, authenticationManager, afterFilter);
    }

    @Override
    public void configure(HttpSecurity http, RequestMatcher loginRequestMatcher, RSAKeyRepository rsaKeyRepository, AuthenticationSuccessHandler ash, AuthenticationFailureHandler afh, AuthenticationManager authenticationManager, Class<? extends Filter> afterFilter) throws UnsupportedOperationException {
        LoginCredentialPopulationFilter populationFilter = new LoginCredentialPopulationFilter(loginRequestMatcher, LOGIN_CREDENTIAL_ATTRIBUTE_KEY, rsaKeyRepository);
        http.addFilterAfter(populationFilter, afterFilter);
    }
}
