package tech.jiyu.paradise.web.authentication;

import com.google.common.base.Throwables;

import org.apache.commons.lang3.StringUtils;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.AuthenticationServiceException;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;

import java.io.IOException;

import tech.jiyu.captcha.domain.CaptchaCode;
import tech.jiyu.captcha.domain.CaptchaCodeKey;
import tech.jiyu.captcha.service.CaptchaCodeMismatchException;
import tech.jiyu.captcha.service.CaptchaService;
import tech.jiyu.paradise.core.account.domain.Account;
import tech.jiyu.paradise.core.account.domain.Invitation;
import tech.jiyu.paradise.core.account.exception.IllegalLoginSourceException;
import tech.jiyu.paradise.core.account.service.AccountLoginSourceService;
import tech.jiyu.paradise.core.account.service.AccountService;
import tech.jiyu.penguin.wechat.config.domain.WeChatConfig;
import tech.jiyu.penguin.wechat.config.exception.NoSuchWeChatConfigException;
import tech.jiyu.penguin.wechat.config.service.WeChatConfigService;
import tech.jiyu.penguin.wxapi.wechat.common.exception.CannotAcquireOpenIdException;
import tech.jiyu.penguin.wxapi.wechat.tinyapp.domain.WeChatTinyAppLoginToken;
import tech.jiyu.penguin.wxapi.wechat.tinyapp.service.WeChatTinyAppService;
import tech.jiyu.utility.cache.CacheStrategy;

/**
 * @author <a href="mailto:chenshao@dekeapp.com">shawsmith</a>
 */
public class PhoneNumberAuthenticationProvider implements AuthenticationProvider {

    private final Boolean needValidateCaptchaCode;
    private final Boolean registerNewPhoneNumber;
    private final Long defaultWeChatConfigId;

    private final CaptchaService captchaService;
    private final AccountService accountService;
    private final WeChatTinyAppService weChatTinyAppService;
    private final WeChatConfigService weChatConfigService;
    private final AccountLoginSourceService accountLoginSourceService;

    public PhoneNumberAuthenticationProvider(Boolean needValidateCaptchaCode,
                                             Boolean registerNewPhoneNumber,
                                             Long defaultWeChatConfigId,
                                             CaptchaService captchaService,
                                             AccountService accountService,
                                             WeChatTinyAppService weChatTinyAppService,
                                             WeChatConfigService weChatConfigService,
                                             AccountLoginSourceService accountLoginSourceService) {
        this.needValidateCaptchaCode = needValidateCaptchaCode;
        this.registerNewPhoneNumber = registerNewPhoneNumber;
        this.defaultWeChatConfigId = defaultWeChatConfigId;
        this.captchaService = captchaService;
        this.accountService = accountService;
        this.weChatTinyAppService = weChatTinyAppService;
        this.weChatConfigService = weChatConfigService;
        this.accountLoginSourceService = accountLoginSourceService;
    }

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        try {
            PhoneNumberAuthenticationToken authenticationToken = (PhoneNumberAuthenticationToken) authentication;
            CaptchaCode captchaCode = authenticationToken.getCaptchaCode();

            CaptchaCodeKey key = null;
            Long companyId = null;
            String phoneNumber = null;
            String code = null;
            if (captchaCode != null) {
                key = captchaCode.getKey();
                if (key != null) {
                    companyId = key.getCompanyId();
                    phoneNumber = key.getPhoneNumber();
                }

                code = captchaCode.getCode();
            }

            if (!captchaService.validate(key, code) && Boolean.TRUE.equals(needValidateCaptchaCode)) {
                throw new CaptchaCodeMismatchException(key, code, captchaService.exchange(key));
            }

            // 如果用户提供了code, 则优先取微信的相关信息.
            String weChatTinyAppOpenId = null;
            String weChatTinyAppJsCode = authenticationToken.getWeChatTinyAppJsCode();
            if (StringUtils.isNotBlank(weChatTinyAppJsCode)) {
                WeChatTinyAppLoginToken loginToken = fetchWeChatTinyAppLoginToken(weChatTinyAppJsCode);
                if (loginToken == null || StringUtils.isBlank(loginToken.getOpenId())) {
                    throw new AuthenticationServiceException(null, new CannotAcquireOpenIdException());
                } else {
                    weChatTinyAppOpenId = loginToken.getOpenId();
                }
            }

            Account account = accountService.findByPhoneNumber(companyId, authenticationToken.getCountryCode(), phoneNumber);
            if (account != null) {
                if (!account.isActuallyEnabled()) {
                    throw new DisabledException("Account with phoneNumber#" + phoneNumber + "was disabled.");
                } else {
                    String loginSource = authenticationToken.getLoginSource();
                    if (!Boolean.TRUE.equals(accountLoginSourceService.supportLoginSource(account.getId(), loginSource))) {
                        throw new IllegalLoginSourceException(null);
                    }
                }

                account.setWeChatTinyAppOpenId(weChatTinyAppOpenId);
                account.setCountryCode(authenticationToken.getCountryCode());
                account.setIsoCode(authenticationToken.getIsoCode());
                account.setPrivilegeIds(authenticationToken.getPrivilegeIds());
                account.setAdditionalProperties(authenticationToken.getAdditionalProperties());
                account.setEmail(authenticationToken.getEmail());
                account.setFirstName(authenticationToken.getFirstName());
                account.setLastName(authenticationToken.getLastName());
                account.setAgreeToReceiveInfo(authenticationToken.getAgreeToReceiveInfo());
                account.setLastLanguageCode(authenticationToken.getLastLanguageCode());

                String weChatOpenId = authenticationToken.getWeChatOpenId();
                if (StringUtils.isNotBlank(weChatOpenId)) {
                    account.setWeChatOpenId(weChatOpenId);
                }

                account = accountService.patch(account);
            }

            // 新用户, 需要注册.
            else {
                if (Boolean.TRUE.equals(registerNewPhoneNumber)) {
                    account = buildAccount(authenticationToken, weChatTinyAppOpenId);
                    account = accountService.register(account, null, authenticationToken.getInvitation());
                } else {
                    throw new NoSuchAccountOnSignInException("Phone number: " + phoneNumber);
                }
            }

            authenticationToken.setDetails(account);
            authenticationToken.setAuthenticated(true);
            return authenticationToken;
        } catch (BadCredentialsException e) {
            throw e;
        } catch (Exception e) {
            if (e instanceof AuthenticationServiceException) {
                throw (AuthenticationServiceException) e;
            } else {
                throw new AuthenticationServiceException(Throwables.getStackTraceAsString(e), e);
            }
        }
    }

    private WeChatTinyAppLoginToken fetchWeChatTinyAppLoginToken(String weChatTinyAppJsCode) throws IOException {
        WeChatConfig weChatConfig = weChatConfigService.findById(defaultWeChatConfigId, CacheStrategy.CACHE_PREFER);
        if (weChatConfig == null) {
            throw new NoSuchWeChatConfigException();
        }
        return weChatTinyAppService.exchangeLoginToken(weChatConfig.getWeChatTinyAppId(), weChatConfig.getWeChatTinyAppSecret(), weChatTinyAppJsCode);
    }

    private Account buildAccount(PhoneNumberAuthenticationToken authentication, String weChatTinyAppOpenId) {
        Account account = new Account();
        account.setRegistrationDate(System.currentTimeMillis());
        account.setRegistrationSource(StringUtils.isNotBlank(authentication.getRegistrationSource()) ? authentication.getRegistrationSource() : authentication.getLoginSource());
        account.setRegistrationTrafficType(authentication.getRegistrationTrafficType());
        account.setCountryCode(authentication.getCountryCode());
        account.setPhoneNumber((String) authentication.getPrincipal());
        account.setIsoCode(authentication.getIsoCode());

        account.setWeChatOpenId(authentication.getWeChatOpenId());
        account.setWeChatTinyAppOpenId(weChatTinyAppOpenId);

        CaptchaCode code = authentication.getCaptchaCode();
        if (code != null && code.getKey() != null) {
            account.setCompanyId(code.getKey().getCompanyId());
        }

        Invitation invitation = authentication.getInvitation();
        if (invitation != null) {
            account.setInvitationCode(invitation.getCode());
        }

        account.setRoleTypeCode(authentication.getRoleTypeCode());
        account.setPrivilegeIds(authentication.getPrivilegeIds());
        account.setEnabled(Boolean.TRUE);
        account.setVerified(Boolean.FALSE);
        account.setAudited(Boolean.FALSE);
        account.setWorkDayTimeEnabled(Boolean.FALSE);
        account.setPolyAdminEnabled(Boolean.TRUE);
        account.setEmail(authentication.getEmail());
        account.setFirstName(authentication.getFirstName());
        account.setLastName(authentication.getLastName());
        account.setAgreeToReceiveInfo(authentication.getAgreeToReceiveInfo());
        account.setLastLanguageCode(authentication.getLastLanguageCode());
        account.setAdditionalProperties(authentication.getAdditionalProperties());
        return account;
    }

    @Override
    public boolean supports(Class<?> authentication) {
        return PhoneNumberAuthenticationToken.class.isAssignableFrom(authentication);
    }
}
