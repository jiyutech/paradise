package tech.jiyu.paradise.web.account.vo;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import tech.jiyu.paradise.core.account.domain.Account;
import tech.jiyu.paradise.core.account.domain.ActivationConfig;
import tech.jiyu.parrot.geography.service.RegionService;

/**
 * @author <a href="mailto:chenshao@dekeapp.com">shawsmith</a>
 */
@NoArgsConstructor
@Getter
@Setter
public class AccountRegistrationDTO extends AccountVO {

    private String confirmedPassword;
    private String captchaCode;
    private Boolean needActivation;
    private String returnUrl;

    @Override
    public Account buildAccount(RegionService regionService) {
        Account account = super.buildAccount(regionService);
        account.setRegistrationDate(System.currentTimeMillis());

        // 如果设置了需要通过邮件验证后才激活账户, 则需要将enabled设置为false.
        Boolean enabled = !Boolean.TRUE.equals(getNeedActivation());
        account.setEnabled(enabled);
        return account;
    }

    public ActivationConfig buildActivationConfig() {
        ActivationConfig activationConfig = new ActivationConfig();
        activationConfig.setNeedActivation(needActivation);
        activationConfig.setReturnUrl(returnUrl);
        return activationConfig;
    }
}