package tech.jiyu.paradise.web.account.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

import tech.jiyu.paradise.core.account.domain.InvitationCodeListQuery;
import tech.jiyu.paradise.core.account.domain.InvitationCodeStatus;
import tech.jiyu.paradise.core.util.SecurityUtils;
import tech.jiyu.paradise.web.account.vo.InvitationCodeVO;
import tech.jiyu.utility.pagination.PaginationConfig;
import tech.jiyu.utility.pagination.PaginationResult;
import tech.jiyu.utility.web.ParameterUtils;

/**
 * Created By InvitationCodeController
 *
 * Date 2018/2/9 16:25
 *
 * @author SimonChan;emailAddress: simonchan@jiyu.tech
 */
@RestController
public class InvitationCodeController {

    @Autowired
    private InvitationCodeDelegate invitationCodeDelegate;

    @RequestMapping(value = "/invitationCodes",
                    method = RequestMethod.POST,
                    consumes = MediaType.APPLICATION_JSON_VALUE,
                    produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @Secured({SecurityUtils.ROLE_SYSTEM, SecurityUtils.ROLE_IT})
    @ResponseBody
    public ResponseEntity<InvitationCodeVO> create() {
        InvitationCodeVO invitationCodeVO = invitationCodeDelegate.create();
        return new ResponseEntity<>(invitationCodeVO, HttpStatus.CREATED);
    }

    @RequestMapping(value = "/invitationCodes",
                    method = RequestMethod.GET,
                    produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @Secured({SecurityUtils.ROLE_SYSTEM, SecurityUtils.ROLE_IT})
    public ResponseEntity<PaginationResult<InvitationCodeVO, Void>> list(@RequestParam Map<String, String> parameters) {
        InvitationCodeListQuery invitationCodeListQuery = parseInvitationCodeListQuery(parameters);
        PaginationResult<InvitationCodeVO, Void> result = invitationCodeDelegate.list(invitationCodeListQuery);
        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    private InvitationCodeListQuery parseInvitationCodeListQuery(Map<String, String> parameters) {
        String statusCode = parameters.get("status");
        InvitationCodeStatus status = InvitationCodeStatus.fromCode(statusCode);
        PaginationConfig paginationConfig = ParameterUtils.parsePaginationConfig(parameters);
        InvitationCodeListQuery invitationCodeListQuery = new InvitationCodeListQuery();
        invitationCodeListQuery.setInvitationCodeStatus(status);
        invitationCodeListQuery.setPaginationConfig(paginationConfig);
        return invitationCodeListQuery;
    }
}
