package tech.jiyu.paradise.web.authentication;

import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.security.web.util.matcher.RequestMatcher;

import javax.servlet.Filter;

import tech.jiyu.utility.security.AuthenticationFilterConfigurer;

/**
 * @author <a href="mailto:chenshao@jiyu.tech">shawsmith</a>
 */
public class OAuth2AuthenticationFilterConfigurer implements AuthenticationFilterConfigurer {

    public static final Long DEFAULT_ORDER = 400L;

    @Override
    public Long getOrder() {
        return DEFAULT_ORDER;
    }

    @Override
    public Class<? extends Filter> getTargetFilterClass() {
        return OAuth2AuthenticationFilter.class;
    }

    @Override
    public void configure(HttpSecurity http, RequestMatcher loginRequestMatcher, AuthenticationSuccessHandler ash, AuthenticationFailureHandler afh, AuthenticationManager authenticationManager, Class<? extends Filter> afterFilter) {
        OAuth2AuthenticationFilter oAuth2AuthenticationFilter = new OAuth2AuthenticationFilter(loginRequestMatcher, LoginCredentialPopulationFilterConfigurer.LOGIN_CREDENTIAL_ATTRIBUTE_KEY);
        oAuth2AuthenticationFilter.setAuthenticationSuccessHandler(ash);
        oAuth2AuthenticationFilter.setAuthenticationFailureHandler(afh);
        oAuth2AuthenticationFilter.setAuthenticationManager(authenticationManager);
        http.addFilterAfter(oAuth2AuthenticationFilter, afterFilter);
    }
}
