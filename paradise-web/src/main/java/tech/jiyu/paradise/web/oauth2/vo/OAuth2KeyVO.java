package tech.jiyu.paradise.web.oauth2.vo;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * CreatedDate 2018-07-09 下午5:24
 *
 * @author SimonChan;emailAddress: simonchan@jinjie.tech
 */
@NoArgsConstructor
@Getter
@Setter
public class OAuth2KeyVO {

    private String authKey;
    private Boolean registered;
}
