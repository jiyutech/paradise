package tech.jiyu.paradise.web.authentication;

import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.security.web.util.matcher.RequestMatcher;

import javax.servlet.Filter;

import tech.jiyu.utility.security.AuthenticationFilterConfigurer;

/**
 * @author <a href="mailto:chenshao@jiyu.tech">shawsmith</a>
 */
public class PhoneNumberAuthenticationFilterConfigurer implements AuthenticationFilterConfigurer {

    public static final Long DEFAULT_ORDER = 300L;

    @Override
    public Long getOrder() {
        return DEFAULT_ORDER;
    }

    @Override
    public Class<? extends Filter> getTargetFilterClass() {
        return PhoneNumberAuthenticationFilter.class;
    }

    @Override
    public void configure(HttpSecurity http, RequestMatcher loginRequestMatcher, AuthenticationSuccessHandler ash, AuthenticationFailureHandler afh, AuthenticationManager authenticationManager, Class<? extends Filter> afterFilter) {
        // 基于手机号码和验证码的登录.
        PhoneNumberAuthenticationFilter phoneNumberAuthenticationFilter = new PhoneNumberAuthenticationFilter(loginRequestMatcher, LoginCredentialPopulationFilterConfigurer.LOGIN_CREDENTIAL_ATTRIBUTE_KEY);
        phoneNumberAuthenticationFilter.setAuthenticationSuccessHandler(ash);
        phoneNumberAuthenticationFilter.setAuthenticationFailureHandler(afh);
        phoneNumberAuthenticationFilter.setAuthenticationManager(authenticationManager);
        http.addFilterAfter(phoneNumberAuthenticationFilter, afterFilter);
    }
}
