package tech.jiyu.paradise.web.authentication;

import org.apache.commons.lang3.StringUtils;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.context.SecurityContextImpl;
import org.springframework.security.web.context.HttpRequestResponseHolder;
import org.springframework.security.web.context.SecurityContextRepository;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import tech.jiyu.paradise.core.login.domain.LoginInfo;
import tech.jiyu.paradise.core.login.service.LoginService;


/**
 * @author <a href="mailto:chenshao@jiyu.tech">shawsmith</a>
 */
public class LoginTokenSecurityContextRepository implements SecurityContextRepository {

    protected static final String DEFAULT_LOGIN_TOKEN_PARAM_NAME = "loginToken";

    protected final LoginService loginService;
    protected final String loginTokenParameter;

    public LoginTokenSecurityContextRepository(LoginService loginService) {
        this(loginService, DEFAULT_LOGIN_TOKEN_PARAM_NAME);
    }

    public LoginTokenSecurityContextRepository(LoginService loginService, String loginTokenParameter) {
        this.loginService = loginService;
        this.loginTokenParameter = loginTokenParameter;
    }

    @Override
    public SecurityContext loadContext(HttpRequestResponseHolder requestResponseHolder) {
        String loginToken = requestResponseHolder.getRequest().getParameter(loginTokenParameter);
        if (StringUtils.isBlank(loginToken)) {
            return SecurityContextHolder.createEmptyContext();
        }

        LoginInfo loginInfo = loginService.findByLoginToken(loginToken);
        if (loginInfo == null) {
            return SecurityContextHolder.createEmptyContext();
        } else {
            LoginInfoAuthenticationToken authentication = new LoginInfoAuthenticationToken(loginInfo);
            authentication.setAuthenticated(true);
            SecurityContext context = new SecurityContextImpl();
            context.setAuthentication(authentication);
            return context;
        }
    }


    @Override
    public void saveContext(SecurityContext context, HttpServletRequest request, HttpServletResponse response) {

    }

    @Override
    public boolean containsContext(HttpServletRequest request) {
        return false;
    }
}
