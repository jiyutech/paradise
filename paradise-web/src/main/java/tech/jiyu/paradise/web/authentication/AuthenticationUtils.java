package tech.jiyu.paradise.web.authentication;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;

import tech.jiyu.paradise.core.account.domain.Account;
import tech.jiyu.paradise.core.account.service.AccountService;
import tech.jiyu.paradise.core.login.domain.LoginInfo;
import tech.jiyu.utility.cache.CacheStrategy;

/**
 * @author <a href="mailto:chenshao@dekeapp.com">shawsmith</a>
 */
public final class AuthenticationUtils {

    private AuthenticationUtils() {
    }

    public static Authentication currentAuthentication() {
        SecurityContext context = SecurityContextHolder.getContext();
        return (context != null) ? context.getAuthentication() : null;

    }

    public static Account loginAccount(AccountService accountService) {
        Authentication authentication = currentAuthentication();
        if (!(authentication instanceof LoginInfoAuthenticationToken)) {
            return null;
        }

        LoginInfoAuthenticationToken authenticationToken = (LoginInfoAuthenticationToken) authentication;
        LoginInfo loginInfo = authenticationToken.getLoginInfo();
        if (loginInfo == null) {
            return null;
        }
        return accountService.findById(Long.parseLong(loginInfo.getLoginId()), CacheStrategy.CACHE_PREFER);
    }

    public static Long currentAccountId(AccountService accountService) {
        Account account = loginAccount(accountService);
        return account == null ? null : account.getId();
    }
}
