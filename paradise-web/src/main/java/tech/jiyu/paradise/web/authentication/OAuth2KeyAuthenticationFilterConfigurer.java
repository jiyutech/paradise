package tech.jiyu.paradise.web.authentication;

import com.fasterxml.jackson.databind.ObjectMapper;

import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.security.web.util.matcher.RequestMatcher;

import javax.servlet.Filter;

import tech.jiyu.utility.security.AuthenticationFilterConfigurer;

/**
 * CreatedDate 2018-07-10 上午11:44
 *
 * @author SimonChan;emailAddress: simonchan@jinjie.tech
 */
public class OAuth2KeyAuthenticationFilterConfigurer implements AuthenticationFilterConfigurer {

    private static final Long DEFAULT_ORDER = 200L;

    @Override
    public Long getOrder() {
        return DEFAULT_ORDER;
    }

    @Override
    public Class<? extends Filter> getTargetFilterClass() {
        return OAuth2KeyAuthenticationFilter.class;
    }

    @Override
    public void configure(HttpSecurity http, RequestMatcher loginRequestMatcher, AuthenticationSuccessHandler ash, AuthenticationFailureHandler afh, AuthenticationManager authenticationManager, Class<? extends Filter> afterFilter) {
        OAuth2KeyAuthenticationFilter keyAuthenticationFilter = new OAuth2KeyAuthenticationFilter(loginRequestMatcher, LoginCredentialPopulationFilterConfigurer.LOGIN_CREDENTIAL_ATTRIBUTE_KEY, new ObjectMapper());
        keyAuthenticationFilter.setAuthenticationSuccessHandler(ash);
        keyAuthenticationFilter.setAuthenticationFailureHandler(afh);
        keyAuthenticationFilter.setAuthenticationManager(authenticationManager);
        http.addFilterAfter(keyAuthenticationFilter, afterFilter);
    }
}
