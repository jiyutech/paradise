package tech.jiyu.paradise.web.privileges.controller;

import com.google.common.collect.Lists;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import tech.jiyu.paradise.core.privileges.domain.Privilege;
import tech.jiyu.paradise.web.privileges.vo.PrivilegeVO;
import tech.jiyu.restful.api.service.ModelViewObjectAssembler;

/**
 * @author <a href="mailto:chenshao@jinjie.tech">shawsmith</a>
 */
@Component("privilegeModelViewObjectAssembler")
public class PrivilegeModelViewObjectAssembler implements ModelViewObjectAssembler<Privilege, PrivilegeVO> {

    @Override
    public Privilege build(PrivilegeVO privilegeVO) {
        return (privilegeVO != null) ? privilegeVO.buildPrivilege() : null;
    }

    @Override
    public List<PrivilegeVO> assemble(List<Privilege> privileges) {
        if (CollectionUtils.isEmpty(privileges)) {
            return Lists.newArrayListWithCapacity(0);
        }

        return privileges.stream()
                .filter(Objects::nonNull)
                .map(PrivilegeVO::new)
                .collect(Collectors.toList());
    }
}
