package tech.jiyu.paradise.web.privileges.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

import tech.jiyu.paradise.core.privileges.domain.PrivilegeListQuery;
import tech.jiyu.paradise.web.privileges.vo.PrivilegeVO;
import tech.jiyu.utility.cache.CacheStrategy;
import tech.jiyu.utility.pagination.PaginationResult;

/**
 * @author <a href="mailto:chenshao@jinjie.tech">shawsmith</a>
 */
@RestController
public class PrivilegeController {

    @Autowired
    private PrivilegeDelegate privilegeDelegate;

    @PostMapping(value = "/privileges",
                 consumes = {MediaType.APPLICATION_JSON_UTF8_VALUE},
                 produces = {MediaType.APPLICATION_JSON_UTF8_VALUE})
    @ResponseBody
    public ResponseEntity<PrivilegeVO> create(@RequestBody PrivilegeVO privilegeVO) {
        privilegeVO = privilegeDelegate.create(privilegeVO);
        return new ResponseEntity<>(privilegeVO, HttpStatus.CREATED);
    }

    @GetMapping(value = "/privileges",
                produces = {MediaType.APPLICATION_JSON_UTF8_VALUE})
    @ResponseBody
    public ResponseEntity<PaginationResult<PrivilegeVO, Void>> create(@RequestParam Map<String, String> parameters) {
        PrivilegeListQuery query = PrivilegeListQuery.parse(parameters);
        PaginationResult<PrivilegeVO, Void> result = privilegeDelegate.list(query);
        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    @GetMapping(value = "/privileges/{id}",
                produces = {MediaType.APPLICATION_JSON_UTF8_VALUE})
    @ResponseBody
    public ResponseEntity<PrivilegeVO> findById(@PathVariable Long id) {
        PrivilegeVO privilegeVO = privilegeDelegate.findById(id, CacheStrategy.CACHE_PREFER);
        HttpStatus status = (privilegeVO != null) ? HttpStatus.OK : HttpStatus.NOT_FOUND;
        return new ResponseEntity<>(privilegeVO, status);
    }

    @PatchMapping(value = "/privileges/{id}",
                  consumes = {MediaType.APPLICATION_JSON_UTF8_VALUE},
                  produces = {MediaType.APPLICATION_JSON_UTF8_VALUE})
    @ResponseBody
    public ResponseEntity<PrivilegeVO> patch(@PathVariable Long id, @RequestBody PrivilegeVO privilegeVO) {
        privilegeVO.setId(id);
        privilegeVO = privilegeDelegate.patch(privilegeVO);
        return new ResponseEntity<>(privilegeVO, HttpStatus.OK);
    }

    @DeleteMapping(value = "/privileges/{id}",
                   produces = {MediaType.APPLICATION_JSON_UTF8_VALUE})
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteById(@PathVariable Long id) {
        privilegeDelegate.deleteById(id);
    }
}
