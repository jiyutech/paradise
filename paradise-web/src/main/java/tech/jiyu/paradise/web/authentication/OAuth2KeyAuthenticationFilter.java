package tech.jiyu.paradise.web.authentication;

import com.fasterxml.jackson.databind.ObjectMapper;

import org.apache.commons.collections.MapUtils;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.AbstractAuthenticationProcessingFilter;
import org.springframework.security.web.util.matcher.RequestMatcher;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import tech.jiyu.paradise.core.account.service.AccountService;
import tech.jiyu.paradise.core.login.service.LoginService;
import tech.jiyu.paradise.core.util.SecurityUtils;
import tech.jiyu.utility.phonenumber.PhoneNumberUtils;

/**
 * CreatedDate 2018-07-09 下午9:11
 *
 * @author SimonChan;emailAddress: simonchan@jinjie.tech
 */
public class OAuth2KeyAuthenticationFilter extends AbstractAuthenticationProcessingFilter {

    private String loginCredentialsKey;
    private ObjectMapper objectMapper;

    public OAuth2KeyAuthenticationFilter(RequestMatcher requestMatcher, String loginCredentialsKey, ObjectMapper objectMapper) {
        super(requestMatcher);
        this.loginCredentialsKey = loginCredentialsKey;
        this.objectMapper = objectMapper;
    }

    @Override
    @SuppressWarnings("unchecked")
    protected boolean requiresAuthentication(HttpServletRequest request, HttpServletResponse response) {
        if (!super.requiresAuthentication(request, response)) {
            return false;
        }

        LoginCredential loginCredential = (LoginCredential) request.getAttribute(loginCredentialsKey);
        return (loginCredential != null) && loginCredential.getStringParameter("authKey", null) != null;
    }

    @Override
    public Authentication attemptAuthentication(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) throws AuthenticationException {
        OAuth2KeyAuthenticationToken keyAuthenticationToken = extractAuthenticationToken(httpServletRequest);
        return this.getAuthenticationManager().authenticate(keyAuthenticationToken);
    }

    @SuppressWarnings("all")
    private OAuth2KeyAuthenticationToken extractAuthenticationToken(HttpServletRequest request) {

        OAuth2KeyAuthenticationToken keyAuthenticationToken = new OAuth2KeyAuthenticationToken(null);

        LoginCredential loginCredential = (LoginCredential) request.getAttribute(loginCredentialsKey);
        Long companyId = loginCredential.getLongParameter("companyId", AccountService.DEFAULT_COMPANY_ID);
        keyAuthenticationToken.setCompanyId(companyId);

        keyAuthenticationToken.setAuthKey(loginCredential.getStringParameter("authKey", null));
        keyAuthenticationToken.setAuthType(loginCredential.getStringParameter("authType", null));

        String phoneNumber = PhoneNumberUtils.filterNonDigitCharacters(loginCredential.getStringParameter("phoneNumber", null));
        keyAuthenticationToken.setPhoneNumber(phoneNumber);
        keyAuthenticationToken.setCaptchaCode(loginCredential.getStringParameter("captchaCode", null));

        String roleTypeCode = loginCredential.getStringParameter("roleTypeCode", SecurityUtils.CUSTOMER);
        keyAuthenticationToken.setRoleTypeCode(roleTypeCode);

        String registrationSource = loginCredential.getStringParameter("registrationSource");
        keyAuthenticationToken.setRegistrationSource(registrationSource);

        String loginSource = loginCredential.getStringParameter("loginSource", LoginService.DEFAULT_LOGIN_SOURCE);
        keyAuthenticationToken.setLoginSource(loginSource);

        String countryCode = loginCredential.getStringParameter("countryCode", null);
        keyAuthenticationToken.setCountryCode(countryCode);

        String isoCode = loginCredential.getStringParameter("isoCode", null);
        keyAuthenticationToken.setIsoCode(isoCode);

        String password = loginCredential.getStringParameter("password", null);
        keyAuthenticationToken.setPassword(password);

        String firstName = loginCredential.getStringParameter("firstName", null);
        keyAuthenticationToken.setFirstName(firstName);

        String lastName = loginCredential.getStringParameter("lastName", null);
        keyAuthenticationToken.setLastName(lastName);

        String email = loginCredential.getStringParameter("email", null);
        keyAuthenticationToken.setEmail(email);

        Boolean agreeToReceiveInfo = loginCredential.getBooleanParameter("agreeToReceiveInfo");
        keyAuthenticationToken.setAgreeToReceiveInfo(agreeToReceiveInfo);

        String lastLanguageCode = loginCredential.getStringParameter("lastLanguageCode", null);
        keyAuthenticationToken.setLastLanguageCode(lastLanguageCode);

        Map<String, Object> additionalProperties = loginCredential.getMapParameter("additionalProperties");
        if (MapUtils.isNotEmpty(additionalProperties)) {
            keyAuthenticationToken.setAdditionalProperties(additionalProperties);
        }
        keyAuthenticationToken.attachLoginSourceToAdditionalProperties(loginSource);

        String weChatTinyAppEncryptedData = loginCredential.getStringParameter("weChatTinyAppEncryptedData", null);
        keyAuthenticationToken.setWeChatTinyAppEncryptedData(weChatTinyAppEncryptedData);

        String weChatTinyAppIv = loginCredential.getStringParameter("weChatTinyAppIv", null);
        keyAuthenticationToken.setWeChatTinyAppIv(weChatTinyAppIv);

        String weChatTinyAppId = loginCredential.getStringParameter("weChatTinyAppId", null);
        keyAuthenticationToken.setWeChatTinyAppId(weChatTinyAppId);

        return keyAuthenticationToken;
    }
}
