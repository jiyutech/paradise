package tech.jiyu.paradise.web.authentication;

import com.google.common.base.Throwables;

import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.logout.LogoutHandler;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import tech.jiyu.paradise.core.login.service.LoginService;

/**
 * @author <a href="mailto:chenshao@jiyu.tech">shawsmith</a>
 */
public class DeleteLoginTokenLogoutHandler implements LogoutHandler {

    protected static final String LOGIN_TOKEN_PARAM_NAME = "loginToken";

    protected LoginService loginService;

    public DeleteLoginTokenLogoutHandler(LoginService loginService) {
        this.loginService = loginService;
    }

    @Override
    public void logout(HttpServletRequest request, HttpServletResponse response, Authentication authentication) {
        try {
            response.setStatus(HttpServletResponse.SC_NO_CONTENT);
            response.setContentType(MediaType.APPLICATION_JSON_UTF8_VALUE);
            response.setHeader(HttpHeaders.ACCESS_CONTROL_ALLOW_ORIGIN, "*");

            String loginToken = request.getParameter(LOGIN_TOKEN_PARAM_NAME);
            loginService.deleteByLoginToken(loginToken);
        } catch (Exception e) {
            throw new RuntimeException(Throwables.getStackTraceAsString(e), e);
        }
    }
}
