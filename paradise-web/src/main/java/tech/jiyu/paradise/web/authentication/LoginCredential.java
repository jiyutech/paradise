package tech.jiyu.paradise.web.authentication;

import com.google.common.collect.Lists;

import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.apache.commons.collections.CollectionUtils;

import java.io.IOException;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.BooleanUtils;
import tech.jiyu.utility.encryption.EncryptionUtils;
import tech.jiyu.utility.encryption.RSAKeyRepository;

/**
 * @author <a href="mailto:chenshao@jinjie.tech">shawsmith</a>
 */
public class LoginCredential {

    private Map<String, Object> parameters;

    public LoginCredential(HttpServletRequest request, RSAKeyRepository rsaKeyRepository, ObjectMapper objectMapper) throws IOException {
        JsonNode node = objectMapper.readTree(request.getReader());
        JsonNode cipherNode = node.get("data");
        JavaType javaType = objectMapper.getTypeFactory().constructMapType(HashMap.class, String.class, Object.class);
        if (cipherNode != null && rsaKeyRepository != null) {
            String cipher = cipherNode.asText();
            this.parameters = EncryptionUtils.rsaDecrypt(cipher, rsaKeyRepository.getPrivateKey(), javaType, objectMapper);
            // 附加数据明文传输无需解密
            if (node.size() > 1) {
                Map<String, Object> additionalAttributes = objectMapper.readValue(node.toString(), javaType);
                additionalAttributes.remove("data");
                parameters.putAll(additionalAttributes);
            }
        } else {
            this.parameters = objectMapper.readValue(node.toString(), javaType);
        }
    }

    public String getStringParameter(String parameterName, String defaultValue) {
        return Optional.ofNullable(parameters.get(parameterName)).map(String::valueOf).orElse(defaultValue);
    }

    public String getStringParameter(String parameterName) {
        return this.getStringParameter(parameterName, null);
    }

    public Boolean getBooleanParameter(String parameterName) {
        return BooleanUtils.toBoolean(this.getStringParameter(parameterName));
    }

    public Long getLongParameter(String parameterName, Long defaultValue) {
        Number number = (Number) parameters.get(parameterName);
        return Optional.ofNullable(number).map(Number::longValue).orElse(defaultValue);
    }

    @SuppressWarnings("unchecked")
    public Collection<Long> getLongArrayValues(String parameterName) {
        Collection<? extends Number> values = (Collection<? extends Number>) parameters.get(parameterName);
        if (CollectionUtils.isEmpty(values)) {
            return Lists.newArrayListWithCapacity(0);
        }
        return values.stream()
                .filter(Objects::nonNull)
                .map(Number::longValue)
                .collect(Collectors.toList());
    }

    public Map<String, Object> getMapParameter(String parameterName) {
        return (Map<String, Object>) parameters.get(parameterName);
    }

    public void setParameter(String parameterName, Object value) {
        parameters.put(parameterName, value);
    }
}
