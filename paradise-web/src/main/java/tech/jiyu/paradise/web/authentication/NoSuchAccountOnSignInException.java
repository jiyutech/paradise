package tech.jiyu.paradise.web.authentication;

import org.springframework.security.core.AuthenticationException;

import tech.jiyu.utility.api.errors.annotations.ApiErrorKey;

/**
 * @author <a href="mailto:chenshao@jiyu.tech">shawsmith</a>
 */
@ApiErrorKey(group = "system",
             id = "no.account.on.sign.in")
public class NoSuchAccountOnSignInException extends AuthenticationException {

    public NoSuchAccountOnSignInException(String msg, Throwable t) {
        super(msg, t);
    }

    public NoSuchAccountOnSignInException(String msg) {
        super(msg);
    }
}
