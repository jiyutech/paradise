package tech.jiyu.paradise.web.account.controller;

import com.google.common.collect.Maps;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import tech.jiyu.idcard.verification.domain.IdCardVerification;
import tech.jiyu.paradise.core.account.annotation.ApiAccessValidation;
import tech.jiyu.paradise.core.account.domain.*;
import tech.jiyu.paradise.core.account.enumtype.ApiAccessEnumType;
import tech.jiyu.paradise.core.login.domain.LoginInfo;
import tech.jiyu.paradise.web.account.vo.*;
import tech.jiyu.utility.cache.CacheStrategy;
import tech.jiyu.utility.pagination.PaginationResult;
import tech.jiyu.utility.web.ParameterUtils;

/**
 * @author <a href="mailto:chenshao@dekeapp.com">shawsmith</a>
 */
@RestController
public class AccountController {

    @Autowired
    private AccountDelegate accountDelegate;

    @Autowired
    private AccountListQueryBuilder accountListQueryBuilder;

    @RequestMapping(value = "/accounts",
            method = RequestMethod.POST,
            produces = {MediaType.APPLICATION_JSON_VALUE})
    @ApiAccessValidation(accessType = ApiAccessEnumType.POST_ACCOUNT)
    public ResponseEntity<AccountVO> register(@RequestBody AccountRegistrationDTO registrationDTO) {
        AccountVO accountVO = accountDelegate.register(registrationDTO);
        HttpStatus status = (accountVO != null) ? HttpStatus.OK : HttpStatus.INTERNAL_SERVER_ERROR;
        return new ResponseEntity<>(accountVO, status);
    }

    @RequestMapping(value = "/accounts",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiAccessValidation(accessType = ApiAccessEnumType.GET_ACCOUNT_LIST)
    public ResponseEntity<PaginationResult<AccountVO, Map<String, Long>>> getAccounts(@RequestParam Map<String, String> parameters) {
        AccountListQuery query = accountListQueryBuilder.buildAccountListQuery(parameters);
        PaginationResult<AccountVO, Map<String, Long>> result = accountDelegate.list(query);
        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    @RequestMapping(value = "/accounts/{accountId}",
            method = RequestMethod.GET,
            produces = {MediaType.APPLICATION_JSON_VALUE})
    @ApiAccessValidation(accessType = ApiAccessEnumType.GET_ACCOUNT_DETAIL)
    public ResponseEntity<AccountVO> findById(@PathVariable Long accountId) {
        AccountVO accountVO = accountDelegate.findById(accountId, CacheStrategy.CACHE_PREFER);
        HttpStatus status = (accountVO != null) ? HttpStatus.OK : HttpStatus.NOT_FOUND;
        return new ResponseEntity<>(accountVO, status);
    }

    @GetMapping(value = "/accounts/current",
            produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiAccessValidation(accessType = ApiAccessEnumType.GET_ACCOUNT_CURRENT)
    public ResponseEntity<AccountVO> findByLoginToken(@RequestParam String loginToken) {
        AccountVO accountVO = accountDelegate.findByLoginToken(loginToken, CacheStrategy.CACHE_PREFER);
        HttpStatus status = (accountVO != null) ? HttpStatus.OK : HttpStatus.NOT_FOUND;
        return new ResponseEntity<>(accountVO, status);
    }

    @RequestMapping(value = "/accounts/{accountId}",
            method = RequestMethod.PATCH,
            consumes = {MediaType.APPLICATION_JSON_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE})
    @ApiAccessValidation(accessType = ApiAccessEnumType.PATCH_ACCOUNT)
    public ResponseEntity<AccountVO> patch(@PathVariable Long accountId, @RequestBody AccountVO accountVO) throws IOException {
        accountVO.setId(accountId);
        if (StringUtils.isNotBlank(accountVO.getEmail())) {
            accountVO.setEmail(accountVO.getEmail().trim());
        }
        accountVO = accountDelegate.patch(accountVO);
        return new ResponseEntity<>(accountVO, HttpStatus.OK);
    }

    @RequestMapping(value = "/accounts/{accountId}",
            method = RequestMethod.DELETE,
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = {MediaType.APPLICATION_JSON_VALUE})
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @ApiAccessValidation(accessType = ApiAccessEnumType.DELETE_ACCOUNT)
    public void deleteById(@PathVariable Long accountId, @RequestBody AccountVO vo) {
        vo.setId(accountId);
        accountDelegate.deleteById(vo);
    }

    @RequestMapping(value = "/accounts/{accountId}/historyPhoneNumbers",
            method = RequestMethod.GET,
            produces = {MediaType.APPLICATION_JSON_VALUE})
    @ApiAccessValidation(accessType = ApiAccessEnumType.GET_ACCOUNT_HISTORY_NUMBER)
    public ResponseEntity<List<AccountHistoryPhoneNumberVO>> getHistoryPhoneNumbers(@PathVariable Long accountId) {
        List<AccountHistoryPhoneNumberVO> historyPhoneNumberVOs = accountDelegate.getHistoryPhoneNumbers(accountId);
        return new ResponseEntity<>(historyPhoneNumberVOs, HttpStatus.OK);
    }

    @RequestMapping(value = "/accounts/{accountId}/phoneNumber",
            method = RequestMethod.PATCH,
            consumes = {MediaType.APPLICATION_JSON_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE})
    @ApiAccessValidation(accessType = ApiAccessEnumType.PATCH_ACCOUNT_PHONE_NUMBER)
    public ResponseEntity<AccountVO> changePhoneNumber(@PathVariable Long accountId, @RequestBody PhoneNumberChangeInfo changeInfo) {
        changeInfo.setAccountId(accountId);
        AccountVO accountVO = accountDelegate.changePhoneNumber(changeInfo);
        HttpStatus status = (accountVO != null) ? HttpStatus.OK : HttpStatus.INTERNAL_SERVER_ERROR;
        return new ResponseEntity<>(accountVO, status);
    }

    @RequestMapping(value = "/accounts/{accountId}/activations",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiAccessValidation(accessType = ApiAccessEnumType.GET_ACCOUNT_ACTIVE)
    public ResponseEntity<Void> activeAccount(@PathVariable Long accountId,
                                              @RequestParam String code,
                                              @RequestParam(required = false) String returnUrl) {
        accountDelegate.activeAccount(accountId, code);
        return StringUtils.isNotBlank(returnUrl) ?
                createTemporarilyRedirectResponseEntity(returnUrl) : new ResponseEntity<>(HttpStatus.OK);
    }

    private ResponseEntity<Void> createTemporarilyRedirectResponseEntity(String returnUrl) {
        HttpHeaders headers = new HttpHeaders();
        headers.add(HttpHeaders.LOCATION, returnUrl);
        return new ResponseEntity<>(headers, HttpStatus.TEMPORARY_REDIRECT);
    }

    @RequestMapping(value = "/accounts/{accountId}/password",
            method = RequestMethod.PATCH,
            consumes = {MediaType.APPLICATION_JSON_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE})
    @ResponseBody
    @ApiAccessValidation(accessType = ApiAccessEnumType.PATCH_ACCOUNT_PASSWORD)
    public ResponseEntity<AccountVO> changePassword(@PathVariable Long accountId, @RequestBody @Valid AccountChangePasswordDTO changePasswordDTO) {
        AccountVO accountVO = accountDelegate.changePassword(accountId, changePasswordDTO);
        return new ResponseEntity<>(accountVO, HttpStatus.OK);
    }

    @RequestMapping(value = "/accounts/resetPassword",
            method = RequestMethod.PUT,
            consumes = {MediaType.APPLICATION_JSON_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE})
    @ResponseBody
    @ApiAccessValidation(accessType = ApiAccessEnumType.PUT_ACCOUNT_RESET_PASSWORD)
    public ResponseEntity<AccountVO> resetPassword(@RequestBody PhoneNumberPasswordResetInfo resetInfo) {
        AccountVO accountVO = accountDelegate.resetPassword(resetInfo);
        return new ResponseEntity<>(accountVO, HttpStatus.OK);
    }

    @RequestMapping(value = "/accounts/sendEmailResetPassword",
            method = RequestMethod.POST,
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = {MediaType.APPLICATION_JSON_VALUE})
    @ResponseBody
    @ApiAccessValidation(accessType = ApiAccessEnumType.POST_ACCOUNT_RESET_PASSWORD)
    public ResponseEntity<Object> sendEmailResetPassword(@RequestBody Map<String, String> parameters) {
        Long companyId = ParameterUtils.parseLongValue(parameters.get("companyId"), null);
        String mailAddress = parameters.get("mailAddress");
        accountDelegate.sendCaptchaCodeToEmail(companyId, mailAddress);
        return new ResponseEntity<>(Maps.newHashMap(), HttpStatus.OK);
    }

    @RequestMapping(value = "/accounts/resetPassword/fromEmail",
            method = RequestMethod.PUT,
            consumes = {MediaType.APPLICATION_JSON_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE})
    @ResponseBody
    @ApiAccessValidation(accessType = ApiAccessEnumType.PUT_ACCOUNT_RESET_PASSWORD_FROM_EMAIL)
    public ResponseEntity<AccountVO> resetPasswordByEmail(@RequestBody MailPasswordResetInfo mailPasswordResetInfo) {
        AccountVO accountVO = accountDelegate.resetPasswordByEmail(mailPasswordResetInfo);
        return new ResponseEntity<>(accountVO, HttpStatus.OK);
    }

    @RequestMapping(value = "/accounts/{accountId}/verification",
            method = RequestMethod.PATCH,
            consumes = {MediaType.APPLICATION_JSON_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE})
    @ResponseBody
    @ApiAccessValidation(accessType = ApiAccessEnumType.PATCH_ACCOUNT_VERIFICATION)
    public ResponseEntity<AccountVO> verifyAccount(@PathVariable Long accountId, @RequestBody IdCardVerification verification) {
        AccountVO accountVO = accountDelegate.verifyAccount(accountId, verification);
        return new ResponseEntity<>(accountVO, HttpStatus.OK);
    }

    @RequestMapping(value = "/accounts/{accountId}/loginInfo",
            method = RequestMethod.GET,
            produces = {MediaType.APPLICATION_JSON_VALUE})
    @ResponseBody
    @ApiAccessValidation(accessType = ApiAccessEnumType.GET_ACCOUNT_LOGIN_INFO)
    public ResponseEntity<LoginInfo> getAccountLoginInfo(@PathVariable Long accountId, @RequestParam String loginSource) {
        LoginInfo loginInfo = accountDelegate.getAccountLoginInfo(accountId, loginSource);
        return new ResponseEntity<>(loginInfo, HttpStatus.OK);
    }

    @DeleteMapping(value = "/accounts/{accountId}/loginInfo",
            produces = {MediaType.APPLICATION_JSON_VALUE})
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @ApiAccessValidation(accessType = ApiAccessEnumType.DELETE_ACCOUNT_LOGIN_INFO)
    public void deleteAccountLoginInfo(@PathVariable Long accountId, @RequestParam String loginSource) {
        accountDelegate.deleteAccountLoginInfo(accountId, loginSource);
    }

    @GetMapping(value = "/accounts/registration_info",
            produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiAccessValidation(accessType = ApiAccessEnumType.GET_ACCOUNT_REGISTRATION_INFO)
    public ResponseEntity<AccountRegistrationInfo> getAccountRegistrationInfo(@RequestParam(required = false) String countryCode,
                                                                              @RequestParam String phoneNumber,
                                                                              @RequestParam(required = false) Long companyId) {
        AccountRegistrationInfo accountRegistrationInfo = accountDelegate.getAccountRegistrationInfo(countryCode, phoneNumber, companyId);
        return ResponseEntity.ok(accountRegistrationInfo);
    }

    @PostMapping(value = "/accounts/{accountId}/bindCode",
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    @ApiAccessValidation(accessType = ApiAccessEnumType.POST_ACCOUNT_BIND_CODE)
    public ResponseEntity<BindCodeVO> getBindCode(@PathVariable Long accountId, @RequestBody Map<String, String> body) {
        return ResponseEntity.ok(accountDelegate.getBindCode(accountId));
    }
}

