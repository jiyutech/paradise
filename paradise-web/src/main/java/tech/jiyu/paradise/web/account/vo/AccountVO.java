package tech.jiyu.paradise.web.account.vo;

import com.google.common.collect.Collections2;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.data.annotation.Transient;

import java.math.BigDecimal;
import java.util.*;
import java.util.stream.Collectors;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import tech.jiyu.paradise.core.account.domain.Account;
import tech.jiyu.paradise.core.account.domain.AccountHistoryPhoneNumber;
import tech.jiyu.paradise.core.util.AccountUtils;
import tech.jiyu.paradise.web.privileges.vo.PrivilegeVO;
import tech.jiyu.parrot.geography.domain.Region;
import tech.jiyu.parrot.geography.service.RegionService;
import tech.jiyu.utility.cache.CacheStrategy;
import tech.jiyu.utility.datastructure.Pair;

/**
 * @author <a href="mailto:chenshao@dekeapp.com">shawsmith</a>
 */
@NoArgsConstructor
@Getter
@Setter
public class AccountVO {

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private Long id;
    private String name;
    private String avatar;
    private String gender;
    private Long companyId;
    private String email;
    private String countryCode;
    private String phoneNumber;
    private Long registrationDate;
    private String registrationSource;
    private String registrationTrafficType;
    private Date birthday;
    private String isoCode;
    private Long provinceId;
    private String provinceName;
    private Long cityId;
    private String cityName;
    private Long districtId;
    private String districtName;
    private String address;
    private String signature;
    private String roleTypeCode;
    private String accountPermission;
    private Long creatorId;
    private String creatorName;
    private Boolean enabled;
    private Boolean verified;
    private String username;
    private String password;
    private String weChatOpenId;
    private String weChatTinyAppOpenId;
    private String weChatH5AppOpenId;
    private String qqOpenId;
    private String facebookUserId;
    private String unionId;
    private String appleUserId;
    private String whatsappCountryCode;
    private String whatsappPhoneNumber;
    private Long telegramId;
    private String idCardName;
    private String idCardNumber;
    private String invitationCode;
    private String profession;
    private String individualResume;
    private Boolean used;
    private String timeZone;
    private Long lastLoginTime;
    private String lastLanguageCode;
    private Boolean audited;
    private Boolean workDayTimeEnabled;
    private List<String> allowedWorkDays;
    private List<Pair<String, String>> allowedWorkTimes;
    private Boolean polyAdminEnabled;
    private String nickname;
    private Boolean hasPassword;
    private Collection<String> usedPhoneNumbers;
    private Map<String, Object> additionalAttributes;
    private Collection<String> allowedLoginSources;
    private List<PrivilegeVO> privileges;
    @Transient
    private String captchaCode;
    private Long accountEstimateDeletingTime;
    private BigDecimal accountDeletingCoolingOffDays;
    private String firstName;
    private String LastName;
    private Boolean agreeToReceiveInfo;

    public AccountVO(Account account,
                     Boolean used,
                     String provinceName,
                     String cityName,
                     String districtName,
                     String imageUrl,
                     Account creatorAccount,
                     Map<String, Object> additionalAttributes,
                     Collection<String> allowedLoginSources) {
        if (account != null) {
            setId(account.getId());
            setName(account.getName());
            setAvatar(imageUrl);
            setGender(account.getGender());
            setCompanyId(account.getCompanyId());
            setEmail(account.getEmail());
            setCountryCode(account.getCountryCode());
            setPhoneNumber(account.getPhoneNumber());
            setRegistrationDate(account.getRegistrationDate());
            setRegistrationSource(account.getRegistrationSource());
            setRegistrationTrafficType(account.getRegistrationTrafficType());
            setBirthday(account.getBirthday());
            setIsoCode(account.getIsoCode());
            setProvinceId(account.getProvinceId());
            setProvinceName(provinceName);
            setCityId(account.getCityId());
            setCityName(cityName);
            setDistrictId(account.getDistrictId());
            setDistrictName(districtName);
            setAddress(account.getAddress());
            setSignature(account.getSignature());
            setRoleTypeCode(account.getRoleTypeCode());
            setAccountPermission(account.getAccountPermission());
            if (creatorAccount != null) {
                setCreatorId(creatorAccount.getId());
                setCreatorName(creatorAccount.getName());
            }
            setEnabled(account.getEnabled());
            setVerified(account.getVerified());
            setUsername(account.getUsername());
            setWeChatOpenId(account.getWeChatOpenId());
            setWeChatTinyAppOpenId(account.getWeChatTinyAppOpenId());
            setWeChatH5AppOpenId(account.getWeChatH5AppOpenId());
            setQqOpenId(account.getQqOpenId());
            setFacebookUserId(account.getFacebookUserId());
            setUnionId(account.getUnionId());
            setAppleUserId(account.getAppleUserId());
            setWhatsappCountryCode(account.getWhatsappCountryCode());
            setWhatsappPhoneNumber(account.getWhatsappPhoneNumber());
            setTelegramId(account.getTelegramId());
            setIdCardName(account.getIdCardName());
            setIdCardNumber(account.getIdCardNumber());
            setInvitationCode(account.getInvitationCode());
            setProfession(account.getProfession());
            setIndividualResume(account.getIndividualResume());
            setTimeZone(account.getTimeZone());
            setLastLoginTime(account.getLastLoginTime());
            setLastLanguageCode(account.getLastLanguageCode());
            setUsedPhoneNumbers(filterDuplicatedPhoneNumbers(account.getHistoryPhoneNumbers()));
            setUsed(used);
            setAudited(account.getAudited());

            setWorkDayTimeEnabled(account.getWorkDayTimeEnabled());
            setAllowedWorkDays(AccountUtils.formatWorkDays(account.getAllowedWorkDays()));
            setAllowedWorkTimes(AccountUtils.formatWorkTimes(account.getAllowedWorkTimes(), AccountUtils.WORK_TIMES_FORMAT));
            setPolyAdminEnabled(account.getPolyAdminEnabled());
            setNickname(account.getNickname());

            setHasPassword(StringUtils.isNotBlank(account.getPassword()));
            setAdditionalAttributes(additionalAttributes);
            setAllowedLoginSources(Optional.ofNullable(allowedLoginSources).orElse(Lists.newArrayListWithCapacity(0)));

            List<PrivilegeVO> privilegeVOs = Optional.ofNullable(account.getPrivileges())
                    .orElseGet(() -> Lists.newArrayListWithCapacity(0))
                    .stream()
                    .map(PrivilegeVO::new)
                    .collect(Collectors.toList());
            setPrivileges(privilegeVOs);
            setAccountDeletingCoolingOffDays(account.getAccountDeletingCoolingOffDays());
            setAccountEstimateDeletingTime(account.getAccountEstimateDeletingTime());
            setFirstName(account.getFirstName());
            setLastName(account.getLastName());
            setAgreeToReceiveInfo(account.getAgreeToReceiveInfo());
        }
    }

    private Collection<String> filterDuplicatedPhoneNumbers(List<AccountHistoryPhoneNumber> historyPhoneNumbers) {
        if (CollectionUtils.isEmpty(historyPhoneNumbers)) {
            return Lists.newArrayListWithCapacity(0);
        }
        return Lists.newLinkedList(Sets.newLinkedHashSet(Collections2.filter(Collections2.transform(historyPhoneNumbers, AccountHistoryPhoneNumber::getOldPhoneNumber), Objects::nonNull)));
    }

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    public String getPassword() {
        return password;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalAttributes() {
        return additionalAttributes;
    }

    @JsonIgnore
    public void setAdditionalAttributes(Map<String, Object> additionalAttributes) {
        this.additionalAttributes = additionalAttributes;
    }

    @JsonAnySetter
    public void setRestAttributes(String key, Object value) {
        if (additionalAttributes == null) {
            additionalAttributes = Maps.newHashMap();
        }
        additionalAttributes.put(key, value);
    }

    public Account buildAccount(RegionService regionService) {
        Account account = new Account();

        account.setId(getId());
        account.setName(getName());
        account.setAvatar(getAvatar());
        account.setGender(getGender());
        account.setCompanyId(getCompanyId());
        account.setEmail(getEmail());
        account.setCountryCode(getCountryCode());
        account.setPhoneNumber(getPhoneNumber());
        account.setBirthday(getBirthday());

        Long provinceId = getProvinceId();
        Long cityId = getCityId();
        Long districtId = getDistrictId();
        if (provinceId == null) {
            String provinceName = getProvinceName();
            if (StringUtils.isNotBlank(provinceName)) {
                Region province = regionService.findProvince(provinceName, CacheStrategy.CACHE_PREFER);
                if (province != null) {
                    provinceId = province.getId();
                    if (cityId == null) {
                        String cityName = getCityName();
                        if (StringUtils.isNotBlank(cityName)) {
                            Region city = regionService.findCity(cityName, provinceId, CacheStrategy.CACHE_PREFER);
                            if (city != null) {
                                cityId = city.getId();
                                if (districtId == null) {
                                    String districtName = getDistrictName();
                                    if (StringUtils.isNotBlank(districtName)) {
                                        Region district = regionService.findDistrict(districtName, cityId, CacheStrategy.CACHE_PREFER);
                                        if (district != null) {
                                            districtId = district.getId();
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        account.setIsoCode(getIsoCode());
        account.setProvinceId(provinceId);
        account.setCityId(cityId);
        account.setDistrictId(districtId);
        account.setAddress(getAddress());
        account.setSignature(getSignature());
        account.setRegistrationSource(getRegistrationSource());
        account.setRegistrationTrafficType(getRegistrationTrafficType());
        account.setRoleTypeCode(getRoleTypeCode());
        account.setAccountPermission(getAccountPermission());
        account.setEnabled(getEnabled());
        account.setVerified(getVerified());
        account.setUsername(getUsername());
        account.setPassword(getPassword());
        account.setWeChatOpenId(getWeChatOpenId());
        account.setWeChatTinyAppOpenId(getWeChatTinyAppOpenId());
        account.setWeChatH5AppOpenId(getWeChatH5AppOpenId());
        account.setQqOpenId(getQqOpenId());
        account.setFacebookUserId(getFacebookUserId());
        account.setUnionId(getUnionId());
        account.setAppleUserId(getAppleUserId());
        account.setWhatsappCountryCode(getWhatsappCountryCode());
        account.setWhatsappPhoneNumber(getWhatsappPhoneNumber());
        account.setTelegramId(getTelegramId());
        account.setIdCardName(getIdCardName());
        account.setIdCardNumber(getIdCardNumber());
        account.setProfession(getProfession());
        account.setIndividualResume(getIndividualResume());
        account.setTimeZone(getTimeZone());
        account.setLastLanguageCode(getLastLanguageCode());
        account.setLastLoginTime(getLastLoginTime());
        account.setAudited(getAudited());

        account.setWorkDayTimeEnabled(getWorkDayTimeEnabled());
        if (allowedWorkDays != null) {
            account.setAllowedWorkDays(AccountUtils.parseWorkDays(allowedWorkDays));
        }
        if (allowedWorkTimes != null) {
            account.setAllowedWorkTimes(AccountUtils.parseWorkTimes(allowedWorkTimes, AccountUtils.WORK_TIMES_FORMAT));
        }
        account.setPolyAdminEnabled(getPolyAdminEnabled());
        account.setNickname(getNickname());
        account.setFirstName(getFirstName());
        account.setLastName(getLastName());
        account.setAgreeToReceiveInfo(getAgreeToReceiveInfo());

        account.setAdditionalProperties(getAdditionalAttributes());

        if (privileges != null) {
            Collection<Long> privilegeIds = privileges
                    .stream()
                    .filter(privilege -> privilege != null && privilege.getId() != null)
                    .map(PrivilegeVO::getId)
                    .collect(Collectors.toSet());
            account.setPrivilegeIds(privilegeIds);
        }

        return account;
    }
}