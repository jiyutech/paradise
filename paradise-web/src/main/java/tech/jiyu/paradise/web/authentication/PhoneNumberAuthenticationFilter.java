package tech.jiyu.paradise.web.authentication;

import com.google.common.collect.Maps;

import org.apache.commons.lang3.StringUtils;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.AbstractAuthenticationProcessingFilter;
import org.springframework.security.web.util.matcher.RequestMatcher;

import java.util.Collection;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import tech.jiyu.captcha.domain.CaptchaCode;
import tech.jiyu.captcha.domain.CaptchaCodeKey;
import tech.jiyu.paradise.core.account.domain.Invitation;
import tech.jiyu.paradise.core.account.service.AccountService;
import tech.jiyu.paradise.core.login.service.LoginService;
import tech.jiyu.utility.phonenumber.PhoneNumberUtils;

/**
 * @author <a href="mailto:chenshao@dekeapp.com">shawsmith</a>
 */
public class PhoneNumberAuthenticationFilter extends AbstractAuthenticationProcessingFilter {

    private String loginCredentialsKey;

    public PhoneNumberAuthenticationFilter(RequestMatcher loginRequestMatcher, String loginCredentialsKey) {
        super(loginRequestMatcher);
        this.loginCredentialsKey = loginCredentialsKey;
    }

    @Override
    @SuppressWarnings("unchecked")
    protected boolean requiresAuthentication(HttpServletRequest request, HttpServletResponse response) {
        if (!super.requiresAuthentication(request, response)) {
            return false;
        }

        LoginCredential loginCredential = (LoginCredential) request.getAttribute(loginCredentialsKey);
        return (loginCredential != null) && loginCredential.getStringParameter("phoneNumber", null) != null;
    }

    @Override
    public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response) throws AuthenticationException {
        Authentication authentication = extractPhoneNumberLoginToken(request);
        return getAuthenticationManager().authenticate(authentication);
    }

    @SuppressWarnings("unchecked")
    private PhoneNumberAuthenticationToken extractPhoneNumberLoginToken(HttpServletRequest request) {
        LoginCredential loginCredential = (LoginCredential) request.getAttribute(loginCredentialsKey);
        PhoneNumberAuthenticationToken authenticationToken = new PhoneNumberAuthenticationToken(null);

        Long companyId = loginCredential.getLongParameter("companyId", AccountService.DEFAULT_COMPANY_ID);
        String phoneNumber = PhoneNumberUtils.filterNonDigitCharacters(loginCredential.getStringParameter("phoneNumber", null));
        String code = loginCredential.getStringParameter("captchaCode", null);

        String countryCode = loginCredential.getStringParameter("countryCode", null);
        authenticationToken.setCountryCode(countryCode);

        CaptchaCode captchaCode = new CaptchaCode(new CaptchaCodeKey(companyId, countryCode, phoneNumber), code);
        authenticationToken.setCaptchaCode(captchaCode);

        String weChatTinyAppJsCode = loginCredential.getStringParameter("weChatTinyAppJsCode", null);
        authenticationToken.setWeChatTinyAppJsCode(weChatTinyAppJsCode);

        String weChatOpenId = loginCredential.getStringParameter("weChatOpenId", null);
        authenticationToken.setWeChatOpenId(weChatOpenId);

        String roleTypeCode = loginCredential.getStringParameter("roleTypeCode", null);
        authenticationToken.setRoleTypeCode(roleTypeCode);

        String isoCode = loginCredential.getStringParameter("isoCode", null);
        authenticationToken.setIsoCode(isoCode);

        String email = loginCredential.getStringParameter("email", null);
        authenticationToken.setEmail(StringUtils.isEmpty(email) ? email : email.trim());

        String invitationCode = loginCredential.getStringParameter("invitationCode", null);
        Long inviteeId = loginCredential.getLongParameter("inviteeId", null);
        if (StringUtils.isNotBlank(invitationCode)) {
            Invitation invitation = new Invitation();
            invitation.setCode(invitationCode);
            invitation.setInviteeId(inviteeId);
            authenticationToken.setInvitation(invitation);
        }

        String loginSource = loginCredential.getStringParameter("loginSource", LoginService.DEFAULT_LOGIN_SOURCE);
        authenticationToken.setLoginSource(loginSource);

        String registrationSource = loginCredential.getStringParameter("registrationSource");
        authenticationToken.setRegistrationSource(registrationSource);

        String registrationTrafficType = loginCredential.getStringParameter("registrationTrafficType");
        authenticationToken.setRegistrationTrafficType(registrationTrafficType);

        String firstName = loginCredential.getStringParameter("firstName");
        authenticationToken.setFirstName(firstName);

        String lastName = loginCredential.getStringParameter("lastName");
        authenticationToken.setLastName(lastName);

        Boolean agreeToReceiveInfo = loginCredential.getBooleanParameter("agreeToReceiveInfo");
        authenticationToken.setAgreeToReceiveInfo(agreeToReceiveInfo);

        String lastLanguageCode = loginCredential.getStringParameter("lastLanguageCode");
        authenticationToken.setLastLanguageCode(lastLanguageCode);

        Collection<Long> privilegeIds = loginCredential.getLongArrayValues("privilegeIds");
        authenticationToken.setPrivilegeIds(privilegeIds);

        Map<String, Object> additionalProperties = loginCredential.getMapParameter("additionalProperties");
        if (additionalProperties == null) {
            additionalProperties = Maps.newHashMap();
        }
        additionalProperties.put("loginSource", loginSource); // 是否同意法务协议的业务场景需要根据loginSource来做判断
        authenticationToken.setAdditionalProperties(additionalProperties);

        return authenticationToken;
    }
}
