package tech.jiyu.paradise.web.authentication;

import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;

import javax.servlet.http.HttpServletResponse;

import tech.jiyu.paradise.core.account.domain.Account;
import tech.jiyu.paradise.core.login.domain.LoginInfo;

/**
 * @author <a href="mailto:chenshao@dekeapp.com">shawsmith</a>
 */
public interface LoginInfoWriter {

    void writeLoginInfo(LoginInfo loginInfo, Account account, ObjectMapper objectMapper, HttpServletResponse response) throws IOException;
}
