package tech.jiyu.paradise.web.account.vo;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import tech.jiyu.paradise.core.account.domain.InvitationCode;

/**
 * Created By InvitationCodeVO
 *
 * Date 2018/2/9 16:27
 *
 * @author SimonChan;emailAddress: simonchan@jiyu.tech
 */
@NoArgsConstructor
@Getter
@Setter
public class InvitationCodeVO {

    private String code;

    private String type;

    private Long accountId;

    public InvitationCodeVO(InvitationCode invitationCode) {
        if (invitationCode != null) {
            this.code = invitationCode.getCode();
            this.accountId = invitationCode.getAccountId();
            this.type = invitationCode.getStatus().getCode();
        }
    }
}
