package tech.jiyu.paradise.web.account.controller;

import com.google.common.collect.Lists;
import com.google.common.collect.Sets;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

import tech.jiyu.captcha.domain.CaptchaCodeKey;
import tech.jiyu.captcha.service.CaptchaCodeMismatchException;
import tech.jiyu.captcha.service.CaptchaService;
import tech.jiyu.idcard.verification.domain.IdCardVerification;
import tech.jiyu.paradise.core.account.domain.*;
import tech.jiyu.paradise.core.account.exception.*;
import tech.jiyu.paradise.core.account.service.*;
import tech.jiyu.paradise.core.login.domain.LoginInfo;
import tech.jiyu.paradise.core.login.service.LoginService;
import tech.jiyu.paradise.core.util.SecurityUtils;
import tech.jiyu.paradise.web.account.vo.*;
import tech.jiyu.paradise.web.authentication.AuthenticationUtils;
import tech.jiyu.parrot.geography.domain.Region;
import tech.jiyu.parrot.geography.service.RegionService;
import tech.jiyu.restful.api.domain.PaginationListQuery;
import tech.jiyu.restful.api.service.ModelViewObjectAssembler;
import tech.jiyu.restful.api.service.RestfulApiDelegator;
import tech.jiyu.utility.cache.CacheStrategy;
import tech.jiyu.utility.pagination.PaginationResult;
import tech.jiyu.utility.phonenumber.PhoneNumberUtils;

/**
 * @author <a href="mailto:chenshao@dekeapp.com">shawsmith</a>
 */
@Component
public class AccountDelegate extends RestfulApiDelegator<Account, AccountVO, Map<String, Long>> {

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private AccountService accountService;

    @Autowired
    private AccountHistoryPhoneNumberService accountHistoryPhoneNumberService;

    @Autowired
    private AccountActivationService accountActivationService;

    @Autowired
    private CaptchaService captchaService;

    @Autowired
    private RegionService regionService;

    @Value("${paradise.register.check.invitation.code:false}")
    private Boolean needCheckInvitationCode;

    @Value("${paradise.register.filter.firstZero:false}")
    private Boolean needFilterFirstZero;

    @Value("${paradise.need.validate.captcha.code:true}")
    private Boolean needValidateCaptchaCode;

    @Value("${paradise.bind.code.ttl.seconds:20}")
    private Long bindCodeTTlSeconds;

    @Autowired
    private AccountResetPasswordService accountResetPasswordService;

    @Autowired
    private InvitationCodeService invitationCodeService;

    @Autowired
    private LoginService loginService;

    @Autowired
    private AccountDeletingTaskService accountDeletingTaskService;

    @Autowired
    private ApplicationEventPublisher eventPublisher;

    @Autowired
    private AccountRegistrationInfoService accountRegistrationInfoService;

    @Autowired
    public AccountDelegate(AccountService accountService,
                           @Qualifier("accountModelViewObjectAssembler") ModelViewObjectAssembler<Account, AccountVO> accountModelViewObjectAssembler) {
        super(accountService, accountModelViewObjectAssembler);
    }

    public AccountVO register(AccountRegistrationDTO registrationDTO) {
        if (registrationDTO == null) {
            return null;
        }


        // 避免用户随便输入一些空白字符而导致手机号码错误, 所以这里需要做一下过滤.
        registrationDTO.setPhoneNumber(PhoneNumberUtils.filterNonDigitCharacters(registrationDTO.getPhoneNumber()));

        // 国外有些用户喜欢在手机号前面加0，如果有必要把手机号的第一个零过滤掉
        if (needFilterFirstZero) {
            registrationDTO.setPhoneNumber(PhoneNumberUtils.filterFirstZero(registrationDTO.getPhoneNumber()));
        }

        String invitationCode = registrationDTO.getInvitationCode();
        Account currentLoginAccount = AuthenticationUtils.loginAccount(accountService);
        // 如果是SYSTEM, IT创建账号则不需要验证邀请码
        boolean needCheckInvitationCode = currentLoginAccount == null || (!StringUtils.equals(SecurityUtils.SYSTEM, currentLoginAccount.getRoleTypeCode()) && !StringUtils.equals(SecurityUtils.IT, currentLoginAccount.getRoleTypeCode()));
        InvitationCode loadInvitationCode = null;
        if (needCheckInvitationCode) {
            loadInvitationCode = checkInvitationCodeExists(invitationCode);
        }

        // 如果用户填入了手机号码, 还需要校验验证码是否一致.
        validateCaptchaCodeIfNecessary(registrationDTO);
        fillRegionInfo(registrationDTO);

        validatePasswordIfNecessary(registrationDTO);

        Account account = registrationDTO.buildAccount(regionService);

        Account loginAccount = AuthenticationUtils.loginAccount(accountService);
        Long creatorId = (loginAccount != null) ? loginAccount.getId() : null;
        account.setCreatorId(creatorId);
        if (account.getCompanyId() == null) {
            account.setCompanyId(AccountService.DEFAULT_COMPANY_ID);
        }

        if (account.getRegistrationSource() == null) {
            account.setRegistrationSource(AccountService.DEFAULT_REGISTRATION_SOURCE);
        }

        if (account.getRegistrationTrafficType() == null) {
            account.setRegistrationTrafficType(AccountService.DEFAULT_REGISTRATION_TRAFFIC_TYPE);
        }

        if (account.getAudited() == null) {
            account.setAudited(Boolean.FALSE);
        }

        if (account.getWorkDayTimeEnabled() == null) {
            account.setWorkDayTimeEnabled(Boolean.FALSE);
        }

        if (account.getPolyAdminEnabled() == null) {
            account.setPolyAdminEnabled(Boolean.TRUE);
        }

        ActivationConfig activationConfig = registrationDTO.buildActivationConfig();
        Account newAccount = accountService.register(account, activationConfig, null);

        // 修改邀请码状态
        if (loadInvitationCode != null) {
            loadInvitationCode.setStatus(InvitationCodeStatus.USED);
            loadInvitationCode.setAccountId(newAccount.getId());
            invitationCodeService.updateByCode(loadInvitationCode);
        }
        return modelViewObjectAssembler.assemble(newAccount);
    }

    private InvitationCode checkInvitationCodeExists(String invitationCode) {
        if (!Boolean.TRUE.equals(needCheckInvitationCode)) {
            return null;
        }
        InvitationCode loadInvitationCode = invitationCodeService.selectByCode(invitationCode);
        if (loadInvitationCode == null || loadInvitationCode.getStatus() == InvitationCodeStatus.USED) {
            throw new InvitationCodeNotFoundException(invitationCode);
        }
        return loadInvitationCode;
    }

    private void fillRegionInfo(AccountRegistrationDTO registrationDTO) {
        if (registrationDTO == null) {
            return;
        }

        String provinceName = registrationDTO.getProvinceName();
        if (StringUtils.isNotBlank(provinceName)) {
            Region province = regionService.findProvince(provinceName, CacheStrategy.CACHE_PREFER);
            if (province != null) {
                registrationDTO.setProvinceId(province.getId());

                String cityName = registrationDTO.getCityName();
                if (StringUtils.isNotBlank(cityName)) {
                    Region city = regionService.findCity(cityName, province.getId(), CacheStrategy.CACHE_PREFER);
                    if (city != null) {
                        registrationDTO.setCityId(city.getId());


                        String districtName = registrationDTO.getDistrictName();
                        if (StringUtils.isNotBlank(districtName)) {
                            Region district = regionService.findDistrict(districtName, city.getId(), CacheStrategy.CACHE_PREFER);
                            if (district != null) {
                                registrationDTO.setDistrictId(district.getId());
                            }
                        }
                    }
                }
            }
        }
    }

    private void validateCaptchaCodeIfNecessary(AccountRegistrationDTO registrationDTO) {
        String phoneNumber = registrationDTO.getPhoneNumber();
        if (StringUtils.isBlank(phoneNumber)) {
            return;
        }

        Long companyId = registrationDTO.getCompanyId();
        if (companyId == null) {
            companyId = AccountService.DEFAULT_COMPANY_ID;
        }

        CaptchaCodeKey key = new CaptchaCodeKey(companyId, registrationDTO.getCountryCode(), phoneNumber);
        String captchaCode = registrationDTO.getCaptchaCode();
        if (captchaCode != null && !captchaService.validate(key, captchaCode)) {
            throw new CaptchaCodeMismatchException(key, captchaCode, captchaService.exchange(key));
        }
    }

    private void validatePasswordIfNecessary(AccountRegistrationDTO registrationDTO) {
        String password = registrationDTO.getPassword();
        String confirmedPassword = registrationDTO.getConfirmedPassword();
        if (StringUtils.isNotBlank(password) && StringUtils.isNotBlank(confirmedPassword) && !Objects.equals(password, confirmedPassword)) {
            throw new AccountPasswordMismatchException();
        }
    }

    @Override
    public PaginationResult<AccountVO, Map<String, Long>> list(PaginationListQuery query) {
        AccountListQuery accountListQuery = (AccountListQuery) query;
        applyCompanySettings(accountListQuery);
        return super.list(accountListQuery);
    }

    private void applyCompanySettings(AccountListQuery query) {
        if (!(query instanceof DefaultAccountListQuery)) {
            return;
        }


        DefaultAccountListQuery accountListQuery = (DefaultAccountListQuery) query;

        // 如果客户未指定公司ID，则使用当前登录账号所属的公司ID.
        if (accountListQuery.getCompanyId() == null) {
            Long companyId = determineCompanyIdFromLoginInfo();
            accountListQuery.setCompanyId(companyId);
        }
    }

    private Long determineCompanyIdFromLoginInfo() {
        Account account = AuthenticationUtils.loginAccount(accountService);
        return Optional.ofNullable(account)
                .map(Account::getCompanyId)
                .orElse(AccountService.DEFAULT_COMPANY_ID);
    }

    public AccountVO changePhoneNumber(PhoneNumberChangeInfo changeInfo) {
        if (changeInfo == null) {
            return null;
        }

        Account account = accountService.changePhoneNumber(changeInfo);
        return modelViewObjectAssembler.assemble(account);
    }

    public void activeAccount(Long accountId, String activationCode) {
        accountActivationService.activeAccount(accountId, activationCode);
    }

    public List<AccountHistoryPhoneNumberVO> getHistoryPhoneNumbers(Long accountId) {
        if (accountId == null) {
            return Lists.newArrayListWithCapacity(0);
        }
        List<AccountHistoryPhoneNumber> historyPhoneNumbers = accountHistoryPhoneNumberService.findByAccountIds(Sets.newHashSet(accountId), CacheStrategy.CACHE_PREFER)
                .get(accountId);
        if (CollectionUtils.isEmpty(historyPhoneNumbers)) {
            return Lists.newArrayListWithCapacity(0);
        }
        return historyPhoneNumbers.stream()
                .filter(historyPhoneNumber -> historyPhoneNumber != null && StringUtils.isNotBlank(historyPhoneNumber.getOldPhoneNumber()))
                .map(AccountHistoryPhoneNumberVO::new)
                .collect(Collectors.toList());
    }

    public AccountVO changePassword(Long accountId, AccountChangePasswordDTO changePasswordDTO) {
        if (!changePasswordDTO.isPasswordMatched()) {
            throw new AccountPasswordMismatchException();
        }
        Account account = AuthenticationUtils.loginAccount(accountService);
        if (account == null) {
            throw new NoSuchAccountException(String.valueOf(accountId));
        }
        accountService.changePassword(account.getId(), accountId, changePasswordDTO.getNewPassword(), changePasswordDTO.getOldPassword());
        return findById(accountId);
    }

    public AccountVO resetPassword(PhoneNumberPasswordResetInfo resetInfo) {
        if (resetInfo.getCompanyId() == null) {
            resetInfo.setCompanyId(AccountService.DEFAULT_COMPANY_ID);
        }

        Account account = accountService.resetPassword(resetInfo);
        return modelViewObjectAssembler.assemble(account);
    }

    public AccountVO verifyAccount(Long accountId, IdCardVerification verification) {
        Account account = accountService.verify(accountId, verification);
        return modelViewObjectAssembler.assemble(account);
    }

    public void sendCaptchaCodeToEmail(Long companyId, String mailAddress) {
        if (StringUtils.isBlank(mailAddress)) {
            throw new EmptyEmailException();
        }
        Account account = accountService.findByEmail(companyId, mailAddress);
        if (account == null) {
            throw new NoSuchAccountException(mailAddress);
        }
        accountResetPasswordService.sendCaptchaCodeToEmail(account);
    }

    public AccountVO resetPasswordByEmail(MailPasswordResetInfo mailPasswordResetInfo) {
        Account loginAccount = AuthenticationUtils.loginAccount(accountService);
        Long operatorId = (loginAccount != null) ? loginAccount.getId() : null;

        Account account = accountResetPasswordService.resetPasswordFromEmail(operatorId, mailPasswordResetInfo);
        return modelViewObjectAssembler.assemble(account);
    }

    public LoginInfo getAccountLoginInfo(Long accountId, String loginSource) {
        return Optional.ofNullable(loginService.findByLoginIdAndSource(String.valueOf(accountId), loginSource))
                .orElse(createEmptyLoginInfo());
    }

    private LoginInfo createEmptyLoginInfo() {
        return new LoginInfo();
    }

    public void deleteAccountLoginInfo(Long accountId, String loginSource) {
        LoginInfo existed = loginService.findByLoginIdAndSource(String.valueOf(accountId), loginSource);
        if (existed != null) {
            loginService.deleteByLoginToken(existed.getLoginToken());
        }
    }

    public AccountRegistrationInfo getAccountRegistrationInfo(String countryCode, String phoneNumber, Long companyId) {
        return accountRegistrationInfoService.getByCountryCodeAndPhoneNumber(countryCode, phoneNumber, companyId);
    }

    @Transactional
    public void deleteById(AccountVO vo) {
        checkPasswordOrCaptchaCodeBeforeDeleting(vo);
        accountDeletingTaskService.createDeletingTask(vo.getId());

        // 发布申请删除账号的事件
        Account account = accountService.findById(vo.getId());
        eventPublisher.publishEvent(new AccountDeletingApplicationEvent(account));
    }

    private void checkPasswordOrCaptchaCodeBeforeDeleting(AccountVO vo) {
        Account account = accountService.findById(vo.getId());
        Long companyId = account.getCompanyId();
        if (companyId == null) {
            companyId = AccountService.DEFAULT_COMPANY_ID;
        }

        String password = vo.getPassword();
        if (StringUtils.isNotBlank(password) && StringUtils.isNotBlank(account.getPassword())) {
            if (!passwordEncoder.matches(password, account.getPassword())) {
                throw new OldPasswordWrongException();
            }
        } else {
            CaptchaCodeKey key = new CaptchaCodeKey(companyId, account.getCountryCode(), account.getPhoneNumber());
            String captchaCode = vo.getCaptchaCode();
            if (StringUtils.isBlank(captchaCode) || !captchaService.validate(key, captchaCode) && needValidateCaptchaCode) {
                throw new CaptchaCodeMismatchException(key, captchaCode, captchaService.exchange(key));
            }
        }
    }

    public AccountVO findByLoginToken(String loginToken, CacheStrategy cacheStrategy) {
        if (StringUtils.isBlank(loginToken)) {
            return null;
        }
        LoginInfo loginInfo = loginService.findByLoginToken(loginToken);
        if (loginInfo == null) {
            return null;
        }
        return this.findById(Long.valueOf(loginInfo.getLoginId()), cacheStrategy);
    }

    public BindCodeVO getBindCode(Long accountId) {
        String code = accountService.createBindCode(accountId, bindCodeTTlSeconds);
        return new BindCodeVO(code, bindCodeTTlSeconds);
    }
}
