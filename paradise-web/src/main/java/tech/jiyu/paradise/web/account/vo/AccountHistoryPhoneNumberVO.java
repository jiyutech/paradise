package tech.jiyu.paradise.web.account.vo;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import tech.jiyu.paradise.core.account.domain.AccountHistoryPhoneNumber;

/**
 * @author <a href="mailto:chenshao@dekeapp.com">shawsmith</a>
 */
@NoArgsConstructor
@Getter
@Setter
public class AccountHistoryPhoneNumberVO {

    private Long id;
    private Long accountId;
    private String oldPhoneNumber;
    private String newPhoneNumber;
    private Long changeDate;

    public AccountHistoryPhoneNumberVO(AccountHistoryPhoneNumber historyPhoneNumber) {
        if (historyPhoneNumber != null) {
            setId(historyPhoneNumber.getId());
            setAccountId(historyPhoneNumber.getAccountId());
            setOldPhoneNumber(historyPhoneNumber.getOldPhoneNumber());
            setNewPhoneNumber(historyPhoneNumber.getNewPhoneNumber());
            setChangeDate(historyPhoneNumber.getChangeDate());
        }
    }
}
