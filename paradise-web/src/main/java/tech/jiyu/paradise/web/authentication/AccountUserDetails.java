package tech.jiyu.paradise.web.authentication;

import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import org.apache.commons.lang3.StringUtils;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import tech.jiyu.paradise.core.account.domain.Account;
import tech.jiyu.paradise.core.util.AccountUtils;

import java.util.Collection;


/**
 * @author <a href="mailto:chenshao@jiyu.tech">shawsmith</a>
 */
public class AccountUserDetails implements UserDetails {

    private final Account account;

    public AccountUserDetails(Account account) {
        this.account = account;
    }

    public Account getAccount() {
        return account;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        if (account == null || account.getRoleTypeCode() == null) {
            return Lists.newArrayListWithCapacity(0);
        }

        String roleTypeCode = account.getRoleTypeCode();
        GrantedAuthority authority = new SimpleGrantedAuthority(roleTypeCode);
        return Sets.newHashSet(authority);
    }

    @Override
    public String getPassword() {
        return account.getPassword();
    }

    @Override
    public String getUsername() {
        String username = account.getUsername();
        if (StringUtils.isBlank(username)) {
            username = account.getEmail();
        }
        if (StringUtils.isBlank(username)) {
            if (StringUtils.isNotBlank(account.getCountryCode())) {
                username = AccountUtils.buildPhoneNumberUserName(account.getCountryCode(), account.getPhoneNumber());
            }
        }
        if (StringUtils.isBlank(username)) {
            username = account.getPhoneNumber();
        }
        return username;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return (account != null) && account.isActuallyEnabled();
    }
}
