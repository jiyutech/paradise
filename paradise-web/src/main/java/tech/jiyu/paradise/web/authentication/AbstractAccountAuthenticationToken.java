package tech.jiyu.paradise.web.authentication;

import com.google.common.collect.Maps;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.collections.MapUtils;
import org.springframework.security.authentication.AbstractAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;

import java.util.Collection;
import java.util.Map;

@Getter
@Setter
public abstract class AbstractAccountAuthenticationToken extends AbstractAuthenticationToken {

    private String firstName;
    private String lastName;
    private Boolean agreeToReceiveInfo;
    private String lastLanguageCode;
    private Map<String, Object> additionalProperties;

    public AbstractAccountAuthenticationToken(Collection<? extends GrantedAuthority> authorities) {
        super(authorities);
    }

    public void attachLoginSourceToAdditionalProperties(String loginSource) {
        if (this.getAdditionalProperties() == null) {
            this.setAdditionalProperties(Maps.newHashMap());
        }
        this.getAdditionalProperties().put("loginSource", loginSource);
    }

}
