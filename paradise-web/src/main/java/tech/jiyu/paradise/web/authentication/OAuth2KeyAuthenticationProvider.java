package tech.jiyu.paradise.web.authentication;

import com.google.common.base.Throwables;

import org.apache.commons.lang3.StringUtils;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.AuthenticationServiceException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;

import java.util.List;

import tech.jiyu.captcha.domain.CaptchaCodeKey;
import tech.jiyu.captcha.service.CaptchaCodeMismatchException;
import tech.jiyu.captcha.service.CaptchaService;
import tech.jiyu.paradise.core.account.domain.Account;
import tech.jiyu.paradise.core.account.exception.IllegalLoginSourceException;
import tech.jiyu.paradise.core.account.service.AccountLoginSourceService;
import tech.jiyu.paradise.core.authentication.domain.OAuth2Type;
import tech.jiyu.paradise.core.authentication.exception.NoSuchOAuth2ServiceException;
import tech.jiyu.paradise.core.authentication.service.OAuth2Service;

/**
 * CreatedDate 2018-07-10 上午10:31
 *
 * @author SimonChan;emailAddress: simonchan@jinjie.tech
 */
public class OAuth2KeyAuthenticationProvider implements AuthenticationProvider {

    private List<OAuth2Service> oauth2Services;

    private AccountLoginSourceService accountLoginSourceService;

    private CaptchaService captchaService;

    private boolean needValidateCaptchaCode;

    public OAuth2KeyAuthenticationProvider(List<OAuth2Service> oauth2Services, AccountLoginSourceService accountLoginSourceService, CaptchaService captchaService, boolean needValidateCaptchaCode) {
        this.oauth2Services = oauth2Services;
        this.accountLoginSourceService = accountLoginSourceService;
        this.captchaService = captchaService;
        this.needValidateCaptchaCode = needValidateCaptchaCode;
    }

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        OAuth2KeyAuthenticationToken keyAuthenticationToken = (OAuth2KeyAuthenticationToken) authentication;
        OAuth2Type authType = OAuth2Type.fromCode(keyAuthenticationToken.getAuthType());
        try {
            OAuth2Service oauth2Service = oauth2Services.stream().filter(service -> service.supports(authType)).findFirst().orElseThrow(() -> new NoSuchOAuth2ServiceException(String.valueOf(authType)));
            // 如果提供了手机号需要校验验证码
            validateCaptchaCodeIfNecessary(keyAuthenticationToken);

            Account account = oauth2Service.createOrUpdate(keyAuthenticationToken.buildOAuth2KeyParameters());

            if (!accountLoginSourceService.supportLoginSource(account.getId(), keyAuthenticationToken.getLoginSource())) {
                throw new IllegalLoginSourceException(null);
            }

            keyAuthenticationToken.setDetails(account);
            keyAuthenticationToken.setAuthenticated(true);
            return keyAuthenticationToken;
        } catch (Exception e) {
            Throwables.throwIfInstanceOf(e, IllegalLoginSourceException.class);
            throw new AuthenticationServiceException(e.getMessage(), e);
        }
    }

    private void validateCaptchaCodeIfNecessary(OAuth2KeyAuthenticationToken keyAuthenticationToken) {
        String phoneNumber = keyAuthenticationToken.getPhoneNumber();
        if (StringUtils.isBlank(phoneNumber)) {
            return;
        }

        String captchaCode = keyAuthenticationToken.getCaptchaCode();
        if (StringUtils.isBlank(captchaCode) && StringUtils.isBlank(keyAuthenticationToken.getPassword())) {
            throw new CaptchaCodeMismatchException();
        }

        if (StringUtils.isNotBlank(captchaCode)) {
            CaptchaCodeKey captchaCodeKey = new CaptchaCodeKey(keyAuthenticationToken.getCompanyId(), keyAuthenticationToken.getCountryCode(), phoneNumber);
            if (!captchaService.validate(captchaCodeKey, captchaCode) && Boolean.TRUE.equals(needValidateCaptchaCode)) {
                throw new CaptchaCodeMismatchException(captchaCodeKey, captchaCode, captchaService.exchange(captchaCodeKey));
            }
        }
    }

    @Override
    public boolean supports(Class<?> authentication) {
        return OAuth2KeyAuthenticationToken.class.isAssignableFrom(authentication);
    }
}
