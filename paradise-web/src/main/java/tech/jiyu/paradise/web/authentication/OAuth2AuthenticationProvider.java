package tech.jiyu.paradise.web.authentication;

import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.AuthenticationServiceException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;

import java.util.List;

import tech.jiyu.paradise.core.account.domain.Account;
import tech.jiyu.paradise.core.account.exception.IllegalLoginSourceException;
import tech.jiyu.paradise.core.account.service.AccountLoginSourceService;
import tech.jiyu.paradise.core.authentication.domain.OAuth2Type;
import tech.jiyu.paradise.core.authentication.exception.NoSuchOAuth2ServiceException;
import tech.jiyu.paradise.core.authentication.service.OAuth2Service;

/**
 * @author <a href="mailto:chenshao@dekeapp.com">shawsmith</a>
 */
public class OAuth2AuthenticationProvider implements AuthenticationProvider {

    private List<OAuth2Service> oAuth2Services;
    private AccountLoginSourceService accountLoginSourceService;

    public OAuth2AuthenticationProvider(List<OAuth2Service> oAuth2Services, AccountLoginSourceService accountLoginSourceService) {
        this.oAuth2Services = oAuth2Services;
        this.accountLoginSourceService = accountLoginSourceService;
    }

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        try {
            OAuth2AuthenticationToken authenticationToken = (OAuth2AuthenticationToken) authentication;

            OAuth2Type authType = OAuth2Type.fromCode(authenticationToken.getAuthType());
            OAuth2Service oAuth2Service = oAuth2Services.stream()
                    .filter(service -> service != null && service.supports(authType))
                    .findFirst()
                    .orElse(null);
            if (oAuth2Service == null) {
                throw new NoSuchOAuth2ServiceException(String.valueOf(authType));
            }

            Account account = oAuth2Service.createOrUpdate(authenticationToken.buildOAuth2Parameters());
            if (!accountLoginSourceService.supportLoginSource(account.getId(), authenticationToken.getLoginSource())) {
                throw new IllegalLoginSourceException(null);
            }

            authenticationToken.setDetails(account);
            authenticationToken.setAuthenticated(true);
            return authenticationToken;
        } catch (IllegalLoginSourceException e) {
            throw e;
        } catch (Exception e) {
            throw new AuthenticationServiceException(e.getMessage(), e);
        }
    }

    @Override
    public boolean supports(Class<?> authentication) {
        return OAuth2AuthenticationToken.class.isAssignableFrom(authentication);
    }
}
