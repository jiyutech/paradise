package tech.jiyu.paradise.web.authentication;

import org.apache.commons.lang3.StringUtils;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UserDetails;

import lombok.extern.slf4j.Slf4j;
import tech.jiyu.captcha.domain.GoogleRecaptchaScene;
import tech.jiyu.captcha.service.GoogleRecaptchaService;
import tech.jiyu.paradise.conf.PasswordAuthenticationSecurityPolicyProperties;
import tech.jiyu.paradise.core.account.domain.Account;
import tech.jiyu.paradise.core.account.exception.IllegalLoginSourceException;
import tech.jiyu.paradise.core.account.service.AccountLoginSourceService;
import tech.jiyu.paradise.core.authentication.domain.AuthenticationFailedRecord;
import tech.jiyu.paradise.core.authentication.exception.InvalidManMachineVerificationException;
import tech.jiyu.paradise.core.authentication.exception.ManMachineVerificationException;
import tech.jiyu.paradise.core.authentication.service.AuthenticationFailedRecordService;

/**
 * @author <a href="mailto:chenshao@jiyu.tech">shawsmith</a>
 */
@Slf4j
public class ParadiseDaoAuthenticationProvider extends DaoAuthenticationProvider {

    private AccountLoginSourceService accountLoginSourceService;
    private AccountUserDetailsService accountUserDetailsService;
    private AuthenticationFailedRecordService authenticationFailedRecordService;
    private PasswordAuthenticationSecurityPolicyProperties passwordAuthenticationSecurityPolicyProperties;
    private GoogleRecaptchaService googleRecaptchaService;


    public ParadiseDaoAuthenticationProvider(AccountLoginSourceService accountLoginSourceService, AccountUserDetailsService accountUserDetailsService,
                                             AuthenticationFailedRecordService authenticationFailedRecordService, PasswordAuthenticationSecurityPolicyProperties passwordAuthenticationSecurityPolicyProperties,
                                             GoogleRecaptchaService googleRecaptchaService) {
        this.accountLoginSourceService = accountLoginSourceService;
        this.accountUserDetailsService = accountUserDetailsService;
        this.authenticationFailedRecordService = authenticationFailedRecordService;
        this.passwordAuthenticationSecurityPolicyProperties = passwordAuthenticationSecurityPolicyProperties;
        this.googleRecaptchaService = googleRecaptchaService;
    }

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        ParadiseUsernamePasswordAuthenticationToken authenticationToken = (ParadiseUsernamePasswordAuthenticationToken) authentication;
        Long companyId = authenticationToken.getCompanyId();
        try {
            AccountUserDetailsService.CompanyIdHolder.set(companyId);
            Authentication result = super.authenticate(authentication);

            UserDetails userDetails = accountUserDetailsService.loadUserByUsername((String) authenticationToken.getPrincipal());
            Account account = (userDetails != null) ? ((AccountUserDetails) userDetails).getAccount() : null;

            String loginSource = authenticationToken.getLoginSource();
            if (account != null && !Boolean.TRUE.equals(accountLoginSourceService.supportLoginSource(account.getId(), loginSource))) {
                throw new IllegalLoginSourceException(null);
            }

            return result;
        } finally {
            AccountUserDetailsService.CompanyIdHolder.remove();
        }
    }


    @Override
    protected Authentication createSuccessAuthentication(Object principal, Authentication authentication, UserDetails user) {
        ParadiseUsernamePasswordAuthenticationToken authenticationToken = (ParadiseUsernamePasswordAuthenticationToken) authentication;
        String loginSource = authenticationToken.getLoginSource();
        Long companyId = authenticationToken.getCompanyId();

        Authentication parentSuccessAuthentication = super.createSuccessAuthentication(principal, authentication, user);
        ParadiseUsernamePasswordAuthenticationToken result = new ParadiseUsernamePasswordAuthenticationToken(parentSuccessAuthentication.getPrincipal(), parentSuccessAuthentication.getCredentials(), parentSuccessAuthentication.getAuthorities(), loginSource, companyId);
        result.setDetails(parentSuccessAuthentication.getDetails());
        return result;
    }

    @Override
    protected void additionalAuthenticationChecks(UserDetails userDetails, UsernamePasswordAuthenticationToken authentication) throws AuthenticationException {
        // 如果启用了密码登录安全策略，则需要做额外的操作
        if (passwordAuthenticationSecurityPolicyProperties.getEnabled()) {
            AccountUserDetails accountUserDetails = (AccountUserDetails) userDetails;
            Account account = accountUserDetails.getAccount();
            Long accountId = account.getId();
            AuthenticationFailedRecord authenticationFailedRecord = authenticationFailedRecordService.findByAccountId(accountId);
            boolean needRecaptchaKey = authenticationFailedRecord != null && authenticationFailedRecord.getCount() >= passwordAuthenticationSecurityPolicyProperties.getMaximumWrongCount();
            ParadiseUsernamePasswordAuthenticationToken authenticationToken = (ParadiseUsernamePasswordAuthenticationToken) authentication;
            String reCaptchaKey = authenticationToken.getReCaptchaKey();
            String presentedPassword = authentication.getCredentials().toString();
            if (!getPasswordEncoder().matches(presentedPassword, userDetails.getPassword())) {
                if (needRecaptchaKey && StringUtils.isBlank(reCaptchaKey)) {
                    log.info("the account({}) has provided wrong password and the maximum wrong limit was reached. current wrong count:{}", accountId, authenticationFailedRecord.getCount());
                    throw new ManMachineVerificationException("the account(" + accountId + ") has provided wrong password");
                }
                authenticationFailedRecordService.record(accountId);
            }

            // 人机校验检查
            if (needRecaptchaKey && googleRecaptchaService != null) {
                if (StringUtils.isBlank(reCaptchaKey)) {
                    throw new ManMachineVerificationException("the account(" + accountId + ") has no provided reCaptcha Key");
                }
                if (!googleRecaptchaService.verify(GoogleRecaptchaScene.LOGIN, reCaptchaKey)) {
                    log.info("the account({}) has no passed man-machine verification.", accountId);
                    throw new InvalidManMachineVerificationException("accountId:" + accountId);
                }
            }
        }

        super.additionalAuthenticationChecks(userDetails, authentication);
        // 登录成功后还需要移除掉记录
        if (passwordAuthenticationSecurityPolicyProperties.getEnabled()) {
            AccountUserDetails accountUserDetails = (AccountUserDetails) userDetails;
            Account account = accountUserDetails.getAccount();
            Long accountId = account.getId();
            authenticationFailedRecordService.deleteByAccountId(accountId);
        }
    }
}
