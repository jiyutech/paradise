package tech.jiyu.paradise.web.authentication;

import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;

import java.util.Collection;

/**
 * @author <a href="mailto:chenshao@jiyu.tech">shawsmith</a>
 */
public class ParadiseUsernamePasswordAuthenticationToken extends UsernamePasswordAuthenticationToken {

    private String loginSource;
    private Long companyId;
    private String reCaptchaKey;

    public ParadiseUsernamePasswordAuthenticationToken(Object principal, Object credentials, String loginSource, Long companyId, String reCaptchaKey) {
        super(principal, credentials);
        this.loginSource = loginSource;
        this.companyId = companyId;
        this.reCaptchaKey = reCaptchaKey;
    }

    public ParadiseUsernamePasswordAuthenticationToken(Object principal, Object credentials, Collection<? extends GrantedAuthority> authorities, String loginSource, Long companyId) {
        super(principal, credentials, authorities);
        this.loginSource = loginSource;
        this.companyId = companyId;
    }

    public String getLoginSource() {
        return loginSource;
    }

    public void setLoginSource(String loginSource) {
        this.loginSource = loginSource;
    }

    public Long getCompanyId() {
        return companyId;
    }

    public void setCompanyId(Long companyId) {
        this.companyId = companyId;
    }

    public String getReCaptchaKey() {
        return reCaptchaKey;
    }

    public void setReCaptchaKey(String reCaptchaKey) {
        this.reCaptchaKey = reCaptchaKey;
    }
}
