package tech.jiyu.paradise.web.privileges.controller;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import java.util.Optional;

import tech.jiyu.paradise.core.privileges.domain.Privilege;
import tech.jiyu.paradise.core.privileges.service.PrivilegeService;
import tech.jiyu.paradise.web.privileges.vo.PrivilegeVO;
import tech.jiyu.restful.api.service.ModelViewObjectAssembler;
import tech.jiyu.restful.api.service.RestfulApiDelegator;

/**
 * @author <a href="mailto:chenshao@jinjie.tech">shawsmith</a>
 */
@Component
public class PrivilegeDelegate extends RestfulApiDelegator<Privilege, PrivilegeVO, Void> {

    public PrivilegeDelegate(PrivilegeService privilegeService,
                             @Qualifier("privilegeModelViewObjectAssembler") ModelViewObjectAssembler<Privilege, PrivilegeVO> privilegeModelViewObjectAssembler) {
        super(privilegeService, privilegeModelViewObjectAssembler);
    }

    @Override
    public PrivilegeVO create(PrivilegeVO privilegeVO) {
        doCreate(privilegeVO);
        return super.create(privilegeVO);
    }

    private void doCreate(PrivilegeVO privilegeVO) {
        if (privilegeVO == null) {
            return;
        }
        privilegeVO.setOrderNumber(Optional.ofNullable(privilegeVO.getOrderNumber()).orElse(Long.MAX_VALUE));
    }
}
