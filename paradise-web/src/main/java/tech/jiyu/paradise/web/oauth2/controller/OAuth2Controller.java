package tech.jiyu.paradise.web.oauth2.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import tech.jiyu.paradise.core.authentication.domain.OAuth2Parameters;
import tech.jiyu.paradise.web.oauth2.vo.OAuth2KeyVO;

/**
 * CreatedDate 2018-07-09 下午5:25
 *
 * @author SimonChan;emailAddress: simonchan@jinjie.tech
 */
@RestController
public class OAuth2Controller {

    @Autowired
    private OAuth2Delegate oauth2Delegate;

    @PostMapping(value = "/oauth2/keys",
                 consumes = MediaType.APPLICATION_JSON_VALUE,
                 produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<OAuth2KeyVO> createOAuth2Key(@RequestBody OAuth2Parameters oauth2Parameters) {
        OAuth2KeyVO vo = oauth2Delegate.createOAuth2Key(oauth2Parameters);
        return new ResponseEntity<>(vo, HttpStatus.CREATED);
    }
}
