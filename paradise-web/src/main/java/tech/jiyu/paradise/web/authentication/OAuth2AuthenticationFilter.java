package tech.jiyu.paradise.web.authentication;

import org.apache.commons.collections.MapUtils;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.AbstractAuthenticationProcessingFilter;
import org.springframework.security.web.util.matcher.RequestMatcher;

import java.io.IOException;
import java.util.Collection;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import tech.jiyu.paradise.core.account.service.AccountService;
import tech.jiyu.paradise.core.authentication.domain.OAuth2Type;
import tech.jiyu.paradise.core.login.service.LoginService;
import tech.jiyu.paradise.core.util.SecurityUtils;

/**
 * @author <a href="mailto:chenshao@dekeapp.com">shawsmith</a>
 */
public class OAuth2AuthenticationFilter extends AbstractAuthenticationProcessingFilter {

    private String loginCredentialsKey;

    public OAuth2AuthenticationFilter(RequestMatcher loginRequestMatcher, String loginCredentialsKey) {
        super(loginRequestMatcher);
        this.loginCredentialsKey = loginCredentialsKey;
    }

    @Override
    @SuppressWarnings("unchecked")
    protected boolean requiresAuthentication(HttpServletRequest request, HttpServletResponse response) {
        if (!super.requiresAuthentication(request, response)) {
            return false;
        }

        LoginCredential loginCredential = (LoginCredential) request.getAttribute(loginCredentialsKey);
        return (loginCredential != null) && loginCredential.getStringParameter("authCode", null) != null;
    }

    @Override
    public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response) throws AuthenticationException, IOException, ServletException {
        Authentication authentication = extractOAuth2AuthenticationToken(request);
        return getAuthenticationManager().authenticate(authentication);
    }

    @SuppressWarnings("unchecked")
    private OAuth2AuthenticationToken extractOAuth2AuthenticationToken(HttpServletRequest request) {
        OAuth2AuthenticationToken authenticationToken = new OAuth2AuthenticationToken(null);

        LoginCredential loginCredential = (LoginCredential) request.getAttribute(loginCredentialsKey);
        Long companyId = loginCredential.getLongParameter("companyId", AccountService.DEFAULT_COMPANY_ID);
        authenticationToken.setCompanyId(companyId);

        authenticationToken.setAuthCode(loginCredential.getStringParameter("authCode", null));
        authenticationToken.setAuthType(loginCredential.getStringParameter("authType", null));

        OAuth2Type auth2Type = OAuth2Type.fromCode(authenticationToken.getAuthType());
        if (auth2Type == OAuth2Type.QQ) {
            authenticationToken.setLoginSource(LoginService.QQ_LOGIN_SOURCE);
        } else if (auth2Type == OAuth2Type.WECHAT || auth2Type == OAuth2Type.WECHAT_TINY_APP || auth2Type == OAuth2Type.WECHAT_H5_APP) {
            authenticationToken.setLoginSource(LoginService.WECHAT_LOGIN_SOURCE);
        } else if (auth2Type == OAuth2Type.FACEBOOK) {
            authenticationToken.setLoginSource(LoginService.FACEBOOK_LOGIN_SOURCE);
        } else {
            String loginSource = loginCredential.getStringParameter("loginSource", LoginService.DEFAULT_LOGIN_SOURCE);
            authenticationToken.setLoginSource(loginSource);
        }

        String roleTypeCode = loginCredential.getStringParameter("roleTypeCode", SecurityUtils.CUSTOMER);
        authenticationToken.setRoleTypeCode(roleTypeCode);

        String name = loginCredential.getStringParameter("name", null);
        authenticationToken.setNickName(name);

        String avatar = loginCredential.getStringParameter("avatar", null);
        authenticationToken.setAvatar(avatar);

        String weChatTinyAppEncryptedData = loginCredential.getStringParameter("weChatTinyAppEncryptedData", null);
        authenticationToken.setWeChatTinyAppEncryptedData(weChatTinyAppEncryptedData);

        String weChatTinyAppIv = loginCredential.getStringParameter("weChatTinyAppIv", null);
        authenticationToken.setWeChatTinyAppIv(weChatTinyAppIv);

        String weChatTinyAppId = loginCredential.getStringParameter("weChatTinyAppId", null);
        authenticationToken.setWeChatTinyAppId(weChatTinyAppId);

        String firstName = loginCredential.getStringParameter("firstName", null);
        authenticationToken.setFirstName(firstName);

        String lastName = loginCredential.getStringParameter("lastName", null);
        authenticationToken.setLastName(lastName);

        String email = loginCredential.getStringParameter("email", null);
        authenticationToken.setEmail(email);

        Boolean agreeToReceiveInfo = loginCredential.getBooleanParameter("agreeToReceiveInfo");
        authenticationToken.setAgreeToReceiveInfo(agreeToReceiveInfo);

        String lastLanguageCode = loginCredential.getStringParameter("lastLanguageCode", null);
        authenticationToken.setLastLanguageCode(lastLanguageCode);

        Map<String, Object> additionalProperties = loginCredential.getMapParameter("additionalProperties");
        if (MapUtils.isNotEmpty(additionalProperties)) {
            authenticationToken.setAdditionalProperties(additionalProperties);
        }
        authenticationToken.attachLoginSourceToAdditionalProperties(authenticationToken.getLoginSource());

        Collection<Long> privilegeIds = loginCredential.getLongArrayValues("privilegeIds");
        authenticationToken.setPrivilegeIds(privilegeIds);

        return authenticationToken;
    }
}
