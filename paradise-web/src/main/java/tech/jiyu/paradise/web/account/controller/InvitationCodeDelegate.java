package tech.jiyu.paradise.web.account.controller;

import com.google.common.collect.Lists;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import tech.jiyu.paradise.core.account.domain.InvitationCode;
import tech.jiyu.paradise.core.account.domain.InvitationCodeListQuery;
import tech.jiyu.paradise.core.account.service.InvitationCodeService;
import tech.jiyu.paradise.web.account.vo.InvitationCodeVO;
import tech.jiyu.utility.pagination.PaginationResult;

/**
 * Created By InvitationCodeDelegate
 *
 * Date 2018/2/9 16:28
 *
 * @author SimonChan;emailAddress: simonchan@jiyu.tech
 */
@Component
public class InvitationCodeDelegate {

    @Autowired
    private InvitationCodeService invitationCodeService;

    public InvitationCodeVO create() {
        InvitationCode invitationCode = invitationCodeService.generateInvitationCode();
        return assemble(invitationCode);
    }

    private InvitationCodeVO assemble(InvitationCode invitationCode) {
        if (invitationCode == null) {
            return null;
        }
        List<InvitationCodeVO> invitationCodeVOs = assembleList(Collections.singletonList(invitationCode));
        return CollectionUtils.isNotEmpty(invitationCodeVOs) ? invitationCodeVOs.get(0) : null;
    }

    private List<InvitationCodeVO> assembleList(List<InvitationCode> invitationCodes) {
        if (CollectionUtils.isEmpty(invitationCodes)) {
            return Lists.newArrayListWithCapacity(0);
        }

        return invitationCodes.stream()
                .filter(Objects::nonNull)
                .map(InvitationCodeVO::new).collect(Collectors.toList());
    }

    @SuppressWarnings("unchecked")
    public PaginationResult<InvitationCodeVO, Void> list(InvitationCodeListQuery invitationCodeListQuery) {
        PaginationResult<InvitationCode, Void> result = invitationCodeService.list(invitationCodeListQuery);
        return new PaginationResult.Builder<InvitationCodeVO, Void>()
                .setCurrentPage(result.getCurrentPage())
                .setPageSize(result.getPageSize())
                .setStatistics(result.getStatistics())
                .setTotalRecords(result.getTotalRecords())
                .setTotalPages(result.getTotalPages())
                .setResultSet(result.getResultSet().stream().filter(Objects::nonNull).map(this::assemble).collect(Collectors.toList())).build();
    }
}
