package tech.jiyu.paradise.web.oauth2.controller;

import com.google.common.base.Throwables;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.Base64;
import java.util.List;
import java.util.Optional;

import tech.jiyu.paradise.core.account.service.AccountService;
import tech.jiyu.paradise.core.authentication.domain.OAuth2KeyAuthentication;
import tech.jiyu.paradise.core.authentication.domain.OAuth2Parameters;
import tech.jiyu.paradise.core.authentication.domain.OAuth2Type;
import tech.jiyu.paradise.core.authentication.exception.NoSuchOAuth2ServiceException;
import tech.jiyu.paradise.core.authentication.exception.UnableBuildKeyAuthenticationException;
import tech.jiyu.paradise.core.authentication.service.OAuth2Service;
import tech.jiyu.paradise.web.oauth2.vo.OAuth2KeyVO;

/**
 * CreatedDate 2018-07-09 下午5:29
 *
 * @author SimonChan;emailAddress: simonchan@jinjie.tech
 */
@Component
public class OAuth2Delegate {

    @Autowired
    private List<OAuth2Service> oauth2Services;

    public OAuth2KeyVO createOAuth2Key(OAuth2Parameters oauth2Parameters) {
        Optional<OAuth2Type> typeOptional = Optional.ofNullable(OAuth2Type.fromCode(oauth2Parameters.getAuthType()));
        OAuth2Type oauth2Type = typeOptional.orElseThrow(NoSuchOAuth2ServiceException::new);
        OAuth2Service oauth2Service = oauth2Services.stream().filter(service -> service.supports(oauth2Type)).findFirst().orElse(null);

        if (oauth2Service == null) {
            throw new NoSuchOAuth2ServiceException(String.valueOf(oauth2Type));
        }

        if (oauth2Parameters.getCompanyId() == null) {
            oauth2Parameters.setCompanyId(AccountService.DEFAULT_COMPANY_ID);
        }

        try {
            Optional<OAuth2KeyAuthentication> keyAuthenticationOptional = Optional.ofNullable(oauth2Service.createOAuth2KeyAuthentication(oauth2Parameters));
            OAuth2KeyAuthentication keyAuthentication = keyAuthenticationOptional.orElseThrow(UnableBuildKeyAuthenticationException::new);
            return buildOAuth2KeyVo(keyAuthentication);
        } catch (IOException e) {
            throw Throwables.propagate(e);
        }


    }

    private OAuth2KeyVO buildOAuth2KeyVo(OAuth2KeyAuthentication keyAuthentication) {
        Boolean needPhoneNumber = keyAuthentication.getNeedPhoneNumber();
        OAuth2KeyVO oauth2KeyVO = new OAuth2KeyVO();
        // 需要手机号表示没注册过系统.
        oauth2KeyVO.setRegistered(!needPhoneNumber);
        oauth2KeyVO.setAuthKey(Base64.getEncoder().encodeToString(keyAuthentication.getCacheKey().getBytes()));
        return oauth2KeyVO;
    }
}
