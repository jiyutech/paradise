package tech.jiyu.paradise.web.authentication;

import org.apache.commons.lang3.StringUtils;
import org.springframework.security.core.GrantedAuthority;

import java.io.UnsupportedEncodingException;
import java.util.Base64;
import java.util.Collection;

import tech.jiyu.paradise.core.account.service.AccountService;
import tech.jiyu.paradise.core.authentication.domain.OAuth2KeyParameters;


public class OAuth2KeyAuthenticationToken extends AbstractAccountAuthenticationToken {

    private String authKey;

    private String countryCode;

    private String phoneNumber;

    private String isoCode;

    private String authType;

    private String registrationSource;

    private String registrationTrafficType;

    private String loginSource;

    private String roleTypeCode;

    private Long companyId;

    private String captchaCode;

    private String password;
    private String email;

    // 以下这两个参数用于小程序获取union id, countryCode, phoneNumber.
    private String weChatTinyAppEncryptedData;
    private String weChatTinyAppIv;

    // 如果需要获取手机号码, 还需要提供小程序AppId,
    private String weChatTinyAppId;

    private Collection<Long> privilegeIds;

    public OAuth2KeyAuthenticationToken(Collection<? extends GrantedAuthority> authorities) {
        super(authorities);
    }

    @Override
    public Object getCredentials() {
        return authKey;
    }

    @Override
    public Object getPrincipal() {
        return authKey;
    }

    public String getAuthKey() {
        return authKey;
    }

    public void setAuthKey(String authKey) {
        this.authKey = authKey;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getIsoCode() {
        return isoCode;
    }

    public void setIsoCode(String isoCode) {
        this.isoCode = isoCode;
    }

    public String getAuthType() {
        return authType;
    }

    public void setAuthType(String authType) {
        this.authType = authType;
    }

    public String getLoginSource() {
        return loginSource;
    }

    public void setLoginSource(String loginSource) {
        this.loginSource = loginSource;
    }

    public Long getCompanyId() {
        return companyId;
    }

    public void setCompanyId(Long companyId) {
        this.companyId = companyId;
    }

    public String getRoleTypeCode() {
        return roleTypeCode;
    }

    public void setRoleTypeCode(String roleTypeCode) {
        this.roleTypeCode = roleTypeCode;
    }

    public String getCaptchaCode() {
        return captchaCode;
    }

    public void setCaptchaCode(String captchaCode) {
        this.captchaCode = captchaCode;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getWeChatTinyAppEncryptedData() {
        return weChatTinyAppEncryptedData;
    }

    public void setWeChatTinyAppEncryptedData(String weChatTinyAppEncryptedData) {
        this.weChatTinyAppEncryptedData = weChatTinyAppEncryptedData;
    }

    public String getWeChatTinyAppIv() {
        return weChatTinyAppIv;
    }

    public void setWeChatTinyAppIv(String weChatTinyAppIv) {
        this.weChatTinyAppIv = weChatTinyAppIv;
    }

    public String getWeChatTinyAppId() {
        return weChatTinyAppId;
    }

    public void setWeChatTinyAppId(String weChatTinyAppId) {
        this.weChatTinyAppId = weChatTinyAppId;
    }

    public Collection<Long> getPrivilegeIds() {
        return privilegeIds;
    }

    public void setPrivilegeIds(Collection<Long> privilegeIds) {
        this.privilegeIds = privilegeIds;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getRegistrationSource() {
        return registrationSource;
    }

    public void setRegistrationSource(String registrationSource) {
        this.registrationSource = registrationSource;
    }

    public String getRegistrationTrafficType() {
        return registrationTrafficType;
    }

    public void setRegistrationTrafficType(String registrationTrafficType) {
        this.registrationTrafficType = registrationTrafficType;
    }

    public OAuth2KeyParameters buildOAuth2KeyParameters() {
        OAuth2KeyParameters oAuth2KeyParameters = new OAuth2KeyParameters();
        oAuth2KeyParameters.setAuthType(getAuthType());
        oAuth2KeyParameters.setRegistrationSource(getRegistrationSource());
        oAuth2KeyParameters.setRegistrationTrafficType(getRegistrationTrafficType());
        oAuth2KeyParameters.setLoginSource(getLoginSource());
        try {
            oAuth2KeyParameters.setAuthKey(new String(Base64.getDecoder().decode(getAuthKey()), "UTF-8"));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        oAuth2KeyParameters.setRoleTypeCode(getRoleTypeCode());
        oAuth2KeyParameters.setCountryCode(getCountryCode());
        oAuth2KeyParameters.setPhoneNumber(getPhoneNumber());
        oAuth2KeyParameters.setCompanyId(getCompanyId() == null ? AccountService.DEFAULT_COMPANY_ID : getCompanyId());
        oAuth2KeyParameters.setIsoCode(getIsoCode());
        oAuth2KeyParameters.setPassword(getPassword());
        oAuth2KeyParameters.setPrivilegeIds(getPrivilegeIds());
        oAuth2KeyParameters.setEmail(StringUtils.isEmpty(getEmail()) ? getEmail() : getEmail().trim());
        oAuth2KeyParameters.setFirstName(getFirstName());
        oAuth2KeyParameters.setLastName(getLastName());
        oAuth2KeyParameters.setAgreeToReceiveInfo(getAgreeToReceiveInfo());
        oAuth2KeyParameters.setLastLanguageCode(getLastLanguageCode());
        oAuth2KeyParameters.setAdditionalProperties(getAdditionalProperties());
        oAuth2KeyParameters.setWeChatTinyAppId(getWeChatTinyAppId());
        oAuth2KeyParameters.setWeChatTinyAppIv(getWeChatTinyAppIv());
        oAuth2KeyParameters.setWeChatTinyAppEncryptedData(getWeChatTinyAppEncryptedData());

        return oAuth2KeyParameters;
    }
}
