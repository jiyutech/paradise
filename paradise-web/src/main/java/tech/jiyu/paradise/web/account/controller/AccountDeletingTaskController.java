package tech.jiyu.paradise.web.account.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import tech.jiyu.paradise.core.account.annotation.ApiAccessValidation;
import tech.jiyu.paradise.core.account.enumtype.ApiAccessEnumType;
import tech.jiyu.paradise.core.account.service.AccountDeletingTaskService;

@RestController
public class AccountDeletingTaskController {

    @Autowired
    private AccountDeletingTaskService accountDeletingTaskService;

    @DeleteMapping(value = "/accounts/{accountId}/accountDeletingTasks", produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiAccessValidation(accessType = ApiAccessEnumType.DELETE_ACCOUNT_DELETING_TASK)
    public ResponseEntity<Object> register(@PathVariable("accountId") Long accountId) {
        accountDeletingTaskService.deleteTaskByAccountId(accountId);
        return ResponseEntity.noContent().build();
    }

}

