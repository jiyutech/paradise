package tech.jiyu.paradise.web.authentication;

import org.springframework.security.authentication.AbstractAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;

import java.util.Collection;
import java.util.Map;

import tech.jiyu.captcha.domain.CaptchaCode;
import tech.jiyu.paradise.core.account.domain.Invitation;

/**
 * @author <a href="mailto:chenshao@dekeapp.com">shawsmith</a>
 */
public class PhoneNumberAuthenticationToken extends AbstractAuthenticationToken {

    private CaptchaCode captchaCode;
    private String roleTypeCode;
    private String countryCode;
    private String isoCode;
    private String email;

    // 微信相关信息.
    private String weChatTinyAppJsCode;
    private String weChatOpenId;

    // 邀请信息.
    private Invitation invitation;
    private String loginSource;
    private String registrationSource;
    private String registrationTrafficType;
    private String firstName;
    private String lastName;
    private Boolean agreeToReceiveInfo;
    private String lastLanguageCode;
    private Collection<Long> privilegeIds;
    private Map<String, Object> additionalProperties;

    public PhoneNumberAuthenticationToken(Collection<? extends GrantedAuthority> authorities) {
        super(authorities);
    }

    public CaptchaCode getCaptchaCode() {
        return captchaCode;
    }

    public void setCaptchaCode(CaptchaCode captchaCode) {
        this.captchaCode = captchaCode;
    }

    public String getRoleTypeCode() {
        return roleTypeCode;
    }

    public void setRoleTypeCode(String roleTypeCode) {
        this.roleTypeCode = roleTypeCode;
    }

    public String getIsoCode() {
        return isoCode;
    }

    public void setIsoCode(String isoCode) {
        this.isoCode = isoCode;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getWeChatTinyAppJsCode() {
        return weChatTinyAppJsCode;
    }

    public void setWeChatTinyAppJsCode(String weChatTinyAppJsCode) {
        this.weChatTinyAppJsCode = weChatTinyAppJsCode;
    }

    public String getWeChatOpenId() {
        return weChatOpenId;
    }

    public void setWeChatOpenId(String weChatOpenId) {
        this.weChatOpenId = weChatOpenId;
    }

    public Invitation getInvitation() {
        return invitation;
    }

    public void setInvitation(Invitation invitation) {
        this.invitation = invitation;
    }

    public String getLoginSource() {
        return loginSource;
    }

    public void setLoginSource(String loginSource) {
        this.loginSource = loginSource;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Boolean getAgreeToReceiveInfo() {
        return agreeToReceiveInfo;
    }

    public void setAgreeToReceiveInfo(Boolean agreeToReceiveInfo) {
        this.agreeToReceiveInfo = agreeToReceiveInfo;
    }

    public String getLastLanguageCode() {
        return lastLanguageCode;
    }

    public void setLastLanguageCode(String lastLanguageCode) {
        this.lastLanguageCode = lastLanguageCode;
    }

    public Collection<Long> getPrivilegeIds() {
        return privilegeIds;
    }

    public void setPrivilegeIds(Collection<Long> privilegeIds) {
        this.privilegeIds = privilegeIds;
    }

    public Map<String, Object> getAdditionalProperties() {
        return additionalProperties;
    }

    public void setAdditionalProperties(Map<String, Object> additionalProperties) {
        this.additionalProperties = additionalProperties;
    }

    public String getRegistrationSource() {
        return registrationSource;
    }

    public void setRegistrationSource(String registrationSource) {
        this.registrationSource = registrationSource;
    }

    public String getRegistrationTrafficType() {
        return registrationTrafficType;
    }

    public void setRegistrationTrafficType(String registrationTrafficType) {
        this.registrationTrafficType = registrationTrafficType;
    }

    @Override
    public Object getCredentials() {
        CaptchaCode captchaCode = getCaptchaCode();
        return (captchaCode != null) ? captchaCode.getCode() : null;

    }

    @Override
    public Object getPrincipal() {
        CaptchaCode captchaCode = getCaptchaCode();
        return (captchaCode != null && captchaCode.getKey() != null) ? captchaCode.getKey().getPhoneNumber() : null;
    }
}
