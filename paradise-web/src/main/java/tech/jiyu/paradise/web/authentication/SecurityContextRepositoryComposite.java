package tech.jiyu.paradise.web.authentication;

import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.context.HttpRequestResponseHolder;
import org.springframework.security.web.context.SecurityContextRepository;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import lombok.AllArgsConstructor;

/**
 * CreatedDate 2018-12-27 下午3:30
 *
 * @author SimonChan;emailAddress: simonchan@jinjie.tech
 */
@AllArgsConstructor
public class SecurityContextRepositoryComposite implements SecurityContextRepository {

    private final List<SecurityContextRepository> securityContextRepositories;

    @Override
    public SecurityContext loadContext(HttpRequestResponseHolder requestResponseHolder) {
        for (SecurityContextRepository securityContextRepository : securityContextRepositories) {
            SecurityContext securityContext = securityContextRepository.loadContext(requestResponseHolder);
            if (isNotEmpty(securityContext)) {
                return securityContext;
            }
        }
        return SecurityContextHolder.createEmptyContext();
    }

    private boolean isNotEmpty(SecurityContext securityContext) {
        return securityContext != null && securityContext.getAuthentication() != null && securityContext.getAuthentication().isAuthenticated();
    }

    @Override
    public void saveContext(SecurityContext context, HttpServletRequest request, HttpServletResponse response) {

    }

    @Override
    public boolean containsContext(HttpServletRequest request) {
        return false;
    }
}
