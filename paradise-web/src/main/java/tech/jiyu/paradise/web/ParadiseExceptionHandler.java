package tech.jiyu.paradise.web;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.base.Throwables;
import com.google.common.collect.Lists;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.authentication.*;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerExceptionResolver;
import org.springframework.web.servlet.ModelAndView;
import tech.jiyu.paradise.core.account.exception.DuplicatedPhoneNumberException;
import tech.jiyu.paradise.core.exception.ThrowableDirectlyException;
import tech.jiyu.penguin.wxapi.wechat.common.domain.ErrorCodeResponse;
import tech.jiyu.penguin.wxapi.wechat.common.exception.ErrorCodeResponseException;
import tech.jiyu.utility.api.errors.*;
import tech.jiyu.utility.exception.ExceptionNotificationService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.PrintWriter;
import java.util.Collection;
import java.util.Collections;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Component
public class ParadiseExceptionHandler implements HandlerExceptionResolver {

    private static final Logger LOGGER = LoggerFactory.getLogger(ParadiseExceptionHandler.class);

    private static final Collection<Class<? extends Throwable>> INTERNAL_AUTHENTICATION_SERVICE_CAUSE_CLASSES = Collections.unmodifiableList(
            Lists.newArrayList(
                    AccountExpiredException.class,
                    DisabledException.class,
                    AuthenticationCredentialsNotFoundException.class,
                    AccessDeniedException.class
            )
    );

    @Autowired
    private ApiErrorRepository apiErrorRepository;

    @Autowired(required = false)
    private MessageInterpreter messageInterpreter;

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private ExceptionNotificationService exceptionNotificationService;

    @Override
    public ModelAndView resolveException(HttpServletRequest request, HttpServletResponse response,
                                         Object handler, Exception ex) {
        ApiError fallbackError = apiErrorRepository.get("system", "internal.error", messageInterpreter);
        RestfulApiError error = buildAndNotify(ex, fallbackError, HttpStatus.INTERNAL_SERVER_ERROR);
        sendErrorResponse(error, response);
        return new ModelAndView();
    }

    private void sendErrorResponse(RestfulApiError apiError, HttpServletResponse response) {
        try {
            response.setStatus(apiError.getResponseStatus().value());
            response.setContentType(MediaType.APPLICATION_JSON_UTF8_VALUE);
            String responseContent = objectMapper.writeValueAsString(apiError);
            PrintWriter writer = response.getWriter();
            writer.write(responseContent);
            writer.flush();
        } catch (Throwable t) {
            LOGGER.error(Throwables.getStackTraceAsString(t), t);
        }
    }

    public RestfulApiError buildAndNotify(Throwable t, ApiError fallbackError, HttpStatus fallbackStatus) {
        RestfulApiError apiError = null;
        HttpStatus status = HttpStatus.INTERNAL_SERVER_ERROR;
        ApiError error = null;

        Throwable actualCause = unwrapInternalAuthenticationServiceException(t);
        if (actualCause instanceof AccountExpiredException) {
            error = apiErrorRepository.get("login", "account.expired", messageInterpreter);
        } else if (actualCause instanceof DisabledException) {
            error = apiErrorRepository.get("login", "account.disabled", messageInterpreter);
        } else if (actualCause instanceof BadCredentialsException) {
            error = apiErrorRepository.get("login", "invalid.credential", messageInterpreter);
        } else if (actualCause instanceof AuthenticationCredentialsNotFoundException) {
            error = apiErrorRepository.get("system", "credential.not.found", messageInterpreter);
            status = HttpStatus.UNAUTHORIZED;
        } else if (actualCause instanceof AccessDeniedException) {
            error = apiErrorRepository.get("system", "access.denied", messageInterpreter);
            status = HttpStatus.FORBIDDEN;
        } else if (actualCause instanceof ThrowableDirectlyException) {
            error = new ApiError();
            error.setMessage(actualCause.getMessage());
            error.setCode(((ThrowableDirectlyException) actualCause).getCode());
        } else if (actualCause.getCause() != null && actualCause.getCause().getClass().getName().contains("MysqlDataTruncation") && apiErrorRepository.get("system", "data.too.long", messageInterpreter) != null) {
            // 查两次主要是为了保持原有的 if-else结构不变，当然也可以把这个 else-if 搬到最终的else里去做判断，但是又破坏了之前的结构了，加上这是个不常见的错误，所以就干脆查两次了
            error = apiErrorRepository.get("system", "data.too.long", messageInterpreter);
        } else {
            apiError = RestfulApiErrorUtils.buildRestfulApiErrorFromAnnotation(actualCause, apiErrorRepository, messageInterpreter);
            if (t instanceof ErrorCodeResponseException) {
                rebuildErrorCodeResponseMessage(apiError, ((ErrorCodeResponseException) t).getResponse());
            } else if (actualCause instanceof DuplicatedPhoneNumberException) {
                rebuildDuplicatedPhoneNumberMessage(apiError, ((DuplicatedPhoneNumberException) actualCause));
            }
        }

        if (apiError == null && error != null) {
            apiError = new RestfulApiError(error, status);
        }

        if (isUnexpectedError(apiError)) {
            exceptionNotificationService.notify(actualCause);
        }

        if (apiError == null && fallbackError != null) {
            apiError = new RestfulApiError(fallbackError, fallbackStatus);
        }
        return apiError;
    }

    private Throwable unwrapInternalAuthenticationServiceException(Throwable t) {
        if (!(t instanceof InternalAuthenticationServiceException)) {
            return t;
        }

        Throwable cause = t.getCause();
        while (cause != null) {
            for (Class<?> clazz : INTERNAL_AUTHENTICATION_SERVICE_CAUSE_CLASSES) {
                if (clazz == cause.getClass()) {
                    return cause;
                }
            }
            cause = cause.getCause();
        }
        return t;
    }

    private boolean isUnexpectedError(RestfulApiError error) {
        return error == null || StringUtils.equals("000001", error.getErrorCode());
    }

    private void rebuildErrorCodeResponseMessage(RestfulApiError apiError, ErrorCodeResponse response) {
        if (apiError == null || response == null) {
            return;
        }

        String errorMessage = apiError.getErrorMessage();
        if (StringUtils.isNotBlank(errorMessage)) {
            String errorCodeMessage = String.format(errorMessage, response.getErrorCode(), response.getErrorMessage());
            apiError.setErrorMessage(errorCodeMessage);
        }
    }

    private void rebuildDuplicatedPhoneNumberMessage(RestfulApiError apiError, DuplicatedPhoneNumberException e) {
        if (apiError == null || e == null) {
            return;
        }
        Pattern pattern = Pattern.compile("%s(\\d+)");
        String errorMessage = apiError.getErrorMessage();
        Matcher matcher = pattern.matcher(errorMessage);
        int hitCount = 0;
        while (matcher.find()) {
            hitCount++;
        }
        if (StringUtils.isNotBlank(errorMessage) && hitCount > 0) {
            String countryCode = e.getCountryCode();
            String phoneNumber = e.getPhoneNumber();
            String argument;
            if (StringUtils.isNotBlank(countryCode) && StringUtils.isNotBlank(phoneNumber)) {
                argument = String.format("+%s %s", countryCode, phoneNumber);
            } else if (StringUtils.isNotBlank(phoneNumber)) {
                argument = String.format("%s", phoneNumber);
            } else {
                argument = "";
            }
            Object[] args = new String[hitCount];
            for (int i = 0; i < hitCount; i++) {
                args[i] = argument;
            }
            errorMessage = matcher.replaceAll("%$1\\$s");
            errorMessage = String.format(errorMessage, args);
            apiError.setErrorMessage(errorMessage);
        }
    }
}
