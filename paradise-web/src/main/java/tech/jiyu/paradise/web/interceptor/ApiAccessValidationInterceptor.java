package tech.jiyu.paradise.web.interceptor;

import com.alibaba.fastjson.JSONObject;
import com.google.common.collect.Sets;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.BooleanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.core.Authentication;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.HandlerInterceptor;
import tech.jiyu.paradise.core.account.annotation.ApiAccessValidation;
import tech.jiyu.paradise.core.account.domain.ApiAccessValidationInfo;
import tech.jiyu.paradise.core.account.service.ApiAccessValidationService;
import tech.jiyu.paradise.core.login.domain.LoginInfo;
import tech.jiyu.paradise.core.util.SecurityUtils;
import tech.jiyu.paradise.web.authentication.AuthenticationUtils;
import tech.jiyu.paradise.web.authentication.LoginInfoAuthenticationToken;
import tech.jiyu.utility.api.errors.RestfulApiError;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

@Slf4j
public class ApiAccessValidationInterceptor implements HandlerInterceptor {

    @Autowired
    private List<ApiAccessValidationService> apiAccessValidationServiceList;

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        if (handler instanceof HandlerMethod) {
            ApiAccessValidation accessValidation = ((HandlerMethod) handler).getMethodAnnotation(ApiAccessValidation.class);

            if (accessValidation == null) {
                return true;
            }
            LoginInfo loginInfo = findLoginInfo();

            // 组装校验过程里所需要的参数
            ApiAccessValidationInfo validationInfo = new ApiAccessValidationInfo(accessValidation.accessType(), loginInfo, request);
            // 校验接口
            Boolean checkResult = validate(validationInfo);

            if (BooleanUtils.isTrue(checkResult)) {
                return true;
            }

            try {
                RestfulApiError apiError = new RestfulApiError();
                if (validationInfo.isValidParams()) {
                    response.setStatus(HttpStatus.FORBIDDEN.value());
                    apiError.setErrorMessage("access denied!");
                } else {
                    response.setStatus(HttpStatus.BAD_REQUEST.value());
                    apiError.setErrorMessage("Parameter error");
                }

                response.setContentType(MediaType.APPLICATION_JSON_VALUE);
                PrintWriter printWriter = response.getWriter();
                printWriter.write(JSONObject.toJSONString(apiError));
                printWriter.flush();
                printWriter.close();
            } catch (IOException e) {
                log.error("response write stream error.", e);
                throw e;
            }
            return false;
        }
        return true;
    }

    private LoginInfo findLoginInfo() {
        Authentication authentication = AuthenticationUtils.currentAuthentication();
        if (!(authentication instanceof LoginInfoAuthenticationToken)) {
            return null;
        }
        LoginInfoAuthenticationToken authenticationToken = (LoginInfoAuthenticationToken) authentication;
        return authenticationToken.getLoginInfo();
    }

    private Boolean validate(ApiAccessValidationInfo validationInfo) {
        boolean isAdmin = isAdmin(validationInfo);
        if (isAdmin) {
            return true;
        }

        // service 注入的时候，paradise 的实现默认 Ordered.LOWEST_PRECEDENCE, 所以业务正常注入的话，即可优先用业务的校验结果
        for (ApiAccessValidationService service : apiAccessValidationServiceList) {
            Boolean result = service.validate(validationInfo);
            if (result != null) {
                return result;
            }
        }
        return false;
    }

    private boolean isAdmin(ApiAccessValidationInfo validationInfo) {
        if (validationInfo.getLoginInfo() == null) {
            return false;
        }
        String currentRoleTypeCode = validationInfo.getLoginInfo().getRoleTypeCode();
        return Sets.newHashSet(SecurityUtils.SYSTEM, SecurityUtils.IT).contains(currentRoleTypeCode);
    }
}
