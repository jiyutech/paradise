package tech.jiyu.paradise.web.authentication;

import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;

import tech.jiyu.paradise.conf.WebSecurityConfig;
import tech.jiyu.utility.security.HttpSecurityConfigurer;

/**
 * @author <a href="mailto:chenshao@dekeapp.com">shawsmith</a>
 */
public class ParadiseAccountSecurityConfigurer implements HttpSecurityConfigurer {

    @Override
    public void configure(HttpSecurity security) throws Exception {
        security.authorizeRequests()
                .antMatchers(HttpMethod.POST, WebSecurityConfig.LOGIN_PATH).permitAll()

                // 获取账户列表信息.
                .antMatchers(HttpMethod.GET, "/accounts").authenticated()

                // 获取账户信息.
                .antMatchers(HttpMethod.GET, "/accounts/{accountId:(\\d+)}").authenticated()

                // 修改账户信息.
                .antMatchers(HttpMethod.PATCH, "/accounts/{accountId:(\\d+)}").authenticated()

                // 修改账户信息.
                .antMatchers(HttpMethod.DELETE, "/accounts/{accountId:(\\d+)}").authenticated()

                // 修改手机号码.
                .antMatchers(HttpMethod.PATCH, "/accounts/{accountId:(\\d+)}/phoneNumber").authenticated()

                // 修改账户密码.
                .antMatchers(HttpMethod.PATCH, "/accounts/{accountId:(\\d+)}/password").authenticated()

                // 历史手机号码.
                .antMatchers(HttpMethod.GET, "/accounts/{accountId:(\\d+)}/historyPhoneNumbers").authenticated()

                // 获取当前账号的信息
                .antMatchers(HttpMethod.GET, "/accounts/current").authenticated()

                // 修改实名认证信息.
                .antMatchers(HttpMethod.PATCH, "/accounts/{accountId:(\\d+)}/verification").authenticated()

                // 删除账号删除任务
                .antMatchers(HttpMethod.DELETE, "/accounts/{accountId:(\\d+)}/accountDeletingTask").authenticated()

                // 创建权限.
                .antMatchers(HttpMethod.POST, "/privileges").authenticated()

                // 获取权限列表
                .antMatchers(HttpMethod.GET, "/privileges").authenticated()

                // 获取权限详情
                .antMatchers(HttpMethod.GET, "/privileges/{id:(\\d+)}").authenticated()

                // 修改权限
                .antMatchers(HttpMethod.PATCH, "/privileges/{id:(\\d+)}").authenticated()

                // 删除权限
                .antMatchers(HttpMethod.DELETE, "/privileges/{id:(\\d+)}").authenticated();
    }
}
