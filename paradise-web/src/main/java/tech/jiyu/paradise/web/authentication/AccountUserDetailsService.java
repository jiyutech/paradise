package tech.jiyu.paradise.web.authentication;

import org.springframework.security.authentication.DisabledException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import tech.jiyu.paradise.core.account.domain.Account;
import tech.jiyu.paradise.core.account.service.AccountService;
import tech.jiyu.paradise.core.util.AccountUtils;
import tech.jiyu.utility.datastructure.Pair;
import tech.jiyu.utility.phonenumber.PhoneNumberUtils;

/**
 * @author <a href="mailto:chenshao@dekeapp.com">shawsmith</a>
 */
public class AccountUserDetailsService implements UserDetailsService {

    private AccountService accountService;

    public AccountUserDetailsService(AccountService accountService) {
        this.accountService = accountService;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Long companyId = CompanyIdHolder.get();

        Account account = accountService.findByUserName(companyId, username);
        if (account == null) {
            account = accountService.findByEmail(companyId, username);
        }

        if (account == null && username.startsWith(AccountUtils.PHONE_NUMBER_USERNAME_PREFIX)) {
            Pair<String, String> pair = AccountUtils.parseCountryCodeAndPhoneNumber(username);
            if (pair != null) {
                String countryCode = pair.getLeft();
                String phoneNumber = pair.getRight();
                account = accountService.findByPhoneNumber(companyId, countryCode, phoneNumber);
            }
        }

        if (account == null) {
            account = accountService.findByPhoneNumber(companyId, PhoneNumberUtils.filterNonDigitCharacters(username));
        }

        if (account == null) {
            throw new NoSuchAccountOnSignInException(username);
        }

        if (!account.isActuallyEnabled()) {
            throw new DisabledException(username);
        }
        return new AccountUserDetails(account);
    }

    public static final class CompanyIdHolder {

        private static final ThreadLocal<Long> companyIdHolder = new ThreadLocal<>();

        public static void set(Long companyId) {
            companyIdHolder.set(companyId);
        }

        public static Long get() {
            return companyIdHolder.get();
        }

        public static void remove() {
            companyIdHolder.remove();
        }

    }
}
