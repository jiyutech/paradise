package tech.jiyu.paradise.web.authentication;

import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.security.web.util.matcher.RequestMatcher;

import javax.servlet.Filter;

import tech.jiyu.utility.security.AuthenticationFilterConfigurer;

/**
 * @author <a href="mailto:chenshao@jiyu.tech">shawsmith</a>
 */
public class UsernameEmailPhoneNumberPasswordAuthenticationFilterConfigurer implements AuthenticationFilterConfigurer {

    public static final Long DEFAULT_ORDER = 200L;

    @Override
    public Long getOrder() {
        return DEFAULT_ORDER;
    }

    @Override
    public Class<? extends Filter> getTargetFilterClass() {
        return UsernameEmailPhoneNumberPasswordAuthenticationFilter.class;
    }

    @Override
    public void configure(HttpSecurity http, RequestMatcher loginRequestMatcher, AuthenticationSuccessHandler ash, AuthenticationFailureHandler afh, AuthenticationManager authenticationManager, Class<? extends Filter> afterFilter) {
        UsernameEmailPhoneNumberPasswordAuthenticationFilter usernamePasswordAuthenticationFilter = new UsernameEmailPhoneNumberPasswordAuthenticationFilter(loginRequestMatcher, LoginCredentialPopulationFilterConfigurer.LOGIN_CREDENTIAL_ATTRIBUTE_KEY);
        usernamePasswordAuthenticationFilter.setAuthenticationSuccessHandler(ash);
        usernamePasswordAuthenticationFilter.setAuthenticationFailureHandler(afh);
        usernamePasswordAuthenticationFilter.setAuthenticationManager(authenticationManager);
        http.addFilterAfter(usernamePasswordAuthenticationFilter, afterFilter);
    }
}
