package tech.jiyu.paradise.web.authentication;

import org.apache.commons.lang3.StringUtils;
import org.springframework.security.core.GrantedAuthority;
import tech.jiyu.paradise.core.authentication.domain.OAuth2Parameters;

import java.util.Collection;

/**
 * @author <a href="mailto:chenshao@dekeapp.com">shawsmith</a>
 */
public class OAuth2AuthenticationToken extends AbstractAccountAuthenticationToken {

    private Long companyId;
    private String authCode;
    private String authType;
    private String roleTypeCode;
    private String loginSource;
    private String nickName;
    private String avatar;
    private String email;

    // 以下这两个参数用于小程序获取union id, countryCode, phoneNumber.
    private String weChatTinyAppEncryptedData;
    private String weChatTinyAppIv;

    // 如果需要获取手机号码, 还需要提供小程序AppId,
    private String weChatTinyAppId;

    private Collection<Long> privilegeIds;

    public OAuth2AuthenticationToken(Collection<? extends GrantedAuthority> authorities) {
        super(authorities);
    }

    public Long getCompanyId() {
        return companyId;
    }

    public void setCompanyId(Long companyId) {
        this.companyId = companyId;
    }

    public String getAuthCode() {
        return authCode;
    }

    public void setAuthCode(String authCode) {
        this.authCode = authCode;
    }

    public String getAuthType() {
        return authType;
    }

    public void setAuthType(String authType) {
        this.authType = authType;
    }

    public String getRoleTypeCode() {
        return roleTypeCode;
    }

    public void setRoleTypeCode(String roleTypeCode) {
        this.roleTypeCode = roleTypeCode;
    }

    public String getLoginSource() {
        return loginSource;
    }

    public void setLoginSource(String loginSource) {
        this.loginSource = loginSource;
    }


    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getWeChatTinyAppEncryptedData() {
        return weChatTinyAppEncryptedData;
    }

    public void setWeChatTinyAppEncryptedData(String weChatTinyAppEncryptedData) {
        this.weChatTinyAppEncryptedData = weChatTinyAppEncryptedData;
    }

    public String getWeChatTinyAppIv() {
        return weChatTinyAppIv;
    }

    public void setWeChatTinyAppIv(String weChatTinyAppIv) {
        this.weChatTinyAppIv = weChatTinyAppIv;
    }

    public String getWeChatTinyAppId() {
        return weChatTinyAppId;
    }

    public void setWeChatTinyAppId(String weChatTinyAppId) {
        this.weChatTinyAppId = weChatTinyAppId;
    }

    public Collection<Long> getPrivilegeIds() {
        return privilegeIds;
    }

    public void setPrivilegeIds(Collection<Long> privilegeIds) {
        this.privilegeIds = privilegeIds;
    }

    @Override
    public Object getCredentials() {
        return authCode;
    }

    @Override
    public Object getPrincipal() {
        return authCode;
    }

    public OAuth2Parameters buildOAuth2Parameters() {
        OAuth2Parameters parameters = new OAuth2Parameters();

        parameters.setCompanyId(getCompanyId());
        parameters.setAuthCode(getAuthCode());
        parameters.setAuthType(getAuthType());
        parameters.setRoleTypeCode(getRoleTypeCode());
        parameters.setLoginSource(getLoginSource());
        parameters.setName(getNickName());
        parameters.setAvatar(getAvatar());
        parameters.setWeChatTinyAppEncryptedData(getWeChatTinyAppEncryptedData());
        parameters.setWeChatTinyAppIv(getWeChatTinyAppIv());
        parameters.setWeChatTinyAppId(getWeChatTinyAppId());
        parameters.setPrivilegeIds(getPrivilegeIds());
        parameters.setEmail(StringUtils.isEmpty(getEmail()) ? getEmail() : getEmail().trim());
        parameters.setFirstName(getFirstName());
        parameters.setLastName(getLastName());
        parameters.setAgreeToReceiveInfo(getAgreeToReceiveInfo());
        parameters.setLastLanguageCode(getLastLanguageCode());
        parameters.setAdditionalProperties(getAdditionalProperties());

        return parameters;
    }
}
