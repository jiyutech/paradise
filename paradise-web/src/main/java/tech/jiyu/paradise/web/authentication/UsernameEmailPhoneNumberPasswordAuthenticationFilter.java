package tech.jiyu.paradise.web.authentication;

import org.apache.commons.lang3.StringUtils;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.AbstractAuthenticationProcessingFilter;
import org.springframework.security.web.util.matcher.RequestMatcher;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import tech.jiyu.paradise.core.account.service.AccountService;
import tech.jiyu.paradise.core.login.service.LoginService;
import tech.jiyu.paradise.core.util.AccountUtils;
import tech.jiyu.utility.phonenumber.PhoneNumberUtils;

/**
 * @author <a href="mailto:chenshao@dekeapp.com">shawsmith</a>
 */
public class UsernameEmailPhoneNumberPasswordAuthenticationFilter extends AbstractAuthenticationProcessingFilter {

    private String loginCredentialsKey;

    public UsernameEmailPhoneNumberPasswordAuthenticationFilter(RequestMatcher authenticationRequestMatcher, String loginCredentialsKey) {
        super(authenticationRequestMatcher);
        this.loginCredentialsKey = loginCredentialsKey;
    }

    @Override
    @SuppressWarnings("unchecked")
    protected boolean requiresAuthentication(HttpServletRequest request, HttpServletResponse response) {
        if (!super.requiresAuthentication(request, response)) {
            return false;
        }

        LoginCredential loginCredential = (LoginCredential) request.getAttribute(loginCredentialsKey);
        return (loginCredential != null) && loginCredential.getStringParameter("username", null) != null;

    }

    @Override
    public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response) throws AuthenticationException, IOException, ServletException {
        Authentication authentication = extractAuthenticationFrom(request);
        return getAuthenticationManager().authenticate(authentication);
    }

    @SuppressWarnings("unchecked")
    private Authentication extractAuthenticationFrom(HttpServletRequest request) {
        LoginCredential loginCredential = (LoginCredential) request.getAttribute(loginCredentialsKey);
        String username = loginCredential.getStringParameter("username", null);
        String password = loginCredential.getStringParameter("password", null);
        String countryCode = loginCredential.getStringParameter("countryCode", null);
        String loginSource = loginCredential.getStringParameter("loginSource", LoginService.DEFAULT_LOGIN_SOURCE);
        Long companyId = loginCredential.getLongParameter("companyId", AccountService.DEFAULT_COMPANY_ID);
        String reCaptchaKey = loginCredential.getStringParameter("reCaptchaKey", null);
        // 如果传了countryCode则视为用手机号登录，国际化场景需要用完整的手机号去匹配用户
        username = buildCompletePhoneNumberIfNecessary(username, countryCode);
        return new ParadiseUsernamePasswordAuthenticationToken(username, password, loginSource, companyId, reCaptchaKey);
    }

    private String buildCompletePhoneNumberIfNecessary(String username, String countryCode) {
        if (StringUtils.isNotBlank(countryCode)) {
            countryCode = PhoneNumberUtils.filterNonDigitCharacters(countryCode);
            if (StringUtils.isNotBlank(countryCode)) {
                return AccountUtils.buildPhoneNumberUserName(countryCode, username);
            }
        }
        return username;
    }
}
