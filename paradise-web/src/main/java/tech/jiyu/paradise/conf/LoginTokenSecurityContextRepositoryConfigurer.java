package tech.jiyu.paradise.conf;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.web.context.SecurityContextRepository;

import java.util.List;

import tech.jiyu.paradise.core.login.service.LoginService;
import tech.jiyu.paradise.web.authentication.LoginTokenSecurityContextRepository;
import tech.jiyu.utility.security.SecurityContextRepositoryConfigurer;

/**
 * CreatedDate 2018-12-27 下午3:42
 *
 * @author SimonChan;emailAddress: simonchan@jinjie.tech
 */
@Configuration
public class LoginTokenSecurityContextRepositoryConfigurer implements SecurityContextRepositoryConfigurer {

    @Autowired
    private LoginService loginService;

    @Override
    public void configureSecurityContextRepositories(List<SecurityContextRepository> securityContextRepositories) {
        securityContextRepositories.add(new LoginTokenSecurityContextRepository(loginService));
    }
}
