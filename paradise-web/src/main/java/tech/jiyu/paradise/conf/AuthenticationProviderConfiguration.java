package tech.jiyu.paradise.conf;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.crypto.password.PasswordEncoder;
import tech.jiyu.captcha.service.CaptchaService;
import tech.jiyu.captcha.service.GoogleRecaptchaService;
import tech.jiyu.paradise.core.account.service.AccountLoginSourceService;
import tech.jiyu.paradise.core.account.service.AccountService;
import tech.jiyu.paradise.core.authentication.service.AuthenticationFailedRecordService;
import tech.jiyu.paradise.core.authentication.service.OAuth2Service;
import tech.jiyu.paradise.web.authentication.*;
import tech.jiyu.penguin.wechat.config.service.WeChatConfigService;
import tech.jiyu.penguin.wxapi.wechat.tinyapp.service.WeChatTinyAppService;

import java.util.List;

/**
 * @author <a href="mailto:chenshao@jiyu.tech">shawsmith</a>
 */
@Configuration
@ConditionalOnClass(AuthenticationProvider.class)
public class AuthenticationProviderConfiguration {


    @Bean
    @Autowired
    @ConditionalOnMissingBean(AccountUserDetailsService.class)
    public AccountUserDetailsService accountUserDetailsService(AccountService accountService) {
        return new AccountUserDetailsService(accountService);
    }

    @Bean
    @Autowired
    public AuthenticationProvider usernamePasswordAuthenticationProvider(AccountUserDetailsService userDetailsService,
                                                                         AccountLoginSourceService accountLoginSourceService,
                                                                         PasswordEncoder passwordEncoder,
                                                                         AuthenticationFailedRecordService authenticationFailedRecordService,
                                                                         PasswordAuthenticationSecurityPolicyProperties passwordAuthenticationSecurityPolicyProperties,
                                                                         @Autowired(required = false) GoogleRecaptchaService googleRecaptchaService) {
        ParadiseDaoAuthenticationProvider authenticationProvider = new ParadiseDaoAuthenticationProvider(accountLoginSourceService, userDetailsService, authenticationFailedRecordService, passwordAuthenticationSecurityPolicyProperties, googleRecaptchaService);
        authenticationProvider.setUserDetailsService(userDetailsService);
        authenticationProvider.setPasswordEncoder(passwordEncoder);

        return authenticationProvider;
    }

    @Bean
    @Autowired
    public AuthenticationProvider phoneNumberAuthenticationProvider(@Value("${paradise.need.validate.captcha.code:true}") Boolean needValidateCaptchaCode,
                                                                    @Value("${paradise.register.new.phone.number:true}") Boolean registerNewPhoneNumber,
                                                                    @Value("${paradise.default.wechat.config.id:-1}") Long defaultWeChatConfigId,
                                                                    CaptchaService captchaService,
                                                                    AccountService accountService,
                                                                    WeChatTinyAppService weChatTinyAppService,
                                                                    WeChatConfigService weChatConfigService,
                                                                    AccountLoginSourceService accountLoginSourceService) {
        return new PhoneNumberAuthenticationProvider(needValidateCaptchaCode,
                registerNewPhoneNumber,
                defaultWeChatConfigId,
                captchaService,
                accountService,
                weChatTinyAppService,
                weChatConfigService,
                accountLoginSourceService);
    }

    @Bean
    @Autowired
    public AuthenticationProvider oAuth2AuthenticationProvider(List<OAuth2Service> oAuth2Services,
                                                               AccountLoginSourceService accountLoginSourceService) {
        return new OAuth2AuthenticationProvider(oAuth2Services, accountLoginSourceService);
    }

    @Bean
    @Autowired
    public AuthenticationProvider oauth2KeyAuthenticationProvider(List<OAuth2Service> oauth2Services,
                                                                  AccountLoginSourceService accountLoginSourceService,
                                                                  CaptchaService captchaService,
                                                                  @Value("${paradise.need.validate.captcha.code:true}") Boolean needValidateCaptchaCode) {
        return new OAuth2KeyAuthenticationProvider(oauth2Services, accountLoginSourceService, captchaService, needValidateCaptchaCode);
    }

}
