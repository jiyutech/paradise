package tech.jiyu.paradise.conf;

import java.util.Map;

import tech.jiyu.paradise.core.account.domain.AccountListQuery;
import tech.jiyu.paradise.core.account.domain.AccountListQueryBuilder;
import tech.jiyu.paradise.core.account.domain.DefaultAccountListQuery;

/**
 * @author <a href="mailto:chenshao@dekeapp.com">shawsmith</a>
 */
public class DefaultAccountListQueryBuilder implements AccountListQueryBuilder {

    @Override
    public AccountListQuery buildAccountListQuery(Map<String, String> parameters) {
        return DefaultAccountListQuery.parse(parameters);
    }
}
