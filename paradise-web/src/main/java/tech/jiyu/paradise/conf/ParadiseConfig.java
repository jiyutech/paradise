package tech.jiyu.paradise.conf;

import com.fasterxml.jackson.databind.ObjectMapper;

import org.apache.commons.collections.CollectionUtils;
import org.apache.http.NoHttpResponseException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.config.SocketConfig;
import org.apache.http.conn.ConnectTimeoutException;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Lazy;
import org.springframework.core.task.TaskExecutor;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.scheduling.TaskScheduler;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.security.web.authentication.logout.LogoutHandler;
import org.springframework.security.web.firewall.DefaultHttpFirewall;
import org.springframework.security.web.firewall.HttpFirewall;

import java.util.List;

import tech.jiyu.paradise.core.account.dao.mapper.AccountMapper;
import tech.jiyu.paradise.core.account.domain.AccountActivationProperties;
import tech.jiyu.paradise.core.account.domain.AccountListQueryBuilder;
import tech.jiyu.paradise.core.account.domain.AccountListener;
import tech.jiyu.paradise.core.account.domain.AccountListenerSupport;
import tech.jiyu.paradise.core.account.domain.AccountResetPasswordProperties;
import tech.jiyu.paradise.core.account.service.*;
import tech.jiyu.paradise.core.account.service.impl.*;
import tech.jiyu.paradise.core.login.service.LoginService;
import tech.jiyu.paradise.core.privileges.service.AccountPrivilegeService;
import tech.jiyu.paradise.core.privileges.service.PrivilegeService;
import tech.jiyu.paradise.web.ParadiseExceptionHandler;
import tech.jiyu.paradise.web.account.controller.AccountModelViewObjectAssembler;
import tech.jiyu.paradise.web.authentication.DeleteLoginTokenLogoutHandler;
import tech.jiyu.paradise.web.authentication.Http500AuthenticationFailureHandler;
import tech.jiyu.paradise.web.authentication.LoginInfoWriter;
import tech.jiyu.utility.api.errors.ApiErrorRepository;
import tech.jiyu.utility.api.errors.MessageInterpreter;
import tech.jiyu.utility.pagination.PaginationTemplate;

/**
 * @author <a href="mailto:chenshao@dekeapp.com">shawsmith</a>
 */
@Configuration
@EnableConfigurationProperties({AccountActivationProperties.class, AccountResetPasswordProperties.class, PasswordAuthenticationSecurityPolicyProperties.class})
@EnableAsync
@EnableScheduling
public class ParadiseConfig {

    @Bean
    @ConditionalOnMissingBean(PasswordEncoder.class)
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Bean(destroyMethod = "close")
    @ConditionalOnMissingBean(HttpClient.class)
    public HttpClient httpClient(@Value("${paradise.http.timeout:30000}") Integer timeout,
                                 @Value("${paradise.http.connection.pool.maxActive:50}") Integer maxActive,
                                 @Value("${paradise.http.connection.pool.maxPerRoute:25}") Integer maxPerRote,
                                 @Value("${paradise.http.connection.pool.validateAfterInactivity:30000}") Integer validateAfterInactivity) {
        PoolingHttpClientConnectionManager connectionManager = createConnectionManager(maxActive, maxPerRote, validateAfterInactivity);
        RequestConfig requestConfig = RequestConfig.custom()
                .setConnectTimeout(timeout)
                .setConnectionRequestTimeout(timeout)
                .setSocketTimeout(timeout).build();
        return HttpClients.custom()
                .setConnectionManager(connectionManager)
                .setDefaultRequestConfig(requestConfig)
                .setRetryHandler((exception, executionCount, context) -> executionCount < 3 && (exception instanceof NoHttpResponseException || exception instanceof ConnectTimeoutException))
                .build();
    }

    private PoolingHttpClientConnectionManager createConnectionManager(Integer maxActive, Integer maxPerRote, Integer validateAfterInactivity) {
        PoolingHttpClientConnectionManager connectionManager = new PoolingHttpClientConnectionManager();
        connectionManager.setMaxTotal(maxActive);
        connectionManager.setDefaultMaxPerRoute(maxPerRote);
        connectionManager.setValidateAfterInactivity(validateAfterInactivity);
        SocketConfig sc = SocketConfig.custom().setSoKeepAlive(true).setTcpNoDelay(true).build();
        connectionManager.setDefaultSocketConfig(sc);
        return connectionManager;
    }

    @Bean
    @Autowired
    @ConditionalOnMissingBean(AccountListenerSupport.class)
    public AccountListenerSupport accountListenerSupport(List<AccountListener> listeners) {
        AccountListenerSupport support = new AccountListenerSupport();
        if (CollectionUtils.isNotEmpty(listeners)) {
            listeners.forEach(support::addListener);
        }
        return support;
    }

    @Bean
    @Autowired
    @ConditionalOnMissingBean(LoginInfoWriter.class)
    public LoginInfoWriter loginInfoWriter(AccountModelViewObjectAssembler accountModelViewObjectAssembler) {
        DefaultLoginInfoWriter loginInfoWriter = new DefaultLoginInfoWriter();
        loginInfoWriter.setAccountModelViewObjectAssembler(accountModelViewObjectAssembler);
        return loginInfoWriter;
    }

    @Bean
    @Autowired
    @ConditionalOnMissingBean(TaskScheduler.class)
    public TaskScheduler taskScheduler() {
        ThreadPoolTaskScheduler taskScheduler = new ThreadPoolTaskScheduler();
        taskScheduler.setPoolSize(10);
        return taskScheduler;
    }

    @Bean
    @Autowired
    @ConditionalOnMissingBean(TaskExecutor.class)
    public TaskExecutor taskExecutor() {
        ThreadPoolTaskExecutor taskExecutor = new ThreadPoolTaskExecutor();
        taskExecutor.setCorePoolSize(10);
        return taskExecutor;
    }

    @Bean
    @Autowired
    @ConditionalOnMissingBean(AccountListQueryBuilder.class)
    public AccountListQueryBuilder accountListQueryBuilder() {
        return new DefaultAccountListQueryBuilder();
    }

    @Bean
    @Autowired
    @ConditionalOnMissingBean(AccountListService.class)
    public AccountListService accountListService(AccountMapper accountMapper,
                                                 PaginationTemplate paginationTemplate,
                                                 AccountHistoryPhoneNumberService accountHistoryPhoneNumberService,
                                                 AccountPrivilegeService accountPrivilegeService,
                                                 PrivilegeService privilegeService) {
        return new DefaultAccountListServiceImpl(accountMapper, paginationTemplate, accountHistoryPhoneNumberService, accountPrivilegeService, privilegeService);
    }

    @Bean
    @Autowired
    @ConditionalOnMissingBean(InvitationCodeGenerator.class)
    public InvitationCodeGenerator invitationCodeGenerator(AccountMapper accountMapper, @Value("${paradise.account.invitation.code.length:6}") Integer codeLength) {
        return new DefaultInvitationCodeGenerator(accountMapper, codeLength);
    }

    @Bean
    @Autowired
    @ConditionalOnMissingBean(AccountLoginSourceService.class)
    public AccountLoginSourceService accountLoginSourceService(AccountService accountService) {
        return new DefaultAccountLoginSourceService(accountService);
    }

    @Bean
    @Autowired
    @ConditionalOnMissingBean(AuthenticationSuccessHandler.class)
    public AuthenticationSuccessHandler authenticationSuccessHandler(AccountService accountService,
                                                                     LoginService loginService,
                                                                     ObjectMapper objectMapper,
                                                                     LoginInfoWriter loginInfoWriter) {
        return new AccountAuthenticationSuccessHandler(accountService, loginService, objectMapper, loginInfoWriter);
    }


    @Bean
    @Autowired
    @ConditionalOnMissingBean(AuthenticationFailureHandler.class)
    public AuthenticationFailureHandler authenticationFailureHandler(ApiErrorRepository apiErrorRepository,
                                                                     @Autowired(required = false) MessageInterpreter messageInterpreter,
                                                                     ParadiseExceptionHandler paradiseExceptionHandler,
                                                                     MappingJackson2HttpMessageConverter messageConverter) {
        return new Http500AuthenticationFailureHandler(apiErrorRepository, messageInterpreter, paradiseExceptionHandler, messageConverter);
    }

    @Bean
    @Autowired
    @ConditionalOnMissingBean(LogoutHandler.class)
    public LogoutHandler logoutHandler(LoginService loginService) {
        return new DeleteLoginTokenLogoutHandler(loginService);
    }

    @Bean
    @Autowired
    public HttpFirewall httpFirewall() {
        return new DefaultHttpFirewall();
    }

    @Bean
    @ConditionalOnMissingBean(RolePermissionService.class)
    public RolePermissionService rolePermissionService() {
        return new DefaultRolePermissionService();
    }

    @Bean
    @Autowired
    public ApiAccessValidationService apiAccessValidationService(List<ApiAuthenticationStrategyService> strategyServiceList) {
        return new ApiAccessValidationServiceImpl(strategyServiceList);
    }

    @Bean
    @Autowired
    @ConditionalOnMissingBean(AccountEmailAndPasswordValidatorService.class)
    public AccountEmailAndPasswordValidatorService accountEmailAndPasswordValidatorService(@Lazy AccountService accountService) {
        return new AccountPasswordValidatorServiceImpl(accountService);
    }

    @Bean
    @Autowired
    @ConditionalOnMissingBean(AccountRegistrationInfoService.class)
    public AccountRegistrationInfoService accountRegistrationInfoService(@Lazy AccountService accountService) {
        return new AccountRegistrationInfoServiceImpl(accountService);
    }
}
