package tech.jiyu.paradise.conf;

import com.google.common.collect.Lists;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.web.context.SecurityContextRepository;

import java.util.List;

import tech.jiyu.paradise.web.authentication.SecurityContextRepositoryComposite;
import tech.jiyu.utility.security.SecurityContextRepositoryConfigurer;

/**
 * CreatedDate 2018-12-27 下午3:27
 *
 * @author SimonChan;emailAddress: simonchan@jinjie.tech
 */
@Configuration
public class SecurityContextRepositoryConfiguration {

    @Autowired(required = false)
    private List<SecurityContextRepositoryConfigurer> securityContextRepositoryConfigurers;

    @Bean
    public SecurityContextRepository securityContextRepository() {
        List<SecurityContextRepository> securityContextRepositories = Lists.newArrayList();
        if (CollectionUtils.isNotEmpty(securityContextRepositoryConfigurers)) {
            securityContextRepositoryConfigurers.forEach(configurer -> configurer.configureSecurityContextRepositories(securityContextRepositories));
        }

        return new SecurityContextRepositoryComposite(securityContextRepositories);
    }
}
