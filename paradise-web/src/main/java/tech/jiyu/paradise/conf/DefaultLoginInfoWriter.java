package tech.jiyu.paradise.conf;

import com.fasterxml.jackson.annotation.JsonUnwrapped;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.http.HttpServletResponse;

import tech.jiyu.paradise.core.account.domain.Account;
import tech.jiyu.paradise.core.login.domain.LoginInfo;
import tech.jiyu.paradise.web.account.controller.AccountModelViewObjectAssembler;
import tech.jiyu.paradise.web.account.vo.AccountVO;
import tech.jiyu.paradise.web.authentication.LoginInfoWriter;

/**
 * @author <a href="mailto:chenshao@dekeapp.com">shawsmith</a>
 */
public class DefaultLoginInfoWriter implements LoginInfoWriter {

    private AccountModelViewObjectAssembler accountModelViewObjectAssembler;

    public AccountModelViewObjectAssembler getAccountModelViewObjectAssembler() {
        return accountModelViewObjectAssembler;
    }

    public void setAccountModelViewObjectAssembler(AccountModelViewObjectAssembler accountModelViewObjectAssembler) {
        this.accountModelViewObjectAssembler = accountModelViewObjectAssembler;
    }

    @Override
    public void writeLoginInfo(LoginInfo loginInfo, Account account, ObjectMapper objectMapper, HttpServletResponse response) throws IOException {
        response.setStatus(HttpServletResponse.SC_CREATED);
        response.setContentType(MediaType.APPLICATION_JSON_UTF8_VALUE);
        response.setHeader(HttpHeaders.ACCESS_CONTROL_ALLOW_ORIGIN, "*");

        if (loginInfo != null) {
            loginInfo.setId(null);
        }

        AccountVO accountVO = accountModelViewObjectAssembler.assemble(account);
        if (accountVO != null) {
            accountVO.setId(null);
        }

        LoginInfoAccount loginInfoAccount = new LoginInfoAccount(loginInfo, accountVO);
        PrintWriter writer = response.getWriter();
        objectMapper.writeValue(writer, loginInfoAccount);
        writer.flush();
    }

    private static class LoginInfoAccount {

        @JsonUnwrapped
        private LoginInfo loginInfo;

        @JsonUnwrapped
        private AccountVO accountVO;


        public LoginInfoAccount(LoginInfo loginInfo, AccountVO accountVO) {
            this.loginInfo = loginInfo;
            this.accountVO = accountVO;
        }

        public LoginInfo getLoginInfo() {
            return loginInfo;
        }

        public void setLoginInfo(LoginInfo loginInfo) {
            this.loginInfo = loginInfo;
        }

        public AccountVO getAccountVO() {
            return accountVO;
        }

        public void setAccountVO(AccountVO accountVO) {
            this.accountVO = accountVO;
        }
    }
}