package tech.jiyu.paradise.conf;

import com.fasterxml.jackson.databind.ObjectMapper;

import org.apache.commons.lang3.StringUtils;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import tech.jiyu.paradise.core.account.domain.Account;
import tech.jiyu.paradise.core.account.service.AccountService;
import tech.jiyu.paradise.core.login.domain.LoginInfo;
import tech.jiyu.paradise.core.login.service.LoginService;
import tech.jiyu.paradise.web.authentication.AccountUserDetails;
import tech.jiyu.paradise.web.authentication.LoginInfoWriter;
import tech.jiyu.paradise.web.authentication.OAuth2AuthenticationToken;
import tech.jiyu.paradise.web.authentication.OAuth2KeyAuthenticationToken;
import tech.jiyu.paradise.web.authentication.ParadiseUsernamePasswordAuthenticationToken;
import tech.jiyu.paradise.web.authentication.PhoneNumberAuthenticationToken;

/**
 * @author <a href="mailto:chenshao@dekeapp.com">shawsmith</a>
 */
public class AccountAuthenticationSuccessHandler implements AuthenticationSuccessHandler {

    private AccountService accountService;
    private LoginService loginService;
    private ObjectMapper objectMapper;
    private LoginInfoWriter loginInfoWriter;

    public AccountAuthenticationSuccessHandler(AccountService accountService, LoginService loginService, ObjectMapper objectMapper, LoginInfoWriter loginInfoWriter) {
        this.accountService = accountService;
        this.loginService = loginService;
        this.objectMapper = objectMapper;
        this.loginInfoWriter = loginInfoWriter;
    }

    @Override
    public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication) throws IOException, ServletException {
        Account account = null;
        String loginSource = null;
        if (authentication instanceof ParadiseUsernamePasswordAuthenticationToken) {
            AccountUserDetails accountUserDetails = (AccountUserDetails) authentication.getPrincipal();
            account = accountUserDetails.getAccount();
            loginSource = ((ParadiseUsernamePasswordAuthenticationToken) authentication).getLoginSource();
        } else if (authentication instanceof PhoneNumberAuthenticationToken) {
            account = (Account) authentication.getDetails();
            loginSource = ((PhoneNumberAuthenticationToken) authentication).getLoginSource();
        } else if (authentication instanceof OAuth2AuthenticationToken) {
            account = (Account) authentication.getDetails();
            loginSource = ((OAuth2AuthenticationToken) authentication).getLoginSource();
        } else if (authentication instanceof OAuth2KeyAuthenticationToken) {
            account = (Account) authentication.getDetails();
            loginSource = ((OAuth2KeyAuthenticationToken) authentication).getLoginSource();
        }

        if (account != null) {
            refreshLoginInfo(account, loginSource, response);
            updateLastLoginTime(account);
        }
    }

    private void refreshLoginInfo(Account account, String loginSource, HttpServletResponse response) throws IOException {
        LoginInfo loginInfo = buildLoginInfo(account, loginSource);
        if (loginInfo != null) {
            loginService.createOrUpdate(loginInfo);
            loginInfoWriter.writeLoginInfo(loginInfo, account, objectMapper, response);
        }
    }

    private LoginInfo buildLoginInfo(Account account, String loginSource) {
        if (account == null || StringUtils.isBlank(loginSource)) {
            return null;
        }

        LoginInfo loginInfo = new LoginInfo();
        loginInfo.setLoginId(String.valueOf(account.getId()));
        loginInfo.setLoginSource(loginSource);
        loginInfo.setRoleTypeCode(account.getRoleTypeCode());
        return loginInfo;
    }

    private void updateLastLoginTime(Account account) {
        if (account == null || account.getId() == null) {
            return;
        }

        account.setLastLoginTime(System.currentTimeMillis());
        accountService.patch(account);
    }
}
