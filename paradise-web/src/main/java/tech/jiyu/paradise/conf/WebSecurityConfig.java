package tech.jiyu.paradise.conf;

import com.google.common.collect.Lists;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingClass;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.annotation.web.configurers.SecurityContextConfigurer;
import org.springframework.security.web.access.channel.ChannelProcessingFilter;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.security.web.authentication.logout.HttpStatusReturningLogoutSuccessHandler;
import org.springframework.security.web.authentication.logout.LogoutFilter;
import org.springframework.security.web.authentication.logout.LogoutHandler;
import org.springframework.security.web.context.SecurityContextRepository;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import org.springframework.security.web.util.matcher.RequestMatcher;

import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.ServiceLoader;

import javax.servlet.Filter;

import tech.jiyu.paradise.web.authentication.Http401AuthenticationEntryPoint;
import tech.jiyu.parrot.i18n.conf.I18nFilter;
import tech.jiyu.utility.encryption.RSAKeyRepository;
import tech.jiyu.utility.security.AuthenticationFilterConfigurer;
import tech.jiyu.utility.security.HttpSecurityConfigurer;
import tech.jiyu.utility.web.filter.AllowAllCorsFilter;
import tech.jiyu.utility.web.filter.BusinessExtensibleFilter;
import tech.jiyu.utility.web.filter.XRealMethodFilter;
import tech.jiyu.utility.web.filter.ZoneOffsetFilter;

/**
 * @author <a href="mailto:chenshao@dekeapp.com">shawsmith</a>
 */
@EnableGlobalMethodSecurity(securedEnabled = true)
@ConditionalOnMissingClass({"tech.jiyu.oauth2.conf.OAuth2ResourceServerConfiguration"})
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    public static final String LOGIN_PATH = "/logins";

    @Autowired
    private List<AuthenticationProvider> authenticationProviders;

    @Autowired(required = false)
    private RSAKeyRepository rsaKeyRepository;

    @Autowired
    private AuthenticationSuccessHandler authenticationSuccessHandler;

    @Autowired
    private AuthenticationFailureHandler authenticationFailureHandler;

    @Autowired(required = false)
    private LogoutHandler logoutHandler;

    @Autowired(required = false)
    private SecurityContextRepository securityContextRepository;

    @Autowired(required = false)
    private AllowAllCorsFilter allowAllCorsFilter;

    @Autowired(required = false)
    private XRealMethodFilter xRealMethodFilter;

    @Autowired(required = false)
    private ZoneOffsetFilter zoneOffsetFilter;

    @Autowired(required = false)
    private I18nFilter i18nFilter;

    @Autowired(required = false)
    private BusinessExtensibleFilter businessExtensibleFilter;

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        if (authenticationProviders != null) {
            authenticationProviders.forEach(provider -> {
                if (provider != null) {
                    auth.authenticationProvider(provider);
                }
            });
        }
    }

    @Override
    @SuppressWarnings("unchecked")
    protected void configure(HttpSecurity http) throws Exception {
        configureFilters(http);
        configurePermissions(http);
        http
                .httpBasic().disable()
                .anonymous().disable()
                .rememberMe().disable()
                .csrf().disable()
                .sessionManagement().disable()
                .formLogin().disable()
                .exceptionHandling().authenticationEntryPoint(new Http401AuthenticationEntryPoint());

        if (logoutHandler != null) {
            http.logout()
                    .logoutRequestMatcher(new AntPathRequestMatcher(LOGIN_PATH, HttpMethod.DELETE.name()))
                    .addLogoutHandler(logoutHandler)
                    .logoutSuccessHandler(new HttpStatusReturningLogoutSuccessHandler(HttpStatus.NO_CONTENT))
                    .permitAll();

        }

        if (securityContextRepository != null) {
            http.getConfigurer(SecurityContextConfigurer.class)
                    .securityContextRepository(securityContextRepository);
        }
    }

    private void configureFilters(HttpSecurity http) throws Exception {
        addFilters(http);

        // 配置收集登录信息的Filter. 登录信息存放在Request请求的Body中.
        RequestMatcher loginRequestMatcher = new AntPathRequestMatcher(LOGIN_PATH, HttpMethod.POST.name());
        AuthenticationManager authenticationManager = authenticationManager();

        List<AuthenticationFilterConfigurer> configurers = Lists.newArrayListWithCapacity(0);
        ServiceLoader<AuthenticationFilterConfigurer> filterConfigurers = ServiceLoader.load(AuthenticationFilterConfigurer.class);
        Iterator<AuthenticationFilterConfigurer> configurerIterator = filterConfigurers.iterator();
        while (configurerIterator.hasNext()) {
            AuthenticationFilterConfigurer configurer = configurerIterator.next();
            if (configurer != null) {
                configurers.add(configurer);
            }
        }

        Comparator<AuthenticationFilterConfigurer> filterOrderComparator = Comparator.comparingLong(AuthenticationFilterConfigurer::getOrder);
        configurers.sort(filterOrderComparator);

        Class<? extends Filter> afterFilter = LogoutFilter.class;
        for (AuthenticationFilterConfigurer configurer : configurers) {
            try {
                configurer.configure(http, loginRequestMatcher, rsaKeyRepository, authenticationSuccessHandler, authenticationFailureHandler, authenticationManager, afterFilter);
            } catch (UnsupportedOperationException e) {
                configurer.configure(http, loginRequestMatcher, authenticationSuccessHandler, authenticationFailureHandler, authenticationManager, afterFilter);
            }
            afterFilter = configurer.getTargetFilterClass();
        }
    }

    private void addFilters(HttpSecurity http) {
        // allowAllCorsFilter > xRealMethodFilter > zoneOffsetFilter > i18nFilter > businessExtensibleFilter > ChannelProcessFilter

        Class<? extends Filter> lastFilterClass = ChannelProcessingFilter.class;

        if (businessExtensibleFilter != null) {
            http.addFilterBefore(businessExtensibleFilter, lastFilterClass);
            lastFilterClass = businessExtensibleFilter.getClass();
        }

        if (i18nFilter != null) {
            http.addFilterBefore(i18nFilter, lastFilterClass);
            lastFilterClass = i18nFilter.getClass();
        }

        if (zoneOffsetFilter != null) {
            http.addFilterBefore(zoneOffsetFilter, lastFilterClass);
            lastFilterClass = zoneOffsetFilter.getClass();
        }

        if (xRealMethodFilter != null) {
            http.addFilterBefore(xRealMethodFilter, lastFilterClass);
            lastFilterClass = xRealMethodFilter.getClass();
        }

        if (allowAllCorsFilter != null) {
            http.addFilterBefore(allowAllCorsFilter, lastFilterClass);
        }
    }

    private void configurePermissions(HttpSecurity security) throws Exception {
        ServiceLoader<HttpSecurityConfigurer> securityConfigurers = ServiceLoader.load(HttpSecurityConfigurer.class);
        Iterator<HttpSecurityConfigurer> configurerIterator = securityConfigurers.iterator();
        while (configurerIterator.hasNext()) {
            HttpSecurityConfigurer configurer = configurerIterator.next();
            if (configurer != null) {
                configurer.configure(security);
            }
        }
    }
}