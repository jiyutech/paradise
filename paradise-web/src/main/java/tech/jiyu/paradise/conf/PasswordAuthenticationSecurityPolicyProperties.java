package tech.jiyu.paradise.conf;

import org.springframework.boot.context.properties.ConfigurationProperties;

import lombok.Data;

/**
 * @author Simon [simonchan@jinjie.tech]
 */
@Data
@ConfigurationProperties(prefix = PasswordAuthenticationSecurityPolicyProperties.PREFIX)
public class PasswordAuthenticationSecurityPolicyProperties {

    public static final String PREFIX = "paradise.account.password.authentication.security";
    private static final Boolean DEFAULT_ENABLED = Boolean.FALSE;
    private static final int DEFAULT_MAXIMUM_WRONG_COUNT = 5;

    /**
     * 是否启用密码登录安全
     */
    private Boolean enabled = DEFAULT_ENABLED;

    /**
     * 最大连续输错密码次数
     */
    private Integer maximumWrongCount = DEFAULT_MAXIMUM_WRONG_COUNT;
}
