package tech.jiyu.paradise.conf;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.core.Ordered;
import org.springframework.web.servlet.config.annotation.*;
import tech.jiyu.paradise.web.interceptor.ApiAccessValidationInterceptor;

/**
 * @author <a href="mailto:chenshao@jiyu.tech">shawsmith</a>
 */
@Configuration
@EnableWebMvc
@EnableAspectJAutoProxy
public class WebConfig implements WebMvcConfigurer {

    @Override
    public void addCorsMappings(CorsRegistry registry) {
        registry.addMapping("/**")
                .allowedOrigins("*")
                .allowedHeaders("*")
                .allowedMethods("GET", "HEAD", "POST", "PUT", "PATCH", "DELETE", "OPTIONS", "TRACE")
                .allowCredentials(false);

    }

    @Override
    public void configureContentNegotiation(ContentNegotiationConfigurer configurer) {
        configurer.favorPathExtension(false);
    }

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        // 拦截所有请求
        registry.addInterceptor(validationInterceptor()).addPathPatterns("/**").order(Ordered.LOWEST_PRECEDENCE);
    }

    @Bean
    public ApiAccessValidationInterceptor validationInterceptor() {
        return new ApiAccessValidationInterceptor();
    }
}
