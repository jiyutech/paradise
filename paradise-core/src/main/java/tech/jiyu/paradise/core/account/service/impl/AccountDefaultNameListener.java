package tech.jiyu.paradise.core.account.service.impl;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;
import tech.jiyu.paradise.core.account.domain.Account;
import tech.jiyu.paradise.core.account.domain.AccountEvent;
import tech.jiyu.paradise.core.account.domain.AccountListener;
import tech.jiyu.paradise.core.account.domain.AccountPostCreatedEvent;
import tech.jiyu.paradise.core.account.service.AccountService;

/**
 * Created By AccountDefaultNameListener
 * <p>
 * Date 2018/3/12 15:11
 *
 * @author SimonChan;emailAddress: simonchan@jiyu.tech
 */
@Component
public class AccountDefaultNameListener implements AccountListener {

    @Value("${paradise.account.projectName:user}")
    private String projectName;

    @Autowired
    @Lazy
    private AccountService accountService;

    @Override
    public void onEvent(AccountEvent event) {
        if (event instanceof AccountPostCreatedEvent) {
            AccountPostCreatedEvent accountPostCreatedEvent = (AccountPostCreatedEvent) event;
            Account account = accountPostCreatedEvent.getAccount();
            if (StringUtils.isBlank(account.getName())) {
                account.setName(projectName + account.getId());
                accountService.patch(account);
            }
        }
    }
}
