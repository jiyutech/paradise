package tech.jiyu.paradise.core.privileges.service;

import org.springframework.validation.annotation.Validated;

import java.util.Collection;
import java.util.List;

import tech.jiyu.paradise.core.privileges.domain.Privilege;
import tech.jiyu.restful.api.service.RestfulApi;

/**
 * @author <a href="mailto:chenshao@jinjie.tech">shawsmith</a>
 */
@Validated
public interface PrivilegeService extends RestfulApi<Privilege, Void> {

    String CACHE_PREFIX = "paradise:privileges:";

    Privilege findByName(String name);

    List<Privilege> findByNames(Collection<String> names);
}
