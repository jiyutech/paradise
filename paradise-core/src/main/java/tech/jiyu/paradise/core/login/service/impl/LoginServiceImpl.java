package tech.jiyu.paradise.core.login.service.impl;

import com.google.common.collect.Collections2;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.support.TransactionSynchronization;
import org.springframework.transaction.support.TransactionSynchronizationManager;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.UUID;
import java.util.stream.Collectors;

import lombok.extern.slf4j.Slf4j;
import tech.jiyu.paradise.core.login.dao.mapper.LoginMapper;
import tech.jiyu.paradise.core.login.dao.po.LoginPO;
import tech.jiyu.paradise.core.login.domain.LoginInfo;
import tech.jiyu.paradise.core.login.domain.LoginInfoEvent;
import tech.jiyu.paradise.core.login.service.LoginService;
import tech.jiyu.sequences.SequenceService;
import tech.jiyu.utility.cache.CacheCallback;
import tech.jiyu.utility.cache.CacheStrategy;
import tech.jiyu.utility.cache.CacheTemplate;
import tech.jiyu.utility.cache.CacheUtils;

/**
 * @author <a href="mailto:chenshao@jiyu.tech">shawsmith</a>
 */
@Service
@Slf4j
public class LoginServiceImpl implements LoginService {

    @Autowired
    private LoginMapper loginMapper;

    @Autowired
    private SequenceService sequenceService;

    @Autowired
    private CacheTemplate cacheTemplate;

    @Autowired
    private ApplicationEventPublisher eventPublisher;

    @Override
    @Transactional
    public void createOrUpdate(LoginInfo loginInfo) {
        String loginToken = UUID.randomUUID().toString();
        loginInfo.setLoginToken(loginToken);

        String loginId = loginInfo.getLoginId();
        String loginSource = loginInfo.getLoginSource();
        LoginPO existedPO = loginMapper.selectByLoginIdAndSource(loginId, loginSource);
        if (existedPO != null) {
            String oldLoginToken = existedPO.getLoginToken();

            existedPO.setLoginToken(loginInfo.getLoginToken());
            existedPO.setRoleTypeCode(loginInfo.getRoleTypeCode());
            existedPO.setLoginSource(loginInfo.getLoginSource());
            loginMapper.updateById(existedPO);
            loginInfo.setId(existedPO.getId());

            // 删除缓存中旧的登录信息.
            removeCache(oldLoginToken);

            // 发布登录信息事件，主要用于日志记录
            eventPublisher.publishEvent(new LoginInfoEvent(existedPO.getLoginId(), oldLoginToken, existedPO.getLoginToken()));
        } else {
            LoginPO newLoginPO = LoginPO.create(sequenceService.nextLongValue(), loginInfo);
            loginMapper.insert(newLoginPO);
            loginInfo.setId(newLoginPO.getId());

            // 发布登录信息事件，主要用于日志记录
            eventPublisher.publishEvent(new LoginInfoEvent(newLoginPO.getLoginId(), "", newLoginPO.getLoginToken()));
        }
    }

    @Override
    public List<String> findLoginTokensByLoginId(String loginId) {
        return loginMapper.selectLoginTokensByLoginId(loginId);
    }

    @Override
    @Transactional
    public void deleteByLoginToken(String loginToken) {
        loginMapper.deleteByToken(loginToken);
        removeCache(loginToken);
    }

    @Override
    public LoginInfo findByLoginToken(String loginToken) {
        Collection<Object> cacheKeys = Collections.singletonList(buildCacheKey(loginToken));
        List<LoginInfo> loginInfoGroups = cacheTemplate.load(cacheKeys, CacheStrategy.CACHE_PREFER, new CacheCallback() {
            @Override
            public Map<Object, Object> getKeyValuePairs(Collection<Object> objects) {
                if (CollectionUtils.isEmpty(objects)) {
                    return Maps.newHashMap();
                }

                Map<Object, Object> result = Maps.newHashMap();
                objects.forEach(object -> {
                    if (object instanceof LoginInfo) {
                        LoginInfo loginInfo = (LoginInfo) object;

                        String loginTokenCacheKey = buildCacheKey(loginInfo.getLoginToken());
                        if (StringUtils.isNotBlank(loginTokenCacheKey)) {
                            result.put(loginTokenCacheKey, loginInfo);
                        }
                    }
                });
                return result;
            }

            @Override
            public List<Object> loadNewObjects(Collection<Object> keys) {
                if (CollectionUtils.isEmpty(keys)) {
                    return Lists.newArrayListWithCapacity(0);
                }

                Collection<String> loginTokens = Collections2.transform(keys, key -> parseLoginToken((String) key));
                if (CollectionUtils.isEmpty(loginTokens)) {
                    return Lists.newArrayListWithCapacity(0);
                }

                return loginMapper.selectByLoginTokens(loginTokens).stream()
                        .map(loginPO -> (Object) transformToLoginInfo(loginPO))
                        .filter(Objects::nonNull)
                        .collect(Collectors.toList());

            }
        });

        return CollectionUtils.isNotEmpty(loginInfoGroups) ? loginInfoGroups.get(0) : null;
    }

    private LoginInfo transformToLoginInfo(LoginPO loginPO) {
        if (loginPO == null) {
            return null;
        }

        LoginInfo loginInfo = new LoginInfo();

        loginInfo.setId(loginPO.getId());
        loginInfo.setLoginId(loginPO.getLoginId());
        loginInfo.setLoginToken(loginPO.getLoginToken());
        loginInfo.setRoleTypeCode(loginPO.getRoleTypeCode());
        loginInfo.setLoginSource(loginPO.getLoginSource());

        return loginInfo;
    }

    @Override
    public Collection<String> findLoggedIds(Collection<String> loginIds) {
        if (CollectionUtils.isEmpty(loginIds)) {
            return Lists.newArrayListWithCapacity(0);
        }

        return loginMapper.selectLoggedIds(loginIds);
    }

    private void removeCache(String loginToken) {
        if (StringUtils.isBlank(loginToken)) {
            return;
        }

        if (TransactionSynchronizationManager.isActualTransactionActive()) {
            TransactionSynchronizationManager.registerSynchronization(new TransactionSynchronization() {
                @Override
                public void afterCompletion(int status) {
                    if (log.isDebugEnabled()) {
                        log.debug("after transaction completion to delete loginToken cache. loginToken: {} txStatus: {}", loginToken, status);
                    }

                    if (status == STATUS_COMMITTED) {
                        doRemoveCache(loginToken);
                    }
                }
            });
        } else {
            doRemoveCache(loginToken);
        }
    }

    private void doRemoveCache(String loginToken) {
        String loginTokenCacheKey = buildCacheKey(loginToken);
        cacheTemplate.delete(loginTokenCacheKey);
    }

    private String buildCacheKey(String loginToken) {
        return CacheUtils.buildCacheKey(CACHE_PREFIX, loginToken);
    }

    private String parseLoginToken(String cacheKey) {
        return CacheUtils.parseString(cacheKey, CACHE_PREFIX);
    }

    @Override
    public LoginInfo findByLoginIdAndSource(String loginId, String loginSource) {
        LoginPO loginPO = loginMapper.selectByLoginIdAndSource(loginId, loginSource);
        return transformToLoginInfo(loginPO);
    }
}
