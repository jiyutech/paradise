package tech.jiyu.paradise.core.account.domain;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author <a href="mailto:chenshao@dekeapp.com">shawsmith</a>
 */
@NoArgsConstructor
@Getter
@Setter
public class AccountHistoryPhoneNumber {

    private Long id;
    private Long accountId;
    private String oldPhoneNumber;
    private String newPhoneNumber;
    private Long changeDate;

}
