package tech.jiyu.paradise.core.account.service.impl;

import com.google.common.collect.Collections2;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tech.jiyu.paradise.core.account.dao.mapper.AccountHistoryPhoneNumberMapper;
import tech.jiyu.paradise.core.account.dao.po.AccountHistoryPhoneNumberPO;
import tech.jiyu.paradise.core.account.domain.AccountHistoryPhoneNumber;
import tech.jiyu.paradise.core.account.service.AccountHistoryPhoneNumberService;
import tech.jiyu.sequences.SequenceService;
import tech.jiyu.utility.cache.CacheCallback;
import tech.jiyu.utility.cache.CacheStrategy;
import tech.jiyu.utility.cache.CacheTemplate;
import tech.jiyu.utility.cache.CacheUtils;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * @author <a href="mailto:chenshao@dekeapp.com">shawsmith</a>
 */
@Service
public class AccountHistoryPhoneNumberServiceImpl implements AccountHistoryPhoneNumberService {

    @Autowired
    private AccountHistoryPhoneNumberMapper accountHistoryPhoneNumberMapper;

    @Autowired
    private SequenceService sequenceService;

    @Autowired
    private CacheTemplate cacheTemplate;

    @Override
    @Transactional
    public AccountHistoryPhoneNumber save(AccountHistoryPhoneNumber historyPhoneNumber) {
        AccountHistoryPhoneNumberPO historyPhoneNumberPO = AccountHistoryPhoneNumberPO.create(sequenceService.nextLongValue(), historyPhoneNumber);
        accountHistoryPhoneNumberMapper.insert(historyPhoneNumberPO);
        historyPhoneNumber.setId(historyPhoneNumberPO.getId());
        removeCache(historyPhoneNumber.getAccountId());
        return historyPhoneNumber;
    }

    @Override
    @SuppressWarnings("unchecked")
    public Map<Long, List<AccountHistoryPhoneNumber>> findByAccountIds(Collection<Long> accountIds, CacheStrategy strategy) {
        if (CollectionUtils.isEmpty(accountIds)) {
            return Maps.newHashMap();
        }

        Collection<Object> cacheKeys = Collections2.transform(accountIds, this::buildCacheKey);
        List<List<AccountHistoryPhoneNumber>> accountHistoryPhoneNumbers = cacheTemplate.load(cacheKeys, strategy, new CacheCallback() {
            @Override
            public Map<Object, Object> getKeyValuePairs(Collection<Object> objects) {
                if (CollectionUtils.isEmpty(objects)) {
                    return Maps.newHashMap();
                }

                Map<Object, Object> keyValuePairs = Maps.newHashMap();

                Collection<Long> missedAccountIds = Lists.newArrayList(accountIds);
                objects.forEach(object -> {
                    List<AccountHistoryPhoneNumber> phoneNumbers = (List<AccountHistoryPhoneNumber>) object;
                    if (CollectionUtils.isNotEmpty(phoneNumbers)) {
                        Long accountId = phoneNumbers.get(0).getAccountId();
                        keyValuePairs.put(buildCacheKey(accountId), phoneNumbers);
                        missedAccountIds.remove(accountId);
                    }
                });

                if (CollectionUtils.isNotEmpty(missedAccountIds)) {
                    missedAccountIds.forEach(accountId -> keyValuePairs.put(buildCacheKey(accountId), Lists.newArrayListWithCapacity(0)));
                }
                return keyValuePairs;
            }

            @Override
            public List<Object> loadNewObjects(Collection<Object> keys) {
                if (CollectionUtils.isEmpty(keys)) {
                    return Lists.newArrayListWithCapacity(0);
                }

                Collection<Long> accountIds = Collections2.transform(keys, AccountHistoryPhoneNumberServiceImpl.this::parseAccountId);
                Map<Long, List<AccountHistoryPhoneNumber>> historyPhoneNumberGroups = fetchAccountHistoryPhoneNumbers(accountIds);

                List<Object> result = Lists.newArrayList();

                accountIds.forEach(accountId -> {
                    List<AccountHistoryPhoneNumber> phoneNumbers = historyPhoneNumberGroups.get(accountId);
                    if (phoneNumbers == null) {
                        phoneNumbers = Lists.newArrayListWithCapacity(0);
                    }
                    result.add(phoneNumbers);
                });
                return result;
            }
        });
        if (CollectionUtils.isEmpty(accountHistoryPhoneNumbers)) {
            return Maps.newHashMap();
        }

        Map<Long, List<AccountHistoryPhoneNumber>> historyPhoneNumberGroups = Maps.newHashMap();
        accountHistoryPhoneNumbers.forEach(phoneNumbers -> {
            if (CollectionUtils.isNotEmpty(phoneNumbers)) {
                Long accountId = phoneNumbers.get(0).getAccountId();
                historyPhoneNumberGroups.put(accountId, phoneNumbers);
            }
        });

        return historyPhoneNumberGroups;
    }

    private Map<Long, List<AccountHistoryPhoneNumber>> fetchAccountHistoryPhoneNumbers(Collection<Long> accountIds) {
        List<AccountHistoryPhoneNumberPO> historyPhoneNumberPOs = accountHistoryPhoneNumberMapper.selectByAccountIds(accountIds);
        return assembleAccountHistoryPhoneNumbers(historyPhoneNumberPOs)
                .stream()
                .collect(Collectors.groupingBy(AccountHistoryPhoneNumber::getAccountId));
    }

    private String buildCacheKey(Long accountId) {
        return CacheUtils.buildCacheKey(CACHE_PREFIX, accountId);
    }

    private Long parseAccountId(Object cacheKey) {
        return CacheUtils.parseId(cacheKey, CACHE_PREFIX);

    }

    private void removeCache(Long accountId) {
        String cacheKey = buildCacheKey(accountId);
        cacheTemplate.delete(cacheKey);
    }

    private List<AccountHistoryPhoneNumber> assembleAccountHistoryPhoneNumbers(List<AccountHistoryPhoneNumberPO> historyPhoneNumberPOs) {
        if (CollectionUtils.isEmpty(historyPhoneNumberPOs)) {
            return Lists.newArrayListWithCapacity(0);
        }

        return historyPhoneNumberPOs
                .stream()
                .map(this::transformToAccountHistoryPhoneNumber)
                .filter(Objects::nonNull)
                .collect(Collectors.toList());

    }

    private AccountHistoryPhoneNumber transformToAccountHistoryPhoneNumber(AccountHistoryPhoneNumberPO accountHistoryPhoneNumberPO) {
        if (accountHistoryPhoneNumberPO == null) {
            return null;
        }

        AccountHistoryPhoneNumber historyPhoneNumber = new AccountHistoryPhoneNumber();
        historyPhoneNumber.setId(accountHistoryPhoneNumberPO.getId());
        historyPhoneNumber.setAccountId(accountHistoryPhoneNumberPO.getAccountId());
        historyPhoneNumber.setOldPhoneNumber(accountHistoryPhoneNumberPO.getOldPhoneNumber());
        historyPhoneNumber.setNewPhoneNumber(accountHistoryPhoneNumberPO.getNewPhoneNumber());
        historyPhoneNumber.setChangeDate(accountHistoryPhoneNumberPO.getChangeDate());

        return historyPhoneNumber;
    }

    @Override
    @Transactional
    public void deleteByAccountId(Long accountId) {
        accountHistoryPhoneNumberMapper.deleteByAccountId(accountId);
        removeCache(accountId);
    }
}
