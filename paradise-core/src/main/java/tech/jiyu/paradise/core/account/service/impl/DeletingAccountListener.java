package tech.jiyu.paradise.core.account.service.impl;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import tech.jiyu.paradise.core.account.dao.mapper.AccountMapper;
import tech.jiyu.paradise.core.account.dao.po.AccountPO;
import tech.jiyu.paradise.core.account.domain.AccountEvent;
import tech.jiyu.paradise.core.account.domain.AccountListener;
import tech.jiyu.paradise.core.account.domain.AccountPostDeletedEvent;
import tech.jiyu.paradise.core.account.domain.AccountPreDeletedEvent;
import tech.jiyu.paradise.core.account.exception.DeletingReversedAccountException;
import tech.jiyu.paradise.core.account.service.AccountHistoryPhoneNumberService;
import tech.jiyu.paradise.core.account.service.EducationExperienceService;
import tech.jiyu.paradise.core.account.service.ResidenceService;
import tech.jiyu.paradise.core.account.service.WorkHistoryService;
import tech.jiyu.paradise.core.authentication.service.OAuth2AuthenticationService;
import tech.jiyu.paradise.core.login.service.LoginService;
import tech.jiyu.paradise.core.privileges.service.AccountPrivilegeService;
import tech.jiyu.paradise.core.util.LoginUtils;
import tech.jiyu.paradise.core.util.SecurityUtils;

/**
 * @author <a href="mailto:chenshao@jiyu.tech">shawsmith</a>
 */
@Component
public class DeletingAccountListener implements AccountListener {

    @Autowired
    private AccountMapper accountMapper;

    @Autowired
    private AccountHistoryPhoneNumberService accountHistoryPhoneNumberService;

    @Autowired
    private OAuth2AuthenticationService oAuth2AuthenticationService;

    @Autowired
    private ResidenceService residenceService;

    @Autowired
    private EducationExperienceService educationExperienceService;

    @Autowired
    private WorkHistoryService workHistoryService;

    @Autowired
    private LoginService loginService;

    @Autowired
    private AccountPrivilegeService accountPrivilegeService;

    @Override
    public void onEvent(AccountEvent event) {
        if (event instanceof AccountPreDeletedEvent) {
            Long accountId = ((AccountPreDeletedEvent) event).getAccountId();
            checkAccountReserved(accountId);
        } else if (event instanceof AccountPostDeletedEvent) {
            Long accountId = ((AccountPostDeletedEvent) event).getAccountId();
            accountHistoryPhoneNumberService.deleteByAccountId(accountId);
            oAuth2AuthenticationService.deleteByAccountId(accountId);
            LoginUtils.logout(String.valueOf(accountId), loginService);

            residenceService.deleteByAccountId(accountId);
            educationExperienceService.deleteByAccountId(accountId);
            workHistoryService.deleteByAccountId(accountId);
            accountPrivilegeService.deleteByAccountId(accountId);
        }
    }

    private void checkAccountReserved(Long accountId) {
        AccountPO accountPO = accountMapper.selectById(accountId);
        if (accountPO != null && StringUtils.equals(SecurityUtils.SYSTEM, accountPO.getRoleTypeCode())) {
            throw new DeletingReversedAccountException();
        }
    }
}
