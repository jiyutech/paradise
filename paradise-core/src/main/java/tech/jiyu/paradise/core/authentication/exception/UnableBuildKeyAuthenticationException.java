package tech.jiyu.paradise.core.authentication.exception;

import tech.jiyu.utility.api.errors.annotations.ApiErrorKey;

/**
 * CreatedDate 2018-07-10 上午12:22
 *
 * @author SimonChan;emailAddress: simonchan@jinjie.tech
 */
@ApiErrorKey(group = "oauth2",
             id = "unable.build.keyAuthentication")
public class UnableBuildKeyAuthenticationException extends RuntimeException {

    public UnableBuildKeyAuthenticationException() {
        super();
    }

    public UnableBuildKeyAuthenticationException(String message) {
        super(message);
    }

    public UnableBuildKeyAuthenticationException(String message, Throwable cause) {
        super(message, cause);
    }

    public UnableBuildKeyAuthenticationException(Throwable cause) {
        super(cause);
    }

    protected UnableBuildKeyAuthenticationException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
