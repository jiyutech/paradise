package tech.jiyu.paradise.core.account.service.impl;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tech.jiyu.paradise.core.account.dao.mapper.EducationExperienceMapper;
import tech.jiyu.paradise.core.account.dao.po.EducationExperiencePO;
import tech.jiyu.paradise.core.account.domain.EducationExperience;
import tech.jiyu.paradise.core.account.service.EducationExperienceService;
import tech.jiyu.sequences.SequenceService;
import tech.jiyu.utility.cache.CacheCallback;
import tech.jiyu.utility.cache.CacheStrategy;
import tech.jiyu.utility.cache.CacheTemplate;
import tech.jiyu.utility.cache.CacheUtils;

import java.util.*;
import java.util.stream.Collectors;

/**
 * Created By EducationExperienceServiceImpl
 * <p>
 * Date 2017/12/27 20:09
 *
 * @author SimonChan;emailAddress: simonchan@jiyu.tech
 */
@Service
public class EducationExperienceServiceImpl implements EducationExperienceService {

    @Autowired
    private EducationExperienceMapper educationExperienceMapper;

    @Autowired
    private SequenceService sequenceService;

    @Autowired
    private CacheTemplate cacheTemplate;

    @Override
    @Transactional
    public List<EducationExperience> saveOrUpdate(Long accountId, List<EducationExperience> experiences) {
        if (CollectionUtils.isEmpty(experiences)) {
            deleteByAccountId(accountId);
            return Lists.newArrayListWithCapacity(0);
        }
        educationExperienceMapper.deleteByAccountId(accountId);
        List<EducationExperiencePO> newEducationExperiencePos = Lists.newArrayList();
        int order = 0;
        for (EducationExperience experience : experiences) {
            experience.setAccountId(accountId);
            EducationExperiencePO educationExperiencePO = EducationExperiencePO.create(sequenceService.nextLongValue(), experience);
            educationExperiencePO.setOrderNum(order++);
            newEducationExperiencePos.add(educationExperiencePO);
        }
        educationExperienceMapper.batchSave(newEducationExperiencePos);
        removeCache(accountId);
        return findByAccountId(accountId, CacheStrategy.NO_CACHE);
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<EducationExperience> findByAccountId(Long accountId, CacheStrategy strategy) {
        String educationExperienceCacheKey = buildCacheKey(accountId);
        Collection<Object> keys = Collections.singletonList(educationExperienceCacheKey);
        List<List<EducationExperience>> educationExperiences = cacheTemplate.load(keys, strategy, new CacheCallback() {
            @Override
            public Map<Object, Object> getKeyValuePairs(Collection<Object> objects) {
                if (CollectionUtils.isEmpty(objects)) {
                    return Maps.newHashMap();
                }

                List<EducationExperience> educationExperiences = (List<EducationExperience>) objects.iterator().next();
                Map<Object, Object> pairs = Maps.newHashMap();
                pairs.put(educationExperienceCacheKey, educationExperiences);
                return pairs;
            }

            @Override
            public List<Object> loadNewObjects(Collection<Object> keys) {
                if (CollectionUtils.isEmpty(keys)) {
                    return Lists.newArrayListWithCapacity(0);
                }

                List<EducationExperience> experiences = educationExperienceMapper.selectByAccountId(accountId).stream()
                        .map(this::transformToEducationExperience)
                        .filter(Objects::nonNull)
                        .collect(Collectors.toList());
                List<Object> experienceGroups = Lists.newArrayList();
                experienceGroups.add(experiences);
                return experienceGroups;
            }

            private EducationExperience transformToEducationExperience(EducationExperiencePO educationExperiencePO) {
                if (educationExperiencePO == null) {
                    return null;
                }

                EducationExperience educationExperience = new EducationExperience();
                educationExperience.setId(educationExperiencePO.getId());
                educationExperience.setAccountId(educationExperiencePO.getAccountId());
                educationExperience.setSchoolName(educationExperiencePO.getSchoolName());
                educationExperience.setSubjectName(educationExperiencePO.getSubjectName());

                return educationExperience;
            }
        });

        return CollectionUtils.isNotEmpty(educationExperiences) ? educationExperiences.get(0) : Lists.newArrayListWithCapacity(0);
    }

    @Override
    @Transactional
    public void deleteByAccountId(Long accountId) {
        educationExperienceMapper.deleteByAccountId(accountId);
        removeCache(accountId);
    }

    private String buildCacheKey(Long accountId) {
        return CacheUtils.buildCacheKey(CACHE_PREFIX, accountId);
    }

    private void removeCache(Long accountId) {
        String cacheKey = buildCacheKey(accountId);
        cacheTemplate.delete(cacheKey);
    }
}
