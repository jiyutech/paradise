package tech.jiyu.paradise.core.canal.handlers;

import com.alibaba.otter.canal.protocol.CanalEntry;
import tech.jiyu.paradise.core.privileges.service.PrivilegeService;
import tech.jiyu.utility.cache.CacheTemplate;
import tech.jiyu.utility.canal.CacheExpirationRowChangeHandler;
import tech.jiyu.utility.exception.ExceptionNotificationService;

public class PrivilegeCacheRowChangeHandler extends CacheExpirationRowChangeHandler {

    public PrivilegeCacheRowChangeHandler(CacheTemplate cacheTemplate, ExceptionNotificationService exceptionNotificationService) {
        super("privileges", cacheTemplate, exceptionNotificationService);
    }

    @Override
    protected void doExpireCaches(CanalEntry.RowChange row) {
        super.expireCachesByIds(row, PrivilegeService.CACHE_PREFIX);
    }
}
