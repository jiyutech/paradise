package tech.jiyu.paradise.core.account.domain;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Created By WorkHistory
 *
 * Date 2017/12/27 10:03
 *
 * @author SimonChan;emailAddress: simonchan@jiyu.tech
 */
@NoArgsConstructor
@Getter
@Setter
public class WorkHistory {

    private Long id;

    private Long accountId;

    @NotNull(message = "{account.work.history.company.name.limit}")
    @Size(min = 1,
          max = 128,
          message = "{account.work.history.company.name.limit}")
    private String companyName;

    @NotNull(message = "{account.work.history.job.title.limit}")
    @Size(min = 1,
          max = 128,
          message = "{account.work.history.job.title.limit}")
    private String jobTitle;

}
