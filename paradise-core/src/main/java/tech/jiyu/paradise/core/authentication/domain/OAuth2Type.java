package tech.jiyu.paradise.core.authentication.domain;

/**
 * @author <a href="mailto:chenshao@dekeapp.com">shawsmith</a>
 */
public enum OAuth2Type {

    WECHAT_TINY_APP("wechat_tinyapp"),

    WECHAT("wechat"),

    WECHAT_H5_APP("wechat_h5_app"),

    QQ("qq"),

    FACEBOOK("facebook"),

    POLY_ADMIN("poly_admin"),

    APPLE("apple");


    OAuth2Type(String code) {
        this.code = code;
    }

    public static OAuth2Type fromCode(String code) {
        for (OAuth2Type type : values()) {
            if (type.getCode().equals(code)) {
                return type;
            }
        }
        return null;
    }

    public String getCode() {
        return code;
    }

    private final String code;
}
