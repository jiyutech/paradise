package tech.jiyu.paradise.core.account.domain;

import java.util.Map;

/**
 * @author <a href="mailto:chenshao@dekeapp.com">shawsmith</a>
 */
public class AccountPreCreatedEvent extends AccountEvent {

    private final Account account;
    private final Map<String, Object> additionalAttributes;

    public AccountPreCreatedEvent(Account account, Map<String, Object> additionalAttributes) {
        this.account = account;
        this.additionalAttributes = additionalAttributes;
    }

    public Account getAccount() {
        return account;
    }

    public Map<String, Object> getAdditionalAttributes() {
        return additionalAttributes;
    }
}
