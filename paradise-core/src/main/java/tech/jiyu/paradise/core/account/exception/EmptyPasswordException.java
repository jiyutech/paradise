package tech.jiyu.paradise.core.account.exception;

import tech.jiyu.utility.api.errors.annotations.ApiErrorKey;

/**
 * @author <a href="mailto:chenshao@dekeapp.com">shawsmith</a>
 */
@ApiErrorKey(group = "account",
             id = "empty.password")
public class EmptyPasswordException extends RuntimeException {

    public EmptyPasswordException() {
    }

    public EmptyPasswordException(String message) {
        super(message);
    }

    public EmptyPasswordException(String message, Throwable cause) {
        super(message, cause);
    }

    public EmptyPasswordException(Throwable cause) {
        super(cause);
    }
}
