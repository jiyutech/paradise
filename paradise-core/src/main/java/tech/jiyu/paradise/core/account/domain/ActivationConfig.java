package tech.jiyu.paradise.core.account.domain;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author <a href="mailto:chenshao@dekeapp.com">shawsmith</a>
 */
@NoArgsConstructor
@Getter
@Setter
public class ActivationConfig {

    private Boolean needActivation;
    private String returnUrl;
}
