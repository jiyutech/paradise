package tech.jiyu.paradise.core.privileges.dao.po;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author <a href="mailto:chenshao@jinjie.tech">shawsmith</a>
 */
@NoArgsConstructor
@Getter
@Setter
public class AccountPrivilegePO {

    private Long id;
    private Long createdDate;
    private Long modifiedDate;
    private Long accountId;
    private Long privilegeId;

    public static AccountPrivilegePO create(Long id, Long accountId, Long accountPrivilegeId) {
        AccountPrivilegePO accountPrivilegePO = new AccountPrivilegePO();

        accountPrivilegePO.setId(id);
        Long now = System.currentTimeMillis();
        accountPrivilegePO.setCreatedDate(now);
        accountPrivilegePO.setModifiedDate(now);
        accountPrivilegePO.setAccountId(accountId);
        accountPrivilegePO.setPrivilegeId(accountPrivilegeId);

        return accountPrivilegePO;
    }
}
