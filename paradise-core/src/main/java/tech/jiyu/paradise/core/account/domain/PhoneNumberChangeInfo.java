package tech.jiyu.paradise.core.account.domain;

import javax.validation.constraints.NotNull;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import tech.jiyu.captcha.domain.CaptchaCodeKey;
import tech.jiyu.utility.phonenumber.PhoneNumberUtils;

/**
 * @author <a href="mailto:chenshao@dekeapp.com">shawsmith</a>
 */
@NoArgsConstructor
@Getter
@Setter
public class PhoneNumberChangeInfo {

    @NotNull
    private Long accountId;

    @NotNull(message = "{account.change.phone.new.phone.number}")
    private String newPhoneNumber;

    @NotNull(message = "{account.change.phone.captcha.code}")
    private String captchaCode;

    private String password;

    private String countryCode;

    public CaptchaCodeKey buildCaptchaCodeKey(Long companyId) {
        return new CaptchaCodeKey(companyId, PhoneNumberUtils.filterNonDigitCharacters(getCountryCode()), PhoneNumberUtils.filterNonDigitCharacters(getNewPhoneNumber()));
    }
}
