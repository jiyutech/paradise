package tech.jiyu.paradise.core.account.enumtype;

public enum ApiAccessEnumType {

    POST_ACCOUNT("创建账号", ApiAuthenticationStrategy.DENY),
    DELETE_ACCOUNT("删除账号", ApiAuthenticationStrategy.SELF),
    PATCH_ACCOUNT("更新账号", ApiAuthenticationStrategy.SELF),
    PATCH_ACCOUNT_PHONE_NUMBER("变更账号的手机号", ApiAuthenticationStrategy.SELF),
    PATCH_ACCOUNT_PASSWORD("修改账号密码", ApiAuthenticationStrategy.SELF),
    PUT_ACCOUNT_RESET_PASSWORD("重置账号密码", ApiAuthenticationStrategy.ALLOW),
    POST_ACCOUNT_RESET_PASSWORD("发送重置账号密码的邮件", ApiAuthenticationStrategy.DENY),
    PUT_ACCOUNT_RESET_PASSWORD_FROM_EMAIL("通过邮件重置账号密码", ApiAuthenticationStrategy.DENY),
    PATCH_ACCOUNT_VERIFICATION("账号实名认证", ApiAuthenticationStrategy.DENY),
    GET_ACCOUNT_LIST("查询账号列表", ApiAuthenticationStrategy.ALLOW),
    GET_ACCOUNT_DETAIL("查询账号详情", ApiAuthenticationStrategy.SELF),
    GET_ACCOUNT_CURRENT("根据loginToken查询当前账号信息", ApiAuthenticationStrategy.ALLOW),
    GET_ACCOUNT_HISTORY_NUMBER("查询账号的历史手机号", ApiAuthenticationStrategy.DENY),
    GET_ACCOUNT_ACTIVE("激活账号", ApiAuthenticationStrategy.DENY),
    GET_ACCOUNT_LOGIN_INFO("获取账号登录信息", ApiAuthenticationStrategy.DENY),
    GET_ACCOUNT_REGISTRATION_INFO("获取账号注册信息", ApiAuthenticationStrategy.ALLOW),
    DELETE_ACCOUNT_LOGIN_INFO("删除账号登录信息", ApiAuthenticationStrategy.DENY),
    DELETE_ACCOUNT_DELETING_TASK("删除账号删除任务", ApiAuthenticationStrategy.SELF),
    POST_ACCOUNT_BIND_CODE("创建绑定鉴权码", ApiAuthenticationStrategy.SELF)
    ;

    private String desc;
    private ApiAuthenticationStrategy strategy;

    ApiAccessEnumType(String desc, ApiAuthenticationStrategy strategy) {
        this.desc = desc;
        this.strategy = strategy;
    }

    public ApiAuthenticationStrategy getStrategy() {
        return this.strategy;
    }

}
