package tech.jiyu.paradise.core.account.service.impl;

import lombok.AllArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import tech.jiyu.paradise.core.account.domain.Account;
import tech.jiyu.paradise.core.account.exception.DuplicatedEmailException;
import tech.jiyu.paradise.core.account.exception.EmptyPasswordException;
import tech.jiyu.paradise.core.account.service.AccountEmailAndPasswordValidatorService;
import tech.jiyu.paradise.core.account.service.AccountService;

import java.util.Objects;

@AllArgsConstructor
public class AccountPasswordValidatorServiceImpl implements AccountEmailAndPasswordValidatorService {

    private AccountService accountService;

    @Override
    public void checkPasswordEmpty(Account account) {
        // 如果用户名和密码有任意一个不为空, 则密码不能为空.
        boolean usernameAndPhoneNumberAndEmailAllEmpty = StringUtils.isBlank(account.getUsername()) && StringUtils.isBlank(account.getEmail());
        if (!usernameAndPhoneNumberAndEmailAllEmpty && StringUtils.isBlank(account.getPassword())) {
            throw new EmptyPasswordException();
        }
    }

    @Override
    public void checkEmailDuplicated(Long accountId, Long companyId, String email) {
        Account account = accountService.findByEmail(companyId, email);
        if (account != null && !Objects.equals(accountId, account.getId())) {
            throw new DuplicatedEmailException(email);
        }
    }
}
