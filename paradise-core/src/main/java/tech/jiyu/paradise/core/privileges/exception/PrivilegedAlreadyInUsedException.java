package tech.jiyu.paradise.core.privileges.exception;

import tech.jiyu.utility.api.errors.annotations.ApiErrorKey;

/**
 * @author <a href="mailto:chenshao@jinjie.tech">shawsmith</a>
 */
@ApiErrorKey(group = "account",
             id = "privilege.already.in.used")
public class PrivilegedAlreadyInUsedException extends RuntimeException {
}
