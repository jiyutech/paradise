package tech.jiyu.paradise.core.account.dao.po;

import lombok.Data;

@Data
public class AccountDeletingTaskPO {
    private Long id;
    private Long createdDate;
    private Long modifiedDate;
    private Boolean deleted;
    private Long deletedTime;
    private Long accountId;
    private Boolean hasCompleted;
    private Long completedTime;
    private Long accountEstimateDeletingTime; // 账号预计删除时间
}
