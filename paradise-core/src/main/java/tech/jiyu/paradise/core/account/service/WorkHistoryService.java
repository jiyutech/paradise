package tech.jiyu.paradise.core.account.service;

import org.springframework.validation.annotation.Validated;

import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import tech.jiyu.paradise.core.account.domain.WorkHistory;
import tech.jiyu.utility.cache.CacheStrategy;

/**
 * Created By WorkHistoryService
 *
 * Date 2017/12/27 20:07
 *
 * @author SimonChan;emailAddress: simonchan@jiyu.tech
 */
@Validated
public interface WorkHistoryService {

    String CACHE_PREFIX = AccountService.CACHE_PREFIX + "work_histories:";

    List<WorkHistory> saveOrUpdate(@NotNull Long accountId, @NotNull @Valid List<WorkHistory> histories);

    List<WorkHistory> findByAccountId(Long accountId, CacheStrategy strategy);

    void deleteByAccountId(Long accountId);
}
