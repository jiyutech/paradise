package tech.jiyu.paradise.core.authentication.dao.mapper;

import org.apache.ibatis.annotations.Mapper;

import tech.jiyu.paradise.core.authentication.domain.AuthenticationFailedRecord;

/**
 * @author Simon [simonchan@jinjie.tech]
 */
@Mapper
public interface AuthenticationFailedRecordMapper {

    void insertOrIncrement(AuthenticationFailedRecord record);

    void deleteByAccountId(Long accountId);

    AuthenticationFailedRecord selectByAccountId(Long accountId);
}
