package tech.jiyu.paradise.core.account.service;

import tech.jiyu.paradise.core.account.domain.AccountRegistrationInfo;

public interface AccountRegistrationInfoService {
    AccountRegistrationInfo getByCountryCodeAndPhoneNumber(String countryCode, String phoneNumber, Long companyId);
}
