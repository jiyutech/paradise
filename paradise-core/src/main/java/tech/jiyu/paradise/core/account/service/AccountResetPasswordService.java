package tech.jiyu.paradise.core.account.service;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import tech.jiyu.paradise.core.account.domain.Account;
import tech.jiyu.paradise.core.account.domain.MailPasswordResetInfo;

/**
 * Created By AccountRestPasswordService
 *
 * @author Xhh52
 */
public interface AccountResetPasswordService {

    void sendCaptchaCodeToEmail(Account account);

    Account resetPasswordFromEmail(@NotNull Long operatorId, @NotNull @Valid MailPasswordResetInfo mailPasswordResetInfo);
}
