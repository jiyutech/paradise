package tech.jiyu.paradise.core.authentication.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;

import java.util.function.Function;

import tech.jiyu.paradise.core.authentication.dao.mapper.OAuth2AuthenticationMapper;
import tech.jiyu.paradise.core.authentication.dao.po.OAuth2AuthenticationPO;
import tech.jiyu.paradise.core.authentication.domain.OAuth2Authentication;
import tech.jiyu.paradise.core.authentication.domain.OAuth2Type;
import tech.jiyu.paradise.core.authentication.service.OAuth2AuthenticationService;
import tech.jiyu.sequences.SequenceService;

/**
 * @author <a href="mailto:chenshao@dekeapp.com">shawsmith</a>
 */
@Service
public class OAuth2AuthenticationServiceImpl implements OAuth2AuthenticationService {

    @Autowired
    private OAuth2AuthenticationMapper oAuth2AuthenticationMapper;

    @Autowired
    private SequenceService sequenceService;

    @Override
    @Transactional
    @Validated
    public OAuth2Authentication saveOrUpdate(OAuth2Authentication authentication) {
        OAuth2AuthenticationPO existed = oAuth2AuthenticationMapper.selectByAccountIdAndTypeCode(authentication.getAccountId(), authentication.getType().getCode());
        if (existed == null) {
            doSave(authentication);
        } else {
            doUpdate(existed, authentication);
        }
        return authentication;
    }

    private void doSave(OAuth2Authentication authentication) {
        OAuth2AuthenticationPO authenticationPO = OAuth2AuthenticationPO.create(sequenceService.nextLongValue(), authentication);
        oAuth2AuthenticationMapper.insert(authenticationPO);
        authentication.setId(authenticationPO.getId());
    }

    private void doUpdate(OAuth2AuthenticationPO existed, OAuth2Authentication authentication) {
        existed.setAccountId(authentication.getAccountId());

        OAuth2Type type = authentication.getType();
        if (type != null) {
            existed.setType(type.getCode());
        }

        existed.setAccessToken(authentication.getAccessToken());
        existed.setRefreshToken(authentication.getRefreshToken());
        existed.setExpiresIn(authentication.getExpiresIn());
        existed.setScope(authentication.getScope());

        oAuth2AuthenticationMapper.updateById(existed);
        authentication.setId(existed.getId());
    }

    @Override
    @Transactional
    public void deleteByAccountId(Long accountId) {
        oAuth2AuthenticationMapper.deleteByAccountId(accountId);
    }

    @Override
    public OAuth2Authentication findByAccountIdAndOAuth2Type(Long accountId, OAuth2Type type) {
        OAuth2AuthenticationPO oAuth2AuthenticationPO = oAuth2AuthenticationMapper.selectByAccountIdAndTypeCode(accountId, type.getCode());
        return transformer().apply(oAuth2AuthenticationPO);
    }

    private Function<OAuth2AuthenticationPO, OAuth2Authentication> transformer() {
        return po -> {
            if (po == null) {
                return null;
            }
            OAuth2Authentication oAuth2Authentication = new OAuth2Authentication();
            oAuth2Authentication.setType(OAuth2Type.fromCode(po.getType()));
            oAuth2Authentication.setScope(po.getScope());
            oAuth2Authentication.setRefreshToken(po.getRefreshToken());
            oAuth2Authentication.setExpiresIn(po.getExpiresIn());
            oAuth2Authentication.setAccountId(po.getAccountId());
            oAuth2Authentication.setId(po.getId());
            oAuth2Authentication.setAccessToken(po.getAccessToken());
            return oAuth2Authentication;
        };
    }


}
