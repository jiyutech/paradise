package tech.jiyu.paradise.core.authentication.domain;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author <a href="mailto:chenshao@dekeapp.com">shawsmith</a>
 */
@NoArgsConstructor
@Getter
@Setter
public class OAuth2Authentication {

    private Long id;

    @NotNull
    private Long accountId;

    @NotNull
    private OAuth2Type type;

    @NotNull
    @Size(min = 1, max = 200)
    private String accessToken;

    @NotNull
    @Size(min = 1, max = 200)
    private String refreshToken;

    private Long expiresIn;

    @Size(max = 128)
    private String scope;

}
