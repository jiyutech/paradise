package tech.jiyu.paradise.core.privileges.dao.mapper;

import org.apache.ibatis.annotations.Mapper;

import java.util.Collection;
import java.util.List;

import tech.jiyu.paradise.core.privileges.dao.po.PrivilegePO;
import tech.jiyu.restful.api.domain.RestfulApiMapper;

/**
 * @author <a href="mailto:chenshao@jinjie.tech">shawsmith</a>
 */
@Mapper
public interface PrivilegeMapper extends RestfulApiMapper<PrivilegePO> {

    PrivilegePO selectByName(String name);

    List<PrivilegePO> selectByNames(Collection<String> names);
}
