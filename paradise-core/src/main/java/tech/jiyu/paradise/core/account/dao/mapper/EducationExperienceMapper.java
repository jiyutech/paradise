package tech.jiyu.paradise.core.account.dao.mapper;

import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Set;

import tech.jiyu.paradise.core.account.dao.po.EducationExperiencePO;

/**
 * Created By EducationExperienceMapper
 *
 * Date 2017/12/27 18:28
 *
 * @author SimonChan;emailAddress: simonchan@jiyu.tech
 */
@Mapper
public interface EducationExperienceMapper {

    void batchSave(List<EducationExperiencePO> educationExperiencePOS);

    void deleteByAccountId(Long accountId);

    List<EducationExperiencePO> selectByAccountId(Long accountId);

    List<EducationExperiencePO> selectByIds(Set<Long> experienceIds);

    void updateById(EducationExperiencePO educationExperiencePO);
}
