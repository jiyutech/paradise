package tech.jiyu.paradise.core.authentication.domain;

import lombok.Data;

/**
 * @author Simon [simonchan@jinjie.tech]
 */
@Data
public class AuthenticationFailedRecord {

    private Long id;
    private Long createdDate;
    private Long modifiedDate;
    private Long accountId;
    private Integer count;
}
