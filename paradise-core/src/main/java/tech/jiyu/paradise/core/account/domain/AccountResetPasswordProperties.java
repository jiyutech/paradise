package tech.jiyu.paradise.core.account.domain;

import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * Created By AccountResetPasswordProperties
 *
 * @author Xhh52
 */
@ConfigurationProperties(prefix = AccountResetPasswordProperties.PREFIX)
public class AccountResetPasswordProperties extends MailProperties {

    public static final String PREFIX = "paradise.account.reset";
}
