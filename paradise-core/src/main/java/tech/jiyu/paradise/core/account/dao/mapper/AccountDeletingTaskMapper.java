package tech.jiyu.paradise.core.account.dao.mapper;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import tech.jiyu.paradise.core.account.dao.po.AccountDeletingTaskPO;
import tech.jiyu.utility.web.TimeRange;

import java.util.Collection;
import java.util.List;

@Mapper
public interface AccountDeletingTaskMapper {

    void insert(AccountDeletingTaskPO po);

    List<AccountDeletingTaskPO> selectByAccountIds(@Param("accountIds") Collection<Long> accountIdList);

    void updateByAccountId(AccountDeletingTaskPO po);

    List<AccountDeletingTaskPO> selectByTimeRange(@Param("timeRange") TimeRange timeRange);

    AccountDeletingTaskPO selectByAccountId(@Param("accountId") Long accountId);
}
