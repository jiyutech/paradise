package tech.jiyu.paradise.core.account.exception;

import tech.jiyu.utility.api.errors.annotations.ApiErrorKey;

/**
 * Created By InvitationCodeNotFountException
 *
 * Date 2017/12/26 11:47
 *
 * @author SimonChan;emailAddress: simonchan@jiyu.tech
 */
@ApiErrorKey(group = "account",
             id = "invitation.code.not.found")
public class InvitationCodeNotFoundException extends RuntimeException {

    public InvitationCodeNotFoundException() {
    }

    public InvitationCodeNotFoundException(String message) {
        super(message);
    }

    public InvitationCodeNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }

    public InvitationCodeNotFoundException(Throwable cause) {
        super(cause);
    }

    public InvitationCodeNotFoundException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
