package tech.jiyu.paradise.core.account.service.impl;

import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Objects;
import java.util.function.Function;
import java.util.stream.Collectors;

import tech.jiyu.paradise.core.account.dao.mapper.InvitationCodeMapper;
import tech.jiyu.paradise.core.account.dao.po.InvitationCodePO;
import tech.jiyu.paradise.core.account.domain.InvitationCode;
import tech.jiyu.paradise.core.account.domain.InvitationCodeListQuery;
import tech.jiyu.paradise.core.account.domain.InvitationCodeStatus;
import tech.jiyu.paradise.core.account.exception.InvitationCodeNotFoundException;
import tech.jiyu.paradise.core.account.service.InvitationCodeService;
import tech.jiyu.utility.pagination.PaginationCallback;
import tech.jiyu.utility.pagination.PaginationConfig;
import tech.jiyu.utility.pagination.PaginationResult;
import tech.jiyu.utility.pagination.PaginationTemplate;

/**
 * Created By InvitationCodeServiceImpl
 *
 * Date 2018/2/9 16:24
 *
 * @author SimonChan;emailAddress: simonchan@jiyu.tech
 */
@Service
public class InvitationCodeServiceImpl implements InvitationCodeService {

    @Autowired
    private InvitationCodeMapper invitationCodeMapper;

    @Autowired
    private PaginationTemplate paginationTemplate;

    @Override
    @Transactional
    public InvitationCode generateInvitationCode() {
        String code;
        while (true) {
            code = RandomStringUtils.randomNumeric(6);
            InvitationCodePO invitationCodePO = invitationCodeMapper.selectByCode(code);
            if (invitationCodePO == null) {
                break;
            }
        }
        InvitationCode invitationCode = build(code);
        InvitationCodePO invitationCodePO = InvitationCodePO.create(invitationCode);
        invitationCodeMapper.insert(invitationCodePO);
        return invitationCode;
    }

    @Override
    @SuppressWarnings("unchecked")
    public PaginationResult<InvitationCode, Void> list(InvitationCodeListQuery invitationCodeListQuery) {
        PaginationCallback<InvitationCodePO, Void> paginationCallback = new PaginationCallback<InvitationCodePO, Void>() {
            @Override
            public Integer countRecords() {
                return invitationCodeMapper.countByQuery(invitationCodeListQuery);
            }

            @Override
            public List<InvitationCodePO> loadRecords(PaginationConfig paginationConfig) {
                return invitationCodeMapper.selectByQuery(invitationCodeListQuery);
            }

            @Override
            public Void getStatistics(List<InvitationCodePO> list) {
                return null;
            }
        };
        PaginationResult<InvitationCodePO, Void> result = paginationTemplate.query(invitationCodeListQuery.getPaginationConfig(), paginationCallback);
        return (new PaginationResult.Builder<InvitationCode, Void>())
                .setTotalPages(result.getTotalPages())
                .setTotalRecords(result.getTotalRecords())
                .setPageSize(result.getPageSize())
                .setCurrentPage(result.getCurrentPage())
                .setStatistics(result.getStatistics())
                .setResultSet(result.getResultSet().stream().filter(Objects::nonNull).map(transformer()).collect(Collectors.toList())).build();
    }

    @Override
    public InvitationCode selectByCode(String invitationCode) {
        InvitationCodePO invitationCodePO = invitationCodeMapper.selectByCode(invitationCode);
        return transformer().apply(invitationCodePO);
    }

    @Override
    public InvitationCode updateByCode(InvitationCode invitationCode) {
        InvitationCodePO invitationCodePO = invitationCodeMapper.selectByCode(invitationCode.getCode());
        if (invitationCodePO == null) {
            throw new InvitationCodeNotFoundException(invitationCode.getCode());
        }
        updateEditableProperties(invitationCode,invitationCodePO);
        invitationCodeMapper.updateByCode(invitationCodePO);
        return invitationCode;
    }

    private void updateEditableProperties(InvitationCode from , InvitationCodePO to) {
        to.setStatus(from.getStatus().getCode());
        to.setAccountId(from.getAccountId());
    }

    private Function<InvitationCodePO, InvitationCode> transformer() {
        return invitationCodePO -> {
            if (invitationCodePO == null) {
                return null;
            }

            InvitationCode invitationCode = new InvitationCode();
            invitationCode.setCode(invitationCodePO.getCode());
            invitationCode.setStatus(InvitationCodeStatus.fromCode(invitationCodePO.getStatus()));
            invitationCode.setAccountId(invitationCodePO.getAccountId());

            return invitationCode;
        };
    }

    private InvitationCode build(String code) {
        InvitationCode invitationCode = new InvitationCode();
        invitationCode.setCode(code);
        invitationCode.setStatus(InvitationCodeStatus.UN_USED);
        return invitationCode;
    }
}
