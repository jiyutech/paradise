package tech.jiyu.paradise.core.conf.mybatis.spi;

import com.google.common.collect.Sets;

import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;

import java.util.Set;

import tech.jiyu.utility.mybatis.MyBatisConfigurer;

/**
 * @author <a href="mailto:chenshao@dekeapp.com">shawsmith</a>
 */
public class ParadiseMyBatisConfigurer implements MyBatisConfigurer {

    @Override
    public boolean supports(String databaseName) {
        Set<String> excludedDataSourceNames = Sets.newHashSet("baisonDataSource", "jlhDataSource");
        return !excludedDataSourceNames.contains(databaseName);
    }

    @Override
    public Resource[] getMapperLocations() {
        return new Resource[]{
                new ClassPathResource("/mappers/AccountHistoryPhoneNumberMapper.xml"),
                new ClassPathResource("/mappers/AccountMapper.xml"),
                new ClassPathResource("/mappers/AccountPrivilegeMapper.xml"),
                new ClassPathResource("/mappers/EducationExperienceMapper.xml"),
                new ClassPathResource("/mappers/LoginMapper.xml"),
                new ClassPathResource("/mappers/OAuth2AuthenticationMapper.xml"),
                new ClassPathResource("/mappers/PrivilegeMapper.xml"),
                new ClassPathResource("/mappers/ResidenceMapper.xml"),
                new ClassPathResource("/mappers/WorkHistoryMapper.xml"),
                new ClassPathResource("/mappers/InvitationCodeMapper.xml"),
                new ClassPathResource("/mappers/AuthenticationFailedRecordMapper.xml"),
                new ClassPathResource("/mappers/AccountDeletingTaskMapper.xml"),
                new ClassPathResource("/mappers/AccountPhoneNumberVerificationResultMapper.xml"),
        };
    }

    @Override
    public String[] getAliasPackages() {
        return new String[]{
                "tech.jiyu.paradise.core.account.dao.po",
                "tech.jiyu.paradise.core.authentication.dao.po",
                "tech.jiyu.paradise.core.login.dao.po",
                "tech.jiyu.paradise.core.privileges.dao.po"
        };
    }

    @Override
    public String[] getScannerBasePackages() {
        return new String[]{
                "tech.jiyu.paradise.core.account.dao.mapper",
                "tech.jiyu.paradise.core.authentication.dao.mapper",
                "tech.jiyu.paradise.core.login.dao.mapper",
                "tech.jiyu.paradise.core.privileges.dao.mapper"
        };
    }
}
