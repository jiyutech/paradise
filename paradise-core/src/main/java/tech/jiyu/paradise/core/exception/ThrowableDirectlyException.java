package tech.jiyu.paradise.core.exception;

/**
 * 直接抛出到客户端的异常，主要用于错误信息可直接给客户看到的场景，比如支付过程中，可能会存在有货币不支持或者其他错误
 */
public class ThrowableDirectlyException extends RuntimeException {

    private String code;

    public ThrowableDirectlyException(String message) {
        super(message);
    }

    public String getCode() {
        return this.code;
    }
}
