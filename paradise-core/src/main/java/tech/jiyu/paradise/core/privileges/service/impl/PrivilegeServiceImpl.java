package tech.jiyu.paradise.core.privileges.service.impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.Lists;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tech.jiyu.paradise.core.privileges.dao.mapper.PrivilegeMapper;
import tech.jiyu.paradise.core.privileges.dao.po.PrivilegePO;
import tech.jiyu.paradise.core.privileges.domain.Privilege;
import tech.jiyu.paradise.core.privileges.exception.DuplicatedPrivilegeNameException;
import tech.jiyu.paradise.core.privileges.exception.PrivilegedAlreadyInUsedException;
import tech.jiyu.paradise.core.privileges.service.AccountPrivilegeService;
import tech.jiyu.paradise.core.privileges.service.PrivilegeService;
import tech.jiyu.restful.api.service.AbstractRestfulApi;
import tech.jiyu.sequences.SequenceService;
import tech.jiyu.utility.cache.CacheTemplate;
import tech.jiyu.utility.cache.CacheUtils;
import tech.jiyu.utility.pagination.PaginationTemplate;

import java.util.Collection;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.function.Function;

/**
 * @author <a href="mailto:chenshao@jinjie.tech">shawsmith</a>
 */
@Service
public class PrivilegeServiceImpl extends AbstractRestfulApi<Privilege, Void, PrivilegePO> implements PrivilegeService {

    @Autowired
    private PrivilegeMapper privilegeMapper;

    @Autowired
    private AccountPrivilegeService accountPrivilegeService;

    private static Function<PrivilegePO, Privilege> transformer() {
        return (privilegePO) -> {
            if (privilegePO == null) {
                return null;
            }

            Privilege privilege = new Privilege();
            privilege.setId(privilegePO.getId());
            privilege.setName(privilegePO.getName());
            privilege.setDescription(privilegePO.getDescription());
            privilege.setOrderNumber(privilegePO.getOrderNumber());
            return privilege;
        };
    }

    public PrivilegeServiceImpl(PrivilegeMapper privilegeMapper, SequenceService sequenceService, PaginationTemplate paginationTemplate, ObjectMapper objectMapper, CacheTemplate cacheTemplate) {
        super(transformer(), privilegeMapper, sequenceService, paginationTemplate, objectMapper, cacheTemplate);
    }

    @Override
    public PrivilegePO build(SequenceService sequenceService, Privilege privilege, ObjectMapper objectMapper) {
        return PrivilegePO.create(sequenceService.nextLongValue(), privilege);
    }


    @Override
    protected Object buildCacheKey(Long id) {
        return CacheUtils.buildCacheKey(CACHE_PREFIX, id);
    }

    @Override
    protected Long parseRawKey(Object cacheKey) {
        return CacheUtils.parseId(cacheKey, CACHE_PREFIX);
    }

    @Override
    protected void onModelPreCreated(Privilege privilege) {
        checkNameDuplicated(privilege.getName(), null);
    }

    @Override
    protected void onModelPrePatched(Privilege privilege, PrivilegePO privilegePO) {
        String name = Optional.ofNullable(privilege.getName()).orElse(privilegePO.getName());
        checkNameDuplicated(name, privilegePO.getId());
    }

    private void checkNameDuplicated(String name, Long id) {
        PrivilegePO existed = privilegeMapper.selectByName(name);
        if ((existed != null) && (id == null || !Objects.equals(id, existed.getId()))) {
            throw new DuplicatedPrivilegeNameException();
        }
    }

    @Override
    protected void onModelPreDeleted(PrivilegePO privilegePO) {
        ensurePrivilegeNotUsed(privilegePO);
    }

    private void ensurePrivilegeNotUsed(PrivilegePO privilegePO) {
        if (privilegePO == null) {
            return;
        }

        Long privilegeId = privilegePO.getId();
        ensureAccountPrivilegeNotUsed(privilegeId);
    }

    private void ensureAccountPrivilegeNotUsed(Long privilegeId) {
        Long useCount = accountPrivilegeService.countByPrivilegeId(privilegeId);
        if (useCount != null && useCount > 0L) {
            throw new PrivilegedAlreadyInUsedException();
        }
    }

    @Override
    public Privilege findByName(String name) {
        if (StringUtils.isBlank(name)) {
            return null;
        }

        PrivilegePO privilegePO = privilegeMapper.selectByName(name);
        return modelAssembler.assemble(privilegePO);
    }

    @Override
    public List<Privilege> findByNames(Collection<String> names) {
        if (CollectionUtils.isEmpty(names)) {
            return Lists.newArrayListWithCapacity(0);
        }

        List<PrivilegePO> privilegePOs = privilegeMapper.selectByNames(names);
        return modelAssembler.assemble(privilegePOs);
    }
}
