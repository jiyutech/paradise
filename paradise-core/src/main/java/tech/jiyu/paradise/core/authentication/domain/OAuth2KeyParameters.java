package tech.jiyu.paradise.core.authentication.domain;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Collection;

/**
 * CreatedDate 2018-07-10 上午10:39
 *
 * @author SimonChan;emailAddress: simonchan@jinjie.tech
 */
@NoArgsConstructor
@Getter
@Setter
public class OAuth2KeyParameters extends OAuth2ParametersBase {

    private String authType;

    private String loginSource;

    private String authKey;

    private String roleTypeCode;

    private String countryCode;

    private String phoneNumber;

    private String isoCode;

    private Long companyId;

    private String password;

    // 以下这两个参数用于小程序获取union id, countryCode, phoneNumber.
    private String weChatTinyAppEncryptedData;
    private String weChatTinyAppIv;

    // 如果需要获取手机号码, 还需要提供小程序AppId,
    private String weChatTinyAppId;

    private Collection<Long> privilegeIds;

}
