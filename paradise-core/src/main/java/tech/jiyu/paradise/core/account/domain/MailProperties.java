package tech.jiyu.paradise.core.account.domain;

import java.util.concurrent.TimeUnit;

/**
 * Created By AccountMailProperties
 *
 * @author Xhh52
 */
public class MailProperties {

    public static final Long DEFAULT_ACTIVATION_CODE_EXPIRE_TIME = TimeUnit.DAYS.toMillis(1L);

    protected Long codeExpireTime = DEFAULT_ACTIVATION_CODE_EXPIRE_TIME;
    protected String mailSubject;
    protected String mailTemplatePath;
    protected String baseUrl;
    protected String senderAddress;
    protected String projectName;
    protected String signature;

    public Long getCodeExpireTime() {
        return codeExpireTime;
    }

    public void setCodeExpireTime(Long codeExpireTime) {
        this.codeExpireTime = codeExpireTime;
    }

    public String getMailSubject() {
        return mailSubject;
    }

    public void setMailSubject(String mailSubject) {
        this.mailSubject = mailSubject;
    }

    public String getMailTemplatePath() {
        return mailTemplatePath;
    }

    public void setMailTemplatePath(String mailTemplatePath) {
        this.mailTemplatePath = mailTemplatePath;
    }

    public String getBaseUrl() {
        return baseUrl;
    }

    public void setBaseUrl(String baseUrl) {
        this.baseUrl = baseUrl;
    }

    public String getSenderAddress() {
        return senderAddress;
    }

    public void setSenderAddress(String senderAddress) {
        this.senderAddress = senderAddress;
    }

    public String getProjectName() {
        return projectName;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    public String getSignature() {
        return signature;
    }

    public void setSignature(String signature) {
        this.signature = signature;
    }
}
