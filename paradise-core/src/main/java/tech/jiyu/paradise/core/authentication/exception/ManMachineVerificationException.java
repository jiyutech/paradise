package tech.jiyu.paradise.core.authentication.exception;

import org.springframework.security.core.AuthenticationException;

import tech.jiyu.utility.api.errors.annotations.ApiErrorKey;

/**
 * @author Simon [simonchan@jinjie.tech]
 */
@ApiErrorKey(group = "system", id = "man.machine.verification")
public class ManMachineVerificationException extends AuthenticationException {

    public ManMachineVerificationException(String msg, Throwable t) {
        super(msg, t);
    }

    public ManMachineVerificationException(String msg) {
        super(msg);
    }
}
