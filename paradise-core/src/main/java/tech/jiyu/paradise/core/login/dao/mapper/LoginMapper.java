package tech.jiyu.paradise.core.login.dao.mapper;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.Collection;
import java.util.List;

import tech.jiyu.paradise.core.login.dao.po.LoginPO;

/**
 * @author <a href="mailto:chenshao@jiyu.tech">shawsmith</a>
 */
@Mapper
public interface LoginMapper {

    void insert(LoginPO loginPO);

    void updateById(LoginPO loginPO);

    void deleteByToken(String token);

    List<String> selectLoginTokensByLoginId(String loginId);

    LoginPO selectByLoginIdAndSource(@Param("loginId") String loginId, @Param("loginSource") String loginSource);

    Collection<String> selectLoggedIds(Collection<String> loginIds);

    List<LoginPO> selectByLoginTokens(Collection<String> loginTokens);
}
