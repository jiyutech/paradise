package tech.jiyu.paradise.core.account.service.impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.base.Throwables;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.script.RedisScript;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;
import tech.jiyu.captcha.domain.CaptchaCodeKey;
import tech.jiyu.captcha.service.CaptchaCodeMismatchException;
import tech.jiyu.captcha.service.CaptchaService;
import tech.jiyu.idcard.verification.domain.IdCardVerification;
import tech.jiyu.idcard.verification.exception.IdCardNumberAndNameMismatchException;
import tech.jiyu.idcard.verification.service.IdCardVerificationService;
import tech.jiyu.paradise.core.account.dao.mapper.AccountMapper;
import tech.jiyu.paradise.core.account.dao.po.AccountPO;
import tech.jiyu.paradise.core.account.domain.*;
import tech.jiyu.paradise.core.account.exception.*;
import tech.jiyu.paradise.core.account.service.AccountHistoryPhoneNumberService;
import tech.jiyu.paradise.core.account.service.AccountListService;
import tech.jiyu.paradise.core.account.service.AccountService;
import tech.jiyu.paradise.core.account.service.RolePermissionService;
import tech.jiyu.paradise.core.login.service.LoginService;
import tech.jiyu.paradise.core.privileges.service.AccountPrivilegeService;
import tech.jiyu.paradise.core.privileges.service.PrivilegeService;
import tech.jiyu.paradise.core.util.AccountUtils;
import tech.jiyu.paradise.core.util.LoginUtils;
import tech.jiyu.restful.api.domain.Create;
import tech.jiyu.restful.api.domain.PaginationListQuery;
import tech.jiyu.restful.api.service.AbstractRestfulApi;
import tech.jiyu.restful.api.service.ModelAssembler;
import tech.jiyu.restful.api.service.MyBatisMapperModelStorage;
import tech.jiyu.sequences.SequenceService;
import tech.jiyu.utility.cache.CacheStrategy;
import tech.jiyu.utility.cache.CacheTemplate;
import tech.jiyu.utility.cache.CacheUtils;
import tech.jiyu.utility.datastructure.Pair;
import tech.jiyu.utility.pagination.PaginationResult;
import tech.jiyu.utility.pagination.PaginationTemplate;
import tech.jiyu.utility.phonenumber.PhoneNumberUtils;

import javax.validation.groups.Default;
import java.io.IOException;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

/**
 * @author <a href="mailto:chenshao@dekeapp.com">shawsmith</a>
 */
@Service
public class AccountServiceImpl extends AbstractRestfulApi<Account, Map<String, Long>, AccountPO> implements AccountService {

    private static final String BIND_CODE_KEY_PREFIX = CACHE_PREFIX + "bind_code:";

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private AccountMapper accountMapper;

    @Autowired
    private CaptchaService captchaService;

    @Autowired
    private AccountHistoryPhoneNumberService accountHistoryPhoneNumberService;

    @Autowired
    private AccountPrivilegeService accountPrivilegeService;

    @Autowired
    private PrivilegeService privilegeService;

    @Autowired(required = false)
    private AccountListenerSupport accountListenerSupport;

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private LoginService loginService;

    @Autowired(required = false)
    private IdCardVerificationService idCardVerificationService;

    @Autowired
    private AccountListService accountListService;

    @Autowired
    private RolePermissionService rolePermissionService;

    @Autowired
    private RedisTemplate<Object, Object> redisTemplate;

    @Value("${paradise.need.validate.captcha.code:true}")
    private Boolean needValidateCaptchaCode;

    @Value("${paradise.account.password.regx:.*}")
    private String passwordRegx;

    @Value("${paradise.account.password.logout.on.changed:true}")
    private Boolean logout;

    @Value("${paradise.account.password.random.length: 16}")
    private int defaultRandomPasswordLength;

    @Value("${paradise.bind.code.length: 6}")
    private int bindCodeLength;

    @Autowired
    public AccountServiceImpl(@Qualifier("accountModelAssembler") ModelAssembler<Account, AccountPO> accountModelAssembler,
                              AccountMapper accountMapper,
                              SequenceService sequenceService,
                              PaginationTemplate paginationTemplate,
                              ObjectMapper objectMapper,
                              CacheTemplate cacheTemplate) {
        super(accountModelAssembler, new MyBatisMapperModelStorage<>(accountMapper), sequenceService, paginationTemplate, objectMapper, cacheTemplate);
    }

    @Override
    public AccountPO build(SequenceService sequenceService, Account account, ObjectMapper objectMapper) {
        return AccountPO.create(sequenceService.nextLongValue(), account, objectMapper);
    }

    @Override
    public List<Account> findSimpleAccounts(Collection<Long> accountIds, CacheStrategy strategy) {
        List<AccountPO> accountPOs = loadModelPOs(accountIds, strategy);
        return AccountUtils.assembleAccounts(accountPOs);
    }

    @Override
    public List<Account> findBySimpleAccountsByCompanyIds(Collection<Long> companyIds) {
        if (CollectionUtils.isEmpty(companyIds)) {
            return Lists.newArrayListWithCapacity(0);
        }

        List<AccountPO> accountPOs = accountMapper.selectByCompanyIds(companyIds);
        return AccountUtils.assembleAccounts(accountPOs);
    }

    @Override
    @Transactional
    @Validated({Create.class, Default.class})
    public Account register(Account account, ActivationConfig activationConfig, Invitation invitation) {
        account = super.create(account);
        if (accountListenerSupport != null) {
            accountListenerSupport.fireAccountPostCreated(account, account.getAdditionalProperties(), activationConfig, invitation);
        }
        return account;
    }

    @Override
    @Transactional
    public Account patch(Account account) {
        Map<String, Object> properties = account.populatePatchableProperties(objectMapper);
        Map<String, Object> additionalProperties = account.getAdditionalProperties();
        if ((properties == null || properties.isEmpty()) && isAdditionalPropertiesOrPrivilegeIdsPresent(account)) {
            if (accountListenerSupport != null) {
                accountListenerSupport.fireAccountPostModified(account, account, additionalProperties);
            }
        } else {
            return super.patch(account);
        }
        return this.findById(account.getId());
    }

    private boolean isAdditionalPropertiesOrPrivilegeIdsPresent(Account account) {
        if (account == null) {
            return false;
        }

        Map<String, Object> additionalProperties = account.getAdditionalProperties();
        if (additionalProperties != null && !additionalProperties.isEmpty()) {
            return true;
        }

        return account.getPrivilegeIds() != null;
    }

    @Override
    protected void onModelPreCreated(Account account) {
        if (accountListenerSupport != null) {
            accountListenerSupport.fireAccountPreCreated(account, account.getAdditionalProperties());
        }
    }

    @Override
    protected void onModelPreUpdated(Account account, AccountPO accountPO) {
        onModelPrePatched(account, accountPO);
    }

    @Override
    protected void onModelPostUpdated(Account account, AccountPO accountPO) {
        onModelPostPatched(account, accountPO);
    }

    @Override
    protected void onModelPrePatched(Account account, AccountPO accountPO) {
        if (accountListenerSupport != null) {
            accountListenerSupport.fireAccountPreModified(account, account.getAdditionalProperties());
        }
    }

    @Override
    protected void onModelPostPatched(Account account, AccountPO accountPO) {
        if (accountListenerSupport != null) {
            Account oldAccount = AccountUtils.assembleAccount(accountPO,
                    accountHistoryPhoneNumberService,
                    accountPrivilegeService,
                    privilegeService);
            accountListenerSupport.fireAccountPostModified(account,
                    oldAccount,
                    account.getAdditionalProperties());
        }
    }

    @Override
    public Account findByUserName(Long companyId, String username) {
        AccountPO accountPO = accountMapper.selectByUserName(companyId, username);
        return modelAssembler.assemble(accountPO);
    }

    @Override
    public Account findByPhoneNumber(Long companyId, String phoneNumber) {
        AccountPO accountPO = accountMapper.selectByPhoneNumber(companyId, phoneNumber);
        return modelAssembler.assemble(accountPO);
    }

    @Override
    public Account findByCountryCodeAndPhoneNumber(Long companyId, String countryCode, String phoneNumber) {
        AccountPO accountPO = accountMapper.selectByCountryCodeAndPhoneNumber(companyId, countryCode, phoneNumber);
        return modelAssembler.assemble(accountPO);
    }

    @Override
    public Account findByEmail(Long companyId, String email) {
        AccountPO accountPO = accountMapper.selectByEmail(companyId, email);
        return modelAssembler.assemble(accountPO);
    }

    @Override
    public Account findByInvitationCode(Long companyId, String invitationCode) {
        AccountPO accountPO = accountMapper.selectByInvitationCode(companyId, invitationCode);
        return modelAssembler.assemble(accountPO);
    }

    @Override
    @Transactional
    @Validated
    public Account changePhoneNumber(PhoneNumberChangeInfo changeInfo) {
        Long accountId = changeInfo.getAccountId();
        AccountPO accountPO = accountMapper.selectById(accountId);
        if (accountPO == null) {
            throw new NoSuchAccountException(String.valueOf(accountId));
        }

        String newPhoneNumber = PhoneNumberUtils.filterNonDigitCharacters(changeInfo.getNewPhoneNumber());

        // 修改前后为同一个手机号码， 无需修改.
        CaptchaCodeKey captchaCodeKey = changeInfo.buildCaptchaCodeKey(accountPO.getCompanyId());
        if (checkPhoneNumberAreSame(accountPO, changeInfo)) {
            captchaService.invalidate(captchaCodeKey);
            throw new PhoneNumberAreSameException();
        }

        // 如果用户提供了密码, 还需要验证密码是否正确.
        else if (StringUtils.isNotBlank(changeInfo.getPassword()) && !passwordEncoder.matches(changeInfo.getPassword(), accountPO.getPassword())) {
            throw new OldPasswordWrongException();
        }

        // 判断一下手机验证码正确.
        else if (!captchaService.validate(captchaCodeKey, changeInfo.getCaptchaCode()) && needValidateCaptchaCode) {
            throw new CaptchaCodeMismatchException(captchaCodeKey, changeInfo.getCaptchaCode(), captchaService.exchange(captchaCodeKey));
        }

        // 还需要判断新的手机号码是否已被其他账户占用.
        else {
            checkPhoneNumberDuplicated(accountPO.getCompanyId(), changeInfo.getCountryCode(), newPhoneNumber);
            try {
                String oldCountryCode = accountPO.getCountryCode();
                String oldPhoneNumber = accountPO.getPhoneNumber();
                saveHistoryPhoneNumber(accountId, accountPO.getPhoneNumber(), newPhoneNumber);

                Account oldAccount = AccountUtils.assembleAccount(accountPO,
                        accountHistoryPhoneNumberService,
                        accountPrivilegeService,
                        privilegeService);
                if (accountListenerSupport != null) {
                    accountListenerSupport.fireAccountPreModified(oldAccount, null);
                }
                String countryCode = changeInfo.getCountryCode();
                if (StringUtils.isNotBlank(countryCode)) {
                    accountPO.setCountryCode(countryCode);
                }

                accountPO.setPhoneNumber(newPhoneNumber);
                accountMapper.updateById(accountPO);
                AccountUtils.removeAccountCache(accountPO.getId(), cacheTemplate);
                if (Boolean.TRUE.equals(logout)) {
                    LoginUtils.logout(String.valueOf(accountPO.getId()), loginService);
                }

                if (accountListenerSupport != null) {
                    Account account = AccountUtils.assembleAccount(accountPO,
                            accountHistoryPhoneNumberService,
                            accountPrivilegeService,
                            privilegeService);
                    accountListenerSupport.fireAccountPostModified(account,
                            oldAccount,
                            null);
                    accountListenerSupport.fireAccountPhoneNumberChange(accountId,
                            oldCountryCode,
                            oldPhoneNumber,
                            countryCode,
                            newPhoneNumber);
                }

            } catch (Exception e) {
                if (e instanceof RuntimeException) {
                    throw new RuntimeException(e);
                } else {
                    throw new RuntimeException(Throwables.getStackTraceAsString(e), e);
                }
            }
        }

        return modelAssembler.assemble(accountPO);
    }

    private boolean checkPhoneNumberAreSame(AccountPO oldAccount, PhoneNumberChangeInfo changeInfo) {
        String oldPhoneNumber = oldAccount.getCountryCode() != null ? oldAccount.getCountryCode() + oldAccount.getPhoneNumber() : oldAccount.getPhoneNumber();
        String newPhoneNumber = changeInfo.getCountryCode() != null ? changeInfo.getCountryCode() + changeInfo.getNewPhoneNumber() : changeInfo.getNewPhoneNumber();
        oldPhoneNumber = PhoneNumberUtils.filterNonDigitCharacters(oldPhoneNumber);
        newPhoneNumber = PhoneNumberUtils.filterNonDigitCharacters(newPhoneNumber);
        return StringUtils.equals(oldPhoneNumber, newPhoneNumber);
    }

    private void checkPhoneNumberDuplicated(Long companyId, String countryCode, String phoneNumber) {
        if (this.findByPhoneNumber(companyId, countryCode, phoneNumber) != null) {
            String message = "companyId: " + companyId + "-" + countryCode + "-" + phoneNumber;
            throw new DuplicatedPhoneNumberException(countryCode, phoneNumber, message);
        }
    }

    private void saveHistoryPhoneNumber(Long accountId, String oldPhoneNumber, String newPhoneNumber) {
        AccountHistoryPhoneNumber accountHistoryPhoneNumber = new AccountHistoryPhoneNumber();
        accountHistoryPhoneNumber.setAccountId(accountId);
        accountHistoryPhoneNumber.setOldPhoneNumber(oldPhoneNumber);
        accountHistoryPhoneNumber.setNewPhoneNumber(newPhoneNumber);
        accountHistoryPhoneNumber.setChangeDate(System.currentTimeMillis());
        accountHistoryPhoneNumberService.save(accountHistoryPhoneNumber);
    }

    @Override
    @Validated
    public PaginationResult<Account, Map<String, Long>> list(PaginationListQuery query) {
        return accountListService.getAccounts((AccountListQuery) query);
    }

    @Override
    public Account findByWeChatOpenId(Long companyId, String openId) {
        AccountPO accountPO = accountMapper.selectByWeChatOpenId(companyId, openId);
        return modelAssembler.assemble(accountPO);
    }

    @Override
    @Transactional
    public void unbindWeChatOpenId(Long companyId, String openId) {
        AccountPO accountPO = accountMapper.selectByWeChatOpenId(companyId, openId);
        if (accountPO != null) {
            Account account = modelAssembler.assemble(accountPO);
            onModelPrePatched(account, accountPO);

            accountPO.setWeChatOpenId(null);
            accountMapper.updateById(accountPO);
            AccountUtils.removeAccountCache(accountPO.getId(), cacheTemplate);

            onModelPostPatched(account, accountPO);
        }
    }

    @Override
    public Account findByWeChatTinyAppOpenId(Long companyId, String openId) {
        AccountPO accountPO = accountMapper.selectByWeChatTinyAppOpenId(companyId, openId);
        return modelAssembler.assemble(accountPO);
    }

    @Override
    @Transactional
    public void unbindWeChatTinyAppOpenId(Long companyId, String openId) {
        AccountPO accountPO = accountMapper.selectByWeChatTinyAppOpenId(companyId, openId);
        if (accountPO != null) {
            Account account = modelAssembler.assemble(accountPO);
            onModelPrePatched(account, accountPO);

            accountPO.setWeChatTinyAppOpenId(null);
            accountMapper.updateById(accountPO);
            AccountUtils.removeAccountCache(accountPO.getId(), cacheTemplate);

            onModelPostPatched(account, accountPO);
        }
    }

    @Override
    public Account findByWeChatH5AppOpenId(Long companyId, String openId) {
        AccountPO accountPO = accountMapper.selectByWeChatH5AppOpenId(companyId, openId);
        return modelAssembler.assemble(accountPO);
    }

    @Override
    @Transactional
    public void unbindWeChatH5AppOpenId(Long companyId, String openId) {
        AccountPO accountPO = accountMapper.selectByWeChatH5AppOpenId(companyId, openId);
        if (accountPO != null) {
            Account account = modelAssembler.assemble(accountPO);
            onModelPrePatched(account, accountPO);

            accountPO.setWeChatH5AppOpenId(null);
            accountMapper.updateById(accountPO);
            AccountUtils.removeAccountCache(accountPO.getId(), cacheTemplate);

            onModelPostPatched(account, accountPO);
        }
    }

    @Override
    public Account findByQQOpenId(Long companyId, String openId) {
        AccountPO accountPO = accountMapper.selectByQqOpenId(companyId, openId);
        return modelAssembler.assemble(accountPO);
    }

    @Override
    @Transactional
    public void unbindQQOpenId(Long companyId, String openId) {
        AccountPO accountPO = accountMapper.selectByQqOpenId(companyId, openId);
        if (accountPO != null) {
            Account account = modelAssembler.assemble(accountPO);
            onModelPrePatched(account, accountPO);

            accountPO.setQqOpenId(null);
            accountMapper.updateById(accountPO);
            AccountUtils.removeAccountCache(accountPO.getId(), cacheTemplate);

            onModelPostPatched(account, accountPO);
        }
    }

    @Override
    @Transactional
    public void changePassword(Long operatorAccountId, Long accountId, String newPassword, String oldPassword) {
        AccountPO operatorAccount = accountMapper.selectById(operatorAccountId);
        if (operatorAccount == null) {
            throw new NoSuchAccountException(String.valueOf(operatorAccountId));
        }

        AccountPO accountPO = accountMapper.selectById(accountId);
        if (accountPO == null) {
            throw new NoSuchAccountException(String.valueOf(accountId));
        }

        /*
         *  默认权限大小: system > it > 管理员 > 普通.
         *
         *  修改密码的逻辑:
         *     1. 权限高的账户可以直接改权限低的账户的密码, 并且不需要提供老密码.
         *     2. 修改自己的账户的密码, 则必须提供老密码.
         *
         */

        String existedOldPassword = accountPO.getPassword();
        boolean sameAccount = Objects.equals(operatorAccountId, accountId);
        if (sameAccount) {
            if (StringUtils.isNotBlank(existedOldPassword) && !checkOldPasswordMatched(oldPassword, existedOldPassword)) {
                throw new OldPasswordWrongException();
            }
        } else {
            boolean roleTypeCodePermissionMatched = AccountUtils.isPermissionGreaterOrEqualsThan(operatorAccount.getRoleTypeCode(), accountPO.getRoleTypeCode(), rolePermissionService);
            if (!roleTypeCodePermissionMatched) {
                throw new AccountOperationPermissionDeniedException("The account[" + operatorAccountId + "] has no permissions to change the account[" + accountId + "]'s password.");
            }
        }

        checkPasswordFormat(newPassword);
        String newEncodedPassword = passwordEncoder.encode(newPassword);
        if (!Objects.equals(accountPO.getPassword(), newEncodedPassword)) {
            Account oldAccount = modelAssembler.assemble(accountPO);
            if (accountListenerSupport != null) {
                accountListenerSupport.fireAccountPreModified(oldAccount, null);
            }
            accountPO.setPassword(newEncodedPassword);
            accountMapper.updateById(accountPO);
            AccountUtils.removeAccountCache(accountPO.getId(), cacheTemplate);
            // 仅在修改密码的场景踢出登录
            if (Boolean.TRUE.equals(logout) && StringUtils.isNotBlank(existedOldPassword)) {
                LoginUtils.logout(String.valueOf(accountId), loginService);
            }

            if (accountListenerSupport != null) {
                Account account = modelAssembler.assemble(accountPO);
                accountListenerSupport.fireAccountPostModified(account,
                        oldAccount,
                        null);
            }
        }
    }

    private boolean checkOldPasswordMatched(String inputOldPassword, String existedOldPassword) {
        return StringUtils.isNotBlank(inputOldPassword) && passwordEncoder.matches(inputOldPassword, existedOldPassword);
    }

    private void checkPasswordFormat(String password) {
        if (password != null) {
            if (!matchPasswordFormat(password)) {
                throw new PasswordFormatException();
            }
        }
    }

    private boolean matchPasswordFormat(String password) {
        return Pattern.matches(passwordRegx, password);
    }

    @Override
    @Transactional
    @Validated
    public Account resetPassword(PhoneNumberPasswordResetInfo resetInfo) {
        String countryCode = PhoneNumberUtils.filterNonDigitCharacters(resetInfo.getCountryCode());
        String phoneNumber = PhoneNumberUtils.filterNonDigitCharacters(resetInfo.getPhoneNumber());
        AccountPO accountPO;
        if (StringUtils.isNotBlank(countryCode)) {
            accountPO = accountMapper.selectByCountryCodeAndPhoneNumber(resetInfo.getCompanyId(), countryCode, phoneNumber);
        } else {
            accountPO = accountMapper.selectByPhoneNumber(resetInfo.getCompanyId(), phoneNumber);
        }

        if (accountPO == null) {
            throw new NoSuchAccountException("phoneNumber " + phoneNumber);
        }

        CaptchaCodeKey captchaCodeKey = resetInfo.buildCaptchaCodeKey();
        // 新密码不一致.
        if (!StringUtils.equals(resetInfo.getNewPassword(), resetInfo.getConfirmedNewPassword())) {
            throw new AccountPasswordMismatchException();
        }

        // 判断一下手机验证码正确.
        else if (!captchaService.validate(captchaCodeKey, resetInfo.getCaptchaCode())) {
            throw new CaptchaCodeMismatchException(captchaCodeKey, resetInfo.getCaptchaCode(), captchaService.exchange(captchaCodeKey));
        } else {
            String newPassword = resetInfo.getNewPassword();
            checkPasswordFormat(newPassword);
            String newEncodedPassword = passwordEncoder.encode(newPassword);
            if (!Objects.equals(accountPO.getPassword(), newEncodedPassword)) {
                Account oldAccount = modelAssembler.assemble(accountPO);
                if (accountListenerSupport != null) {
                    accountListenerSupport.fireAccountPreModified(oldAccount, null);
                }

                accountPO.setPassword(newEncodedPassword);
                accountMapper.updateById(accountPO);
                AccountUtils.removeAccountCache(accountPO.getId(), cacheTemplate);

                if (Boolean.TRUE.equals(logout)) {
                    LoginUtils.logout(String.valueOf(accountPO.getId()), loginService);
                }

                if (accountListenerSupport != null) {
                    Account account = modelAssembler.assemble(accountPO);
                    accountListenerSupport.fireAccountPostModified(account,
                            oldAccount,
                            null);
                }
            }
        }

        return modelAssembler.assemble(accountPO);
    }

    @Override
    @Transactional
    public PasswordResetResult resetPassword(Long systemAccountId, Long accountId) {
        // 使用系统管理员的账户重置密码.
        AccountPO systemAccountPO = doLoadById(systemAccountId);
        if (systemAccountPO == null || !rolePermissionService.isSystemAccount(systemAccountPO.getRoleTypeCode())) {
            throw new AccountOperationPermissionDeniedException(systemAccountId + "is not a system account");
        }

        AccountPO resetPasswordAccountPO = doLoadById(accountId);
        if (resetPasswordAccountPO == null) {
            throw new NoSuchAccountException(String.valueOf(accountId));
        }

        String newPassword = generateNewPassword(resetPasswordAccountPO);
        if (newPassword != null) {
            Account oldAccount = modelAssembler.assemble(resetPasswordAccountPO);
            if (accountListenerSupport != null) {
                accountListenerSupport.fireAccountPreModified(oldAccount, null);
            }

            resetPasswordAccountPO.setPassword(passwordEncoder.encode(newPassword));
            accountMapper.updateById(resetPasswordAccountPO);
            AccountUtils.removeAccountCache(accountId, cacheTemplate);

            if (Boolean.TRUE.equals(logout)) {
                LoginUtils.logout(String.valueOf(accountId), loginService);
            }

            if (accountListenerSupport != null) {
                Account account = modelAssembler.assemble(resetPasswordAccountPO);
                accountListenerSupport.fireAccountPostModified(account,
                        oldAccount,
                        null);
            }
        }

        return new PasswordResetResult(accountId, resetPasswordAccountPO.getUserName(), newPassword);
    }

    private String generateNewPassword(AccountPO accountPO) {
        String oldEncodedPassword = accountPO.getPassword();
        int passwordFormatMismatchedCount = 0;
        boolean formatMismatched = false;
        String newPassword = randomGeneratePassword();

        // 新密码不能和老密码一致.
        while (passwordEncoder.matches(newPassword, oldEncodedPassword) || !(formatMismatched = matchPasswordFormat(newPassword))) {
            if (formatMismatched) {
                passwordFormatMismatchedCount++;
                if (passwordFormatMismatchedCount >= 3) {          // 设置一个密码格式不匹配的最大次数, 避免死循环的可能.
                    break;
                }
            }
            newPassword = randomGeneratePassword();
        }
        return newPassword;
    }

    private String randomGeneratePassword() {
        return RandomStringUtils.randomAlphanumeric(defaultRandomPasswordLength);
    }


    @Override
    @Transactional
    public Account verify(Long accountId, IdCardVerification verification) {
        AccountPO accountPO = accountMapper.selectById(accountId);
        if (accountPO == null) {
            throw new NoSuchAccountException(String.valueOf(accountId));
        }

        if (!Boolean.TRUE.equals(accountPO.getEnabled())) {
            throw new AccountDisabledException(String.valueOf(accountId));
        }

        try {
            Boolean success = idCardVerificationService.validate(verification);
            if (!Boolean.TRUE.equals(success)) {
                throw new IdCardNumberAndNameMismatchException();
            }

            Account oldAccount = AccountUtils.assembleAccount(accountPO,
                    accountHistoryPhoneNumberService,
                    accountPrivilegeService,
                    privilegeService);
            if (accountListenerSupport != null) {
                accountListenerSupport.fireAccountPreModified(oldAccount, null);
            }
            accountPO.setVerified(success);
            accountPO.setIdCardName(verification.getName());
            accountPO.setIdCardNumber(verification.getIdCardNumber());
            accountMapper.updateById(accountPO);
            AccountUtils.removeAccountCache(accountPO.getId(), cacheTemplate);

            if (accountListenerSupport != null) {
                Account account = AccountUtils.assembleAccount(accountPO,
                        accountHistoryPhoneNumberService,
                        accountPrivilegeService,
                        privilegeService);
                accountListenerSupport.fireAccountPostModified(account,
                        oldAccount,
                        null);
            }

        } catch (IOException e) {
            throw new RuntimeException(Throwables.getStackTraceAsString(e), e);
        }

        return modelAssembler.assemble(accountPO);
    }

    @Override
    public Boolean validate(Invitation invitation) {
        if (invitation == null) {
            return Boolean.FALSE;
        }

        AccountPO accountPO = accountMapper.selectById(invitation.getInviteeId());
        if (accountPO == null) {
            return Boolean.FALSE;
        }

        return StringUtils.equals(accountPO.getInvitationCode(), invitation.getCode());
    }

    @Override
    public Account findByFacebookUserId(Long companyId, String userId) {
        AccountPO accountPO = accountMapper.selectByFacebookUserId(companyId, userId);
        return modelAssembler.assemble(accountPO);
    }

    @Override
    @Transactional
    public void unbindFacebookUserId(Long companyId, String facebookUserId) {
        AccountPO accountPO = accountMapper.selectByFacebookUserId(companyId, facebookUserId);
        if (accountPO != null) {
            Account account = modelAssembler.assemble(accountPO);
            onModelPrePatched(account, accountPO);

            accountPO.setFacebookUserId(null);
            accountMapper.updateById(accountPO);
            AccountUtils.removeAccountCache(accountPO.getId(), cacheTemplate);

            onModelPostPatched(account, accountPO);
        }
    }

    @Override
    protected void onModelPreDeleted(AccountPO accountPO) {
        if (accountListenerSupport != null) {
            accountListenerSupport.fireAccountPreDeleted(accountPO.getId());
        }
    }

    @Override
    protected void doDelete(Long id, AccountPO accountPO) {
        accountPO.setId(id);
        accountPO.setModifiedDate(System.currentTimeMillis());
        accountPO.setDeleted(true);
        accountPO.setDeletedTime(System.currentTimeMillis());
        accountMapper.deleteById(accountPO);
    }

    @Override
    protected void onModelPostDeleted(AccountPO accountPO) {
        if (accountListenerSupport != null) {
            accountListenerSupport.fireAccountPostDeleted(accountPO.getId());
        }
    }

    @Override
    protected Object buildCacheKey(Long id) {
        return CacheUtils.buildCacheKey(CACHE_PREFIX, id);
    }

    @Override
    protected Long parseRawKey(Object cacheKey) {
        return CacheUtils.parseId(cacheKey, CACHE_PREFIX);
    }

    @Override
    public List<Account> findByCompanyId(Long companyId, CacheStrategy strategy) {
        List<Long> accountIds = accountMapper.selectIdsByCompanyId(companyId);
        return findByIds(accountIds, strategy);
    }

    @Override
    public List<Account> findByRoleTypeCodes(Long companyId, Collection<String> roleTypeCodes, CacheStrategy strategy) {
        if (companyId == null || CollectionUtils.isEmpty(roleTypeCodes)) {
            return Lists.newArrayListWithCapacity(0);
        }

        List<Long> accountIds = accountMapper.selectIdsByCompanyIdAndRoleTypeCodes(companyId, roleTypeCodes);
        return findByIds(accountIds, strategy);
    }

    @Override
    public Account findByUnionId(Long companyId, String unionId) {
        AccountPO accountPO = accountMapper.selectByUnionId(companyId, unionId);
        return modelAssembler.assemble(accountPO);
    }

    @Override
    @Transactional
    public void unbindUnionId(Long companyId, String unionId) {
        AccountPO accountPO = accountMapper.selectByUnionId(companyId, unionId);
        if (accountPO != null) {
            Account account = modelAssembler.assemble(accountPO);
            onModelPrePatched(account, accountPO);

            accountPO.setUnionId(null);
            accountMapper.updateById(accountPO);
            AccountUtils.removeAccountCache(accountPO.getId(), cacheTemplate);

            onModelPostPatched(account, accountPO);
        }
    }

    @Override
    public Account findByAppleUserId(Long companyId, String appleUserId) {
        AccountPO accountPO = accountMapper.selectByAppleUserId(companyId, appleUserId);
        return modelAssembler.assemble(accountPO);
    }

    @Override
    @Transactional
    public void unbindAppleUserId(Long companyId, String appleUserId) {
        AccountPO accountPO = accountMapper.selectByAppleUserId(companyId, appleUserId);
        if (accountPO != null) {
            Account account = modelAssembler.assemble(accountPO);
            onModelPrePatched(account, accountPO);

            accountPO.setAppleUserId(null);
            accountMapper.updateById(accountPO);
            AccountUtils.removeAccountCache(accountPO.getId(), cacheTemplate);

            onModelPostPatched(account, accountPO);
        }
    }

    @Override
    public Account findByWhatsappCountryCodeAndPhoneNumber(Long companyId, String countryCode, String phoneNumber) {
        AccountPO accountPO = accountMapper.selectByWhatsappCountryCodeAndPhoneNumber(companyId, countryCode, phoneNumber);
        return modelAssembler.assemble(accountPO);
    }

    @Override
    @Transactional
    public void unbindWhatsappCountryCodeAndPhoneNumber(Long companyId, String countryCode, String phoneNumber) {
        AccountPO accountPO = accountMapper.selectByWhatsappCountryCodeAndPhoneNumber(companyId, countryCode, phoneNumber);
        if (accountPO != null) {
            Account account = modelAssembler.assemble(accountPO);
            onModelPrePatched(account, accountPO);

            accountPO.setWhatsappCountryCode(null);
            accountPO.setWhatsappPhoneNumber(null);
            accountMapper.updateById(accountPO);
            AccountUtils.removeAccountCache(accountPO.getId(), cacheTemplate);

            onModelPostPatched(account, accountPO);
        }
    }

    @Override
    public Account findByTelegramId(Long companyId, Long telegramId) {
        AccountPO accountPO = accountMapper.selectByTelegramId(companyId, telegramId);
        return modelAssembler.assemble(accountPO);
    }

    @Override
    @Transactional
    public void unbindTelegramId(Long companyId, Long telegramId) {
        AccountPO accountPO = accountMapper.selectByTelegramId(companyId, telegramId);
        if (accountPO != null) {
            Account account = modelAssembler.assemble(accountPO);
            onModelPrePatched(account, accountPO);

            accountPO.setTelegramId(null);
            accountMapper.updateById(accountPO);
            AccountUtils.removeAccountCache(accountPO.getId(), cacheTemplate);

            onModelPostPatched(account, accountPO);
        }
    }

    @Override
    public Map<Long, Long> countCompanyAccounts(Collection<Long> companyIds) {
        if (CollectionUtils.isEmpty(companyIds)) {
            return Maps.newHashMap();
        }

        List<Pair<Long, Long>> pairs = accountMapper.countByCompanyIds(companyIds);
        return pairs.stream()
                .collect(Collectors.toMap(Pair::getLeft, Pair::getRight));
    }

    @Override
    public void deleteCache(Long id) {
        if (id != null) {
            AccountUtils.removeAccountCache(id, cacheTemplate);
        }
    }

    @Override
    public List<Account> findAccountByIds(Collection<Long> ids, Boolean deleted) {
        if (CollectionUtils.isEmpty(ids)) {
            return Lists.newArrayList();
        }
        List<AccountPO> accountPOs = accountMapper.selectByIdsWithDeletedCondition(ids, deleted);
        return AccountUtils.assembleAccounts(accountPOs, accountHistoryPhoneNumberService, accountPrivilegeService, privilegeService);
    }

    @Override
    public String createBindCode(Long accountId, Long bindCodeTTlSeconds) {
        int retryTimes = 10;
        for (int i = 0; i < retryTimes; i++) {
            String bindCode = RandomStringUtils.randomAlphanumeric(bindCodeLength);
            if (BooleanUtils.isTrue(redisTemplate.opsForValue().setIfAbsent(BIND_CODE_KEY_PREFIX + bindCode, accountId, bindCodeTTlSeconds, TimeUnit.SECONDS))) {
                return bindCode;
            }
        }
        throw new RuntimeException("create bind code failed");
    }

    @Override
    @Transactional
    public void bindWhatsappByBindCode(String bindCode, Long companyId, String whatsappCountryCode, String whatsappPhoneNumber) {
        Long accountId = getAccountIdByBindCode(bindCode);
        bindWhatsApp(accountId, companyId, whatsappCountryCode, whatsappPhoneNumber);
    }

    @Override
    @Transactional
    public void bindWhatsApp(Long accountId, Long companyId, String whatsappCountryCode, String whatsappPhoneNumber) {
        AccountPO accountPO = accountMapper.selectById(accountId);
        if (accountPO == null || whatsappCountryCode.equals(accountPO.getWhatsappCountryCode()) && whatsappPhoneNumber.equals(accountPO.getWhatsappPhoneNumber())) {
            return;
        }

        unbindWhatsappCountryCodeAndPhoneNumber(companyId, whatsappCountryCode, whatsappPhoneNumber);

        Account account = modelAssembler.assemble(accountPO);
        onModelPrePatched(account, accountPO);

        accountPO.setWhatsappCountryCode(whatsappCountryCode);
        accountPO.setWhatsappPhoneNumber(whatsappPhoneNumber);
        accountMapper.updateById(accountPO);
        AccountUtils.removeAccountCache(accountPO.getId(), cacheTemplate);

        onModelPostPatched(account, accountPO);
    }

    @Override
    @Transactional
    public void bindTelegramByBindCode(String bindCode, Long companyId, Long telegramId) {
        Long accountId = getAccountIdByBindCode(bindCode);
        bindTelegram(accountId, companyId, telegramId);
    }

    @Override
    @Transactional
    public void bindTelegram(Long accountId, Long companyId, Long telegramId) {
        AccountPO accountPO = accountMapper.selectById(accountId);
        if (accountPO == null || telegramId.equals(accountPO.getTelegramId())) {
            return;
        }

        unbindTelegramId(companyId, telegramId);

        Account account = modelAssembler.assemble(accountPO);
        onModelPrePatched(account, accountPO);

        accountPO.setTelegramId(telegramId);
        accountMapper.updateById(accountPO);
        AccountUtils.removeAccountCache(accountPO.getId(), cacheTemplate);

        onModelPostPatched(account, accountPO);
    }

    /**
     * @throws BindCodeNotFoundException
     */
    private Long getAccountIdByBindCode(String bindCode) {
        RedisScript<Long> script = RedisScript.of("local result = redis.call('get', KEYS[1]); redis.call('del', KEYS[1]); return result;", Long.class);
        Long accountId = redisTemplate.execute(script, Collections.singletonList(BIND_CODE_KEY_PREFIX + bindCode));
        if (accountId == null) {
            throw new BindCodeNotFoundException();
        }
        return accountId;
    }
}
