package tech.jiyu.paradise.core.authentication.service;

import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.NotNull;

import tech.jiyu.paradise.core.authentication.domain.AuthenticationFailedRecord;

/**
 * @author Simon [simonchan@jinjie.tech]
 */
@Validated
public interface AuthenticationFailedRecordService {

    @Validated
    void record(@NotNull Long accountId);

    void deleteByAccountId(Long accountId);

    AuthenticationFailedRecord findByAccountId(Long accountId);
}
