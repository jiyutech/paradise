package tech.jiyu.paradise.core.authentication.service.impl;

/**
 * Created by lu.dengji on 2018/7/5.
 */
/*
 * 文件名：AESUtils.java
 * 版权：
 * 描述：
 * 修改人：Awoke
 * 修改时间：2018-1-24
 * 跟踪单号：
 * 修改单号：
 * 修改内容：
 */

import org.bouncycastle.jce.provider.BouncyCastleProvider;

import java.security.AlgorithmParameters;
import java.security.Key;
import java.security.Security;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;


/**
 * @author Awoke
 * @version 2018-1-24
 * @see AESUtils
 * @since
 */
public class AESUtils {
    public static boolean initialized = false;

    public static byte[] decrypt(byte[] content, byte[] keyByte, byte[] ivByte) throws Exception {
        initialize();
        Cipher cipher = Cipher.getInstance("AES/CBC/PKCS7Padding");
        Key sKeySpec = new SecretKeySpec(keyByte, "AES");

        cipher.init(Cipher.DECRYPT_MODE, sKeySpec, generateIV(ivByte));// 初始化
        return cipher.doFinal(content);
    }

    public static void initialize() {
        if (initialized) return;
        Security.addProvider(new BouncyCastleProvider());
        initialized = true;
    }

    public static AlgorithmParameters generateIV(byte[] iv)
            throws Exception {
        AlgorithmParameters params = AlgorithmParameters.getInstance("AES");
        params.init(new IvParameterSpec(iv));
        return params;
    }

}

