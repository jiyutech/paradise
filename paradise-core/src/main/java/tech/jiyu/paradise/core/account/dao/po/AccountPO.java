package tech.jiyu.paradise.core.account.dao.po;

import com.fasterxml.jackson.databind.ObjectMapper;

import java.util.Date;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import tech.jiyu.paradise.core.account.domain.Account;
import tech.jiyu.paradise.core.util.AccountUtils;
import tech.jiyu.utility.json.JsonHelper;

/**
 * @author <a href="mailto:chenshao@dekeapp.com">shawsmith</a>
 */
@NoArgsConstructor
@Getter
@Setter
public class AccountPO {

    private Long id;
    private Long createdDate;
    private Long modifiedDate;
    private Boolean deleted;
    private Long deletedTime;
    private String name;
    private String avatar;
    private String gender;
    private Long companyId;
    private String email;
    private String countryCode;
    private String phoneNumber;
    private Long registrationDate;
    private String registrationSource;
    private String registrationTrafficType;
    private Date birthday;
    private String isoCode;
    private Long provinceId;
    private Long cityId;
    private Long districtId;
    private String address;
    private String signature;
    private String roleTypeCode;
    private String accountPermission;
    private Long creatorId;
    private Boolean enabled;
    private Boolean verified;
    private String userName;
    private String password;
    private String weChatOpenId;
    private String weChatTinyAppOpenId;
    private String weChatH5AppOpenId;
    private String qqOpenId;
    private String facebookUserId;
    private String unionId;
    private String appleUserId;
    private String whatsappCountryCode;
    private String whatsappPhoneNumber;
    private Long telegramId;
    private String idCardName;
    private String idCardNumber;
    private String invitationCode;
    private String profession;
    private String individualResume;
    private String timeZone;
    private Long lastLoginTime;
    private String lastLanguageCode;
    private Boolean audited;
    private Boolean workDayTimeEnabled;
    private String allowedWorkDays;
    private String allowedWorkTimes;
    private Boolean polyAdminEnabled;
    private String nickname;
    private String firstName;
    private String lastName;
    private Boolean agreeToReceiveInfo;

    public static AccountPO create(Long id,
                                   Account account,
                                   ObjectMapper objectMapper) {
        AccountPO accountPO = new AccountPO();

        accountPO.setId(id);

        Long now = System.currentTimeMillis();
        accountPO.setCreatedDate(now);
        accountPO.setModifiedDate(now);
        accountPO.setDeleted(false);
        accountPO.setDeletedTime(0L);

        if (account != null) {
            accountPO.setName(account.getName());
            accountPO.setAvatar(account.getAvatar());
            accountPO.setGender(account.getGender());
            accountPO.setCompanyId(account.getCompanyId());
            accountPO.setEmail(account.getEmail());
            accountPO.setCountryCode(account.getCountryCode());
            accountPO.setPhoneNumber(account.getPhoneNumber());
            accountPO.setRegistrationDate(account.getRegistrationDate());
            accountPO.setRegistrationSource(account.getRegistrationSource());
            accountPO.setRegistrationTrafficType(account.getRegistrationTrafficType());
            accountPO.setBirthday(account.getBirthday());
            accountPO.setIsoCode(account.getIsoCode());
            accountPO.setProvinceId(account.getProvinceId());
            accountPO.setCityId(account.getCityId());
            accountPO.setDistrictId(account.getDistrictId());
            accountPO.setAddress(account.getAddress());
            accountPO.setSignature(account.getSignature());
            accountPO.setRoleTypeCode(account.getRoleTypeCode());
            accountPO.setAccountPermission(account.getAccountPermission());
            accountPO.setCreatorId(account.getCreatorId());
            accountPO.setEnabled(account.getEnabled());
            accountPO.setVerified(account.getVerified());
            accountPO.setUserName(account.getUsername());
            accountPO.setPassword(account.getPassword());
            accountPO.setWeChatOpenId(account.getWeChatOpenId());
            accountPO.setWeChatTinyAppOpenId(account.getWeChatTinyAppOpenId());
            accountPO.setWeChatH5AppOpenId(account.getWeChatH5AppOpenId());
            accountPO.setQqOpenId(account.getQqOpenId());
            accountPO.setFacebookUserId(account.getFacebookUserId());
            accountPO.setUnionId(account.getUnionId());
            accountPO.setAppleUserId(account.getAppleUserId());
            accountPO.setWhatsappCountryCode(account.getWhatsappCountryCode());
            accountPO.setWhatsappPhoneNumber(account.getWhatsappPhoneNumber());
            accountPO.setTelegramId(account.getTelegramId());
            accountPO.setIdCardName(account.getIdCardName());
            accountPO.setIdCardNumber(account.getIdCardNumber());
            accountPO.setInvitationCode(account.getInvitationCode());
            accountPO.setProfession(account.getProfession());
            accountPO.setIndividualResume(account.getIndividualResume());
            accountPO.setTimeZone(account.getTimeZone());
            accountPO.setLastLoginTime(account.getLastLoginTime());
            accountPO.setLastLanguageCode(account.getLastLanguageCode());
            accountPO.setAudited(account.getAudited());
            accountPO.setWorkDayTimeEnabled(account.getWorkDayTimeEnabled());
            accountPO.setAllowedWorkDays(JsonHelper.serialize(AccountUtils.formatWorkDays(account.getAllowedWorkDays()), objectMapper));
            accountPO.setAllowedWorkTimes(JsonHelper.serialize(AccountUtils.formatWorkTimes(account.getAllowedWorkTimes(), AccountUtils.WORK_TIMES_FORMAT), objectMapper));
            accountPO.setPolyAdminEnabled(account.getPolyAdminEnabled());
            accountPO.setNickname(account.getNickname());
            accountPO.setFirstName(account.getFirstName());
            accountPO.setLastName(account.getLastName());
            accountPO.setAgreeToReceiveInfo(account.getAgreeToReceiveInfo());
            accountPO.setLastLanguageCode(account.getLastLanguageCode());
        }

        return accountPO;
    }
}
