package tech.jiyu.paradise.core.authentication.exception;

import tech.jiyu.utility.api.errors.annotations.ApiErrorKey;

/**
 * Created By CannotAcquireAppAccessTokenException
 *
 * Date 2018/3/13 12:23
 *
 * @author SimonChan;emailAddress: simonchan@jiyu.tech
 */
@ApiErrorKey(group = "oauth2",
             id = "cannot.acquire.appAccessToken")
public class CannotAcquireAppAccessTokenException extends RuntimeException {

    public CannotAcquireAppAccessTokenException() {
        super();
    }

    public CannotAcquireAppAccessTokenException(String message) {
        super(message);
    }

    public CannotAcquireAppAccessTokenException(String message, Throwable cause) {
        super(message, cause);
    }

    public CannotAcquireAppAccessTokenException(Throwable cause) {
        super(cause);
    }

    protected CannotAcquireAppAccessTokenException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
