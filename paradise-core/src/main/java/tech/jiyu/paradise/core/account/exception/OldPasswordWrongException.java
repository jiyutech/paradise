package tech.jiyu.paradise.core.account.exception;

import tech.jiyu.utility.api.errors.annotations.ApiErrorKey;

/**
 * @author <a href="mailto:chenshao@dekeapp.com">shawsmith</a>
 */
@ApiErrorKey(group = "account",
             id = "old.password.wrong")
public class OldPasswordWrongException extends RuntimeException {

    public OldPasswordWrongException() {
    }

    public OldPasswordWrongException(String message) {
        super(message);
    }

    public OldPasswordWrongException(String message, Throwable cause) {
        super(message, cause);
    }

    public OldPasswordWrongException(Throwable cause) {
        super(cause);
    }
}
