package tech.jiyu.paradise.core.login.domain;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class LoginInfoEvent {
    private String loginId;
    private String oldLoginToken;
    private String newLoginToken;
}
