package tech.jiyu.paradise.core.account.domain;

import java.util.Map;

/**
 * @author <a href="mailto:chenshao@dekeapp.com">shawsmith</a>
 */
public class AccountPostCreatedEvent extends AccountEvent {

    private final Account account;
    private final Map<String, Object> additionalAttributes;
    private final ActivationConfig activationConfig;
    private final Invitation invitation;

    public AccountPostCreatedEvent(Account account,
                                   Map<String, Object> additionalAttributes,
                                   ActivationConfig activationConfig,
                                   Invitation invitation) {
        this.account = account;
        this.additionalAttributes = additionalAttributes;
        this.activationConfig = activationConfig;
        this.invitation = invitation;
    }

    public Account getAccount() {
        return account;
    }

    public Map<String, Object> getAdditionalAttributes() {
        return additionalAttributes;
    }

    public ActivationConfig getActivationConfig() {
        return activationConfig;
    }

    public Invitation getInvitation() {
        return invitation;
    }
}
