package tech.jiyu.paradise.core.util;

import org.apache.commons.collections.CollectionUtils;

import java.util.List;

import tech.jiyu.paradise.core.login.service.LoginService;

/**
 * @author <a href="mailto:chenshao@jiyu.tech">shawsmith</a>
 */
public final class LoginUtils {

    private LoginUtils() {
    }

    public static void logout(String loginId, LoginService loginService) {
        List<String> loginTokens = loginService.findLoginTokensByLoginId(loginId);
        if (CollectionUtils.isEmpty(loginTokens)) {
            return;
        }
        loginTokens.forEach(loginService::deleteByLoginToken);
    }
}
