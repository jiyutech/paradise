package tech.jiyu.paradise.core.util;

import com.google.common.collect.Collections2;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

import com.fasterxml.jackson.databind.ObjectMapper;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;

import java.time.*;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.*;
import java.util.stream.Collectors;

import tech.jiyu.paradise.core.account.dao.po.AccountPO;
import tech.jiyu.paradise.core.account.domain.Account;
import tech.jiyu.paradise.core.account.domain.AccountDeletingTask;
import tech.jiyu.paradise.core.account.domain.AccountHistoryPhoneNumber;
import tech.jiyu.paradise.core.account.service.AccountDeletingTaskService;
import tech.jiyu.paradise.core.account.service.AccountHistoryPhoneNumberService;
import tech.jiyu.paradise.core.account.service.AccountService;
import tech.jiyu.paradise.core.account.service.RolePermissionService;
import tech.jiyu.paradise.core.privileges.domain.Privilege;
import tech.jiyu.paradise.core.privileges.service.AccountPrivilegeService;
import tech.jiyu.paradise.core.privileges.service.PrivilegeService;
import tech.jiyu.utility.cache.CacheStrategy;
import tech.jiyu.utility.cache.CacheTemplate;
import tech.jiyu.utility.cache.CacheUtils;
import tech.jiyu.utility.datastructure.Pair;
import tech.jiyu.utility.json.JsonHelper;

/**
 * @author <a href="mailto:chenshao@dekeapp.com">shawsmith</a>
 */
public final class AccountUtils {

    public static final String PHONE_NUMBER_USERNAME_PREFIX = "phone_number_username:";
    public static final String WORK_TIMES_FORMAT = "HH:mm:ss";
    private static final ObjectMapper DEFAULT_OBJECT_MAPPER = new ObjectMapper();

    private AccountUtils() {
    }

    public static String parseRoleTypeCode(Map<String, ?> parameters, String defaultRoleTypeCode) {
        Object roleTypeCode = parameters.get("roleTypeCode");
        return (roleTypeCode != null) ? roleTypeCode.toString() : defaultRoleTypeCode;
    }

    public static String buildPhoneNumberUserName(String countryCode, String phoneNumber) {
        return String.format("%s%s-%s", PHONE_NUMBER_USERNAME_PREFIX, countryCode, phoneNumber);
    }

    public static Pair<String, String> parseCountryCodeAndPhoneNumber(String phoneNumberUsername) {
        if (StringUtils.isBlank(phoneNumberUsername)) {
            return null;
        }

        phoneNumberUsername = phoneNumberUsername.replace(PHONE_NUMBER_USERNAME_PREFIX, "");
        String[] split = phoneNumberUsername.split("-");
        if (split.length != 2) {
            return null;
        }

        return new Pair<>(split[0], split[1]);
    }

    public static Account assembleAccount(AccountPO accountPO,
                                          AccountHistoryPhoneNumberService accountHistoryPhoneNumberService,
                                          AccountPrivilegeService accountPrivilegeService,
                                          PrivilegeService privilegeService) {
        if (accountPO == null) {
            return null;
        }

        List<Account> accounts = assembleAccounts(Collections.singletonList(accountPO),
                accountHistoryPhoneNumberService,
                accountPrivilegeService,
                privilegeService);
        return CollectionUtils.isNotEmpty(accounts) ? accounts.get(0) : null;
    }

    public static List<Account> assembleAccounts(Collection<AccountPO> accountPOs,
                                                 AccountHistoryPhoneNumberService accountHistoryPhoneNumberService,
                                                 AccountPrivilegeService accountPrivilegeService,
                                                 PrivilegeService privilegeService) {
        return assembleAccounts(accountPOs,
                accountHistoryPhoneNumberService,
                accountPrivilegeService,
                privilegeService,
                null);
    }

    public static Account assembleAccount(AccountPO accountPO,
                                          AccountHistoryPhoneNumberService accountHistoryPhoneNumberService,
                                          AccountPrivilegeService accountPrivilegeService,
                                          PrivilegeService privilegeService,
                                          AccountDeletingTaskService accountDeletingTaskService) {
        if (accountPO == null) {
            return null;
        }

        List<Account> accounts = assembleAccounts(Collections.singletonList(accountPO),
                accountHistoryPhoneNumberService,
                accountPrivilegeService,
                privilegeService,
                accountDeletingTaskService);
        return CollectionUtils.isNotEmpty(accounts) ? accounts.get(0) : null;
    }


    public static List<Account> assembleAccounts(Collection<AccountPO> accountPOs,
                                                 AccountHistoryPhoneNumberService accountHistoryPhoneNumberService,
                                                 AccountPrivilegeService accountPrivilegeService,
                                                 PrivilegeService privilegeService,
                                                 AccountDeletingTaskService accountDeletingTaskService) {
        if (CollectionUtils.isEmpty(accountPOs)) {
            return Lists.newArrayListWithCapacity(0);
        }

        Collection<Long> accountIds = Collections2.transform(accountPOs, AccountPO::getId);
        Map<Long, List<AccountHistoryPhoneNumber>> historyPhoneNumberGroups = accountHistoryPhoneNumberService.findByAccountIds(accountIds, CacheStrategy.CACHE_PREFER);
        Map<Long, Collection<Long>> accountPrivilegeIdGroups = accountPrivilegeService.findByAccountIds(accountIds, CacheStrategy.CACHE_PREFER);

        Collection<Long> allPrivilegeIds = accountPrivilegeIdGroups.values()
                .stream()
                .flatMap(Collection::stream)
                .collect(Collectors.toSet());
        List<Privilege> allPrivileges = privilegeService.findByIds(allPrivilegeIds, CacheStrategy.CACHE_PREFER);

        Map<Long, AccountDeletingTask> accountDeletingTaskMap = accountDeletingTaskService == null ? Maps.newHashMap() : accountDeletingTaskService.getAndGroupByAccountIdList(accountIds);

        return accountPOs.stream()
                .filter(Objects::nonNull)
                .map(accountPO -> {
                    Long accountId = accountPO.getId();
                    List<AccountHistoryPhoneNumber> historyPhoneNumbers = historyPhoneNumberGroups.get(accountId);

                    Collection<Long> accountPrivilegeIds = Optional.ofNullable(accountPrivilegeIdGroups.get(accountId)).orElseGet(() -> Lists.newArrayListWithCapacity(0));
                    List<Privilege> accountPrivileges = allPrivileges
                            .stream()
                            .filter(privilege -> accountPrivilegeIds.contains(privilege.getId()))
                            .sorted(Comparator.comparingLong(Privilege::getOrderNumber))
                            .collect(Collectors.toList());

                    AccountDeletingTask accountDeletingTask = accountDeletingTaskMap.get(accountId);
                    return transformToAccount(accountPO, historyPhoneNumbers, accountPrivilegeIds, accountPrivileges, accountDeletingTask);
                })
                .filter(Objects::nonNull)
                .collect(Collectors.toList());
    }

    public static List<Account> assembleAccounts(Collection<AccountPO> accountPOs) {
        if (CollectionUtils.isEmpty(accountPOs)) {
            return Lists.newArrayListWithCapacity(0);
        }
        return accountPOs
                .stream()
                .map(accountPO -> transformToAccount(accountPO, null, null, null, null))
                .collect(Collectors.toList());
    }

    public static Account transformToAccount(AccountPO accountPO,
                                             List<AccountHistoryPhoneNumber> historyPhoneNumbers,
                                             Collection<Long> accountPrivilegeIds,
                                             List<Privilege> privileges,
                                             AccountDeletingTask accountDeletingTask) {
        Account account = new Account();
        account.setId(accountPO.getId());
        account.setDeleted(accountPO.getDeleted());
        account.setDeletedTime(accountPO.getDeletedTime());
        account.setName(accountPO.getName());
        account.setAvatar(accountPO.getAvatar());
        account.setGender(accountPO.getGender());
        account.setCompanyId(accountPO.getCompanyId());
        account.setEmail(accountPO.getEmail());
        account.setCountryCode(accountPO.getCountryCode());
        account.setPhoneNumber(accountPO.getPhoneNumber());
        account.setRegistrationDate(accountPO.getRegistrationDate());
        account.setRegistrationSource(accountPO.getRegistrationSource());
        account.setRegistrationTrafficType(accountPO.getRegistrationTrafficType());
        account.setBirthday(accountPO.getBirthday());
        account.setIsoCode(accountPO.getIsoCode());
        account.setProvinceId(accountPO.getProvinceId());
        account.setCityId(accountPO.getCityId());
        account.setDistrictId(accountPO.getDistrictId());
        account.setAddress(accountPO.getAddress());
        account.setSignature(accountPO.getSignature());
        account.setRoleTypeCode(accountPO.getRoleTypeCode());
        account.setAccountPermission(accountPO.getAccountPermission());
        account.setCreatorId(accountPO.getCreatorId());
        account.setEnabled(accountPO.getEnabled());
        account.setVerified(accountPO.getVerified());
        account.setUsername(accountPO.getUserName());
        account.setPassword(accountPO.getPassword());
        account.setWeChatOpenId(accountPO.getWeChatOpenId());
        account.setWeChatTinyAppOpenId(accountPO.getWeChatTinyAppOpenId());
        account.setWeChatH5AppOpenId(accountPO.getWeChatH5AppOpenId());
        account.setQqOpenId(accountPO.getQqOpenId());
        account.setFacebookUserId(accountPO.getFacebookUserId());
        account.setUnionId(accountPO.getUnionId());
        account.setAppleUserId(accountPO.getAppleUserId());
        account.setWhatsappCountryCode(accountPO.getWhatsappCountryCode());
        account.setWhatsappPhoneNumber(accountPO.getWhatsappPhoneNumber());
        account.setTelegramId(accountPO.getTelegramId());
        account.setIdCardName(accountPO.getIdCardName());
        account.setIdCardNumber(accountPO.getIdCardNumber());
        account.setInvitationCode(accountPO.getInvitationCode());
        account.setTimeZone(accountPO.getTimeZone());
        account.setLastLoginTime(accountPO.getLastLoginTime());
        account.setLastLanguageCode(accountPO.getLastLanguageCode());
        account.setProfession(accountPO.getProfession());
        account.setIndividualResume(accountPO.getIndividualResume());
        account.setAudited(accountPO.getAudited());
        account.setWorkDayTimeEnabled(accountPO.getWorkDayTimeEnabled());

        List<String> workDayNames = Lists.newArrayList(JsonHelper.deserializeAsCollection(accountPO.getAllowedWorkDays(), DEFAULT_OBJECT_MAPPER, String.class));
        account.setAllowedWorkDays(parseWorkDays(workDayNames));

        List<Pair<String, String>> allowedWorkTimes = JsonHelper.deserializeAsCollection(accountPO.getAllowedWorkTimes(), DEFAULT_OBJECT_MAPPER, Pair.class)
                .stream()
                .map(pair -> Pair.of((String) pair.getLeft(), (String) pair.getRight()))
                .collect(Collectors.toList());
        account.setAllowedWorkTimes(parseWorkTimes(allowedWorkTimes, WORK_TIMES_FORMAT));
        account.setPolyAdminEnabled(accountPO.getPolyAdminEnabled());
        account.setNickname(accountPO.getNickname());
        account.setFirstName(accountPO.getFirstName());
        account.setLastName(accountPO.getLastName());
        account.setAgreeToReceiveInfo(accountPO.getAgreeToReceiveInfo());

        account.setHistoryPhoneNumbers(Optional.ofNullable(historyPhoneNumbers).orElseGet(() -> Lists.newArrayListWithCapacity(0)));
        account.setPrivilegeIds(Optional.ofNullable(accountPrivilegeIds).orElseGet(() -> Lists.newArrayListWithCapacity(0)));
        account.setPrivileges(Optional.ofNullable(privileges).orElseGet(() -> Lists.newArrayListWithCapacity(0)));
        if (accountDeletingTask != null) {
            account.setAccountDeletingCoolingOffDays(accountDeletingTask.getAccountDeletingCoolingOffDays());
            account.setAccountEstimateDeletingTime(accountDeletingTask.getAccountEstimateDeletingTime());
        }
        return account;
    }

    public static boolean isPermissionGreaterOrEqualsThan(String aRoleTypeCode, String bRoleTypeCode, RolePermissionService rolePermissionService) {
        int aRoleTypeCodeValue = roleTypeCodeAsValue(aRoleTypeCode, rolePermissionService);
        int bRoleTypeCodeValue = roleTypeCodeAsValue(bRoleTypeCode, rolePermissionService);
        return aRoleTypeCodeValue <= bRoleTypeCodeValue;
    }

    private static int roleTypeCodeAsValue(String roleTypeCode, RolePermissionService rolePermissionService) {
        if (rolePermissionService.isSystemAccount(roleTypeCode)) {
            return 0;
        } else if (rolePermissionService.isITAccount(roleTypeCode)) {
            return 1;
        } else if (rolePermissionService.isAdministrationAccount(roleTypeCode)) {
            return 2;
        } else {
            return 3;
        }
    }

    public static void removeAccountCache(Long accountId, CacheTemplate cacheTemplate) {
        String cacheKey = CacheUtils.buildCacheKey(AccountService.CACHE_PREFIX, accountId);
        cacheTemplate.delete(cacheKey);
    }

    public static List<String> formatWorkDays(List<DayOfWeek> workDays) {
        if (CollectionUtils.isEmpty(workDays)) {
            return Lists.newArrayListWithCapacity(0);
        }

        return workDays
                .stream()
                .sorted(Comparator.comparingInt(DayOfWeek::getValue))
                .map(day -> StringUtils.lowerCase(day.name()))
                .distinct()
                .collect(Collectors.toList());
    }

    public static List<DayOfWeek> parseWorkDays(List<String> workDayNames) {
        if (CollectionUtils.isEmpty(workDayNames)) {
            return Lists.newArrayListWithCapacity(0);
        }
        return workDayNames
                .stream()
                .map(name -> {
                    try {
                        return Enum.valueOf(DayOfWeek.class, StringUtils.upperCase(name));
                    } catch (Exception e) {
                        return null;
                    }
                })
                .filter(Objects::nonNull)
                .distinct()
                .sorted(Comparator.comparingInt(DayOfWeek::getValue))
                .collect(Collectors.toList());
    }

    public static List<Pair<String, String>> formatWorkTimes(List<Pair<LocalTime, LocalTime>> times, String pattern) {
        if (CollectionUtils.isEmpty(times)) {
            return Lists.newArrayListWithCapacity(0);
        }
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(pattern);
        return times
                .stream()
                .map(pair -> {
                    String left = null;
                    String right = null;

                    LocalTime startTime = pair.getLeft();
                    if (startTime != null) {
                        left = formatter.format(startTime);
                    }

                    LocalTime endTime = pair.getRight();
                    if (endTime != null) {
                        right = formatter.format(endTime);
                    }

                    return Pair.of(left, right);
                }).collect(Collectors.toList());
    }

    public static List<Pair<LocalTime, LocalTime>> parseWorkTimes(List<Pair<String, String>> times, String pattern) {
        if (CollectionUtils.isEmpty(times)) {
            return Lists.newArrayListWithCapacity(0);
        }

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(pattern);
        return times
                .stream()
                .map(pair -> {
                    LocalTime start = null;
                    LocalTime end = null;

                    String left = pair.getLeft();
                    if (left != null) {
                        try {
                            start = LocalTime.parse(left, formatter);
                        } catch (DateTimeParseException e) {
                        }
                    }

                    String right = pair.getRight();
                    if (right != null) {
                        try {
                            end = LocalTime.parse(right, formatter);
                        } catch (DateTimeParseException e) {
                        }
                    }

                    return Pair.of(start, end);
                }).collect(Collectors.toList());

    }

    public static boolean isInWorkTimes(Account account, ZoneId zoneId) {
        if (account == null || zoneId == null) {
            return false;
        }

        // 未启用账户工作时间限制功能时，应该返回true.
        if (!Boolean.TRUE.equals(account.getWorkDayTimeEnabled())) {
            return Boolean.TRUE;
        }

        ZonedDateTime dateTime = Instant.now().atZone(zoneId);
        DayOfWeek dayOfWeek = dateTime.getDayOfWeek();

        List<DayOfWeek> allowedWorkDays = account.getAllowedWorkDays();

        // 如果未设置工作日, 则忽略.
        if (CollectionUtils.isNotEmpty(allowedWorkDays) && !allowedWorkDays.contains(dayOfWeek)) {
            return false;
        }

        LocalTime time = dateTime.toLocalTime();
        List<Pair<LocalTime, LocalTime>> allowedWorkTimes = account.getAllowedWorkTimes();

        // 如果未设置上下班时间, 则忽略.
        if (CollectionUtils.isEmpty(allowedWorkTimes)) {
            return true;
        }

        return allowedWorkTimes
                .stream()
                .anyMatch(pair -> inWorkTime(time, pair.getLeft(), pair.getRight()));
    }

    public static boolean inWorkTime(LocalTime time, LocalTime start, LocalTime end) {
        // 未设置时间.
        if (time == null) {
            return false;
        }

        if (start == null) {
            start = LocalTime.of(0, 0, 0);
        }
        if (end == null) {
            end = LocalTime.of(23, 59, 59);
        }

        // 在一个合理的起始范围内, 例如：「10:00 ~ 18:30」
        if (!end.isBefore(start)) {
            return !time.isBefore(start) && !time.isAfter(end);
        }

        // 隔天时间. 例如: 「09:30 ~ 01:30」
        else {
            boolean todayMatched = !time.isBefore(start) && !time.isAfter(LocalTime.of(23, 59, 59));
            boolean tomorrowMatched = !time.isAfter(end);
            return todayMatched || tomorrowMatched;
        }
    }
}
