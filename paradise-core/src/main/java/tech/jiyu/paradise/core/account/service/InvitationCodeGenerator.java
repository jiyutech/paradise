package tech.jiyu.paradise.core.account.service;

import tech.jiyu.paradise.core.account.domain.Account;

/**
 * @author <a href="mailto:chenshao@jiyu.tech">shawsmith</a>
 */
public interface InvitationCodeGenerator {

    String generate(Account account);
}
