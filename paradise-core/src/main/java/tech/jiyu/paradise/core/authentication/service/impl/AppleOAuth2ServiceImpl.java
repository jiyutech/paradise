package tech.jiyu.paradise.core.authentication.service.impl;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tech.jiyu.apple.domain.AppleUserInfo;
import tech.jiyu.apple.oauth2.AppleUserService;
import tech.jiyu.paradise.core.account.domain.Account;
import tech.jiyu.paradise.core.account.service.AccountService;
import tech.jiyu.paradise.core.authentication.domain.OAuth2KeyAuthentication;
import tech.jiyu.paradise.core.authentication.domain.OAuth2KeyParameters;
import tech.jiyu.paradise.core.authentication.domain.OAuth2Parameters;
import tech.jiyu.paradise.core.authentication.domain.OAuth2Type;
import tech.jiyu.paradise.core.authentication.exception.AppleIdTokenExpiredException;
import tech.jiyu.paradise.core.authentication.exception.CannotAcquireAppleUserInfoException;
import tech.jiyu.paradise.core.authentication.exception.NoProvidePhoneNumberException;
import tech.jiyu.paradise.core.authentication.exception.OAuth2KeyExpiredException;
import tech.jiyu.paradise.core.authentication.service.OAuth2Service;
import tech.jiyu.utility.cache.CacheTemplate;

import java.util.Optional;

/**
 * @author Simon [simonchan@jinjie.tech]
 */
@Service
public class AppleOAuth2ServiceImpl extends AbstractOAuth2Service<AppleUserInfo> implements OAuth2Service {

    @Autowired
    private AppleUserService appleUserService;

    @Autowired
    private AccountService accountService;

    @Autowired
    private CacheTemplate cacheTemplate;

    @Override
    public boolean supports(OAuth2Type type) {
        return type == OAuth2Type.APPLE;
    }

    @Override
    public Account createOrUpdate(OAuth2Parameters parameters) {
        AppleUserInfo appleUserInfo;
        try {
            appleUserInfo = appleUserService.parseAppleUserInfo(parameters.getAuthCode());
            if (appleUserInfo == null || Boolean.TRUE.equals(appleUserInfo.getExpired())) {
                throw new AppleIdTokenExpiredException();
            }
        } catch (Exception e) {
            if (e instanceof AppleIdTokenExpiredException) {
                throw ((AppleIdTokenExpiredException) e);
            }
            throw new CannotAcquireAppleUserInfoException();
        }
        return super.registerAccountIfNecessary(parameters.getCompanyId(), appleUserInfo, parameters);
    }

    @Override
    public Account createOrUpdate(OAuth2KeyParameters keyParameters) {
        String authKey = keyParameters.getAuthKey();
        OAuth2KeyAuthentication authentication = Optional.ofNullable(loadKeyAuthenticationFromCache(authKey)).orElseThrow(OAuth2KeyExpiredException::new);
        checkPhoneNumberIfNecessary(keyParameters, authentication);
        AppleUserInfo appleUserInfo = (AppleUserInfo) authentication.getOauthUserInfo();
        Account account = super.registerAccountIfNecessary(keyParameters.getCompanyId(), appleUserInfo, keyParameters);
        deleteKeyAuthenticationFromCache(authKey);
        return account;
    }

    @Override
    public OAuth2KeyAuthentication createOAuth2KeyAuthentication(OAuth2Parameters parameters) {
        String appleIdToken = parameters.getAuthCode();
        AppleUserInfo appleUserInfo;
        try {
            appleUserInfo = appleUserService.parseAppleUserInfo(appleIdToken);
            if (appleUserInfo == null || Boolean.TRUE.equals(appleUserInfo.getExpired())) {
                throw new AppleIdTokenExpiredException();
            }
        } catch (Exception e) {
            if (e instanceof AppleIdTokenExpiredException) {
                throw ((AppleIdTokenExpiredException) e);
            }
            throw new CannotAcquireAppleUserInfoException();
        }

        OAuth2KeyAuthentication authentication = buildOAuth2KeyAuthentication(parameters, appleUserInfo);
        storeKeyAuthenticationToCache(authentication);
        return authentication;
    }

    private void checkPhoneNumberIfNecessary(OAuth2KeyParameters keyParameters, OAuth2KeyAuthentication oAuth2KeyAuthentication) {
        if (oAuth2KeyAuthentication.getNeedPhoneNumber() && StringUtils.isBlank(keyParameters.getPhoneNumber())) {
            throw new NoProvidePhoneNumberException(keyParameters.getAuthKey());
        }
    }

    private void storeKeyAuthenticationToCache(OAuth2KeyAuthentication authentication) {
        String cacheKeySuffix = String.valueOf(System.currentTimeMillis());
        AppleUserInfo appleUserInfo = (AppleUserInfo) authentication.getOauthUserInfo();
        String cacheKey = OAuth2Service.OAUTH2_KEY_CACHE_PREFIX.concat(appleUserInfo.getAppleUserId()).concat(cacheKeySuffix);
        authentication.setCacheKey(cacheKey);
        cacheTemplate.set(cacheKey, authentication);
    }

    private OAuth2KeyAuthentication loadKeyAuthenticationFromCache(String authKey) {
        return cacheTemplate.get(authKey, OAuth2KeyAuthentication.class);
    }

    private void deleteKeyAuthenticationFromCache(String cacheKey) {
        cacheTemplate.delete(cacheKey);
    }


    private OAuth2KeyAuthentication buildOAuth2KeyAuthentication(OAuth2Parameters parameters, AppleUserInfo appleUserInfo) {
        Account account = accountService.findByAppleUserId(parameters.getCompanyId(), appleUserInfo.getAppleUserId());
        OAuth2KeyAuthentication authentication = new OAuth2KeyAuthentication();
        authentication.setOauthUserInfo(appleUserInfo);

        if (account == null || StringUtils.isBlank(account.getPhoneNumber())) {
            authentication.setNeedPhoneNumber(Boolean.TRUE);
        } else {
            authentication.setNeedPhoneNumber(Boolean.FALSE);
        }

        return authentication;
    }

    @Override
    Account findByExternalId(Long companyId, AppleUserInfo appleUserInfo) {
        return accountService.findByAppleUserId(companyId, appleUserInfo.getAppleUserId());
    }

    @Override
    void buildAccountByExternalInfo(Account account, AppleUserInfo appleUserInfo) {
        account.setAppleUserId(appleUserInfo.getAppleUserId());
    }

}
