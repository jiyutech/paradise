package tech.jiyu.paradise.core.authentication.exception;

import tech.jiyu.utility.api.errors.annotations.ApiErrorKey;

/**
 * @author Simon [simonchan@jinjie.tech]
 */
@ApiErrorKey(group = "oauth2", id = "apple.id.token.expired")
public class AppleIdTokenExpiredException extends RuntimeException {

    public AppleIdTokenExpiredException() {
        super();
    }

    public AppleIdTokenExpiredException(String message) {
        super(message);
    }

    public AppleIdTokenExpiredException(String message, Throwable cause) {
        super(message, cause);
    }

    public AppleIdTokenExpiredException(Throwable cause) {
        super(cause);
    }

    protected AppleIdTokenExpiredException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
