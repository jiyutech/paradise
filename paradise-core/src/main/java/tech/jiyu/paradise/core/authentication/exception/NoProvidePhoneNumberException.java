package tech.jiyu.paradise.core.authentication.exception;

import tech.jiyu.utility.api.errors.annotations.ApiErrorKey;

/**
 * CreatedDate 2018-07-10 上午11:01
 *
 * @author SimonChan;emailAddress: simonchan@jinjie.tech
 */
@ApiErrorKey(group = "account",
             id = "no.provide.phoneNumber")
public class NoProvidePhoneNumberException extends RuntimeException {

    public NoProvidePhoneNumberException() {
        super();
    }

    public NoProvidePhoneNumberException(String message) {
        super(message);
    }

    public NoProvidePhoneNumberException(String message, Throwable cause) {
        super(message, cause);
    }

    public NoProvidePhoneNumberException(Throwable cause) {
        super(cause);
    }

    protected NoProvidePhoneNumberException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
