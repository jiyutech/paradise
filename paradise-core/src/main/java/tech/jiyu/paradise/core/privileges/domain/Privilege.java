package tech.jiyu.paradise.core.privileges.domain;

import com.google.common.collect.Maps;

import com.fasterxml.jackson.databind.ObjectMapper;

import org.apache.commons.lang3.StringUtils;

import java.io.IOException;
import java.util.Map;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import tech.jiyu.restful.api.domain.Create;
import tech.jiyu.restful.api.domain.Edit;
import tech.jiyu.restful.api.domain.Model;

/**
 * @author <a href="mailto:chenshao@jinjie.tech">shawsmith</a>
 */
@NoArgsConstructor
@Getter
@Setter
public class Privilege implements Model {

    private Long id;

    @NotNull(groups = {Create.class, Edit.class},
             message = "{account.privilege.name.limit}")
    @Size(min = 1,
          max = 64,
          message = "{account.privilege.name.limit}")
    private String name;

    @Size(max = 32,
          message = "{account.privilege.description.limit}")
    private String description;

    private Long orderNumber;

    @Override
    public Map<String, Object> populatePatchableProperties(ObjectMapper objectMapper) throws IOException {
        Map<String, Object> properties = Maps.newHashMap();

        if (StringUtils.isNotBlank(name)) {
            properties.put("name", name);
        }

        if (description != null) {
            properties.put("description", description);
        }

        if (orderNumber != null) {
            properties.put("orderNumber", orderNumber);
        }

        return properties;
    }
}
