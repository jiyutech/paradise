package tech.jiyu.paradise.core.account.dao.po;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import tech.jiyu.paradise.core.account.domain.EducationExperience;

/**
 * Created By EducationExperience
 *
 * Date 2017/12/27 10:18
 *
 * @author SimonChan;emailAddress: simonchan@jiyu.tech
 */
@NoArgsConstructor
@Getter
@Setter
public class EducationExperiencePO {

    private Long id;

    private Long accountId;

    private Long createdDate;

    private Long modifiedDate;

    private String schoolName;

    private String subjectName;

    private Integer orderNum;

    public static EducationExperiencePO create(Long id, EducationExperience educationExperience) {
        if (educationExperience == null) {
            return null;
        }

        EducationExperiencePO educationExperiencePO = new EducationExperiencePO();
        Long now = System.currentTimeMillis();

        educationExperiencePO.setId(id);
        educationExperiencePO.setAccountId(educationExperience.getAccountId());
        educationExperiencePO.setCreatedDate(now);
        educationExperiencePO.setModifiedDate(now);
        educationExperiencePO.setSchoolName(educationExperience.getSchoolName());
        educationExperiencePO.setSubjectName(educationExperience.getSubjectName());

        return educationExperiencePO;
    }
}
