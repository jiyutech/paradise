package tech.jiyu.paradise.core.authentication.exception;

import tech.jiyu.utility.api.errors.annotations.ApiErrorKey;

/**
 * @author <a href="mailto:chenshao@dekeapp.com">shawsmith</a>
 */
@ApiErrorKey(group = "oauth2", id= "not.supported")
public class NoSuchOAuth2ServiceException extends RuntimeException {

    public NoSuchOAuth2ServiceException() {
    }

    public NoSuchOAuth2ServiceException(String message) {
        super(message);
    }

    public NoSuchOAuth2ServiceException(String message, Throwable cause) {
        super(message, cause);
    }

    public NoSuchOAuth2ServiceException(Throwable cause) {
        super(cause);
    }
}
