package tech.jiyu.paradise.core.conf;

import com.alibaba.otter.canal.client.CanalConnector;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import tech.jiyu.paradise.core.canal.handlers.*;
import tech.jiyu.utility.cache.CacheTemplate;
import tech.jiyu.utility.canal.CanalAgent;
import tech.jiyu.utility.canal.RowChangeHandler;
import tech.jiyu.utility.exception.ExceptionNotificationService;

@Configuration
@ConditionalOnClass({CanalAgent.class, CanalConnector.class})
public class ParadiseRowChangeHandlerConfiguration {

    @Bean
    public RowChangeHandler accountRowChangeHandler(CacheTemplate cacheTemplate, ExceptionNotificationService exceptionNotificationService) {
        return new AccountCacheRowChangeHandler(cacheTemplate, exceptionNotificationService);
    }

    @Bean
    public RowChangeHandler accountHistoryPhoneNumberRowChangeHandler(CacheTemplate cacheTemplate, ExceptionNotificationService exceptionNotificationService) {
        return new AccountHistoryPhoneNumberCacheRowChangeHandler(cacheTemplate, exceptionNotificationService);
    }

    @Bean
    public RowChangeHandler accountPrivilegeRowChangeHandler(CacheTemplate cacheTemplate, ExceptionNotificationService exceptionNotificationService) {
        return new AccountPrivilegeCacheRowChangeHandler(cacheTemplate, exceptionNotificationService);
    }

    @Bean
    public RowChangeHandler educationRowChangeHandler(CacheTemplate cacheTemplate, ExceptionNotificationService exceptionNotificationService) {
        return new EducationCacheRowChangeHandler(cacheTemplate, exceptionNotificationService);
    }

    @Bean
    public RowChangeHandler loginRowChangeHandler(CacheTemplate cacheTemplate, ExceptionNotificationService exceptionNotificationService) {
        return new LoginCacheRowChangeHandler(cacheTemplate, exceptionNotificationService);
    }

    @Bean
    public RowChangeHandler privilegeRowChangeHandler(CacheTemplate cacheTemplate, ExceptionNotificationService exceptionNotificationService) {
        return new PrivilegeCacheRowChangeHandler(cacheTemplate, exceptionNotificationService);
    }

    @Bean
    public RowChangeHandler residenceRowChangeHandler(CacheTemplate cacheTemplate, ExceptionNotificationService exceptionNotificationService) {
        return new ResidenceCacheRowChangeHandler(cacheTemplate, exceptionNotificationService);
    }

    @Bean
    public RowChangeHandler workHistoryRowChangeHandler(CacheTemplate cacheTemplate, ExceptionNotificationService exceptionNotificationService) {
        return new WorkHistoryCacheRowChangeHandler(cacheTemplate, exceptionNotificationService);
    }
}
