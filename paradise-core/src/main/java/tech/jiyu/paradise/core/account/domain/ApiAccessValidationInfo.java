package tech.jiyu.paradise.core.account.domain;

import lombok.Data;
import org.apache.commons.lang3.StringUtils;
import org.springframework.web.servlet.HandlerMapping;
import tech.jiyu.paradise.core.account.enumtype.ApiAccessEnumType;
import tech.jiyu.paradise.core.login.domain.LoginInfo;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

@Data
public class ApiAccessValidationInfo {

    private static final String ACCOUNT_ID_NAME = "accountId";
    private static final String LOGIN_TOKEN_NAME = "loginToken";

    private ApiAccessEnumType accessEnumType; // 访问的接口类型
    private LoginInfo loginInfo; // 当前账号的登录信息
    private Long targetAccountId; // 需要访问的目标账号id
    private String countryCode; // 需要访问的目标账号的国家码
    private String phoneNumber; // 需要访问的目标账号的手机号
    private Map pathVariables;
    private boolean validParams = true;

    public ApiAccessValidationInfo(ApiAccessEnumType accessEnumType, LoginInfo loginInfo, HttpServletRequest request) {
        this.accessEnumType = accessEnumType;
        this.loginInfo = loginInfo;
        this.pathVariables = (Map) request.getAttribute(HandlerMapping.URI_TEMPLATE_VARIABLES_ATTRIBUTE);
        if (pathVariables != null && pathVariables.containsKey(ACCOUNT_ID_NAME)) {
            if (StringUtils.isNumeric(pathVariables.get(ACCOUNT_ID_NAME).toString())) {
                this.targetAccountId = Long.valueOf(pathVariables.get(ACCOUNT_ID_NAME).toString());
            } else {
                this.validParams = false;
            }
        }
        this.countryCode = request.getParameter("countryCode");
        this.phoneNumber = request.getParameter("phoneNumber");
    }

}
