package tech.jiyu.paradise.core.authentication.exception;

import tech.jiyu.utility.api.errors.annotations.ApiErrorKey;

/**
 * @author <a href="mailto:chenshao@dekeapp.com">shawsmith</a>
 */
@ApiErrorKey(group = "oauth2",
             id = "cannot.acquire.userInfo")
public class CannotAcquireOpenIdException extends RuntimeException{
    public CannotAcquireOpenIdException() {
    }

    public CannotAcquireOpenIdException(String message) {
        super(message);
    }

    public CannotAcquireOpenIdException(String message, Throwable cause) {
        super(message, cause);
    }

    public CannotAcquireOpenIdException(Throwable cause) {
        super(cause);
    }
}
