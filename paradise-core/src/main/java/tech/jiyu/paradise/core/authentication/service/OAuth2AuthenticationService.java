package tech.jiyu.paradise.core.authentication.service;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import tech.jiyu.paradise.core.authentication.domain.OAuth2Authentication;
import tech.jiyu.paradise.core.authentication.domain.OAuth2Type;

/**
 * @author <a href="mailto:chenshao@dekeapp.com">shawsmith</a>
 */
public interface OAuth2AuthenticationService {

    OAuth2Authentication saveOrUpdate(@NotNull @Valid OAuth2Authentication authentication);

    void deleteByAccountId(Long accountId);

    OAuth2Authentication findByAccountIdAndOAuth2Type(Long accountId, @NotNull OAuth2Type type);
}
