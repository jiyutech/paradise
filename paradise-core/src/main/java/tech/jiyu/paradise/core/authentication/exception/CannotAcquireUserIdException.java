package tech.jiyu.paradise.core.authentication.exception;

import tech.jiyu.utility.api.errors.annotations.ApiErrorKey;

/**
 * Created By CannotAcquireUserIdException
 *
 * Date 2018/3/13 11:25
 *
 * @author SimonChan;emailAddress: simonchan@jiyu.tech
 */
@ApiErrorKey(group = "oauth2",
             id = "cannot.acquire.userId")
public class CannotAcquireUserIdException extends RuntimeException {

    public CannotAcquireUserIdException() {
        super();
    }

    public CannotAcquireUserIdException(String message) {
        super(message);
    }

    public CannotAcquireUserIdException(String message, Throwable cause) {
        super(message, cause);
    }

    public CannotAcquireUserIdException(Throwable cause) {
        super(cause);
    }

    protected CannotAcquireUserIdException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
