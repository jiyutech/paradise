package tech.jiyu.paradise.core.account.dao.mapper;

import org.apache.ibatis.annotations.Mapper;

import java.util.Collection;
import java.util.List;

import tech.jiyu.paradise.core.account.dao.po.AccountHistoryPhoneNumberPO;

/**
 * @author <a href="mailto:chenshao@dekeapp.com">shawsmith</a>
 */
@Mapper
public interface AccountHistoryPhoneNumberMapper {

    void insert(AccountHistoryPhoneNumberPO historyPhoneNumber);

    List<AccountHistoryPhoneNumberPO> selectByAccountIds(Collection<Long> accountIds);

    void deleteByAccountId(Long accountId);
}
