package tech.jiyu.paradise.core.privileges.domain;

import java.util.Map;

import tech.jiyu.restful.api.domain.KeywordPaginationListQuery;

/**
 * @author <a href="mailto:chenshao@jinjie.tech">shawsmith</a>
 */
public class PrivilegeListQuery extends KeywordPaginationListQuery {

    public static PrivilegeListQuery parse(Map<String, String> parameters) {
        PrivilegeListQuery query = new PrivilegeListQuery();
        query.doParse(parameters, "order_number", Boolean.TRUE);
        return query;
    }
}
