package tech.jiyu.paradise.core.account.exception;

import tech.jiyu.utility.api.errors.annotations.ApiErrorKey;

/**
 * @author <a href="mailto:chenshao@dekeapp.com">shawsmith</a>
 */
@ApiErrorKey(group = "account",
             id = "already.activated")
public class AccountAlreadyActivatedExeception extends RuntimeException {

    public AccountAlreadyActivatedExeception() {
    }

    public AccountAlreadyActivatedExeception(String message) {
        super(message);
    }

    public AccountAlreadyActivatedExeception(String message, Throwable cause) {
        super(message, cause);
    }

    public AccountAlreadyActivatedExeception(Throwable cause) {
        super(cause);
    }
}
