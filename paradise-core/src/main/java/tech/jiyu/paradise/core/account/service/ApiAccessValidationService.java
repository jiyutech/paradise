package tech.jiyu.paradise.core.account.service;

import tech.jiyu.paradise.core.account.domain.ApiAccessValidationInfo;

public interface ApiAccessValidationService {

    Boolean validate(ApiAccessValidationInfo validationInfo);
}
