package tech.jiyu.paradise.core.account.service.impl;

import com.google.common.collect.Maps;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import tech.jiyu.paradise.core.account.domain.Account;
import tech.jiyu.paradise.core.account.domain.EducationExperience;
import tech.jiyu.paradise.core.account.domain.Residence;
import tech.jiyu.paradise.core.account.domain.WorkHistory;
import tech.jiyu.paradise.core.account.service.AccountAdditionalAttributeService;
import tech.jiyu.paradise.core.account.service.EducationExperienceService;
import tech.jiyu.paradise.core.account.service.ResidenceService;
import tech.jiyu.paradise.core.account.service.WorkHistoryService;
import tech.jiyu.utility.cache.CacheStrategy;

/**
 * Created By AccountAdditionalListAttributeServiceImpl
 *
 * Date 2017/12/27 19:59
 *
 * @author SimonChan;emailAddress: simonchan@jiyu.tech
 */
@Service
public class AccountBuildInAdditionalAttributeService implements AccountAdditionalAttributeService {

    @Autowired
    private EducationExperienceService educationExperienceService;

    @Autowired
    private WorkHistoryService workHistoryService;

    @Autowired
    private ResidenceService residenceService;

    @Override
    public Map<Long, Map<String, Object>> getAdditionalAttributes(Collection<Account> accounts) {
        Map<Long, Map<String, Object>> accountAdditionalAttributes = Maps.newHashMap();

        accounts.stream().filter(Objects::nonNull).forEach(account -> {
            Map<String, Object> properties = Maps.newHashMap();

            Long accountId = account.getId();
            List<EducationExperience> educationExperiences = educationExperienceService.findByAccountId(accountId, CacheStrategy.CACHE_PREFER);
            List<WorkHistory> workHistories = workHistoryService.findByAccountId(accountId, CacheStrategy.CACHE_PREFER);
            List<Residence> residences = residenceService.findByAccountId(accountId, CacheStrategy.CACHE_PREFER);

            if (CollectionUtils.isNotEmpty(educationExperiences)) {
                properties.put("educationExperiences", educationExperiences);
            }
            if (CollectionUtils.isNotEmpty(workHistories)) {
                properties.put("workHistories", workHistories);
            }
            if (CollectionUtils.isNotEmpty(residences)) {
                properties.put("residences", residences);
            }
            accountAdditionalAttributes.put(accountId, properties);
        });

        return accountAdditionalAttributes;
    }
}
