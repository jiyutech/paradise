package tech.jiyu.paradise.core.account.service;

import org.springframework.validation.annotation.Validated;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import tech.jiyu.paradise.core.account.domain.InvitationCode;
import tech.jiyu.paradise.core.account.domain.InvitationCodeListQuery;
import tech.jiyu.utility.pagination.PaginationResult;

/**
 * Created By InvitationCodeService
 *
 * Date 2018/2/9 16:23
 *
 * @author SimonChan;emailAddress: simonchan@jiyu.tech
 */
@Validated
public interface InvitationCodeService {

    InvitationCode generateInvitationCode();

    PaginationResult<InvitationCode, Void> list(InvitationCodeListQuery invitationCodeListQuery);

    InvitationCode selectByCode(String invitationCode);

    InvitationCode updateByCode(@NotNull @Valid InvitationCode invitationCode);
}
