package tech.jiyu.paradise.core.account.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import tech.jiyu.paradise.core.account.domain.Account;
import tech.jiyu.paradise.core.account.domain.AccountEvent;
import tech.jiyu.paradise.core.account.domain.AccountListener;
import tech.jiyu.paradise.core.account.domain.AccountPostCreatedEvent;
import tech.jiyu.paradise.core.account.domain.ActivationConfig;
import tech.jiyu.paradise.core.account.service.AccountActivationService;

/**
 * @author <a href="mailto:chenshao@dekeapp.com">shawsmith</a>
 */
@Component
public class AccountActivationListener implements AccountListener {

    @Autowired
    private AccountActivationService accountActivationService;

    @Override
    public void onEvent(AccountEvent event) {
        if (event instanceof AccountPostCreatedEvent) {
            AccountPostCreatedEvent postCreatedEvent = (AccountPostCreatedEvent) event;
            ActivationConfig activationConfig = postCreatedEvent.getActivationConfig();
            Account account = postCreatedEvent.getAccount();
            if (activationConfig != null && Boolean.TRUE.equals(activationConfig.getNeedActivation()) && account != null) {
                accountActivationService.sendActivationMail(account, activationConfig);
            }
        }
    }
}
