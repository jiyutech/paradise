package tech.jiyu.paradise.core.account.dao.po;

import lombok.Data;

/**
 * @author Simon [simonchan@jinjie.tech]
 */
@Data
public class AccountPhoneNumber {
    private Long id;
    private String countryCode;
    private String phoneNumber;
}
