package tech.jiyu.paradise.core.account.domain;

/**
 * @author <a href="mailto:chenshao@dekeapp.com">shawsmith</a>
 */
public interface AccountListener {

    void onEvent(AccountEvent event);
}
