package tech.jiyu.paradise.core.account.exception;

import tech.jiyu.utility.api.errors.annotations.ApiErrorKey;

/**
 * @author <a href="mailto:chenshao@dekeapp.com">shawsmith</a>
 */
@ApiErrorKey(group = "account",
        id = "duplicated.phone.number.v2")
public class DuplicatedPhoneNumberException extends RuntimeException {

    private final String countryCode;
    private final String phoneNumber;

    public DuplicatedPhoneNumberException(String countryCode, String phoneNumber, String message) {
        super(message);
        this.countryCode = countryCode;
        this.phoneNumber = phoneNumber;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }
}
