package tech.jiyu.paradise.core.account.dao.mapper;

import org.apache.ibatis.annotations.Mapper;

import java.util.List;

import tech.jiyu.paradise.core.account.dao.po.InvitationCodePO;
import tech.jiyu.paradise.core.account.domain.InvitationCodeListQuery;


/**
 * Created By InvitationCodeMapper
 *
 * Date 2018/2/9 15:58
 *
 * @author SimonChan;emailAddress: simonchan@jiyu.tech
 */
@Mapper
public interface InvitationCodeMapper {

    void insert(InvitationCodePO invitationCodePO);

    InvitationCodePO selectByCode(String code);

    List<InvitationCodePO> selectByQuery(InvitationCodeListQuery invitationCodeListQuery);

    Integer countByQuery(InvitationCodeListQuery invitationCodeListQuery);

    void updateByCode(InvitationCodePO invitationCodePO);
}
