package tech.jiyu.paradise.core.account.domain;

public class PasswordResetResult {

    private final Long accountId;
    private final String accountUserName;
    private final String newPassword;

    public PasswordResetResult(Long accountId,
                               String accountUserName,
                               String newPassword) {
        this.accountId = accountId;
        this.accountUserName = accountUserName;
        this.newPassword = newPassword;
    }

    public Long getAccountId() {
        return accountId;
    }

    public String getAccountUserName() {
        return accountUserName;
    }

    public String getNewPassword() {
        return newPassword;
    }
}
