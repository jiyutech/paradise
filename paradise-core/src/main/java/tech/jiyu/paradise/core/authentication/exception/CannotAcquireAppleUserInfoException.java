package tech.jiyu.paradise.core.authentication.exception;

import tech.jiyu.utility.api.errors.annotations.ApiErrorKey;

/**
 * @author Simon [simonchan@jinjie.tech]
 */
@ApiErrorKey(group = "oauth2", id = "can.not.acquire.apple.user.info")
public class CannotAcquireAppleUserInfoException extends RuntimeException {

    public CannotAcquireAppleUserInfoException() {
        super();
    }

    public CannotAcquireAppleUserInfoException(String message) {
        super(message);
    }

    public CannotAcquireAppleUserInfoException(String message, Throwable cause) {
        super(message, cause);
    }

    public CannotAcquireAppleUserInfoException(Throwable cause) {
        super(cause);
    }

    protected CannotAcquireAppleUserInfoException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
