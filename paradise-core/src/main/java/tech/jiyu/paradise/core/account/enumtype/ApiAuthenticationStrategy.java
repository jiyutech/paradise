package tech.jiyu.paradise.core.account.enumtype;

public enum ApiAuthenticationStrategy {

    DENY("拒绝"), // 默认拒绝
    ALLOW("允许"), // 可能是不做处理，也有可能只需要校验登录即可，不要求被操作账号必须是当前账号
    SELF("当前账号"), // 被操作的账号必须是当前账号
    ;

    private String desc;

    ApiAuthenticationStrategy(String desc) {
        this.desc = desc;
    }
}
