package tech.jiyu.paradise.core.account.dao.mapper;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.Collection;
import java.util.List;

import tech.jiyu.paradise.core.account.dao.po.AccountPO;
import tech.jiyu.paradise.core.account.domain.AccountListQuery;
import tech.jiyu.restful.api.domain.RestfulApiMapper;
import tech.jiyu.utility.datastructure.Pair;
import tech.jiyu.utility.web.TimeRange;

/**
 * @author <a href="mailto:chenshao@dekeapp.com">shawsmith</a>
 */
@Mapper
public interface AccountMapper extends RestfulApiMapper<AccountPO> {

    AccountPO selectByPhoneNumber(@Param("companyId") Long companyId, @Param("phoneNumber") String phoneNumber);

    AccountPO selectByUserName(@Param("companyId") Long companyId, @Param("userName") String userName);

    AccountPO selectByEmail(@Param("companyId") Long companyId, @Param("email") String email);

    AccountPO selectByWeChatOpenId(@Param("companyId") Long companyId, @Param("weChatOpenId") String weChatOpenId);

    AccountPO selectByWeChatTinyAppOpenId(@Param("companyId") Long companyId, @Param("weChatTinyAppOpenId") String openId);

    AccountPO selectByWeChatH5AppOpenId(@Param("companyId") Long companyId, @Param("weChatH5AppOpenId") String openId);

    AccountPO selectByQqOpenId(@Param("companyId") Long companyId, @Param("qqOpenId") String qqOpenId);

    List<AccountPO> selectByRegistrationRange(@Param("companyId") Long companyId, @Param("range") TimeRange range, @Param("roleTypeCode") String roleTypeCode);

    AccountPO selectByInvitationCode(@Param("companyId") Long companyId, @Param("invitationCode") String invitationCode);

    List<Pair<String, Long>> countByRoleTypeCodes(AccountListQuery accountListQuery);

    AccountPO selectByFacebookUserId(@Param("companyId") Long companyId, @Param("facebookUserId") String facebookUserId);

    List<Long> selectIdsByCompanyId(Long companyId);

    List<Long> selectIdsByCompanyIdAndRoleTypeCodes(@Param("companyId") Long companyId, @Param("roleTypeCodes") Collection<String> roleTypeCodes);

    AccountPO selectByUnionId(@Param("companyId") Long companyId, @Param("unionId") String unionId);

    List<Pair<Long, Long>> countByCompanyIds(Collection<Long> companyIds);

    List<AccountPO> selectByCompanyIds(Collection<Long> companyIds);

    List<AccountPO> selectByIdRange(@Param("minId") Long minId, @Param("count") Long count);

    AccountPO selectByAppleUserId(@Param("companyId") Long companyId, @Param("appleUserId") String appleUserId);

    AccountPO selectByWhatsappCountryCodeAndPhoneNumber(@Param("companyId") Long companyId, @Param("whatsappCountryCode") String whatsappCountryCode, @Param("whatsappPhoneNumber") String whatsappPhoneNumber);

    AccountPO selectByTelegramId(@Param("companyId") Long companyId, @Param("telegramId") Long telegramId);

    AccountPO selectByCountryCodeAndPhoneNumber(@Param("companyId") Long companyId, @Param("countryCode") String countryCode, @Param("phoneNumber") String phoneNumber);

    void deleteById(AccountPO po);

    List<AccountPO> selectByIdsWithDeletedCondition(@Param("ids") Collection<Long> ids, @Param("deleted") Boolean deleted);
}
