package tech.jiyu.paradise.core.authentication.dao.mapper;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import tech.jiyu.paradise.core.authentication.dao.po.OAuth2AuthenticationPO;

/**
 * @author <a href="mailto:chenshao@dekeapp.com">shawsmith</a>
 */
@Mapper
public interface OAuth2AuthenticationMapper {

    void insert(OAuth2AuthenticationPO authentication);

    OAuth2AuthenticationPO selectByAccountIdAndTypeCode(@Param("accountId") Long accountId, @Param("type") String type);

    void updateById(OAuth2AuthenticationPO authentication);

    void deleteByAccountId(Long accountId);
}
