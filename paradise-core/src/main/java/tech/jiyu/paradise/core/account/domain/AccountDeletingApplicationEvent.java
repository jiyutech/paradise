package tech.jiyu.paradise.core.account.domain;

import org.springframework.context.ApplicationEvent;

public class AccountDeletingApplicationEvent extends ApplicationEvent {

    private Account account;

    public AccountDeletingApplicationEvent(Account account) {
        super(account);
        this.account = account;
    }

    public Account getAccount() {
        return account;
    }
}
