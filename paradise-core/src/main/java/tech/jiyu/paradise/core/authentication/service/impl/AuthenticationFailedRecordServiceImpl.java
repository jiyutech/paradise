
package tech.jiyu.paradise.core.authentication.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import tech.jiyu.paradise.core.authentication.dao.mapper.AuthenticationFailedRecordMapper;
import tech.jiyu.paradise.core.authentication.domain.AuthenticationFailedRecord;
import tech.jiyu.paradise.core.authentication.service.AuthenticationFailedRecordService;
import tech.jiyu.sequences.SequenceService;

/**
 * @author Simon [simonchan@jinjie.tech]
 */
@Service
public class AuthenticationFailedRecordServiceImpl implements AuthenticationFailedRecordService {

    @Autowired
    private SequenceService sequenceService;

    @Autowired
    private AuthenticationFailedRecordMapper authenticationFailedRecordMapper;

    @Override
    @Transactional
    public void record(Long accountId) {
        AuthenticationFailedRecord record = build(accountId);
        authenticationFailedRecordMapper.insertOrIncrement(record);
    }

    @Override
    @Transactional
    public void deleteByAccountId(Long accountId) {
        authenticationFailedRecordMapper.deleteByAccountId(accountId);
    }

    @Override
    public AuthenticationFailedRecord findByAccountId(Long accountId) {
        return authenticationFailedRecordMapper.selectByAccountId(accountId);
    }

    private AuthenticationFailedRecord build(Long accountId) {
        long now = System.currentTimeMillis();
        AuthenticationFailedRecord record = new AuthenticationFailedRecord();
        record.setId(sequenceService.nextLongValue());
        record.setCreatedDate(now);
        record.setModifiedDate(now);
        record.setAccountId(accountId);
        record.setCount(1);
        return record;
    }
}
