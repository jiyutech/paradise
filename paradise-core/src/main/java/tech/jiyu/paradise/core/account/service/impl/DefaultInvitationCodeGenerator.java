package tech.jiyu.paradise.core.account.service.impl;

import org.apache.commons.lang3.RandomStringUtils;

import tech.jiyu.paradise.core.account.dao.mapper.AccountMapper;
import tech.jiyu.paradise.core.account.domain.Account;
import tech.jiyu.paradise.core.account.service.InvitationCodeGenerator;

/**
 * @author <a href="mailto:chenshao@jiyu.tech">shawsmith</a>
 */
public class DefaultInvitationCodeGenerator implements InvitationCodeGenerator {

    private AccountMapper accountMapper;
    private Integer codeLength;

    public DefaultInvitationCodeGenerator(AccountMapper accountMapper, Integer codeLength) {
        this.accountMapper = accountMapper;
        this.codeLength = codeLength;
    }

    @Override
    public String generate(Account account) {
        String invitationCode = RandomStringUtils.randomNumeric(codeLength);
        while (accountMapper.selectByInvitationCode(account.getCompanyId(), invitationCode) != null) {
            invitationCode = RandomStringUtils.randomNumeric(codeLength);
        }
        return invitationCode;
    }
}
