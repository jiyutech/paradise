package tech.jiyu.paradise.core.account.domain;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Created By EducationExperience
 *
 * Date 2017/12/27 10:02
 *
 * @author SimonChan;emailAddress: simonchan@jiyu.tech
 */

@NoArgsConstructor
@Getter
@Setter
public class EducationExperience {

    private Long id;

    private Long accountId;

    @NotNull(message = "{account.education.experience.school.name.limit}")
    @Size(min = 1,
          max = 128,
          message = "{account.education.experience.school.name.limit}")
    private String schoolName;

    @NotNull(message = "{account.education.experience.subject.name.limit}")
    @Size(min = 1,
          max = 128,
          message = "{account.education.experience.subject.name.limit}")
    private String subjectName;
}
