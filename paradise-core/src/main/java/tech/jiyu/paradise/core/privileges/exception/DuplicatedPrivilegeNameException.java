package tech.jiyu.paradise.core.privileges.exception;

import tech.jiyu.utility.api.errors.annotations.ApiErrorKey;

/**
 * @author <a href="mailto:chenshao@jinjie.tech">shawsmith</a>
 */
@ApiErrorKey(group = "account",
             id = "duplicated.privilege.name")
public class DuplicatedPrivilegeNameException extends RuntimeException {
}
