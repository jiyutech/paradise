package tech.jiyu.paradise.core.authentication.exception;

import tech.jiyu.utility.api.errors.annotations.ApiErrorKey;

/**
 * CreatedDate 2018-07-09 下午8:36
 *
 * @author SimonChan;emailAddress: simonchan@jinjie.tech
 */
@ApiErrorKey(group = "oauth2",
             id = "cannot.acquire.facebook.userInfo")
public class NoSuchFacebookUserInfoException extends RuntimeException {

    public NoSuchFacebookUserInfoException() {
        super();
    }

    public NoSuchFacebookUserInfoException(String message) {
        super(message);
    }

    public NoSuchFacebookUserInfoException(String message, Throwable cause) {
        super(message, cause);
    }

    public NoSuchFacebookUserInfoException(Throwable cause) {
        super(cause);
    }

    protected NoSuchFacebookUserInfoException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
