package tech.jiyu.paradise.core.account.dao.mapper;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

import tech.jiyu.paradise.core.account.dao.po.AccountPhoneNumber;
import tech.jiyu.paradise.core.account.dao.po.AccountPhoneNumberVerificationResult;

/**
 * @author Simon [simonchan@jinjie.tech]
 */
@Mapper
public interface AccountPhoneNumberVerificationResultMapper {

    List<AccountPhoneNumber> selectByIdRange(@Param("minId") Long minId);

    void batchInsertResults(@Param("results") List<AccountPhoneNumberVerificationResult> results);
}
