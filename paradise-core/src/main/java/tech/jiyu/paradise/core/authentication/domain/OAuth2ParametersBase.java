package tech.jiyu.paradise.core.authentication.domain;

import java.util.Collection;
import java.util.Map;

import lombok.Data;

/**
 * 子类里有部分字段和当前类的重复了，目前暂时不去删除了，减少改动吧，后续可以删除，问题不大
 */
@Data
public abstract class OAuth2ParametersBase {
    private String roleTypeCode;
    private String phoneNumber;
    private String countryCode;
    private String isoCode;
    private String registrationSource;
    private String registrationTrafficType;
    private String loginSource;
    private String password;
    private String email;
    private Collection<Long> privilegeIds;
    private String firstName;
    private String lastName;
    private Boolean agreeToReceiveInfo;
    private String lastLanguageCode;
    private Map<String, Object> additionalProperties;
}
