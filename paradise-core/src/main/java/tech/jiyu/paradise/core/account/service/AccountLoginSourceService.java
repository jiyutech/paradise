package tech.jiyu.paradise.core.account.service;

import java.util.Collection;
import java.util.Map;

/**
 * @author <a href="mailto:chenshao@jiyu.tech">shawsmith</a>
 */
public interface AccountLoginSourceService {

    Map<Long, Collection<String>> getAccountLoginSources(Collection<Long> accountIds);

    Boolean supportLoginSource(Long accountId, String loginSource);
}
