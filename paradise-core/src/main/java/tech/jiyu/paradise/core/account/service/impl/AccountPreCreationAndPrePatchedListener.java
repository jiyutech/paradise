package tech.jiyu.paradise.core.account.service.impl;

import com.google.common.base.Throwables;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.Objects;

import tech.jiyu.idcard.verification.domain.IdCardVerification;
import tech.jiyu.idcard.verification.service.IdCardVerificationService;
import tech.jiyu.paradise.core.account.domain.*;
import tech.jiyu.paradise.core.account.exception.DuplicatedPhoneNumberException;
import tech.jiyu.paradise.core.account.exception.DuplicatedUsernameException;
import tech.jiyu.paradise.core.account.exception.EmptyPasswordException;
import tech.jiyu.paradise.core.account.service.AccountEmailAndPasswordValidatorService;
import tech.jiyu.paradise.core.account.service.AccountService;
import tech.jiyu.paradise.core.account.service.InvitationCodeGenerator;
import tech.jiyu.paradise.core.login.service.LoginService;
import tech.jiyu.paradise.core.util.LoginUtils;

/**
 * @author <a href="mailto:chenshao@jiyu.tech">shawsmith</a>
 */
@Component
public class AccountPreCreationAndPrePatchedListener implements AccountListener {

    @Autowired
    @Lazy
    private AccountService accountService;

    @Autowired(required = false)
    private IdCardVerificationService idCardVerificationService;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private InvitationCodeGenerator invitationCodeGenerator;

    @Autowired
    private LoginService loginService;

    @Autowired
    private AccountEmailAndPasswordValidatorService accountEmailAndPasswordValidatorService;

    @Override
    public void onEvent(AccountEvent event) {
        if (event instanceof AccountPreCreatedEvent) {
            Account account = ((AccountPreCreatedEvent) event).getAccount();
            validatePropertyOnCreate(account);
            encryptPassword(account);
            generateInvitationCode(account);
            populateDefaultProperties(account);
        } else if (event instanceof AccountPrePatchedEvent) {
            Account account = ((AccountPrePatchedEvent) event).getAccount();
            validatePropertyOnPatch(account);
        } else if (event instanceof AccountPostPatchedEvent) {
            // 如果账户被禁用, 立即清空登录信息.
            Account account = ((AccountPostPatchedEvent) event).getAccount();
            clearLoginInfoIfDisabled(account);
        }
    }

    private void validatePropertyOnCreate(Account account) {
        if (account == null) {
            return;
        }

        Long companyId = account.getCompanyId();

        checkPhoneNumberDuplicated(companyId, account.getCountryCode(), account.getPhoneNumber());
        checkUserNameDuplicated(companyId, account.getUsername());
        accountEmailAndPasswordValidatorService.checkEmailDuplicated(null, companyId, account.getEmail());
        unbindWeChatOpenIdIfNecessary(companyId, account.getWeChatOpenId(), null);
        unbindWeChatTinyAppOpenIdIfNecessary(companyId, account.getWeChatTinyAppOpenId(), null);
        unbindWeChatH5AppOpenIdIfNecessary(companyId, account.getWeChatH5AppOpenId(), null);
        unbindQqOpenIdIfNecessary(companyId, account.getQqOpenId(), null);
        unbindUnionIdIfNecessary(companyId, account.getUnionId(), null);
        unbindFacebookUserIdIfNecessary(companyId, account.getFacebookUserId(), null);
        unbindAppleUserIdIfNecessary(companyId, account.getAppleUserId(), null);
        unbindWhatsappCountryCodeAndPhoneNumberIfNecessary(companyId, account.getWhatsappCountryCode(), account.getWhatsappPhoneNumber(), null);
        unbindTelegramIdIfNecessary(companyId, account.getTelegramId(), null);
        accountEmailAndPasswordValidatorService.checkPasswordEmpty(account);

        Boolean verified = checkIdCardNameAndNumber(account.getIdCardName(), account.getIdCardNumber());
        account.setVerified(verified);
    }

    private void checkPhoneNumberDuplicated(Long companyId, String countryCode, String phoneNumber) {
        if (accountService.findByPhoneNumber(companyId, countryCode, phoneNumber) != null) {
            String message = "companyId: " + companyId + "-" + countryCode + "-" + phoneNumber;
            throw new DuplicatedPhoneNumberException(countryCode, phoneNumber, message);
        }
    }

    private void checkUserNameDuplicated(Long companyId, String userName) {
        if (accountService.findByUserName(companyId, userName) != null) {
            throw new DuplicatedUsernameException(userName);
        }
    }

    private void unbindWeChatOpenIdIfNecessary(Long companyId, String weChatOpenId, Long accountId) {
        if (weChatOpenId == null) {
            return;
        }

        Account account = accountService.findByWeChatOpenId(companyId, weChatOpenId);
        if (account != null && (accountId == null || !Objects.equals(account.getId(), accountId))) {
            accountService.unbindWeChatOpenId(companyId, weChatOpenId);
        }
    }

    private void unbindWeChatTinyAppOpenIdIfNecessary(Long companyId, String weChatTinyAppOpenId, Long accountId) {
        if (weChatTinyAppOpenId == null) {
            return;
        }

        Account account = accountService.findByWeChatTinyAppOpenId(companyId, weChatTinyAppOpenId);
        if (account != null && (accountId == null || !Objects.equals(account.getId(), accountId))) {
            accountService.unbindWeChatTinyAppOpenId(companyId, weChatTinyAppOpenId);
        }
    }

    private void unbindWeChatH5AppOpenIdIfNecessary(Long companyId, String weChatH5AppOpenId, Long accountId) {
        if (weChatH5AppOpenId == null) {
            return;
        }

        Account account = accountService.findByWeChatH5AppOpenId(companyId, weChatH5AppOpenId);
        if (account != null && (accountId == null || !Objects.equals(account.getId(), accountId))) {
            accountService.unbindWeChatH5AppOpenId(companyId, weChatH5AppOpenId);
        }
    }

    private void unbindQqOpenIdIfNecessary(Long companyId, String qqOpenId, Long accountId) {
        if (qqOpenId == null) {
            return;
        }

        Account account = accountService.findByQQOpenId(companyId, qqOpenId);
        if (account != null && (accountId == null || !Objects.equals(account.getId(), accountId))) {
            accountService.unbindQQOpenId(companyId, qqOpenId);
        }
    }

    private void unbindUnionIdIfNecessary(Long companyId, String unionId, Long accountId) {
        if (unionId == null) {
            return;
        }

        Account account = accountService.findByUnionId(companyId, unionId);
        if (account != null && (accountId == null || !Objects.equals(account.getId(), accountId))) {
            accountService.unbindUnionId(companyId, unionId);
        }
    }

    private void unbindFacebookUserIdIfNecessary(Long companyId, String facebookUserId, Long accountId) {
        if (facebookUserId == null) {
            return;
        }

        Account account = accountService.findByFacebookUserId(companyId, facebookUserId);
        if (account != null && (accountId == null || !Objects.equals(account.getId(), accountId))) {
            accountService.unbindFacebookUserId(companyId, facebookUserId);
        }
    }

    private void unbindAppleUserIdIfNecessary(Long companyId, String appleUserId, Long accountId) {
        if (appleUserId == null) {
            return;
        }

        Account account = accountService.findByAppleUserId(companyId, appleUserId);
        if (account != null && (accountId == null || !Objects.equals(account.getId(), accountId))) {
            accountService.unbindAppleUserId(companyId, appleUserId);
        }
    }

    private void unbindWhatsappCountryCodeAndPhoneNumberIfNecessary(Long companyId, String countryCode, String phoneNumber, Long accountId) {
        if (countryCode == null || phoneNumber == null) {
            return;
        }

        Account account = accountService.findByWhatsappCountryCodeAndPhoneNumber(companyId, countryCode, phoneNumber);
        if (account != null && (accountId == null || !Objects.equals(account.getId(), accountId))) {
            accountService.unbindWhatsappCountryCodeAndPhoneNumber(companyId, countryCode, phoneNumber);
        }
    }

    private void unbindTelegramIdIfNecessary(Long companyId, Long telegramId, Long accountId) {
        if (telegramId == null) {
            return;
        }

        Account account = accountService.findByTelegramId(companyId, telegramId);
        if (account != null && (accountId == null || !Objects.equals(account.getId(), accountId))) {
            accountService.unbindTelegramId(companyId, telegramId);
        }
    }

    private void checkPasswordEmpty(Account account) {
        // 如果用户名和密码有任意一个不为空, 则密码不能为空.
        boolean usernameAndPhoneNumberAndEmailAllEmpty = StringUtils.isBlank(account.getUsername()) && StringUtils.isBlank(account.getEmail());
        if (!usernameAndPhoneNumberAndEmailAllEmpty && StringUtils.isBlank(account.getPassword())) {
            throw new EmptyPasswordException();
        }
    }

    private Boolean checkIdCardNameAndNumber(String idCardName, String idCardNumber) {
        if (StringUtils.isNotBlank(idCardName) && StringUtils.isNotBlank(idCardNumber)) {
            try {
                IdCardVerification verification = new IdCardVerification();
                verification.setName(idCardName);
                verification.setIdCardNumber(idCardNumber);
                Boolean valid = idCardVerificationService.validate(verification);
                if (valid == null) {
                    valid = Boolean.FALSE;
                }
                return valid;
            } catch (IOException e) {
                throw new RuntimeException(Throwables.getStackTraceAsString(e), e);
            }
        }
        return Boolean.FALSE;
    }

    private void encryptPassword(Account account) {
        String plainPassword = account.getPassword();
        if (StringUtils.isNoneBlank(plainPassword)) {
            String encryptedPassword = passwordEncoder.encode(plainPassword);
            account.setPassword(encryptedPassword);
        }
    }

    private void generateInvitationCode(Account account) {
        if (account == null) {
            return;
        }

        String invitationCode = invitationCodeGenerator.generate(account);
        account.setInvitationCode(invitationCode);
    }

    private static void populateDefaultProperties(Account account) {
        if (StringUtils.isBlank(account.getRegistrationTrafficType())) {
            account.setRegistrationTrafficType(AccountService.DEFAULT_REGISTRATION_TRAFFIC_TYPE);
        }
    }

    private void validatePropertyOnPatch(Account account) {
        Long companyId = account.getCompanyId();

        String email = account.getEmail();
        if (email != null) {
            accountEmailAndPasswordValidatorService.checkEmailDuplicated(account.getId(), companyId, email);
        }

        Long accountId = account.getId();

        unbindWeChatOpenIdIfNecessary(companyId, account.getWeChatOpenId(), accountId);
        unbindQqOpenIdIfNecessary(companyId, account.getWeChatOpenId(), accountId);
        unbindWeChatTinyAppOpenIdIfNecessary(companyId, account.getWeChatTinyAppOpenId(), accountId);
        unbindWeChatH5AppOpenIdIfNecessary(companyId, account.getWeChatH5AppOpenId(), accountId);
        unbindFacebookUserIdIfNecessary(companyId, account.getFacebookUserId(), accountId);
        unbindUnionIdIfNecessary(companyId, account.getUnionId(), accountId);
        unbindAppleUserIdIfNecessary(companyId, account.getAppleUserId(), accountId);
        unbindWhatsappCountryCodeAndPhoneNumberIfNecessary(companyId, account.getWhatsappCountryCode(), account.getWhatsappPhoneNumber(), accountId);
        unbindTelegramIdIfNecessary(companyId, account.getTelegramId(), accountId);
    }

    private void clearLoginInfoIfDisabled(Account account) {
        if (isDisabledOrPolyAdminDisabled(account)) {
            LoginUtils.logout(String.valueOf(account.getId()), loginService);
        }
    }

    private boolean isDisabledOrPolyAdminDisabled(Account account) {
        if (account == null) {
            return false;
        }

        // account中只包含页面传递过来的那部分属性, 所以必须要先进行判空,而不能直接使用account.isActuallyEnabled()方法.
        return (account.getEnabled() != null && Boolean.FALSE.equals(account.getEnabled())) ||
                (account.getPolyAdminEnabled() != null && Boolean.FALSE.equals(account.getPolyAdminEnabled()));
    }
}
