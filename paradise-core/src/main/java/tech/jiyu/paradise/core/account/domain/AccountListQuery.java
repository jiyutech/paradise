package tech.jiyu.paradise.core.account.domain;

import tech.jiyu.restful.api.domain.PaginationListQuery;

/**
 * @author <a href="mailto:chenshao@dekeapp.com">shawsmith</a>
 */
public interface AccountListQuery extends PaginationListQuery {
}
