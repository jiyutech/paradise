package tech.jiyu.paradise.core.authentication.domain;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Collection;
import java.util.Set;

/**
 * @author <a href="mailto:chenshao@jiyu.tech">shawsmith</a>
 */
@NoArgsConstructor
@Getter
@Setter
public class OAuth2Parameters extends OAuth2ParametersBase {

    private Long companyId;

    /*
     *
     * 有以下几种调用方式:
     *
     *   1: authCode+authType方式, 先让客户授权(网页方式)
     *   2. accessToken + scopes + expiresIn + authType方式, App方式
     *   3. authCode + authType + weChatTinyAppId + weChatTinyAppEncryptedData + weChatTinyAppIv (获取手机号码的方式登录， 仅限于三方授权的方式).
     */
    private String authCode;
    private String authType;

    private String accessToken;
    private Set<String> scopes;
    private Long expiresIn;

    private String roleTypeCode;
    private String loginSource;
    private String name;
    private String avatar;

    // 以下这两个参数用于小程序获取union id, countryCode, phoneNumber.
    private String weChatTinyAppEncryptedData;
    private String weChatTinyAppIv;

    // 如果需要获取手机号码, 还需要提供小程序AppId,
    private String weChatTinyAppId;

    // 部分场景下需要由客户端指定登录后的重定向URL.
    private String loginRedirectUrl;

    private Collection<Long> privilegeIds;
}
