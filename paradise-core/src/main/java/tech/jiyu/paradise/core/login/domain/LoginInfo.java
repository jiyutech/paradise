package tech.jiyu.paradise.core.login.domain;

import com.fasterxml.jackson.annotation.JsonInclude;

import java.io.Serializable;

/**
 * @author <a href="mailto:chenshao@jiyu.tech">shawsmith</a>
 */
public class LoginInfo implements Serializable {

    private static final long serialVersionUID = -128391029810928010L;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private Long id;
    private String loginId;
    private String loginToken;
    private String roleTypeCode;
    private String loginSource;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLoginId() {
        return loginId;
    }

    public void setLoginId(String loginId) {
        this.loginId = loginId;
    }

    public String getLoginToken() {
        return loginToken;
    }

    public void setLoginToken(String loginToken) {
        this.loginToken = loginToken;
    }

    public String getRoleTypeCode() {
        return roleTypeCode;
    }

    public void setRoleTypeCode(String roleTypeCode) {
        this.roleTypeCode = roleTypeCode;
    }

    public String getLoginSource() {
        return loginSource;
    }

    public void setLoginSource(String loginSource) {
        this.loginSource = loginSource;
    }
}
