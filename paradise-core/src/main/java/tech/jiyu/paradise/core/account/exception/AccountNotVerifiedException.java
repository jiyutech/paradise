package tech.jiyu.paradise.core.account.exception;

import tech.jiyu.utility.api.errors.annotations.ApiErrorKey;

/**
 * @author <a href="mailto:chenshao@dekeapp.com">shawsmith</a>
 */
@ApiErrorKey(group = "account",
             id = "not.verified")
public class AccountNotVerifiedException extends RuntimeException {

    public AccountNotVerifiedException() {
    }

    public AccountNotVerifiedException(String message) {
        super(message);
    }

    public AccountNotVerifiedException(String message, Throwable cause) {
        super(message, cause);
    }

    public AccountNotVerifiedException(Throwable cause) {
        super(cause);
    }
}
