package tech.jiyu.paradise.core.util;

/**
 * @author <a href="mailto:chenshao@dekeapp.com">shawsmith</a>
 */
public final class SecurityUtils {

    private SecurityUtils() {
    }

    public static final String ROLE_PREFIX = "ROLE_";

    public static final String SYSTEM = "sys";
    public static final String IT = "it";
    public static final String CUSTOMER = "cs";
    public static final String BUYER = "buyer";
    public static final String OPERATOR = "op";
    public static final String DEVELOPER = "dev";
    public static final String SALES = "sales";
    public static final String SERVICE = "service";
    public static final String COPY = "copy";
    public static final String BOSS = "boss";
    public static final String STAFF = "staff";
    public static final String FASONIST = "fasonist";

    public static final String ROLE_SYSTEM = ROLE_PREFIX + SYSTEM;
    public static final String ROLE_IT = ROLE_PREFIX + IT;
    public static final String ROLE_CUSTOMER = ROLE_PREFIX + CUSTOMER;
    public static final String ROLE_BUYER = ROLE_PREFIX + BUYER;
    public static final String ROLE_OPERATOR = ROLE_PREFIX + OPERATOR;
    public static final String ROLE_SALES = ROLE_PREFIX + SALES;
    public static final String ROLE_DEVELOPER = ROLE_PREFIX + DEVELOPER;
    public static final String ROLE_SERVICE = ROLE_PREFIX + SERVICE;
    public static final String ROLE_COPY = ROLE_PREFIX + COPY;
    public static final String ROLE_BOSS = ROLE_PREFIX + BOSS;
    public static final String ROLE_STAFF = ROLE_PREFIX + STAFF;
    public static final String ROLE_FASONIST = ROLE_PREFIX + FASONIST;

}
