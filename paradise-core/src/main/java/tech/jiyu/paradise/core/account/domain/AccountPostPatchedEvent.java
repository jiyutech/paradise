package tech.jiyu.paradise.core.account.domain;

import java.util.Map;

/**
 * @author <a href="mailto:chenshao@dekeapp.com">shawsmith</a>
 */
public class AccountPostPatchedEvent extends AccountEvent {

    private final Account account;
    private final Account oldAccount;
    private final Map<String, Object> additionalAttributes;

    public AccountPostPatchedEvent(Account account,
                                   Account oldAccount,
                                   Map<String, Object> additionalAttributes) {
        this.account = account;
        this.oldAccount = oldAccount;
        this.additionalAttributes = additionalAttributes;
    }

    public Account getAccount() {
        return account;
    }

    public Account getOldAccount() {
        return oldAccount;
    }

    public Map<String, Object> getAdditionalAttributes() {
        return additionalAttributes;
    }
}
