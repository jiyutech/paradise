package tech.jiyu.paradise.core.privileges.service;

import java.util.Collection;
import java.util.Map;

import tech.jiyu.utility.cache.CacheStrategy;

/**
 * @author <a href="mailto:chenshao@jinjie.tech">shawsmith</a>
 */
public interface AccountPrivilegeService {

    String CACHE_PREFIX = "paradise:accountPrivileges:";

    void saveOrUpdate(Long accountId, Collection<Long> privilegeIds);

    Map<Long, Collection<Long>> findByAccountIds(Collection<Long> accountIds, CacheStrategy strategy);

    void deleteByAccountId(Long accountId);

    Long countByPrivilegeId(Long privilegeId);
}
