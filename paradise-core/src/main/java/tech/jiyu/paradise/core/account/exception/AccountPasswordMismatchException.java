package tech.jiyu.paradise.core.account.exception;

import tech.jiyu.utility.api.errors.annotations.ApiErrorKey;

/**
 * @author <a href="mailto:chenshao@jiyu.tech">shawsmith</a>
 */
@ApiErrorKey(group = "account",
             id = "password.mismatch")
public class AccountPasswordMismatchException extends RuntimeException {

    public AccountPasswordMismatchException() {
    }
}
