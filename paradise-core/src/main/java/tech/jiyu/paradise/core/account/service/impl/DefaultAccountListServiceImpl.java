package tech.jiyu.paradise.core.account.service.impl;

import com.google.common.collect.Maps;

import java.util.List;
import java.util.Map;

import tech.jiyu.paradise.core.account.dao.mapper.AccountMapper;
import tech.jiyu.paradise.core.account.dao.po.AccountPO;
import tech.jiyu.paradise.core.account.domain.Account;
import tech.jiyu.paradise.core.account.domain.AccountListQuery;
import tech.jiyu.paradise.core.account.domain.DefaultAccountListQuery;
import tech.jiyu.paradise.core.account.service.AccountHistoryPhoneNumberService;
import tech.jiyu.paradise.core.account.service.AccountListService;
import tech.jiyu.paradise.core.privileges.service.AccountPrivilegeService;
import tech.jiyu.paradise.core.privileges.service.PrivilegeService;
import tech.jiyu.paradise.core.util.AccountUtils;
import tech.jiyu.utility.datastructure.Pair;
import tech.jiyu.utility.pagination.PaginationCallback;
import tech.jiyu.utility.pagination.PaginationConfig;
import tech.jiyu.utility.pagination.PaginationResult;
import tech.jiyu.utility.pagination.PaginationTemplate;

/**
 * @author <a href="mailto:chenshao@dekeapp.com">shawsmith</a>
 */
public class DefaultAccountListServiceImpl implements AccountListService {

    private AccountMapper accountMapper;
    private PaginationTemplate paginationTemplate;
    private AccountHistoryPhoneNumberService accountHistoryPhoneNumberService;
    private AccountPrivilegeService accountPrivilegeService;
    private PrivilegeService privilegeService;

    public DefaultAccountListServiceImpl(AccountMapper accountMapper,
                                         PaginationTemplate paginationTemplate,
                                         AccountHistoryPhoneNumberService accountHistoryPhoneNumberService,
                                         AccountPrivilegeService accountPrivilegeService,
                                         PrivilegeService privilegeService) {
        this.accountMapper = accountMapper;
        this.paginationTemplate = paginationTemplate;
        this.accountHistoryPhoneNumberService = accountHistoryPhoneNumberService;
        this.accountPrivilegeService = accountPrivilegeService;
        this.privilegeService = privilegeService;
    }

    @Override
    public PaginationResult<Account, Map<String, Long>> getAccounts(AccountListQuery query) {
        DefaultAccountListQuery defaultQuery = (DefaultAccountListQuery) query;
        PaginationCallback<Account, Map<String, Long>> callback = new PaginationCallback<Account, Map<String, Long>>() {
            @Override
            public Integer countRecords() {
                return accountMapper.countByQuery(defaultQuery);
            }

            @Override
            public List<Account> loadRecords(PaginationConfig pc) {
                List<AccountPO> accountPOs = accountMapper.selectByQuery(defaultQuery);
                return AccountUtils.assembleAccounts(accountPOs, accountHistoryPhoneNumberService, accountPrivilegeService, privilegeService);
            }

            @Override
            public Map<String, Long> getStatistics(List<Account> accounts) {
                List<Pair<String, Long>> roleTypeCodeCounts = accountMapper.countByRoleTypeCodes(defaultQuery);
                Map<String, Long> statistics = Maps.newHashMap();
                roleTypeCodeCounts.forEach(pair -> statistics.put(pair.getLeft(), pair.getRight()));
                return statistics;
            }
        };
        return paginationTemplate.query(query.getPaginationConfig(), callback);
    }
}
