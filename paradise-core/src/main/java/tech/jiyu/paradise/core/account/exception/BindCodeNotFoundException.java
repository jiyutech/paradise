package tech.jiyu.paradise.core.account.exception;

import tech.jiyu.utility.api.errors.annotations.ApiErrorKey;

/**
 * CreatedDate 2023-02-21 15:15
 *
 * @author wudawei;emailAddress: wudawei@neyber.tech
 */
@ApiErrorKey(group = "account",
             id = "bind.code.not.found")
public class BindCodeNotFoundException extends RuntimeException {

    public BindCodeNotFoundException() {
        super();
    }

    public BindCodeNotFoundException(String message) {
        super(message);
    }

    public BindCodeNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }

    public BindCodeNotFoundException(Throwable cause) {
        super(cause);
    }

    protected BindCodeNotFoundException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}