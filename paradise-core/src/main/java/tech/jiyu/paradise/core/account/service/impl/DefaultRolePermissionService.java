package tech.jiyu.paradise.core.account.service.impl;

import tech.jiyu.paradise.core.account.service.RolePermissionService;

/**
 * @author <a href="mailto:chenshao@jinjie.tech">shawsmith</a>
 */
public class DefaultRolePermissionService implements RolePermissionService {

    @Override
    public boolean isAdministrationAccount(String roleTypeCode) {
        return isSystemAccount(roleTypeCode) || isITAccount(roleTypeCode);
    }
}
