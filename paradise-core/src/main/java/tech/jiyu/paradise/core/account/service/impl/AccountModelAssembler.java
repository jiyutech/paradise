package tech.jiyu.paradise.core.account.service.impl;

import com.google.common.collect.Lists;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

import tech.jiyu.paradise.core.account.dao.po.AccountPO;
import tech.jiyu.paradise.core.account.domain.Account;
import tech.jiyu.paradise.core.account.service.AccountDeletingTaskService;
import tech.jiyu.paradise.core.account.service.AccountHistoryPhoneNumberService;
import tech.jiyu.paradise.core.privileges.service.AccountPrivilegeService;
import tech.jiyu.paradise.core.privileges.service.PrivilegeService;
import tech.jiyu.paradise.core.util.AccountUtils;
import tech.jiyu.restful.api.service.ModelAssembler;

/**
 * @author <a href="mailto:chenshao@jiyu.tech">shawsmith</a>
 */
@Component("accountModelAssembler")
public class AccountModelAssembler implements ModelAssembler<Account, AccountPO> {

    @Autowired
    private AccountHistoryPhoneNumberService accountHistoryPhoneNumberService;

    @Autowired
    private AccountPrivilegeService accountPrivilegeService;

    @Autowired
    private PrivilegeService privilegeService;

    @Autowired
    private AccountDeletingTaskService accountDeletingTaskService;

    @Override
    public List<Account> assemble(List<AccountPO> accountPOs) {
        if (CollectionUtils.isEmpty(accountPOs)) {
            return Lists.newArrayListWithCapacity(0);
        }
        return AccountUtils.assembleAccounts(accountPOs,
                accountHistoryPhoneNumberService,
                accountPrivilegeService,
                privilegeService,
                accountDeletingTaskService);
    }
}
