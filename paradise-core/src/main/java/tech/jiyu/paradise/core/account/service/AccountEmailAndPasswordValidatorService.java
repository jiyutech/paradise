package tech.jiyu.paradise.core.account.service;

import tech.jiyu.paradise.core.account.domain.Account;

/**
 * 目前 nanoshop 只需要邮箱，不需要密码，所以提供一个密码校验的扩展
 */
public interface AccountEmailAndPasswordValidatorService {
    void checkPasswordEmpty(Account account);

    void checkEmailDuplicated(Long accountId, Long companyId, String email);
}
