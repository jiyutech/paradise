package tech.jiyu.paradise.core.account.dao.po;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import tech.jiyu.paradise.core.account.domain.Residence;

/**
 * Created By ResidencePO
 *
 * Date 2017/12/27 10:09
 *
 * @author SimonChan;emailAddress: simonchan@jiyu.tech
 */
@NoArgsConstructor
@Getter
@Setter
public class ResidencePO {

    private Long id;
    private Long accountId;
    private Long createdDate;
    private Long modifiedDate;
    private String cityName;
    private Integer orderNum;

    public static ResidencePO create(Long id, Residence residence) {
        if (residence == null) {
            return null;
        }

        ResidencePO residencePO = new ResidencePO();
        Long now = System.currentTimeMillis();

        residencePO.setId(id);
        residencePO.setAccountId(residence.getAccountId());
        residencePO.setCreatedDate(now);
        residencePO.setModifiedDate(now);
        residencePO.setCityName(residence.getCityName());

        return residencePO;
    }
}
