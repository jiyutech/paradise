package tech.jiyu.paradise.core.account.dao.po;

import lombok.Data;

/**
 * @author Simon [simonchan@jinjie.tech]
 */
@Data
public class AccountPhoneNumberVerificationResult {

    private Long accountId;
    private String originalCountryCode;
    private String originalPhoneNumber;
    private String correctCountryCode;
    private String correctPhoneNumber;
    private String reason;
}
