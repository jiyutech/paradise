package tech.jiyu.paradise.core.authentication.domain;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import tech.jiyu.penguin.wxapi.wechat.oauth2.domain.OAuth2AccessToken;

/**
 * CreatedDate 2018-07-09 下午11:54
 *
 * @author SimonChan;emailAddress: simonchan@jinjie.tech
 */
@NoArgsConstructor
@Getter
@Setter
public class OAuth2KeyAuthentication {

    private String cacheKey;

    private Boolean needPhoneNumber;

    private Object oauthUserInfo;

    private OAuth2AccessToken accessToken;
}
