package tech.jiyu.paradise.core.account.dao.mapper;

import org.apache.ibatis.annotations.Mapper;

import java.util.Collection;
import java.util.List;

import tech.jiyu.paradise.core.account.dao.po.ResidencePO;

/**
 * Created By ResidenceMapper
 *
 * Date 2017/12/27 11:37
 *
 * @author SimonChan;emailAddress: simonchan@jiyu.tech
 */
@Mapper
public interface ResidenceMapper {

    List<ResidencePO> selectByAccountId(Long accountId);

    void deleteByAccountId(Long accountId);

    void batchSave(Collection<ResidencePO> residencePOs);

    List<ResidencePO> selectByIds(Collection<Long> residenceIds);

    void updateById(ResidencePO residencePO);
}
