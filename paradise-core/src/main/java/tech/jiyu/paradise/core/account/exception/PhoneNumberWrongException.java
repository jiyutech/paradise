package tech.jiyu.paradise.core.account.exception;

import tech.jiyu.utility.api.errors.annotations.ApiErrorKey;

/**
 * @author <a href="mailto:chenshao@jiyu.tech">shawsmith</a>
 */
@ApiErrorKey(group = "account",
             id = "phone.number.wrong")
public class PhoneNumberWrongException extends RuntimeException {

    public PhoneNumberWrongException() {
    }

    public PhoneNumberWrongException(String message) {
        super(message);
    }

    public PhoneNumberWrongException(String message, Throwable cause) {
        super(message, cause);
    }

    public PhoneNumberWrongException(Throwable cause) {
        super(cause);
    }
}
