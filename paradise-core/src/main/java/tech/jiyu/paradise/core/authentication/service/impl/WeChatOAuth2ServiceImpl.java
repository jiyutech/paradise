package tech.jiyu.paradise.core.authentication.service.impl;

import com.alibaba.fastjson.JSONObject;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tech.jiyu.paradise.core.account.domain.Account;
import tech.jiyu.paradise.core.account.service.AccountService;
import tech.jiyu.paradise.core.authentication.domain.*;
import tech.jiyu.paradise.core.authentication.exception.NoProvidePhoneNumberException;
import tech.jiyu.paradise.core.authentication.exception.NoSuchUserInfoException;
import tech.jiyu.paradise.core.authentication.exception.OAuth2KeyExpiredException;
import tech.jiyu.paradise.core.authentication.service.OAuth2AuthenticationService;
import tech.jiyu.paradise.core.authentication.service.OAuth2Service;
import tech.jiyu.parrot.geography.domain.Region;
import tech.jiyu.parrot.geography.service.RegionService;
import tech.jiyu.parrot.preferences.service.PreferenceService;
import tech.jiyu.penguin.wechat.config.domain.WeChatConfig;
import tech.jiyu.penguin.wechat.config.service.WeChatConfigService;
import tech.jiyu.penguin.wxapi.wechat.common.domain.UserInfo;
import tech.jiyu.penguin.wxapi.wechat.common.domain.UserInfoSource;
import tech.jiyu.penguin.wxapi.wechat.common.exception.CannotAcquireAccessTokenException;
import tech.jiyu.penguin.wxapi.wechat.oauth2.domain.OAuth2AccessToken;
import tech.jiyu.penguin.wxapi.wechat.oauth2.service.OAuth2AccessTokenService;
import tech.jiyu.penguin.wxapi.wechat.openplatform.domain.AuthorizerOpenIdAndSessionKey;
import tech.jiyu.penguin.wxapi.wechat.openplatform.service.OpenPlatformService;
import tech.jiyu.penguin.wxapi.wechat.tinyapp.domain.WeChatTinyAppLoginToken;
import tech.jiyu.penguin.wxapi.wechat.tinyapp.service.WeChatTinyAppService;
import tech.jiyu.photo.api.PhotoService;
import tech.jiyu.utility.cache.CacheStrategy;
import tech.jiyu.utility.cache.CacheTemplate;
import tech.jiyu.utility.datastructure.Pair;
import tech.jiyu.utility.exception.ExceptionNotificationService;

import java.io.IOException;
import java.util.Collection;
import java.util.Optional;

/**
 * @author <a href="mailto:chenshao@dekeapp.com">shawsmith</a>
 */
@Service
public class WeChatOAuth2ServiceImpl implements OAuth2Service {

    private static final String DEFAULT_LANGUAGE = "zh_CN";
    private static final String WECHAT_H5_APP_ID_KEY = "wechat.h5.app.id";
    private static final String WECHAT_H5_APP_SECRET_KEY = "wechat.h5.app.secret";

    @Autowired
    private WeChatTinyAppService weChatTinyAppService;

    @Autowired
    private OAuth2AccessTokenService oAuth2AccessTokenService;

    @Autowired
    private AccountService accountService;

    @Autowired
    private OAuth2AuthenticationService oAuth2AuthenticationService;

    @Autowired
    private PhotoService photoService;

    @Autowired
    private WeChatConfigService weChatConfigService;

    @Autowired
    private ExceptionNotificationService exceptionNotificationService;

    @Autowired
    private RegionService regionService;

    @Value("${paradise.default.wechat.config.id:-1}")
    private Long defaultWeChatConfigId;

    @Value("${paradise.use.union.id:false}")
    private Boolean useUnionId;

    @Value("${paradise.only.default.tiny.app.use.union.id:false}")
    private Boolean onlyDefaultTinyAppUseUnionId;

    @Value("${wechat.tinyapp.appId:#{null}")
    private String defaultTinyAppId;

    @Autowired
    private PreferenceService preferenceService;

    @Autowired
    private CacheTemplate cacheTemplate;

    @Autowired
    private OpenPlatformService openPlatformService;

    @Override
    public boolean supports(OAuth2Type type) {
        return (OAuth2Type.WECHAT == type) || (OAuth2Type.WECHAT_TINY_APP == type) || (OAuth2Type.WECHAT_H5_APP == type);
    }

    @Override
    @Transactional
    public Account createOrUpdate(OAuth2Parameters parameters) throws IOException {
        Pair<UserInfo, OAuth2AccessToken> pair = fetchUserInfoAndOAuth2AccessToken(parameters);
        OAuth2Type type = OAuth2Type.fromCode(parameters.getAuthType());

        UserInfo userInfo = pair.getLeft();
        Account account;
        if (type == OAuth2Type.WECHAT_TINY_APP) {
            account = registerAccountIfNecessary(parameters.getCompanyId(), parameters.getRoleTypeCode(), parameters.getWeChatTinyAppId(), null, userInfo, UserInfoSource.WE_CHAT_TINY_APP, parameters.getLoginSource(), parameters.getPrivilegeIds());
        } else {
            UserInfoSource source = null;
            if (type == OAuth2Type.WECHAT) {
                source = UserInfoSource.WE_CHAT_OFFICIAL_ACCOUNT;
            } else if (type == OAuth2Type.WECHAT_H5_APP) {
                source = UserInfoSource.H5_APP;
            }

            account = registerAccountIfNecessary(parameters.getCompanyId(), parameters.getRoleTypeCode(), parameters.getWeChatTinyAppId(), null, userInfo, source, parameters.getLoginSource(), parameters.getPrivilegeIds());
            OAuth2AccessToken accessToken = pair.getRight();

            OAuth2Authentication oAuth2Authentication = buildOAuth2Authentication(account.getId(), accessToken, type);
            oAuth2AuthenticationService.saveOrUpdate(oAuth2Authentication);
        }

        return account;
    }

    private Pair<UserInfo, OAuth2AccessToken> fetchUserInfoAndOAuth2AccessToken(OAuth2Parameters parameters) throws IOException {
        Pair<UserInfo, OAuth2AccessToken> pair = new Pair<>();
        OAuth2Type type = OAuth2Type.fromCode(parameters.getAuthType());
        WeChatConfig weChatConfig = weChatConfigService.findById(defaultWeChatConfigId, CacheStrategy.CACHE_PREFER);
        // 小程序无法获取Access Token, 只能获取Open ID.
        if (type == OAuth2Type.WECHAT_TINY_APP) {
            UserInfo userInfo = fetchWeChatTinyAppUserInfo(parameters, weChatConfig);
            if (userInfo == null) {
                throw new NoSuchUserInfoException();
            }
            pair.setLeft(userInfo);
        }

        //微信公众号(网页)授权，还需要返回Access Token.
        else {
            String appId = null;
            String appSecret = null;
            OAuth2Type oAuth2Type = OAuth2Type.fromCode(parameters.getAuthType());
            if (oAuth2Type == OAuth2Type.WECHAT) {
                appId = Optional.ofNullable(weChatConfig).map(WeChatConfig::getWeChatAppId).orElse(null);
                appSecret = Optional.ofNullable(weChatConfig).map(WeChatConfig::getWeChatAppSecret).orElse(null);
            } else if (oAuth2Type == OAuth2Type.WECHAT_H5_APP) {
                appId = preferenceService.findRequiredPreferenceValue(WECHAT_H5_APP_ID_KEY);
                appSecret = preferenceService.findRequiredPreferenceValue(WECHAT_H5_APP_SECRET_KEY);
            }

            OAuth2AccessToken accessToken = oAuth2AccessTokenService.exchange(appId, appSecret, parameters.getAuthCode());
            if (accessToken == null) {
                throw new CannotAcquireAccessTokenException();
            }

            UserInfo userInfo = oAuth2AccessTokenService.find(accessToken.getOpenId(), accessToken.getAccessToken(), DEFAULT_LANGUAGE);
            if (userInfo == null) {
                throw new NoSuchUserInfoException();
            }

            pair.setLeft(userInfo);
            pair.setRight(accessToken);
        }

        return pair;
    }

    @Override
    public Account createOrUpdate(OAuth2KeyParameters keyParameters) {
        String authKey = keyParameters.getAuthKey();
        OAuth2KeyAuthentication oAuth2KeyAuthentication = Optional.ofNullable(loadKeyAuthenticationFromCache(authKey)).orElseThrow(OAuth2KeyExpiredException::new);
        checkPhoneNumberIfNecessary(keyParameters, oAuth2KeyAuthentication);

        UserInfo userInfo = (UserInfo) oAuth2KeyAuthentication.getOauthUserInfo();
        OAuth2Type type = OAuth2Type.fromCode(keyParameters.getAuthType());
        Account account;
        if (type == OAuth2Type.WECHAT_TINY_APP) {
            account = registerAccountIfNecessary(keyParameters.getCompanyId(), keyParameters.getRoleTypeCode(), keyParameters.getWeChatTinyAppId(), keyParameters.getIsoCode(), userInfo, UserInfoSource.WE_CHAT_TINY_APP, keyParameters.getLoginSource(), keyParameters.getPrivilegeIds());
        }
        // 如果是微信公众号或网页还需要存储AccessToken
        else {
            UserInfoSource source = null;
            if (type == OAuth2Type.WECHAT) {
                source = UserInfoSource.WE_CHAT_OFFICIAL_ACCOUNT;
            } else if (type == OAuth2Type.WECHAT_H5_APP) {
                source = UserInfoSource.H5_APP;
            }
            account = registerAccountIfNecessary(keyParameters.getCompanyId(), keyParameters.getRoleTypeCode(), keyParameters.getWeChatTinyAppId(), keyParameters.getIsoCode(), userInfo, source, keyParameters.getLoginSource(), keyParameters.getPrivilegeIds());
            OAuth2AccessToken accessToken = oAuth2KeyAuthentication.getAccessToken();
            OAuth2Authentication oAuth2Authentication = buildOAuth2Authentication(account.getId(), accessToken, type);

            oAuth2AuthenticationService.saveOrUpdate(oAuth2Authentication);
        }

        deleteKeyAuthenticationFromCache(authKey);
        return account;
    }

    private void checkPhoneNumberIfNecessary(OAuth2KeyParameters keyParameters, OAuth2KeyAuthentication oAuth2KeyAuthentication) {
        if (oAuth2KeyAuthentication.getNeedPhoneNumber() && StringUtils.isBlank(keyParameters.getPhoneNumber())) {
            throw new NoProvidePhoneNumberException(keyParameters.getAuthKey());
        }
    }

    @Override
    public OAuth2KeyAuthentication createOAuth2KeyAuthentication(OAuth2Parameters parameters) throws IOException {
        Pair<UserInfo, OAuth2AccessToken> pair = fetchUserInfoAndOAuth2AccessToken(parameters);
        UserInfo userInfo = pair.getLeft();
        OAuth2AccessToken accessToken = pair.getRight();
        OAuth2KeyAuthentication oAuth2KeyAuthentication = buildOAuth2Authentication(parameters.getCompanyId(), userInfo, accessToken);
        storeKeyAuthenticationToCache(oAuth2KeyAuthentication);

        return oAuth2KeyAuthentication;
    }

    private void storeKeyAuthenticationToCache(OAuth2KeyAuthentication oAuth2KeyAuthentication) {
        UserInfo userInfo = (UserInfo) oAuth2KeyAuthentication.getOauthUserInfo();
        String cacheKeySuffix = String.valueOf(System.currentTimeMillis());
        String cacheKey = OAuth2Service.OAUTH2_KEY_CACHE_PREFIX.concat(userInfo.getOpenId()).concat(cacheKeySuffix);
        oAuth2KeyAuthentication.setCacheKey(cacheKey);

        cacheTemplate.set(cacheKey, oAuth2KeyAuthentication);
    }

    private OAuth2KeyAuthentication loadKeyAuthenticationFromCache(String authKey) {
        return cacheTemplate.get(authKey, OAuth2KeyAuthentication.class);
    }

    private void deleteKeyAuthenticationFromCache(String authKey) {
        cacheTemplate.delete(authKey);
    }

    private OAuth2KeyAuthentication buildOAuth2Authentication(Long companyId, UserInfo userInfo, OAuth2AccessToken accessToken) {
        String openId = userInfo.getOpenId();
        OAuth2KeyAuthentication oAuth2KeyAuthentication = new OAuth2KeyAuthentication();
        oAuth2KeyAuthentication.setOauthUserInfo(userInfo);
        oAuth2KeyAuthentication.setAccessToken(accessToken);
        Account account = accountService.findByWeChatOpenId(companyId, openId);
        if (account == null || StringUtils.isBlank(account.getPhoneNumber())) {
            oAuth2KeyAuthentication.setNeedPhoneNumber(Boolean.TRUE);
        } else {
            oAuth2KeyAuthentication.setNeedPhoneNumber(Boolean.FALSE);
        }

        return oAuth2KeyAuthentication;
    }

    private UserInfo fetchWeChatTinyAppUserInfo(OAuth2Parameters parameters, WeChatConfig weChatConfig) throws IOException {

        String openId = null;
        String unionId = null;
        String sessionKey = null;
        String weChatTinyAppId = parameters.getWeChatTinyAppId();
        String authCode = parameters.getAuthCode();
        if (StringUtils.isNotBlank(weChatTinyAppId)) {
            AuthorizerOpenIdAndSessionKey openIdAndSessionKey = openPlatformService.exchangeOpenId(weChatTinyAppId, authCode);
            if (openIdAndSessionKey != null) {
                openId = openIdAndSessionKey.getOpenId();
                unionId = openIdAndSessionKey.getUnionId();
                sessionKey = openIdAndSessionKey.getSessionKey();
            }
        } else {
            if (weChatConfig != null) {
                WeChatTinyAppLoginToken loginToken = weChatTinyAppService.exchangeLoginToken(weChatConfig.getWeChatTinyAppId(), weChatConfig.getWeChatAppSecret(), authCode);
                if (loginToken != null) {
                    openId = loginToken.getOpenId();
                    unionId = loginToken.getUnionId();
                    sessionKey = loginToken.getSessionKey();
                }
            }
        }


        UserInfo userInfo = new UserInfo();
        userInfo.setOpenId(openId);
        userInfo.setUnionId(unionId);
        userInfo.setNickName(parameters.getName());
        userInfo.setImageUrl(parameters.getAvatar());

        String weChatTinyAppEncryptedData = parameters.getWeChatTinyAppEncryptedData();
        String weChatTinyAppIv = parameters.getWeChatTinyAppIv();
        if (StringUtils.isNotBlank(sessionKey) && StringUtils.isNotBlank(weChatTinyAppEncryptedData) && StringUtils.isNotBlank(weChatTinyAppIv)) {
            JSONObject jsonObject = decryptDataUsingIv(weChatTinyAppId, sessionKey, weChatTinyAppEncryptedData, weChatTinyAppIv);
            if (jsonObject != null) {
                if (StringUtils.isBlank(userInfo.getUnionId())) {
                    userInfo.setUnionId(jsonObject.getString("unionId"));
                }

                String countryCode = StringUtils.trimToNull(jsonObject.getString("countryCode"));
                userInfo.setCountryCode(countryCode);

                String phoneNumber = StringUtils.trimToNull(jsonObject.getString("purePhoneNumber"));
                userInfo.setPhoneNumber(phoneNumber);
            }
        }
        return userInfo;
    }

    private JSONObject decryptDataUsingIv(String weChatTinyAppId, String sessionKey, String encryptData, String iv) {
        WXBizDataCrypt biz = new WXBizDataCrypt(weChatTinyAppId, sessionKey);
        String decryptData = biz.decryptData(encryptData, iv);

        JSONObject node = JSONObject.parseObject(decryptData);
        if (node == null) {
            return null;
        }

        String userInfo = node.getString("userInfo");
        if (StringUtils.isBlank(userInfo)) {
            return null;
        }

        return JSONObject.parseObject(userInfo);
    }

    private Account registerAccountIfNecessary(Long companyId, String roleTypeCode, String wechatTinyAppId, String isoCode, UserInfo userInfo, UserInfoSource source, String loginSource, Collection<Long> privilegeIds) {
        String openId = userInfo.getOpenId();
        Account account = null;

        if (onlyDefaultTinyAppUseUnionId && StringUtils.equals(wechatTinyAppId, defaultTinyAppId)) {
            account = accountService.findByUnionId(companyId, userInfo.getUnionId());
        } else if (useUnionId) {
            account = accountService.findByUnionId(companyId, userInfo.getUnionId());
        }

        // 当使用unionId登录分支没有找到现有账号时，使用下面的分支进行兜底
        if (source == UserInfoSource.WE_CHAT_TINY_APP && account == null) {
            // 如果有手机号码, 则优先以手机号码方式登录.
            String phoneNumber = userInfo.getPhoneNumber();
            if (StringUtils.isNotBlank(phoneNumber)) {
                account = accountService.findByPhoneNumber(companyId, phoneNumber);
            }
            if (account == null) {
                account = accountService.findByWeChatTinyAppOpenId(companyId, openId);
            }
        } else if (source == UserInfoSource.WE_CHAT_OFFICIAL_ACCOUNT && account == null) {
            account = accountService.findByWeChatOpenId(companyId, openId);
        } else if (source == UserInfoSource.H5_APP && account == null) {
            account = accountService.findByWeChatH5AppOpenId(companyId, openId);
        }

        if (account == null) {
            account = accountService.register(buildAccount(companyId, userInfo, source, roleTypeCode, isoCode, loginSource, privilegeIds), null, null);
        } else {
            updateAccountProperties(userInfo, source, privilegeIds, account);
            account = accountService.patch(account);
        }
        return account;
    }

    private Account buildAccount(Long companyId, UserInfo userInfo, UserInfoSource source, String roleTypeCode, String isoCode, String loginSource, Collection<Long> privilegeIds) {
        Account account = new Account();
        account.setRegistrationDate(System.currentTimeMillis());
        account.setRegistrationSource(loginSource);

        String openId = userInfo.getOpenId();
        if (source == UserInfoSource.WE_CHAT_OFFICIAL_ACCOUNT) {
            account.setWeChatOpenId(openId);
        } else if (source == UserInfoSource.WE_CHAT_TINY_APP) {
            account.setWeChatTinyAppOpenId(openId);
        } else if (source == UserInfoSource.H5_APP) {
            account.setWeChatH5AppOpenId(openId);
        }

        account.setUnionId(userInfo.getUnionId());
        account.setCountryCode(userInfo.getCountryCode());
        account.setPhoneNumber(userInfo.getPhoneNumber());
        account.setName(userInfo.getNickName());
        account.setGender(userInfo.getGender());
        account.setCompanyId(companyId);
        account.setIsoCode(isoCode);

        Region province = regionService.findProvince(userInfo.getProvince(), CacheStrategy.CACHE_PREFER);
        if (province != null) {
            account.setProvinceId(province.getId());

            Region city = regionService.findCity(userInfo.getCity(), province.getId(), CacheStrategy.CACHE_PREFER);
            if (city != null) {
                account.setCityId(city.getId());
            }
        }

        try {
            String avatar = photoService.upload(userInfo.getImageUrl());
            account.setAvatar(avatar);
        } catch (Exception e) {
            exceptionNotificationService.notify(e);
        }

        account.setRoleTypeCode(roleTypeCode);
        account.setPrivilegeIds(privilegeIds);
        account.setEnabled(Boolean.TRUE);
        account.setVerified(Boolean.FALSE);
        account.setAudited(Boolean.FALSE);
        account.setWorkDayTimeEnabled(Boolean.FALSE);
        account.setPolyAdminEnabled(Boolean.TRUE);
        return account;
    }

    private void updateAccountProperties(UserInfo userInfo, UserInfoSource source, Collection<Long> privilegeIds, Account account) {
        String openId = userInfo.getOpenId();
        if (source == UserInfoSource.WE_CHAT_OFFICIAL_ACCOUNT) {
            account.setWeChatOpenId(openId);
        } else if (source == UserInfoSource.WE_CHAT_TINY_APP) {
            account.setWeChatTinyAppOpenId(openId);
        } else if (source == UserInfoSource.H5_APP) {
            account.setWeChatH5AppOpenId(openId);
        }
        account.setUnionId(userInfo.getUnionId());
        account.setName(userInfo.getNickName());
        account.setGender(userInfo.getGender());

        Region province = regionService.findProvince(userInfo.getProvince(), CacheStrategy.CACHE_PREFER);
        if (province != null) {
            account.setProvinceId(province.getId());
            Region city = regionService.findCity(userInfo.getCity(), province.getId(), CacheStrategy.CACHE_PREFER);
            if (city != null) {
                account.setCityId(city.getId());
            }
        }

        try {
            String avatar = photoService.upload(userInfo.getImageUrl());
            account.setAvatar(avatar);
        } catch (Exception e) {
            exceptionNotificationService.notify(e);
        }
        account.setPrivilegeIds(privilegeIds);
    }

    private OAuth2Authentication buildOAuth2Authentication(Long accountId, OAuth2AccessToken accessToken, OAuth2Type authType) {
        OAuth2Authentication authentication = new OAuth2Authentication();
        authentication.setAccountId(accountId);
        authentication.setType(authType);
        authentication.setAccessToken(accessToken.getAccessToken());
        authentication.setRefreshToken(accessToken.getRefreshToken());
        authentication.setExpiresIn(accessToken.getExpiresIn());
        authentication.setScope(accessToken.getScope());
        return authentication;
    }

}
