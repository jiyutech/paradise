package tech.jiyu.paradise.core.account.service.impl;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tech.jiyu.paradise.core.account.dao.mapper.ResidenceMapper;
import tech.jiyu.paradise.core.account.dao.po.ResidencePO;
import tech.jiyu.paradise.core.account.domain.Residence;
import tech.jiyu.paradise.core.account.service.ResidenceService;
import tech.jiyu.sequences.SequenceService;
import tech.jiyu.utility.cache.CacheCallback;
import tech.jiyu.utility.cache.CacheStrategy;
import tech.jiyu.utility.cache.CacheTemplate;
import tech.jiyu.utility.cache.CacheUtils;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Created By ResidenceServiceImpl
 *
 * Date 2017/12/27 20:14
 *
 * @author SimonChan;emailAddress: simonchan@jiyu.tech
 */
@Service
public class ResidenceServiceImpl implements ResidenceService {

    @Autowired
    private ResidenceMapper residenceMapper;

    @Autowired
    private SequenceService sequenceService;

    @Autowired
    private CacheTemplate cacheTemplate;

    @Override
    @Transactional
    public List<Residence> saveOrUpdate(Long accountId, List<Residence> residences) {
        if (CollectionUtils.isEmpty(residences)) {
            deleteByAccountId(accountId);
            return Lists.newArrayListWithCapacity(0);
        }
        residenceMapper.deleteByAccountId(accountId);
        List<ResidencePO> newResidencePOs = Lists.newArrayList();
        int order = 0;
        for (Residence residence : residences) {
            residence.setAccountId(accountId);
            ResidencePO residencePO = ResidencePO.create(sequenceService.nextLongValue(), residence);
            residencePO.setOrderNum(order++);
            newResidencePOs.add(residencePO);
        }
        residenceMapper.batchSave(newResidencePOs);
        removeCache(accountId);
        return findByAccountId(accountId, CacheStrategy.NO_CACHE);
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<Residence> findByAccountId(Long accountId, CacheStrategy strategy) {
        String residenceCacheKey = buildCacheKey(accountId);
        Collection<Object> cacheKeys = Collections.singletonList(residenceCacheKey);
        List<List<Residence>> residenceGroups = cacheTemplate.load(cacheKeys, strategy, new CacheCallback() {
            @Override
            public Map<Object, Object> getKeyValuePairs(Collection<Object> objects) {
                if (CollectionUtils.isEmpty(objects)) {
                    return Maps.newHashMap();
                }

                List<Residence> residences = (List<Residence>) objects.iterator().next();
                Map<Object, Object> result = Maps.newHashMap();
                result.put(residenceCacheKey, residences);
                return result;
            }

            @Override
            public List<Object> loadNewObjects(Collection<Object> keys) {
                List<Residence> residences = residenceMapper.selectByAccountId(accountId).stream()
                        .map(this::transformToResidence)
                        .collect(Collectors.toList());

                List<Object> residenceGroups = Lists.newArrayList();
                residenceGroups.add(residences);
                return residenceGroups;
            }

            private Residence transformToResidence(ResidencePO residencePO) {
                if (residencePO == null) {
                    return null;
                }

                Residence residence = new Residence();

                residence.setId(residencePO.getId());
                residence.setAccountId(residencePO.getAccountId());
                residence.setCityName(residencePO.getCityName());


                return residence;
            }
        });

        return CollectionUtils.isNotEmpty(residenceGroups) ? residenceGroups.get(0) : null;
    }

    @Override
    @Transactional
    public void deleteByAccountId(Long accountId) {
        residenceMapper.deleteByAccountId(accountId);
        removeCache(accountId);
    }

    private String buildCacheKey(Long accountId) {
        return CacheUtils.buildCacheKey(CACHE_PREFIX, accountId);
    }

    private void removeCache(Long accountId) {
        String cacheKey = buildCacheKey(accountId);
        cacheTemplate.delete(cacheKey);
    }
}
