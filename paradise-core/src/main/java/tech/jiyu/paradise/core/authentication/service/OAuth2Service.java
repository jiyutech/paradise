package tech.jiyu.paradise.core.authentication.service;

import java.io.IOException;

import tech.jiyu.paradise.core.account.domain.Account;
import tech.jiyu.paradise.core.authentication.domain.OAuth2KeyAuthentication;
import tech.jiyu.paradise.core.authentication.domain.OAuth2KeyParameters;
import tech.jiyu.paradise.core.authentication.domain.OAuth2Parameters;
import tech.jiyu.paradise.core.authentication.domain.OAuth2Type;

/**
 * @author <a href="mailto:chenshao@dekeapp.com">shawsmith</a>
 */
public interface OAuth2Service {

    String OAUTH2_KEY_CACHE_PREFIX = "oauth2_key_auth:";

    boolean supports(OAuth2Type type);

    Account createOrUpdate(OAuth2Parameters parameters) throws IOException;

    Account createOrUpdate(OAuth2KeyParameters keyParameters);

    OAuth2KeyAuthentication createOAuth2KeyAuthentication(OAuth2Parameters parameters) throws IOException;
}
