package tech.jiyu.paradise.core.account.exception;


import org.springframework.security.core.AuthenticationException;

import tech.jiyu.utility.api.errors.annotations.ApiErrorKey;

/**
 * @author <a href="mailto:chenshao@jiyu.tech">shawsmith</a>
 */
@ApiErrorKey(group = "login",
             id = "illegal.login.source")
public class IllegalLoginSourceException extends AuthenticationException {

    public IllegalLoginSourceException(String msg, Throwable t) {
        super(msg, t);
    }

    public IllegalLoginSourceException(String msg) {
        super(msg);
    }
}
