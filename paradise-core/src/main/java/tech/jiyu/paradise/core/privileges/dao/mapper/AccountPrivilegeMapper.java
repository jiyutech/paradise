package tech.jiyu.paradise.core.privileges.dao.mapper;

import org.apache.ibatis.annotations.Mapper;

import java.util.Collection;
import java.util.List;

import tech.jiyu.paradise.core.privileges.dao.po.AccountPrivilegePO;

/**
 * @author <a href="mailto:chenshao@jinjie.tech">shawsmith</a>
 */
@Mapper
public interface AccountPrivilegeMapper {

    void insert(AccountPrivilegePO accountPrivilege);

    void deleteByIds(Collection<Long> ids);

    void deleteByAccountId(Long accountId);

    List<AccountPrivilegePO> selectByAccountId(Long accountId);

    List<AccountPrivilegePO> selectByAccountIds(Collection<Long> accountIds);

    Long countByPrivilegeId(Long privilegeId);
}
