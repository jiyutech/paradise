package tech.jiyu.paradise.core.account.service.impl;

import com.google.api.client.util.Lists;
import com.google.common.collect.Maps;

import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import lombok.extern.slf4j.Slf4j;
import tech.jiyu.paradise.core.account.dao.mapper.AccountDeletingTaskMapper;
import tech.jiyu.paradise.core.account.dao.po.AccountDeletingTaskPO;
import tech.jiyu.paradise.core.account.domain.AccountDeletingTask;
import tech.jiyu.paradise.core.account.domain.AccountEvent;
import tech.jiyu.paradise.core.account.domain.AccountPostDeletedEvent;
import tech.jiyu.paradise.core.account.service.AccountDeletingTaskService;
import tech.jiyu.sequences.SequenceService;
import tech.jiyu.utility.web.TimeRange;

@Service
@Slf4j
public class AccountDeletingTaskServiceImpl implements AccountDeletingTaskService {

    @Value("${paradise.accountDeleting.coolingOffDays:7}")
    private String coolingOffDays;

    @Autowired
    private AccountDeletingTaskMapper taskMapper;

    @Autowired
    private SequenceService sequenceService;

    public AccountDeletingTask createDeletingTask(Long accountId) {
        AccountDeletingTaskPO po = new AccountDeletingTaskPO();
        init(po);
        po.setAccountId(accountId);
        taskMapper.insert(po);
        return this.getByAccountId(accountId);
    }

    private AccountDeletingTaskPO transToPO(AccountDeletingTask task) {
        AccountDeletingTaskPO po = new AccountDeletingTaskPO();
        po.setId(task.getId());
        po.setAccountId(task.getAccountId());
        return po;
    }

    private void init(AccountDeletingTaskPO po) {
        po.setId(sequenceService.nextLongValue());
        po.setCreatedDate(System.currentTimeMillis());
        po.setModifiedDate(System.currentTimeMillis());
        po.setDeleted(false);
        po.setDeletedTime(0L);
        po.setCompletedTime(0L);
        po.setHasCompleted(false);
        po.setAccountEstimateDeletingTime(this.calEstimateDeletingTime(po.getCreatedDate(), this.getCoolingOffDays()));
    }

    private long calEstimateDeletingTime(Long createdDate, BigDecimal coolingOffDays) {
        return new BigDecimal(createdDate).add(coolingOffDays.multiply(BigDecimal.valueOf(DateUtils.MILLIS_PER_DAY)).setScale(2, RoundingMode.HALF_UP)).longValue();
    }

    @Override
    public AccountDeletingTask getByAccountId(Long accountId) {
        AccountDeletingTaskPO po = taskMapper.selectByAccountId(accountId);
        return transToModel(po);
    }

    private AccountDeletingTask transToModel(AccountDeletingTaskPO po) {
        AccountDeletingTask task = new AccountDeletingTask();
        task.setId(po.getId());
        task.setCreatedDate(po.getCreatedDate());
        task.setAccountId(po.getAccountId());
        task.setHasCompleted(BooleanUtils.isTrue(po.getHasCompleted()));
        task.setCompletedTime(po.getCompletedTime());
        task.setAccountDeletingCoolingOffDays(this.getCoolingOffDays());
        task.setAccountEstimateDeletingTime(po.getAccountEstimateDeletingTime());
        return task;
    }

    private List<AccountDeletingTask> transToModelList(List<AccountDeletingTaskPO> taskList) {
        if (CollectionUtils.isEmpty(taskList)) {
            return Lists.newArrayList();
        }
        List<AccountDeletingTask> list = Lists.newArrayList();
        for (AccountDeletingTaskPO po : taskList) {
            list.add(transToModel(po));
        }
        return list;
    }

    @Override
    public List<AccountDeletingTask> getDeletingTaskByTimeRange(TimeRange timeRange) {
        List<AccountDeletingTaskPO> taskList = taskMapper.selectByTimeRange(timeRange);
        return transToModelList(taskList);
    }

    @Override
    public void completeTaskByAccountId(Long accountId) {
        AccountDeletingTaskPO po = new AccountDeletingTaskPO();
        po.setModifiedDate(System.currentTimeMillis());
        po.setAccountId(accountId);
        po.setHasCompleted(true);
        po.setCompletedTime(System.currentTimeMillis());
        taskMapper.updateByAccountId(po);

        log.info("complete accountDeletingTask successfully. accountId: {}", accountId);
    }

    @Override
    public void deleteTaskByAccountId(Long accountId) {
        AccountDeletingTaskPO po = new AccountDeletingTaskPO();
        po.setModifiedDate(System.currentTimeMillis());
        po.setDeleted(true);
        po.setDeletedTime(System.currentTimeMillis());
        po.setAccountId(accountId);
        taskMapper.updateByAccountId(po);

        log.info("delete accountDeletingTask completely. accountId: {}", accountId);
    }

    @Override
    public void onEvent(AccountEvent event) {
        if (event instanceof AccountPostDeletedEvent) {
            Long accountId = ((AccountPostDeletedEvent) event).getAccountId();
            this.completeTaskByAccountId(accountId);
        }
    }

    @Override
    public List<AccountDeletingTask> getByAccountIdList(Collection<Long> accountIds) {
        List<AccountDeletingTaskPO> poList = taskMapper.selectByAccountIds(accountIds);
        return transToModelList(poList);
    }

    @Override
    public Map<Long, AccountDeletingTask> getAndGroupByAccountIdList(Collection<Long> accountIds) {
        List<AccountDeletingTask> list = this.getByAccountIdList(accountIds);
        Map<Long, AccountDeletingTask> map = CollectionUtils.isEmpty(list) ? Maps.newHashMap() : Maps.newHashMap(Maps.uniqueIndex(list, AccountDeletingTask::getAccountId));

        for (Long accountId : accountIds) {
            if (!map.containsKey(accountId)) {
                AccountDeletingTask accountDeletingTask = new AccountDeletingTask();
                accountDeletingTask.setAccountDeletingCoolingOffDays(this.getCoolingOffDays());
                map.put(accountId, accountDeletingTask);
            }
        }
        return map;
    }

    @Override
    public BigDecimal getCoolingOffDays() {
        return new BigDecimal(this.coolingOffDays);
    }

    /*@Override
    public Map<Long, Map<String, Object>> getAdditionalAttributes(Collection<Account> accounts) {
        Map<Long, Map<String, Object>> accountAdditionalAttributes = Maps.newHashMap();
        for (Account account : accounts) {
            Map<String, Object> map = Maps.newHashMap();
            map.put("accountDeletingCoolingOffDays", this.getCoolingOffDays());
            accountAdditionalAttributes.put(account.getId(), map);
        }
        List<Long> accountIdList = accounts.stream().map(Account::getId).collect(Collectors.toList());
        List<AccountDeletingTask> accountDeletingTaskList = this.getByAccountIdList(accountIdList);
        if (CollectionUtils.isEmpty(accountDeletingTaskList)) {
            return accountAdditionalAttributes;
        }
        for (AccountDeletingTask accountDeletingTask : accountDeletingTaskList) {
            if (accountAdditionalAttributes.containsKey(accountDeletingTask.getAccountId())) {
                Map<String, Object> map = accountAdditionalAttributes.get(accountDeletingTask.getAccountId());
                map.put("accountEstimateDeletingTime", accountDeletingTask.calEstimateDeletingTime(this.getCoolingOffDays()));
            }
        }
        return accountAdditionalAttributes;
    }*/
}
