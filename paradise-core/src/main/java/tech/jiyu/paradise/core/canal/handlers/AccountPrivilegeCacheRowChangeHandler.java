package tech.jiyu.paradise.core.canal.handlers;

import com.alibaba.otter.canal.protocol.CanalEntry;
import tech.jiyu.paradise.core.privileges.service.AccountPrivilegeService;
import tech.jiyu.utility.cache.CacheTemplate;
import tech.jiyu.utility.canal.CacheExpirationRowChangeHandler;
import tech.jiyu.utility.canal.util.ColumnUtils;
import tech.jiyu.utility.exception.ExceptionNotificationService;

public class AccountPrivilegeCacheRowChangeHandler extends CacheExpirationRowChangeHandler {

    public AccountPrivilegeCacheRowChangeHandler(CacheTemplate cacheTemplate, ExceptionNotificationService exceptionNotificationService) {
        super("account_privileges", cacheTemplate, exceptionNotificationService);
    }

    @Override
    protected void doInsert(CanalEntry.RowChange row) {
        doExpireCaches(row);
    }

    @Override
    protected void doExpireCaches(CanalEntry.RowChange row) {
        super.expireCachesByColumn(row, "account_id", true, column -> ColumnUtils.getLongValue(column, null), AccountPrivilegeService.CACHE_PREFIX);
        super.expireCachesByColumn(row, "account_id", false, column -> ColumnUtils.getLongValue(column, null), AccountPrivilegeService.CACHE_PREFIX);
    }
}
