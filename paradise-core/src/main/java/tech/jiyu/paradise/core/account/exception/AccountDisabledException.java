package tech.jiyu.paradise.core.account.exception;

import tech.jiyu.utility.api.errors.annotations.ApiErrorKey;

/**
 * @author <a href="mailto:chenshao@dekeapp.com">shawsmith</a>
 */
@ApiErrorKey(group = "account",
             id = "account.disabled")
public class AccountDisabledException extends RuntimeException {

    public AccountDisabledException() {
    }

    public AccountDisabledException(String message) {
        super(message);
    }

    public AccountDisabledException(String message, Throwable cause) {
        super(message, cause);
    }

    public AccountDisabledException(Throwable cause) {
        super(cause);
    }
}
