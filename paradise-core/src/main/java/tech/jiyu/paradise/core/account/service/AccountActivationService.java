package tech.jiyu.paradise.core.account.service;

import tech.jiyu.paradise.core.account.domain.Account;
import tech.jiyu.paradise.core.account.domain.ActivationConfig;

/**
 * @author <a href="mailto:chenshao@dekeapp.com">shawsmith</a>
 */
public interface AccountActivationService {

    void sendActivationMail(Account account, ActivationConfig activationConfig);

    void activeAccount(Long accountId, String activationCode);
}
