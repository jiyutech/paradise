package tech.jiyu.paradise.core.account.domain;

/**
 * Created By InvitationCodeType
 *
 * Date 2018/2/9 15:04
 *
 * @author SimonChan;emailAddress: simonchan@jiyu.tech
 */

public enum InvitationCodeStatus {

    /**
     * 未使用
     */
    UN_USED("un_used"),

    /**
     * 已使用
     */
    USED("used");

    public static InvitationCodeStatus fromCode(String code) {
        for (InvitationCodeStatus status : values()) {
            if (status.getCode().equals(code)) {
                return status;
            }
        }
        return null;
    }

    private final String code;

    InvitationCodeStatus(String code) {
        this.code = code;
    }


    public String getCode() {
        return code;
    }
}
