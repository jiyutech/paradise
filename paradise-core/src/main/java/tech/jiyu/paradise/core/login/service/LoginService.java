package tech.jiyu.paradise.core.login.service;

import org.springframework.validation.annotation.Validated;

import java.util.Collection;
import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import tech.jiyu.paradise.core.account.service.AccountService;
import tech.jiyu.paradise.core.login.domain.LoginInfo;

/**
 * 登录服务接口.
 *
 * @author <a href="mailto:chenshao@jiyu.tech">shawsmith</a>
 */
@Validated
public interface LoginService {

    String DEFAULT_LOGIN_SOURCE = "default";
    String QQ_LOGIN_SOURCE = "qq";
    String WECHAT_LOGIN_SOURCE = "wechat";
    String FACEBOOK_LOGIN_SOURCE = "facebook";

    String CACHE_PREFIX = AccountService.CACHE_PREFIX + "logins:";

    void createOrUpdate(@NotNull @Valid LoginInfo loginInfo);

    List<String> findLoginTokensByLoginId(String loginId);

    void deleteByLoginToken(String loginToken);

    LoginInfo findByLoginToken(String loginToken);

    Collection<String> findLoggedIds(Collection<String> loginIds);

    LoginInfo findByLoginIdAndSource(String loginId, String loginSource);
}
