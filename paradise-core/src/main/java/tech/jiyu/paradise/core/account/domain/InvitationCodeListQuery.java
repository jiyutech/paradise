package tech.jiyu.paradise.core.account.domain;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import tech.jiyu.restful.api.domain.PaginationListQuery;
import tech.jiyu.utility.pagination.PaginationConfig;

/**
 * Created By InvitationCodeListQuery
 *
 * Date 2018/2/13 7:30
 *
 * @author SimonChan;emailAddress: simonchan@jiyu.tech
 */
@NoArgsConstructor
@Getter
@Setter
public class InvitationCodeListQuery implements PaginationListQuery {

    private PaginationConfig paginationConfig;
    private InvitationCodeStatus invitationCodeStatus;
}
