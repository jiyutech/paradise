package tech.jiyu.paradise.core.account.domain;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Created By Residence
 *
 * Date 2017/12/27 10:00
 *
 * @author SimonChan;emailAddress: simonchan@jiyu.tech
 */
@NoArgsConstructor
@Getter
@Setter
public class Residence {

    private Long id;

    private Long accountId;

    @NotNull(message = "{account.residence.city.name.limit}")
    @Size(min = 1,
          max = 64,
          message = "{account.residence.city.name.limit}")
    private String cityName;

}
