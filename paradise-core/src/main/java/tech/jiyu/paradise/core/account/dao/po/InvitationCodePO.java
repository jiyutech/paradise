package tech.jiyu.paradise.core.account.dao.po;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import tech.jiyu.paradise.core.account.domain.InvitationCode;


/**
 * Created By InvitationCodePO
 *
 * Date 2018/2/9 15:09
 *
 * @author SimonChan;emailAddress: simonchan@jiyu.tech
 */

@NoArgsConstructor
@Getter
@Setter
public class InvitationCodePO {

    private String code;
    private Long createdDate;
    private Long modifiedDate;
    private String status;
    private Long accountId;

    public static InvitationCodePO create(InvitationCode invitationCode) {
        if (invitationCode == null) {
            return null;
        }

        InvitationCodePO invitationCodePO = new InvitationCodePO();
        invitationCodePO.setCode(invitationCode.getCode());
        invitationCodePO.setStatus(invitationCode.getStatus().getCode());
        invitationCodePO.setAccountId(invitationCode.getAccountId());

        Long now = System.currentTimeMillis();
        invitationCodePO.setCreatedDate(now);
        invitationCodePO.setModifiedDate(now);

        return invitationCodePO;
    }
}
