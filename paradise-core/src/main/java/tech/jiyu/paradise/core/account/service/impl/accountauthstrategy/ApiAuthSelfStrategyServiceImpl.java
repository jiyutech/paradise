package tech.jiyu.paradise.core.account.service.impl.accountauthstrategy;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import tech.jiyu.paradise.core.account.domain.Account;
import tech.jiyu.paradise.core.account.domain.ApiAccessValidationInfo;
import tech.jiyu.paradise.core.account.enumtype.ApiAuthenticationStrategy;
import tech.jiyu.paradise.core.account.service.ApiAuthenticationStrategyService;
import tech.jiyu.paradise.core.account.service.AccountService;
import tech.jiyu.utility.cache.CacheStrategy;

import java.util.Objects;

@Component
@Slf4j
public class ApiAuthSelfStrategyServiceImpl implements ApiAuthenticationStrategyService {

    @Autowired
    private AccountService accountService;

    @Override
    public Boolean authenticate(ApiAccessValidationInfo validationInfo) {
        Account targetAccount = null;
        if (validationInfo.getTargetAccountId() != null) {
            targetAccount = accountService.findById(validationInfo.getTargetAccountId(), CacheStrategy.CACHE_PREFER);
        }
        return targetAccount != null && Objects.equals(targetAccount.getId(), Long.valueOf(validationInfo.getLoginInfo().getLoginId()));
    }

    @Override
    public ApiAuthenticationStrategy getStrategy() {
        return ApiAuthenticationStrategy.SELF;
    }
}
