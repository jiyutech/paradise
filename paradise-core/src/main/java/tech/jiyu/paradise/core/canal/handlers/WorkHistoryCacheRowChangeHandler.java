package tech.jiyu.paradise.core.canal.handlers;

import com.alibaba.otter.canal.protocol.CanalEntry;
import tech.jiyu.paradise.core.account.service.WorkHistoryService;
import tech.jiyu.utility.cache.CacheTemplate;
import tech.jiyu.utility.canal.CacheExpirationRowChangeHandler;
import tech.jiyu.utility.canal.util.ColumnUtils;
import tech.jiyu.utility.exception.ExceptionNotificationService;

public class WorkHistoryCacheRowChangeHandler extends CacheExpirationRowChangeHandler {

    public WorkHistoryCacheRowChangeHandler(CacheTemplate cacheTemplate, ExceptionNotificationService exceptionNotificationService) {
        super("work_histories", cacheTemplate, exceptionNotificationService);
    }

    @Override
    protected void doInsert(CanalEntry.RowChange row) {
        doExpireCaches(row);
    }

    @Override
    protected void doExpireCaches(CanalEntry.RowChange row) {
        super.expireCachesByColumn(row, "account_id", true, column -> ColumnUtils.getLongValue(column, null), WorkHistoryService.CACHE_PREFIX);
        super.expireCachesByColumn(row, "account_id", false, column -> ColumnUtils.getLongValue(column, null), WorkHistoryService.CACHE_PREFIX);
    }
}
