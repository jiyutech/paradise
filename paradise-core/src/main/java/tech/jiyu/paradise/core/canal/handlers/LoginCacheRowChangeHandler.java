package tech.jiyu.paradise.core.canal.handlers;

import com.alibaba.otter.canal.protocol.CanalEntry;
import tech.jiyu.paradise.core.login.service.LoginService;
import tech.jiyu.utility.cache.CacheTemplate;
import tech.jiyu.utility.canal.CacheExpirationRowChangeHandler;
import tech.jiyu.utility.canal.util.ColumnUtils;
import tech.jiyu.utility.exception.ExceptionNotificationService;

public class LoginCacheRowChangeHandler extends CacheExpirationRowChangeHandler {

    public LoginCacheRowChangeHandler(CacheTemplate cacheTemplate, ExceptionNotificationService exceptionNotificationService) {
        super("logins", cacheTemplate, exceptionNotificationService);
    }

    @Override
    protected void doExpireCaches(CanalEntry.RowChange row) {
        super.expireCachesByColumn(row, "login_token", column -> ColumnUtils.getStringValue(column, null), LoginService.CACHE_PREFIX);
    }
}
