package tech.jiyu.paradise.core.privileges.service.impl;

import com.google.common.collect.Collections2;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tech.jiyu.paradise.core.privileges.dao.mapper.AccountPrivilegeMapper;
import tech.jiyu.paradise.core.privileges.dao.po.AccountPrivilegePO;
import tech.jiyu.paradise.core.privileges.service.AccountPrivilegeService;
import tech.jiyu.sequences.SequenceService;
import tech.jiyu.utility.cache.CacheCallback;
import tech.jiyu.utility.cache.CacheStrategy;
import tech.jiyu.utility.cache.CacheTemplate;
import tech.jiyu.utility.cache.CacheUtils;

import java.util.*;
import java.util.stream.Collectors;

/**
 * @author <a href="mailto:chenshao@jinjie.tech">shawsmith</a>
 */
@Service
public class AccountPrivilegeServiceImpl implements AccountPrivilegeService {

    @Autowired
    private SequenceService sequenceService;

    @Autowired
    private AccountPrivilegeMapper accountPrivilegeMapper;

    @Autowired
    private CacheTemplate cacheTemplate;

    @Override
    @Transactional
    public void saveOrUpdate(Long accountId, Collection<Long> privilegeIds) {
        // privilegeIds.isEmpty()有意义, 表示这个账户没有任何权限.
        if (accountId == null || privilegeIds == null) {
            return;
        }

        List<AccountPrivilegePO> existedPOs = accountPrivilegeMapper.selectByAccountId(accountId);
        Set<Long> existedPrivilegeIds = Sets.newHashSet(Collections2.transform(existedPOs, AccountPrivilegePO::getPrivilegeId));
        Set<Long> newPrivilegeIds = Sets.newHashSet(privilegeIds);

        Set<Long> toBeDeletedPrivilegeIds = Sets.difference(existedPrivilegeIds, newPrivilegeIds);
        if (CollectionUtils.isNotEmpty(toBeDeletedPrivilegeIds)) {
            Collection<Long> toBeDeletedIds = existedPOs.stream()
                    .filter(accountPrivilegePO -> toBeDeletedPrivilegeIds.contains(accountPrivilegePO.getPrivilegeId()))
                    .map(AccountPrivilegePO::getId)
                    .collect(Collectors.toList());
            if (CollectionUtils.isNotEmpty(toBeDeletedIds)) {
                accountPrivilegeMapper.deleteByIds(toBeDeletedIds);
            }
        }

        Set<Long> toBeInsertedPrivilegeIds = Sets.difference(newPrivilegeIds, existedPrivilegeIds);
        if (CollectionUtils.isNotEmpty(toBeInsertedPrivilegeIds)) {
            toBeInsertedPrivilegeIds.forEach(accountPrivilegeId -> {
                AccountPrivilegePO accountPrivilegePO = AccountPrivilegePO.create(sequenceService.nextLongValue(), accountId, accountPrivilegeId);
                accountPrivilegeMapper.insert(accountPrivilegePO);
            });
        }

        removeCache(accountId);
    }

    private void removeCache(Long accountId) {
        if (accountId == null) {
            return;
        }
        cacheTemplate.delete(buildCacheKey(accountId));
    }


    private Object buildCacheKey(Long accountId) {
        return CacheUtils.buildCacheKey(CACHE_PREFIX, accountId);
    }

    @Override
    @SuppressWarnings("unchecked")
    public Map<Long, Collection<Long>> findByAccountIds(Collection<Long> accountIds, CacheStrategy strategy) {
        if (CollectionUtils.isEmpty(accountIds)) {
            return Maps.newHashMap();
        }

        Map<Long, Collection<Long>> result = Maps.newHashMap();
        Collection<Object> cacheKeys = Collections2.transform(accountIds, this::buildCacheKey);
        CacheCallback callback = new CacheCallback() {
            @Override
            public Map<Object, Object> getKeyValuePairs(Collection<Object> objects) {
                if (CollectionUtils.isEmpty(objects)) {
                    return Maps.newHashMap();
                }

                Map<Object, Object> result = Maps.newHashMap();
                objects.forEach(object -> {
                    List<AccountPrivilegePO> accountPrivilegePOs = (List<AccountPrivilegePO>) object;
                    if (CollectionUtils.isEmpty(accountPrivilegePOs)) {
                        Long accountId = accountPrivilegePOs.get(0).getAccountId();
                        Object cacheKey = buildCacheKey(accountId);
                        result.put(cacheKey, accountPrivilegePOs);
                    }
                });
                return result;
            }

            @Override
            public List<Object> loadNewObjects(Collection<Object> keys) {
                if (CollectionUtils.isEmpty(keys)) {
                    return Lists.newArrayListWithCapacity(0);
                }

                List<Object> result = Lists.newArrayList();
                Collection<Long> actualAccountIds = keys.stream()
                        .map(key -> CacheUtils.parseId(key, CACHE_PREFIX))
                        .filter(Objects::nonNull)
                        .collect(Collectors.toList());
                if (CollectionUtils.isNotEmpty(actualAccountIds)) {
                    accountPrivilegeMapper.selectByAccountIds(actualAccountIds)
                            .stream()
                            .collect(Collectors.groupingBy(AccountPrivilegePO::getAccountId))
                            .forEach((accountId, accountPrivilegePOs) -> result.add(accountPrivilegePOs));
                }
                return result;
            }
        };
        List<List<AccountPrivilegePO>> accountPrivilegePOGroups = cacheTemplate.load(cacheKeys, strategy, callback);
        if (CollectionUtils.isNotEmpty(accountPrivilegePOGroups)) {
            accountPrivilegePOGroups.forEach(groupItem -> {
                Long accountId = groupItem.get(0).getAccountId();
                Collection<Long> accountPrivilegeIds = Lists.newArrayList(Collections2.transform(groupItem, AccountPrivilegePO::getPrivilegeId));
                result.put(accountId, accountPrivilegeIds);
            });
        }
        return result;
    }

    @Override
    @Transactional
    public void deleteByAccountId(Long accountId) {
        if (accountId == null) {
            return;
        }

        accountPrivilegeMapper.deleteByAccountId(accountId);
        removeCache(accountId);
    }

    @Override
    public Long countByPrivilegeId(Long privilegeId) {
        if (privilegeId == null) {
            return 0L;
        }

        return accountPrivilegeMapper.countByPrivilegeId(privilegeId);
    }
}
