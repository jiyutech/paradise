package tech.jiyu.paradise.core.account.service.impl;

import com.google.common.collect.Maps;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.annotation.Order;
import tech.jiyu.paradise.core.account.domain.ApiAccessValidationInfo;
import tech.jiyu.paradise.core.account.enumtype.ApiAuthenticationStrategy;
import tech.jiyu.paradise.core.account.service.ApiAccessValidationService;
import tech.jiyu.paradise.core.account.service.ApiAuthenticationStrategyService;

import java.util.List;
import java.util.Map;

@Order
@Slf4j
public class ApiAccessValidationServiceImpl implements ApiAccessValidationService {

    private final Map<ApiAuthenticationStrategy, ApiAuthenticationStrategyService> strategyServiceMap;

    public ApiAccessValidationServiceImpl(List<ApiAuthenticationStrategyService> strategyServiceList) {
        this.strategyServiceMap = Maps.uniqueIndex(strategyServiceList, ApiAuthenticationStrategyService::getStrategy);
    }

    @Override
    public Boolean validate(ApiAccessValidationInfo validationInfo) {
        if (strategyServiceMap.containsKey(validationInfo.getAccessEnumType().getStrategy())) {
            return strategyServiceMap.get(validationInfo.getAccessEnumType().getStrategy()).authenticate(validationInfo);
        }
        return false;
    }

}
