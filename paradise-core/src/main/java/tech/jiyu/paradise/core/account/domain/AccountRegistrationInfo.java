package tech.jiyu.paradise.core.account.domain;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.apache.commons.lang3.StringUtils;


/**
 * @author Simon [simonchan@jinjie.tech]
 */
@Getter
@Setter
@NoArgsConstructor
public class AccountRegistrationInfo {

    private Boolean isRegistered;
    private Boolean hasPassword;

    public AccountRegistrationInfo(Account account) {
        this.isRegistered = account != null;
        this.hasPassword = account != null && StringUtils.isNotBlank(account.getPassword());
    }
}
