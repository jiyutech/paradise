package tech.jiyu.paradise.core.authentication.exception;

import org.springframework.security.core.AuthenticationException;

import tech.jiyu.utility.api.errors.annotations.ApiErrorKey;

/**
 * @author Simon [simonchan@jinjie.tech]
 */
@ApiErrorKey(group = "system", id ="invalid.man.machine.verification")
public class InvalidManMachineVerificationException extends AuthenticationException {

    public InvalidManMachineVerificationException(String msg, Throwable t) {
        super(msg, t);
    }

    public InvalidManMachineVerificationException(String msg) {
        super(msg);
    }
}
