package tech.jiyu.paradise.core.util;

import com.google.common.base.Throwables;

import freemarker.template.Configuration;
import freemarker.template.Template;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.mail.javamail.MimeMessagePreparator;

import java.io.StringWriter;
import java.util.Date;
import java.util.Map;

import tech.jiyu.paradise.core.account.domain.MailProperties;


/**
 * Created By MailUtils
 *
 * @author Xhh52
 */
public class MailUtils {

    private String from;

    private String to;

    private String url;

    private String mailContent;

    private Configuration freemarkerConfiguration;

    private MailProperties mailProperties;

    private Map<String, Object> mailTemplateModel;

    private JavaMailSender javaMailSender;

    private MailUtils() {
    }

    public static MailUtils.Builder builder() {
        return new Builder();
    }

    public void sendMail(String from, String to, String mailContent) {
        if (StringUtils.isBlank(from) || StringUtils.isBlank(to) || StringUtils.isBlank(mailContent)) {
            return;
        }

        MimeMessagePreparator mimeMessagePreparator = (mimeMessage) -> {
            MimeMessageHelper mimeMessageHelper = new MimeMessageHelper(mimeMessage);
            mimeMessageHelper.setFrom(from);
            mimeMessageHelper.setTo(to);
            mimeMessageHelper.setSubject(mailProperties.getMailSubject());
            mimeMessageHelper.setSentDate(new Date());
            mimeMessageHelper.setText(mailContent, true);
        };

        javaMailSender.send(mimeMessagePreparator);
    }

    public void sendMail() {
        if (StringUtils.isBlank(to)) {
            return;
        }

        MimeMessagePreparator mimeMessagePreparator = (mimeMessage) -> {
            MimeMessageHelper mimeMessageHelper = new MimeMessageHelper(mimeMessage);
            mimeMessageHelper.setFrom(mailProperties.getSenderAddress());
            mimeMessageHelper.setTo(to);
            mimeMessageHelper.setSubject(mailProperties.getMailSubject());
            mimeMessageHelper.setSentDate(new Date());
            mimeMessageHelper.setText(mailContent, true);
        };

        javaMailSender.send(mimeMessagePreparator);

    }

    public void buildUrl(String... urlNames) {
        StringBuilder urlBuilder = new StringBuilder();

        if (StringUtils.isNotBlank(mailProperties.getBaseUrl())) {
            urlBuilder.append(mailProperties.getBaseUrl());
        }

        for (String urlName : urlNames) {
            urlBuilder.append("/").append(urlName);
        }

        url = urlBuilder.substring(0, urlBuilder.length() - 1);
    }

    public String buildUrlParameters(Map<String, String> parameters) {
        if (StringUtils.isBlank(url)) {
            return null;
        }

        StringBuilder parameterBuilder = new StringBuilder(url);
        parameterBuilder.append("?");

        for (String parameterKey : parameters.keySet()) {
            String parameterValue = parameters.get(parameterKey);
            if (StringUtils.isNotBlank(parameterValue)) {
                parameterBuilder.append(parameterKey).append('=').append(parameterValue).append('&');
            }
        }

        url = parameterBuilder.substring(0, parameterBuilder.length() - 1);

        return url;
    }

    public String mergeTemplateIntoString(String templatePath) {
        StringWriter result = new StringWriter();

        if (mailTemplateModel == null || mailTemplateModel.isEmpty()) {
            return null;
        }
        try {
            Template template =freemarkerConfiguration.getTemplate(templatePath);
            template.process(mailTemplateModel, result);
            return result.toString();
        } catch (Exception e) {
            throw new RuntimeException(Throwables.getStackTraceAsString(e), e);
        }finally {
            IOUtils.closeQuietly(result);
        }
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getMailContent() {
        return mailContent;
    }

    public void setMailContent(String mailContent) {
        this.mailContent = mailContent;
    }

    public Configuration getFreemarkerConfiguration() {
        return freemarkerConfiguration;
    }

    public void setFreemarkerConfiguration(Configuration freemarkerConfiguration) {
        this.freemarkerConfiguration = freemarkerConfiguration;
    }

    public MailProperties getMailProperties() {
        return mailProperties;
    }

    public void setMailProperties(MailProperties mailProperties) {
        this.mailProperties = mailProperties;
    }

    public Map<String, Object> getMailTemplateModel() {
        return mailTemplateModel;
    }

    public void setMailTemplateModel(Map<String, Object> mailTemplateModel) {
        this.mailTemplateModel = mailTemplateModel;
    }

    public JavaMailSender getJavaMailSender() {
        return javaMailSender;
    }

    public void setJavaMailSender(JavaMailSender javaMailSender) {
        this.javaMailSender = javaMailSender;
    }

    public static class Builder {
        private String from;

        private String to;

        private String url;

        private String mailContent;

        private Configuration freemarkerConfiguration;

        private MailProperties mailProperties;

        private Map<String, Object> mailTemplateModel;

        private JavaMailSender javaMailSender;

        public MailUtils.Builder buildFrom(String from) {
            this.from = from;
            return this;
        }

        public Builder buildTo(String to) {
            this.to = to;
            return this;
        }

        public Builder buildUrl(String url) {
            this.url = url;
            return this;
        }

        public Configuration getFreemarkerConfiguration() {
            return freemarkerConfiguration;
        }

        public Builder setFreemarkerConfiguration(Configuration freemarkerConfiguration) {
            this.freemarkerConfiguration = freemarkerConfiguration;
            return this;
        }

        public Builder buildMailProperties(MailProperties mailProperties) {
            this.mailProperties = mailProperties;
            return this;
        }

        public Builder buildMailTemplateModel(Map<String, Object> mailTemplateModel) {
            this.mailTemplateModel = mailTemplateModel;
            return this;
        }

        public Builder buildJavaMailSender(JavaMailSender javaMailSender) {
            this.javaMailSender = javaMailSender;
            return this;
        }

        public MailUtils build() {
            MailUtils mailUtils = new MailUtils();
            mailUtils.setFrom(this.from);
            mailUtils.setMailContent(this.mailContent);
            mailUtils.setJavaMailSender(this.javaMailSender);
            mailUtils.setMailProperties(this.mailProperties);
            mailUtils.setMailTemplateModel(this.mailTemplateModel);
            mailUtils.setTo(this.to);
            mailUtils.setFreemarkerConfiguration(this.freemarkerConfiguration);
            mailUtils.setUrl(this.url);
            return mailUtils;
        }
    }
}
