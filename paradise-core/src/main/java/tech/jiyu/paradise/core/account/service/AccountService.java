package tech.jiyu.paradise.core.account.service;

import com.google.common.collect.Lists;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import tech.jiyu.idcard.verification.domain.IdCardVerification;
import tech.jiyu.paradise.core.account.domain.*;
import tech.jiyu.restful.api.service.RestfulApi;
import tech.jiyu.utility.cache.CacheStrategy;
import tech.jiyu.utility.phonenumber.PhoneNumberUtils;

/**
 * @author <a href="mailto:chenshao@dekeapp.com">shawsmith</a>
 */
public interface AccountService extends RestfulApi<Account, Map<String, Long>> {

    Long DEFAULT_COMPANY_ID = -1L;

    String DEFAULT_REGISTRATION_SOURCE = "default";
    String DEFAULT_REGISTRATION_TRAFFIC_TYPE = "default";

    String CACHE_PREFIX = "paradise_account:";

    default Account findSimpleAccount(Long accountId, CacheStrategy strategy) {
        if (accountId == null) {
            return null;
        }

        List<Account> accounts = findSimpleAccounts(Collections.singletonList(accountId), strategy);
        return CollectionUtils.isNotEmpty(accounts) ? accounts.get(0) : null;
    }

    List<Account> findSimpleAccounts(Collection<Long> accountIds, CacheStrategy strategy);

    default List<Account> findSimpleAccountsByCompanyId(Long companyId) {
        if (companyId == null) {
            return Lists.newArrayListWithCapacity(0);
        }
        return findBySimpleAccountsByCompanyIds(Collections.singletonList(companyId));
    }

    List<Account> findBySimpleAccountsByCompanyIds(Collection<Long> companyIds);

    Account register(@NotNull @Valid Account account, ActivationConfig activationConfig, Invitation invitation);

    Account findByUserName(Long companyId, String username);

    default Account findByPhoneNumber(Long companyId, String countryCode, String phoneNumber) {
        if (StringUtils.isBlank(countryCode)) {
            return findByPhoneNumber(companyId, phoneNumber);
        }
        countryCode = PhoneNumberUtils.filterNonDigitCharacters(countryCode);
        if (StringUtils.isBlank(countryCode)) {
            return findByPhoneNumber(companyId, phoneNumber);
        }
        return findByCountryCodeAndPhoneNumber(companyId, countryCode, phoneNumber);
    }

    Account findByPhoneNumber(Long companyId, String phoneNumber);

    Account findByCountryCodeAndPhoneNumber(Long companyId, String countryCode, String phoneNumber);

    Account findByEmail(Long companyId, String email);

    Account findByInvitationCode(Long companyId, String invitationCode);

    Account changePhoneNumber(@NotNull @Valid PhoneNumberChangeInfo changeInfo);

    Account findByWeChatOpenId(Long companyId, String openId);

    void unbindWeChatOpenId(Long companyId, String openId);

    Account findByWeChatTinyAppOpenId(Long companyId, String openId);

    void unbindWeChatTinyAppOpenId(Long companyId, String openId);

    Account findByWeChatH5AppOpenId(Long companyId, String openId);

    void unbindWeChatH5AppOpenId(Long companyId, String openId);

    Account findByQQOpenId(Long companyId, String openId);

    void unbindQQOpenId(Long companyId, String openId);

    void changePassword(Long operatorAccountId, Long accountId, String newPassword, String oldPassword);

    Account resetPassword(@NotNull @Valid PhoneNumberPasswordResetInfo resetInfo);

    PasswordResetResult resetPassword(Long systemAccountId, Long accountId);

    Account verify(@NotNull Long accountId, @NotNull IdCardVerification verification);

    Boolean validate(Invitation invitation);

    Account findByFacebookUserId(Long companyId, String facebookUserId);

    void unbindFacebookUserId(Long companyId, String facebookUserId);

    List<Account> findByCompanyId(Long companyId, CacheStrategy strategy);

    List<Account> findByRoleTypeCodes(Long companyId, Collection<String> roleTypeCodes, CacheStrategy strategy);

    Account findByUnionId(Long companyId, String unionId);

    void unbindUnionId(Long companyId, String unionId);

    Account findByAppleUserId(Long companyId, String appleUserId);

    void unbindAppleUserId(Long companyId, String appleUserId);

    Account findByWhatsappCountryCodeAndPhoneNumber(Long companyId, String countryCode, String phoneNumber);

    void unbindWhatsappCountryCodeAndPhoneNumber(Long companyId, String countryCode, String phoneNumber);

    Account findByTelegramId(Long companyId, Long telegramId);

    void unbindTelegramId(Long companyId, Long telegramId);

    Map<Long, Long> countCompanyAccounts(Collection<Long> companyIds);

    void deleteCache(Long id);

    List<Account> findAccountByIds(Collection<Long> ids, Boolean deleted);

    String createBindCode(Long accountId, Long bindCodeTTlSeconds);

    void bindWhatsappByBindCode(String bindCode, Long companyId, String whatsappCountryCode, String whatsappPhoneNumber);

    void bindWhatsApp(Long accountId, Long companyId, String whatsappCountryCode, String whatsappPhoneNumber);

    void bindTelegramByBindCode(String bindCode, Long companyId, Long telegramId);

    void bindTelegram(Long accountId, Long companyId, Long telegramId);
}
