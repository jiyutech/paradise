package tech.jiyu.paradise.core.authentication.service.impl;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.crypto.password.PasswordEncoder;

import tech.jiyu.captcha.domain.CaptchaCode;
import tech.jiyu.captcha.domain.CaptchaCodeKey;
import tech.jiyu.captcha.service.CaptchaService;
import tech.jiyu.paradise.core.account.domain.Account;
import tech.jiyu.paradise.core.account.domain.PhoneNumberChangeInfo;
import tech.jiyu.paradise.core.account.exception.DuplicatedPhoneNumberException;
import tech.jiyu.paradise.core.account.service.AccountService;
import tech.jiyu.paradise.core.authentication.domain.OAuth2ParametersBase;

public abstract class AbstractOAuth2Service<T> {

    @Autowired
    private AccountService accountService;

    @Autowired
    private CaptchaService captchaService;

    @Autowired
    private PasswordEncoder passwordEncoder;

    protected Account registerAccountIfNecessary(Long companyId, T t, OAuth2ParametersBase oAuth2ParametersBase) {
        Account account = findByExternalId(companyId, t);
        // 新用户需注册
        if (account == null) {
            Account accountByPhoneNumber = accountService.findByPhoneNumber(companyId, oAuth2ParametersBase.getCountryCode(), oAuth2ParametersBase.getPhoneNumber());
            // 提供的手机号未注册则走注册逻辑否则走绑定逻辑.
            if (accountByPhoneNumber == null) {
                account = accountService.register(buildAccount(companyId, t, oAuth2ParametersBase),null, null);
            } else {
                assembleAccountInfoByOAuth2CommonParams(t, accountByPhoneNumber, oAuth2ParametersBase);
                account = accountService.patch(accountByPhoneNumber);
            }
        } else {
            checkPassword(oAuth2ParametersBase.getPassword(), account.getPassword());
            assembleAccountInfoByOAuth2CommonParams(t, account, oAuth2ParametersBase);
            account = accountService.patch(account);
            fillPhoneNumber(account, oAuth2ParametersBase.getCountryCode(), oAuth2ParametersBase.getPhoneNumber());
        }

        return account;
    }

    private void assembleAccountInfoByOAuth2CommonParams(T t, Account account, OAuth2ParametersBase oAuth2ParametersBase) {
        buildAccountByExternalInfo(account, t);
        account.setPrivilegeIds(oAuth2ParametersBase.getPrivilegeIds());
        account.setEmail(oAuth2ParametersBase.getEmail());
        account.setFirstName(oAuth2ParametersBase.getFirstName());
        account.setLastName(oAuth2ParametersBase.getLastName());
        account.setAgreeToReceiveInfo(oAuth2ParametersBase.getAgreeToReceiveInfo());
        account.setLastLanguageCode(oAuth2ParametersBase.getLastLanguageCode());
        account.setAdditionalProperties(oAuth2ParametersBase.getAdditionalProperties());
    }

    private Account fillPhoneNumber(Account account, String countryCode, String phoneNumber) {
        String existedPhoneNumber = account.getPhoneNumber();

        if (StringUtils.isBlank(phoneNumber)) {
            return account;
        }

        // 如果已存在手机号则什么也不做.
        if (StringUtils.isNotBlank(existedPhoneNumber)) {
            return account;
        }

        // 构造出PhoneNumberChangeInfo
        CaptchaCodeKey captchaCodeKey = new CaptchaCodeKey(account.getCompanyId(), countryCode, phoneNumber);
        CaptchaCode captchaCode = captchaService.generate(captchaCodeKey);
        PhoneNumberChangeInfo phoneNumberChangeInfo = new PhoneNumberChangeInfo();
        phoneNumberChangeInfo.setAccountId(account.getId());
        phoneNumberChangeInfo.setCaptchaCode(captchaCode.getCode());
        phoneNumberChangeInfo.setNewPhoneNumber(phoneNumber);

        return accountService.changePhoneNumber(phoneNumberChangeInfo);
    }

    abstract Account findByExternalId(Long companyId, T t);

    abstract void buildAccountByExternalInfo(Account account, T t);

    private Account buildAccount(Long companyId, T t, OAuth2ParametersBase oAuth2ParametersBase) {
        Account existedAccount = accountService.findByPhoneNumber(companyId, oAuth2ParametersBase.getCountryCode(), oAuth2ParametersBase.getPhoneNumber());
        if (existedAccount != null) {
            String message = "companyId: " + companyId + "-" + oAuth2ParametersBase.getCountryCode() + "-" + oAuth2ParametersBase.getCountryCode();
            throw new DuplicatedPhoneNumberException(oAuth2ParametersBase.getCountryCode(), oAuth2ParametersBase.getPhoneNumber(), message);
        }
        Account account = new Account();
        assembleAccountInfoByOAuth2CommonParams(t, account, oAuth2ParametersBase);
        account.setCompanyId(companyId);
        account.setRegistrationDate(System.currentTimeMillis());
        account.setRegistrationSource(StringUtils.isNotBlank(oAuth2ParametersBase.getRegistrationSource()) ? oAuth2ParametersBase.getRegistrationSource() : oAuth2ParametersBase.getLoginSource());
        account.setRegistrationTrafficType(oAuth2ParametersBase.getRegistrationTrafficType());
        account.setCountryCode(oAuth2ParametersBase.getCountryCode());
        account.setPhoneNumber(oAuth2ParametersBase.getPhoneNumber());
        account.setIsoCode(oAuth2ParametersBase.getIsoCode());
        account.setRoleTypeCode(oAuth2ParametersBase.getRoleTypeCode());
        account.setPrivilegeIds(oAuth2ParametersBase.getPrivilegeIds());
        account.setEnabled(Boolean.TRUE);
        account.setVerified(Boolean.FALSE);
        account.setAudited(Boolean.FALSE);
        account.setWorkDayTimeEnabled(Boolean.FALSE);
        account.setPolyAdminEnabled(Boolean.TRUE);
        return account;
    }

    private void checkPassword(String password, String oldPassword) {
        if (StringUtils.isBlank(oldPassword)) {
            return;
        }

        if (StringUtils.isBlank(password)) {
            return;
        }

        if (!passwordEncoder.matches(password, oldPassword)) {
            throw new BadCredentialsException("password not match");
        }
    }
}
