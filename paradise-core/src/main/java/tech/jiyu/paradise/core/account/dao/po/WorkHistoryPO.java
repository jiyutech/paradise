package tech.jiyu.paradise.core.account.dao.po;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import tech.jiyu.paradise.core.account.domain.WorkHistory;

/**
 * Created By WorkHistory
 *
 * Date 2017/12/27 10:14
 *
 * @author SimonChan;emailAddress: simonchan@jiyu.tech
 */
@NoArgsConstructor
@Getter
@Setter
public class WorkHistoryPO {

    private Long id;
    private Long accountId;
    private Long createdDate;
    private Long modifiedDate;
    private String companyName;
    private String jobTitle;
    private Integer orderNum;

    public static WorkHistoryPO create(Long id, WorkHistory workHistory) {
        if (workHistory == null) {
            return null;
        }

        WorkHistoryPO workHistoryPO = new WorkHistoryPO();
        Long now = System.currentTimeMillis();

        workHistoryPO.setId(id);
        workHistoryPO.setAccountId(workHistory.getAccountId());
        workHistoryPO.setCreatedDate(now);
        workHistoryPO.setModifiedDate(now);
        workHistoryPO.setCompanyName(workHistory.getCompanyName());
        workHistoryPO.setJobTitle(workHistory.getJobTitle());

        return workHistoryPO;
    }
}
