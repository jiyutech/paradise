package tech.jiyu.paradise.core.account.domain;

import org.springframework.boot.context.properties.ConfigurationProperties;

import java.util.concurrent.TimeUnit;

/**
 * @author <a href="mailto:chenshao@dekeapp.com">shawsmith</a>
 */
@ConfigurationProperties(prefix = AccountActivationProperties.PREFIX)
public class AccountActivationProperties {

    public static final String PREFIX = "paradise.account.activation";
    public static final Long DEFAULT_ACTIVATION_CODE_EXPIRE_TIME = TimeUnit.DAYS.toMillis(1L);

    private Long codeExpireTime = DEFAULT_ACTIVATION_CODE_EXPIRE_TIME;
    private String mailSubject;
    private String activationBaseUrl;
    private String senderAddress;
    private String projectName;
    private String signature;

    public Long getCodeExpireTime() {
        return codeExpireTime;
    }

    public void setCodeExpireTime(Long codeExpireTime) {
        this.codeExpireTime = codeExpireTime;
    }

    public String getMailSubject() {
        return mailSubject;
    }

    public void setMailSubject(String mailSubject) {
        this.mailSubject = mailSubject;
    }

    public String getActivationBaseUrl() {
        return activationBaseUrl;
    }

    public void setActivationBaseUrl(String activationBaseUrl) {
        this.activationBaseUrl = activationBaseUrl;
    }

    public String getSenderAddress() {
        return senderAddress;
    }

    public void setSenderAddress(String senderAddress) {
        this.senderAddress = senderAddress;
    }

    public String getProjectName() {
        return projectName;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    public String getSignature() {
        return signature;
    }

    public void setSignature(String signature) {
        this.signature = signature;
    }
}
