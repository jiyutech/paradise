package tech.jiyu.paradise.core.account.domain;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class AccountDeletingTask {
    private Long id;
    private Long createdDate;
    private Long accountId; // 被删除的账号id
    private Boolean hasCompleted; // 任务是否已完成，完成的话就无需扫描了
    private Long completedTime; // 任务完成时间
    private BigDecimal accountDeletingCoolingOffDays; // 冷静期
    private Long accountEstimateDeletingTime; // 账号预计删除时间
}
