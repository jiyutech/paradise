package tech.jiyu.paradise.core.account.service;

import tech.jiyu.paradise.core.account.domain.AccountDeletingTask;
import tech.jiyu.paradise.core.account.domain.AccountListener;
import tech.jiyu.utility.web.TimeRange;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.List;
import java.util.Map;

public interface AccountDeletingTaskService extends AccountListener {

    AccountDeletingTask createDeletingTask(Long id);

    List<AccountDeletingTask> getDeletingTaskByTimeRange(TimeRange timeRange);

    AccountDeletingTask getByAccountId(Long id);

    void deleteTaskByAccountId(Long accountId);

    void completeTaskByAccountId(Long accountId);

    List<AccountDeletingTask> getByAccountIdList(Collection<Long> accountIds);
    Map<Long, AccountDeletingTask> getAndGroupByAccountIdList(Collection<Long> accountIds);

    BigDecimal getCoolingOffDays();
}
