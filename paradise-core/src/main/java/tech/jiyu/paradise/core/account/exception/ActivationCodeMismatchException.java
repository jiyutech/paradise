package tech.jiyu.paradise.core.account.exception;

import tech.jiyu.utility.api.errors.annotations.ApiErrorKey;

/**
 * @author <a href="mailto:chenshao@dekeapp.com">shawsmith</a>
 */
@ApiErrorKey(group = "account",
             id = "activation.code.mismatch")
public class ActivationCodeMismatchException extends RuntimeException {

    public ActivationCodeMismatchException() {
        super();
    }

    public ActivationCodeMismatchException(String message) {
        super(message);
    }

    public ActivationCodeMismatchException(String message, Throwable cause) {
        super(message, cause);
    }

    public ActivationCodeMismatchException(Throwable cause) {
        super(cause);
    }
}
