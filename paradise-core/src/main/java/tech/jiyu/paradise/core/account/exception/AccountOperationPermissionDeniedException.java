package tech.jiyu.paradise.core.account.exception;

import tech.jiyu.utility.api.errors.annotations.ApiErrorKey;

/**
 * @author <a href="mailto:chenshao@dekeapp.com">shawsmith</a>
 */
@ApiErrorKey(group = "account",
             id = "account.permission.denied")
public class AccountOperationPermissionDeniedException extends RuntimeException {

    public AccountOperationPermissionDeniedException() {
    }

    public AccountOperationPermissionDeniedException(String message) {
        super(message);
    }

    public AccountOperationPermissionDeniedException(String message, Throwable cause) {
        super(message, cause);
    }

    public AccountOperationPermissionDeniedException(Throwable cause) {
        super(cause);
    }
}
