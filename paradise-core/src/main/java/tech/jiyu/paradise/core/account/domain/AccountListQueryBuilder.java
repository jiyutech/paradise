package tech.jiyu.paradise.core.account.domain;

import java.util.Map;

/**
 * @author <a href="mailto:chenshao@dekeapp.com">shawsmith</a>
 */
public interface AccountListQueryBuilder {

    AccountListQuery buildAccountListQuery(Map<String, String> parameters);
}
