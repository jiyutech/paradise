package tech.jiyu.paradise.core.canal.handlers;

import com.alibaba.otter.canal.protocol.CanalEntry;
import tech.jiyu.paradise.core.account.service.AccountService;
import tech.jiyu.utility.cache.CacheTemplate;
import tech.jiyu.utility.canal.CacheExpirationRowChangeHandler;
import tech.jiyu.utility.exception.ExceptionNotificationService;

public class AccountCacheRowChangeHandler extends CacheExpirationRowChangeHandler {

    public AccountCacheRowChangeHandler(CacheTemplate cacheTemplate, ExceptionNotificationService exceptionNotificationService) {
        super("accounts", cacheTemplate, exceptionNotificationService);
    }

    @Override
    protected void doExpireCaches(CanalEntry.RowChange row) {
        super.expireCachesByIds(row, AccountService.CACHE_PREFIX);
    }
}
