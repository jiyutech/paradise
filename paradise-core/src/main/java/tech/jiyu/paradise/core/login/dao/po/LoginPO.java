package tech.jiyu.paradise.core.login.dao.po;


import tech.jiyu.paradise.core.login.domain.LoginInfo;


/**
 * @author <a href="mailto:chenshao@jiyu.tech">shawsmith</a>
 */
public class LoginPO {

    private Long id;
    private Long gmtCreated;
    private Long gmtModified;
    private String loginId;
    private String loginToken;
    private String roleTypeCode;
    private String loginSource;

    public static LoginPO create(Long id, LoginInfo loginInfo) {
        LoginPO loginPO = new LoginPO();
        loginPO.setId(id);
        Long now = System.currentTimeMillis();
        loginPO.setGmtCreated(now);
        loginPO.setGmtModified(now);
        loginPO.setLoginId(loginInfo.getLoginId());
        loginPO.setLoginToken(loginInfo.getLoginToken());
        loginPO.setRoleTypeCode(loginInfo.getRoleTypeCode());
        loginPO.setLoginSource(loginInfo.getLoginSource());
        return loginPO;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getGmtCreated() {
        return gmtCreated;
    }

    public void setGmtCreated(Long gmtCreated) {
        this.gmtCreated = gmtCreated;
    }

    public Long getGmtModified() {
        return gmtModified;
    }

    public void setGmtModified(Long gmtModified) {
        this.gmtModified = gmtModified;
    }

    public String getLoginId() {
        return loginId;
    }

    public void setLoginId(String loginId) {
        this.loginId = loginId;
    }

    public String getLoginToken() {
        return loginToken;
    }

    public void setLoginToken(String loginToken) {
        this.loginToken = loginToken;
    }

    public String getRoleTypeCode() {
        return roleTypeCode;
    }

    public void setRoleTypeCode(String roleTypeCode) {
        this.roleTypeCode = roleTypeCode;
    }

    public String getLoginSource() {
        return loginSource;
    }

    public void setLoginSource(String loginSource) {
        this.loginSource = loginSource;
    }

    @Override
    public String toString() {
        return "LoginPO{" +
                "id=" + id +
                ", gmtCreated=" + gmtCreated +
                ", gmtModified=" + gmtModified +
                ", loginId='" + loginId + '\'' +
                ", loginToken='" + loginToken + '\'' +
                ", roleTypeCode='" + roleTypeCode + '\'' +
                ", loginSource='" + loginSource + '\'' +
                '}';
    }
}
