package tech.jiyu.paradise.core.account.annotation;

import tech.jiyu.paradise.core.account.enumtype.ApiAccessEnumType;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
public @interface ApiAccessValidation {

    ApiAccessEnumType accessType();

}
