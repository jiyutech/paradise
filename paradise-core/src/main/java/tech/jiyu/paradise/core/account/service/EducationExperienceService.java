package tech.jiyu.paradise.core.account.service;

import org.springframework.validation.annotation.Validated;
import tech.jiyu.paradise.core.account.domain.EducationExperience;
import tech.jiyu.utility.cache.CacheStrategy;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * Created By EducationExperienceService
 * <p>
 * Date 2017/12/27 20:06
 *
 * @author SimonChan;emailAddress: simonchan@jiyu.tech
 */
@Validated
public interface EducationExperienceService {

    String CACHE_PREFIX = AccountService.CACHE_PREFIX + "education_experiences:";

    List<EducationExperience> saveOrUpdate(@NotNull Long accountId, @NotNull @Valid List<EducationExperience> experiences);

    List<EducationExperience> findByAccountId(Long accountId, CacheStrategy strategy);

    void deleteByAccountId(Long accountId);
}
