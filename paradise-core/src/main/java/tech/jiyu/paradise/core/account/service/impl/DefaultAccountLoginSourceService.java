package tech.jiyu.paradise.core.account.service.impl;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import tech.jiyu.paradise.core.account.domain.Account;
import tech.jiyu.paradise.core.account.service.AccountLoginSourceService;
import tech.jiyu.paradise.core.account.service.AccountService;
import tech.jiyu.paradise.core.login.service.LoginService;
import tech.jiyu.utility.cache.CacheStrategy;

/**
 * @author <a href="mailto:chenshao@jiyu.tech">shawsmith</a>
 */
public class DefaultAccountLoginSourceService implements AccountLoginSourceService {

    protected AccountService accountService;

    public DefaultAccountLoginSourceService(AccountService accountService) {
        this.accountService = accountService;
    }

    @Override
    public Map<Long, Collection<String>> getAccountLoginSources(Collection<Long> accountIds) {
        List<Account> accounts = accountService.findByIds(accountIds, CacheStrategy.CACHE_PREFER);
        if (CollectionUtils.isEmpty(accounts)) {
            return Maps.newHashMap();
        }

        Map<Long, Collection<String>> accountLoginSources = Maps.newHashMap();
        accounts.forEach(account -> {
            if (account != null) {
                List<String> loginSources = Lists.newArrayList(LoginService.DEFAULT_LOGIN_SOURCE, LoginService.QQ_LOGIN_SOURCE, LoginService.WECHAT_LOGIN_SOURCE, LoginService.FACEBOOK_LOGIN_SOURCE);
                accountLoginSources.put(account.getId(), loginSources);
            }
        });
        return accountLoginSources;
    }

    @Override
    public Boolean supportLoginSource(Long accountId, String loginSource) {
        if (accountId == null) {
            return Boolean.FALSE;
        }

        Map<Long, Collection<String>> loginSourceGroups = getAccountLoginSources(Collections.singletonList(accountId));
        if (loginSourceGroups == null || loginSourceGroups.isEmpty()) {
            return Boolean.FALSE;
        }

        Collection<String> accountLoginSources = loginSourceGroups.get(accountId);
        return (accountLoginSources != null && StringUtils.isNotBlank(loginSource) && accountLoginSources.contains(loginSource));
    }
}
