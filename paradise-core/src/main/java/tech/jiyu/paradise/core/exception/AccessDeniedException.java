package tech.jiyu.paradise.core.exception;

import tech.jiyu.utility.api.errors.annotations.ApiErrorKey;

@ApiErrorKey(group = "system", id = "access.denied", httpResponseStatusCode = 403)
public class AccessDeniedException extends RuntimeException {
}
