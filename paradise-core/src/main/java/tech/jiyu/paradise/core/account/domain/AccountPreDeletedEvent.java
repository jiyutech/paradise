package tech.jiyu.paradise.core.account.domain;

/**
 * @author <a href="mailto:chenshao@jiyu.tech">shawsmith</a>
 */
public class AccountPreDeletedEvent extends AccountEvent {

    private final Long accountId;

    public AccountPreDeletedEvent(Long accountId) {
        this.accountId = accountId;
    }

    public Long getAccountId() {
        return accountId;
    }
}
