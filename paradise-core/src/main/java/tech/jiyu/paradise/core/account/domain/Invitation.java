package tech.jiyu.paradise.core.account.domain;

import java.util.Objects;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author <a href="mailto:chenshao@dekeapp.com">shawsmith</a>
 */
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class Invitation {

    private String code;
    private Long inviteeId;


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Invitation that = (Invitation) o;
        return Objects.equals(code, that.code) &&
                Objects.equals(inviteeId, that.inviteeId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(code, inviteeId);
    }
}
