package tech.jiyu.paradise.core.authentication.service.impl;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.HttpHost;
import org.apache.http.client.config.RequestConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tech.jiyu.captcha.service.CaptchaService;
import tech.jiyu.paradise.core.account.domain.Account;
import tech.jiyu.paradise.core.account.service.AccountService;
import tech.jiyu.paradise.core.authentication.domain.*;
import tech.jiyu.paradise.core.authentication.exception.*;
import tech.jiyu.paradise.core.authentication.service.OAuth2AuthenticationService;
import tech.jiyu.paradise.core.authentication.service.OAuth2Service;
import tech.jiyu.parrot.facebook.domain.FacebookOAuth2AccessToken;
import tech.jiyu.parrot.facebook.domain.FacebookUserInfo;
import tech.jiyu.parrot.facebook.service.FacebookService;
import tech.jiyu.parrot.preferences.service.PreferenceService;
import tech.jiyu.penguin.wxapi.wechat.common.exception.CannotAcquireAccessTokenException;
import tech.jiyu.penguin.wxapi.wechat.oauth2.domain.OAuth2AccessToken;
import tech.jiyu.photo.api.PhotoService;
import tech.jiyu.utility.cache.CacheTemplate;
import tech.jiyu.utility.datastructure.Pair;
import tech.jiyu.utility.exception.ExceptionNotificationService;
import tech.jiyu.utility.requestproxy.RequestProxyConfig;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.TimeUnit;

/**
 * Created By FacebookOAuth2ServiceImpl
 * <p>
 * Date 2018/3/7 17:25
 *
 * @author SimonChan;emailAddress: simonchan@jiyu.tech
 */
@Service
public class FacebookOAuth2ServiceImpl extends AbstractOAuth2Service<FacebookUserInfo> implements OAuth2Service {

    private static final String APP_ID_KEY = "facebookOAuth2.appId";

    private static final String APP_SECRET_KEY = "facebookOAuth2.appSecret";

    private static final String REDIRECT_URI_KEY = "facebookOAuth2.redirectUri";

    private static final Long DEFAULT_KEY_AUTHENTICATION_CACHE_TIMEOUT = 1000L * 60 * 5;

    private RequestConfig requestConfig;

    @Autowired(required = false)
    private RequestProxyConfig proxyConfig;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private PreferenceService preferenceService;

    @Autowired
    private AccountService accountService;

    @Autowired
    private PhotoService photoService;

    @Autowired
    private ExceptionNotificationService exceptionNotificationService;

    @Autowired
    private OAuth2AuthenticationService oAuth2AuthenticationService;

    @Autowired
    private CacheTemplate cacheTemplate;

    @Autowired
    private CaptchaService captchaService;

    @Autowired
    private FacebookService facebookService;

    @PostConstruct
    private void initParameters() {
        if (proxyConfig != null) {
            requestConfig = RequestConfig.custom().setProxy(new HttpHost(proxyConfig.getProxyHostname(), proxyConfig.getProxyPort())).build();
        }
    }

    @Override
    public boolean supports(OAuth2Type type) {
        return type == OAuth2Type.FACEBOOK;
    }

    @Override
    @Transactional
    @SuppressWarnings("unchecked")
    public Account createOrUpdate(OAuth2Parameters parameters) throws IOException {
        Pair<FacebookUserInfo, OAuth2AccessToken> pair = fetchOAuth2Info(parameters);
        FacebookUserInfo facebookUserInfo = pair.getLeft();
        OAuth2AccessToken accessToken = pair.getRight();

        if (facebookUserInfo == null) {
            throw new NoSuchFacebookUserInfoException();
        }

        // 新用户注册,老用户更新账户信息
        Account account = super.registerAccountIfNecessary(parameters.getCompanyId(), facebookUserInfo, parameters);

        // 创建或更新OAuth2.0信息
        OAuth2Type type = OAuth2Type.fromCode(parameters.getAuthType());
        OAuth2Authentication oAuth2Authentication = buildOAuth2Authentication(account.getId(), accessToken, type);
        oAuth2AuthenticationService.saveOrUpdate(oAuth2Authentication);

        return account;
    }

    @Override
    public Account createOrUpdate(OAuth2KeyParameters keyParameters) {
        OAuth2KeyAuthentication keyAuthentication = Optional.ofNullable(loadKeyAuthenticationFromCache(keyParameters.getAuthKey())).orElseThrow(OAuth2KeyExpiredException::new);
        checkPhoneNumberIfNecessary(keyParameters, keyAuthentication);
        FacebookUserInfo facebookUserInfo = (FacebookUserInfo) keyAuthentication.getOauthUserInfo();

        /**
         * 几点情况说明:
         *  1. 用户使用普通的OAuth2登录过,但是没有提供手机号, 再使用这种方式登录也需要提供手机号.
         *  2. 用户第一次使用OAuth2 Key的方式登录,必须提供手机号.
         *  3. 用户已经提供过手机号, 如果使用OAuth2 Key的方式登录又提供了手机号, 那么旧的手机号不会被覆盖.
         */
        Account account = super.registerAccountIfNecessary(keyParameters.getCompanyId(), facebookUserInfo, keyParameters);

        OAuth2Type oauth2Type = OAuth2Type.fromCode(keyParameters.getAuthType());
        OAuth2Authentication oauth2Authentication = buildOAuth2Authentication(account.getId(), keyAuthentication.getAccessToken(), oauth2Type);
        oAuth2AuthenticationService.saveOrUpdate(oauth2Authentication);

        deleteKeyAuthenticationFromCache(keyAuthentication);
        return account;
    }

    @Override
    public OAuth2KeyAuthentication createOAuth2KeyAuthentication(OAuth2Parameters parameters) throws IOException {
        Pair<FacebookUserInfo, OAuth2AccessToken> pair = fetchOAuth2Info(parameters);
        OAuth2KeyAuthentication keyAuthentication = buildOAuth2KeyAuthentication(parameters.getCompanyId(), pair);
        storeKeyAuthenticationToCache(keyAuthentication);
        return keyAuthentication;
    }

    private void checkPhoneNumberIfNecessary(OAuth2KeyParameters keyParameters, OAuth2KeyAuthentication keyAuthentication) {
        String phoneNumber = keyParameters.getPhoneNumber();
        Boolean needPhoneNumber = keyAuthentication.getNeedPhoneNumber();
        if (needPhoneNumber && StringUtils.isBlank(phoneNumber)) {
            throw new NoProvidePhoneNumberException(keyParameters.getAuthKey());
        }
    }

    private void deleteKeyAuthenticationFromCache(OAuth2KeyAuthentication keyAuthentication) {
        cacheTemplate.delete(keyAuthentication.getCacheKey());
    }

    private void storeKeyAuthenticationToCache(OAuth2KeyAuthentication keyAuthentication) {
        FacebookUserInfo facebookUserInfo = (FacebookUserInfo) keyAuthentication.getOauthUserInfo();
        String facebookUserId = facebookUserInfo.getUserId();
        String cacheKeySuffix = String.valueOf(System.currentTimeMillis());
        String cacheKey = OAuth2Service.OAUTH2_KEY_CACHE_PREFIX.concat(facebookUserId).concat(cacheKeySuffix);
        keyAuthentication.setCacheKey(cacheKey);

        cacheTemplate.set(cacheKey, keyAuthentication, DEFAULT_KEY_AUTHENTICATION_CACHE_TIMEOUT, TimeUnit.MILLISECONDS);
    }

    private OAuth2KeyAuthentication loadKeyAuthenticationFromCache(String cacheKey) {
        return cacheTemplate.get(cacheKey, OAuth2KeyAuthentication.class);
    }

    private OAuth2KeyAuthentication buildOAuth2KeyAuthentication(Long companyId, Pair<FacebookUserInfo, OAuth2AccessToken> pair) {
        FacebookUserInfo facebookUserInfo = Optional.ofNullable(pair.getLeft()).orElseThrow(NoSuchFacebookUserInfoException::new);
        Optional<OAuth2AccessToken> accessTokenOptional = Optional.of(pair.getRight());
        Account account = accountService.findByFacebookUserId(companyId, facebookUserInfo.getUserId());

        OAuth2KeyAuthentication authentication = new OAuth2KeyAuthentication();
        authentication.setOauthUserInfo(facebookUserInfo);
        authentication.setAccessToken(accessTokenOptional.get());

        if (account == null || StringUtils.isBlank(account.getPhoneNumber())) {
            authentication.setNeedPhoneNumber(Boolean.TRUE);
        } else {
            authentication.setNeedPhoneNumber(Boolean.FALSE);
        }

        return authentication;
    }


    private Pair<FacebookUserInfo, OAuth2AccessToken> fetchOAuth2Info(OAuth2Parameters parameters) throws IOException {
        FacebookOAuth2AccessToken facebookOAuth2AccessToken = getFacebookOAuth2AccessToken(parameters);
        if (facebookOAuth2AccessToken == null) {
            throw new CannotAcquireAccessTokenException();
        }

        String appAccessToken = facebookOAuth2AccessToken.getAppAccessToken();

        if (StringUtils.isBlank(appAccessToken)) {
            throw new CannotAcquireAppAccessTokenException();
        }

        Set<String> scopes = facebookOAuth2AccessToken.getScopes();

        if (CollectionUtils.isEmpty(scopes)) {
            throw new CannotAcquireUserIdException();
        }

        OAuth2AccessToken accessToken = buildOAuth2AccessToken(facebookOAuth2AccessToken);
        FacebookUserInfo facebookUserInfo = getFacebookUserInfo(facebookOAuth2AccessToken);

        Pair<FacebookUserInfo, OAuth2AccessToken> pair = new Pair<>();
        pair.setLeft(facebookUserInfo);
        pair.setRight(accessToken);
        return pair;
    }

    private OAuth2AccessToken buildOAuth2AccessToken(FacebookOAuth2AccessToken facebookOAuth2AccessToken) {
        OAuth2AccessToken accessToken = new OAuth2AccessToken();

        accessToken.setAccessToken(facebookOAuth2AccessToken.getUserAccessToken());
        accessToken.setRefreshToken(facebookOAuth2AccessToken.getUserAccessToken());
        accessToken.setScope(StringUtils.join(facebookOAuth2AccessToken.getScopes(), ","));
        accessToken.setExpiresIn(facebookOAuth2AccessToken.getExpiresIn());

        return accessToken;
    }

    private OAuth2Authentication buildOAuth2Authentication(Long accountId, OAuth2AccessToken accessToken, OAuth2Type type) {
        OAuth2Authentication oAuth2Authentication = new OAuth2Authentication();

        oAuth2Authentication.setAccountId(accountId);
        oAuth2Authentication.setAccessToken(accessToken.getAccessToken());
        oAuth2Authentication.setScope(accessToken.getScope());
        oAuth2Authentication.setType(type);
        oAuth2Authentication.setExpiresIn(accessToken.getExpiresIn());
        oAuth2Authentication.setRefreshToken(accessToken.getAccessToken());

        return oAuth2Authentication;
    }

    @SuppressWarnings(value = "unchcked")
    private FacebookOAuth2AccessToken getFacebookOAuth2AccessToken(OAuth2Parameters parameters) throws IOException {

        String appId = preferenceService.findRequiredPreferenceValue(APP_ID_KEY);
        String appSecret = preferenceService.findRequiredPreferenceValue(APP_SECRET_KEY);

        String authCode = parameters.getAuthCode();
        if (StringUtils.isNotBlank(authCode)) {
            String redirectUrl = StringUtils.isNotBlank(parameters.getLoginRedirectUrl()) ? parameters.getLoginRedirectUrl() : preferenceService.findRequiredPreferenceValue(REDIRECT_URI_KEY);
            return facebookService.getAccessToken(appId, appSecret, authCode, redirectUrl);
        } else {
            String accessToken = parameters.getAccessToken();
            if (StringUtils.isNotBlank(accessToken)) {
                FacebookOAuth2AccessToken facebookOAuth2AccessToken = new FacebookOAuth2AccessToken();

                facebookOAuth2AccessToken.setUserAccessToken(accessToken);
                facebookOAuth2AccessToken.setScopes(parameters.getScopes());
                facebookOAuth2AccessToken.setExpiresIn(parameters.getExpiresIn());
                String appAccessToken = facebookService.getAppAccessToken(appId, appSecret);
                facebookOAuth2AccessToken.setAppAccessToken(appAccessToken);

                return facebookOAuth2AccessToken;
            }
        }
        return null;
    }

    private FacebookUserInfo getFacebookUserInfo(FacebookOAuth2AccessToken facebookOAuth2AccessToken) throws IOException {
        String appId = preferenceService.findRequiredPreferenceValue(APP_ID_KEY);
        String appSecret = preferenceService.findRequiredPreferenceValue(APP_SECRET_KEY);
        return facebookService.findByAccessToken(appId, appSecret, facebookOAuth2AccessToken);
    }

    @Override
    Account findByExternalId(Long companyId, FacebookUserInfo facebookUserInfo) {
        return accountService.findByFacebookUserId(companyId, facebookUserInfo.getUserId());
    }

    @Override
    void buildAccountByExternalInfo(Account account, FacebookUserInfo facebookUserInfo) {
        account.setFacebookUserId(facebookUserInfo.getUserId());
        account.setName(facebookUserInfo.getName());
        account.setGender(facebookUserInfo.getGender());
        try {
            String avatarKey = photoService.upload(facebookUserInfo.getImageUrl());
            account.setAvatar(avatarKey);
        } catch (Exception e) {
            exceptionNotificationService.notify(e);
        }
    }
}
