package tech.jiyu.paradise.core.account.service;

import java.util.Collection;
import java.util.Map;

import tech.jiyu.paradise.core.account.domain.Account;

/**
 * @author <a href="mailto:chenshao@dekeapp.com">shawsmith</a>
 */
public interface AccountAdditionalAttributeService {

    Map<Long, Map<String, Object>> getAdditionalAttributes(Collection<Account> accounts);
}
