package tech.jiyu.paradise.core.authentication.dao.po;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import tech.jiyu.paradise.core.authentication.domain.OAuth2Authentication;
import tech.jiyu.paradise.core.authentication.domain.OAuth2Type;

/**
 * @author <a href="mailto:chenshao@dekeapp.com">shawsmith</a>
 */
@NoArgsConstructor
@Getter
@Setter
public class OAuth2AuthenticationPO {

    private Long id;
    private Long createdDate;
    private Long modifiedDate;
    private Long accountId;
    private String type;
    private String accessToken;
    private String refreshToken;
    private Long expiresIn;
    private String scope;

    public static OAuth2AuthenticationPO create(Long id, OAuth2Authentication authentication) {
        OAuth2AuthenticationPO authenticationPO = new OAuth2AuthenticationPO();
        authenticationPO.setId(id);

        Long now = System.currentTimeMillis();
        authenticationPO.setCreatedDate(now);
        authenticationPO.setModifiedDate(now);

        if (authentication != null) {
            authenticationPO.setAccountId(authentication.getAccountId());
            OAuth2Type type = authentication.getType();
            if (type != null) {
                authenticationPO.setType(type.getCode());
            }
            authenticationPO.setAccessToken(authentication.getAccessToken());
            authenticationPO.setRefreshToken(authentication.getRefreshToken());
            authenticationPO.setExpiresIn(authentication.getExpiresIn());
            authenticationPO.setScope(authentication.getScope());
        }

        return authenticationPO;
    }
}
