package tech.jiyu.paradise.core.account.domain;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Created By MailPasswordResetInfo
 *
 * @author Xhh52
 */
@NoArgsConstructor
@Getter
@Setter
public class MailPasswordResetInfo {

    @NotNull
    private Long companyId;

    @NotEmpty
    private String code;

    @NotEmpty
    private String newPassword;

    @NotEmpty
    private String confirmedNewPassword;

    @NotEmpty
    private String mailAddress;
}
