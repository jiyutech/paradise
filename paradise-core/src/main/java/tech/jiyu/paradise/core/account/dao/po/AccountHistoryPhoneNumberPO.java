package tech.jiyu.paradise.core.account.dao.po;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import tech.jiyu.paradise.core.account.domain.AccountHistoryPhoneNumber;

/**
 * @author <a href="mailto:chenshao@dekeapp.com">shawsmith</a>
 */
@NoArgsConstructor
@Getter
@Setter
public class AccountHistoryPhoneNumberPO {

    private Long id;
    private Long createdDate;
    private Long modifiedDate;
    private Long accountId;
    private String oldPhoneNumber;
    private String newPhoneNumber;
    private Long changeDate;

    public static AccountHistoryPhoneNumberPO create(Long id, AccountHistoryPhoneNumber historyPhoneNumber) {
        AccountHistoryPhoneNumberPO historyPhoneNumberPO = new AccountHistoryPhoneNumberPO();

        historyPhoneNumberPO.setId(id);

        Long now = System.currentTimeMillis();
        historyPhoneNumberPO.setCreatedDate(now);
        historyPhoneNumberPO.setModifiedDate(now);

        if (historyPhoneNumber != null) {
            historyPhoneNumberPO.setAccountId(historyPhoneNumber.getAccountId());
            historyPhoneNumberPO.setOldPhoneNumber(historyPhoneNumber.getOldPhoneNumber());
            historyPhoneNumberPO.setNewPhoneNumber(historyPhoneNumber.getNewPhoneNumber());
            historyPhoneNumberPO.setChangeDate(historyPhoneNumber.getChangeDate());
        }

        return historyPhoneNumberPO;
    }
}
