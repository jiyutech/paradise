package tech.jiyu.paradise.core.account.domain;

import lombok.Getter;

@Getter
public class AccountPhoneNumberChangedEvent extends AccountEvent {

    private Long accountId;
    private String oldCountryCode;
    private String oldPhoneNumber;
    private String newCountryCode;
    private String newPhoneNumber;

    public AccountPhoneNumberChangedEvent(Long accountId, String oldCountryCode, String oldPhoneNumber, String newCountryCode, String newPhoneNumber) {
        this.accountId = accountId;
        this.oldCountryCode = oldCountryCode;
        this.oldPhoneNumber = oldPhoneNumber;
        this.newCountryCode = newCountryCode;
        this.newPhoneNumber = newPhoneNumber;
    }
}
