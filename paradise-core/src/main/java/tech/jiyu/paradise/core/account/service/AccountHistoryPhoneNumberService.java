package tech.jiyu.paradise.core.account.service;

import java.util.Collection;
import java.util.List;
import java.util.Map;

import tech.jiyu.paradise.core.account.domain.AccountHistoryPhoneNumber;
import tech.jiyu.utility.cache.CacheStrategy;

/**
 * @author <a href="mailto:chenshao@dekeapp.com">shawsmith</a>
 */
public interface AccountHistoryPhoneNumberService {

    String CACHE_PREFIX = AccountService.CACHE_PREFIX + "history_phone_numbers:";

    AccountHistoryPhoneNumber save(AccountHistoryPhoneNumber historyPhoneNumber);

    Map<Long, List<AccountHistoryPhoneNumber>> findByAccountIds(Collection<Long> accountIds, CacheStrategy strategy);

    void deleteByAccountId(Long accountId);
}
