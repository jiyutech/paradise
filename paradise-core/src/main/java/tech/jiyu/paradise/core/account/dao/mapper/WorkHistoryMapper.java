package tech.jiyu.paradise.core.account.dao.mapper;

import org.apache.ibatis.annotations.Mapper;

import java.util.Collection;
import java.util.List;

import tech.jiyu.paradise.core.account.dao.po.WorkHistoryPO;

/**
 * Created By WorkHistoryMapper
 *
 * Date 2017/12/27 18:27
 *
 * @author SimonChan;emailAddress: simonchan@jiyu.tech
 */
@Mapper
public interface WorkHistoryMapper {

    void batchSave(List<WorkHistoryPO> workHistoryPOs);

    void deleteByAccountId(Long accountId);

    List<WorkHistoryPO> selectByAccountId(Long accountId);

    List<WorkHistoryPO> selectByIds(Collection<Long> workHistoryIds);

    void updateById(WorkHistoryPO workHistoryPO);
}
