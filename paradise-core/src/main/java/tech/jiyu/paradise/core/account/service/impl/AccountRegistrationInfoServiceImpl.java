package tech.jiyu.paradise.core.account.service.impl;

import lombok.AllArgsConstructor;
import tech.jiyu.paradise.core.account.domain.Account;
import tech.jiyu.paradise.core.account.domain.AccountRegistrationInfo;
import tech.jiyu.paradise.core.account.service.AccountRegistrationInfoService;
import tech.jiyu.paradise.core.account.service.AccountService;

@AllArgsConstructor
public class AccountRegistrationInfoServiceImpl implements AccountRegistrationInfoService {

    private AccountService accountService;

    @Override
    public AccountRegistrationInfo getByCountryCodeAndPhoneNumber(String countryCode, String phoneNumber, Long companyId) {
        Account account = accountService.findByPhoneNumber(AccountService.DEFAULT_COMPANY_ID, countryCode, phoneNumber);
        return new AccountRegistrationInfo(account);
    }
}
