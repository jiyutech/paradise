package tech.jiyu.paradise.core.account.service.impl;

import com.google.common.base.Throwables;
import com.google.common.collect.Maps;

import freemarker.template.Configuration;
import freemarker.template.Template;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.mail.javamail.MimeMessagePreparator;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.StringWriter;
import java.util.Date;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import tech.jiyu.paradise.core.account.dao.mapper.AccountMapper;
import tech.jiyu.paradise.core.account.dao.po.AccountPO;
import tech.jiyu.paradise.core.account.domain.Account;
import tech.jiyu.paradise.core.account.domain.AccountActivationProperties;
import tech.jiyu.paradise.core.account.domain.AccountListenerSupport;
import tech.jiyu.paradise.core.account.domain.ActivationConfig;
import tech.jiyu.paradise.core.account.exception.AccountAlreadyActivatedExeception;
import tech.jiyu.paradise.core.account.exception.ActivationCodeMismatchException;
import tech.jiyu.paradise.core.account.exception.NoSuchAccountException;
import tech.jiyu.paradise.core.account.service.AccountActivationService;
import tech.jiyu.paradise.core.account.service.AccountHistoryPhoneNumberService;
import tech.jiyu.paradise.core.privileges.service.AccountPrivilegeService;
import tech.jiyu.paradise.core.privileges.service.PrivilegeService;
import tech.jiyu.paradise.core.util.AccountUtils;
import tech.jiyu.utility.cache.CacheTemplate;

/**
 * @author <a href="mailto:chenshao@dekeapp.com">shawsmith</a>
 */
@Service
public class AccountActivationServiceImpl implements AccountActivationService {

    @Autowired
    private JavaMailSender javaMailSender;

    @Autowired
    private Configuration freemarkerConfiguration;

    @Autowired(required = false)
    private AccountActivationProperties activationProperties;

    @Autowired
    private AccountMapper accountMapper;

    @Autowired
    private RedisTemplate<Object, Object> redisTemplate;

    @Autowired
    private CacheTemplate cacheTemplate;

    @Autowired(required = false)
    @Lazy
    private AccountListenerSupport accountListenerSupport;

    @Autowired
    private AccountHistoryPhoneNumberService accountHistoryPhoneNumberService;

    @Autowired
    private AccountPrivilegeService accountPrivilegeService;

    @Autowired
    private PrivilegeService privilegeService;

    @Override
    public void sendActivationMail(Account account, ActivationConfig activationConfig) {
        String receiverAddress;
        if (account == null || StringUtils.isBlank(receiverAddress = account.getEmail())) {
            return;
        }

        // 为了简化配置, 只有当使用时才确认相关属性都已经正确配置.
        ensureActivationPropertiesConfigured();

        String mailContent = mergeTemplateIntoString(account, activationConfig);
        doSend(receiverAddress, mailContent);
    }

    private void ensureActivationPropertiesConfigured() {
        if (activationProperties == null) {
            throw new UnsupportedOperationException();
        }
    }

    private String mergeTemplateIntoString(Account account, ActivationConfig activationConfig) {
        StringWriter result = new StringWriter();
        try {
            Map<String, Object> model = buildMailTemplateModel(account, activationConfig);
            Template template = freemarkerConfiguration.getTemplate("activation-email-template.ftl");
            template.process(model, result);
            return result.toString();
        } catch (Exception e) {
            throw new RuntimeException(Throwables.getStackTraceAsString(e), e);
        } finally {
            IOUtils.closeQuietly(result);
        }
    }

    private Map<String, Object> buildMailTemplateModel(Account account, ActivationConfig activationConfig) {
        Map<String, Object> model = Maps.newHashMap();

        String name = account.getName();
        if (StringUtils.isBlank(name)) {
            name = account.getEmail();
        }
        model.put("userName", name);

        String activationCode = generateActivationCode(account);
        model.put("activationUrl", buildActivationUrl(account, activationConfig, activationCode));
        model.put("projectName", activationProperties.getProjectName());
        model.put("signature", activationProperties.getSignature());
        return model;
    }

    private String generateActivationCode(Account account) {
        String activationCode = RandomStringUtils.randomNumeric(4);
        redisTemplate.opsForValue().set(generateActivationCodeKey(account.getId()), activationCode, activationProperties.getCodeExpireTime(), TimeUnit.MILLISECONDS);
        return activationCode;
    }

    private String generateActivationCodeKey(Long accountId) {
        return "__account_activation_code_" + accountId;
    }

    private String buildActivationUrl(Account account, ActivationConfig activationConfig, String activationCode) {
        StringBuilder activationUrlBuilder = new StringBuilder();

        activationUrlBuilder.append(activationProperties.getActivationBaseUrl())
                .append("/accounts/")
                .append(account.getId())
                .append("/activations?code=" + activationCode)
                .append("&companyId=" + account.getCompanyId());

        String returnUrl = activationConfig.getReturnUrl();
        if (StringUtils.isNotBlank(returnUrl)) {
            activationUrlBuilder.append("&returnUrl=")
                    .append(activationConfig.getReturnUrl());
        }

        return activationUrlBuilder.toString();
    }

    private void doSend(String receiver, String content) {
        MimeMessagePreparator preparator = (mimeMessage) -> {
            MimeMessageHelper message = new MimeMessageHelper(mimeMessage);
            message.setFrom(activationProperties.getSenderAddress());
            message.setTo(receiver);
            message.setSubject(activationProperties.getMailSubject());
            message.setSentDate(new Date());
            message.setText(content, true);
        };

        javaMailSender.send(preparator);
    }

    @Override
    @Transactional
    public void activeAccount(Long accountId, String activationCode) {
        AccountPO accountPO = accountMapper.selectById(accountId);
        if (accountPO == null) {
            throw new NoSuchAccountException(String.valueOf(accountId));
        }

        if (Boolean.TRUE.equals(accountPO.getEnabled())) {
            throw new AccountAlreadyActivatedExeception(String.valueOf(accountId));
        }

        String activationCodeKey = generateActivationCodeKey(accountId);
        String existedActivationCode = (String) redisTemplate.opsForValue().get(activationCodeKey);

        if (!StringUtils.equals(existedActivationCode, activationCode)) {
            throw new ActivationCodeMismatchException();
        }


        Account oldAccount = AccountUtils.assembleAccount(accountPO,
                accountHistoryPhoneNumberService,
                accountPrivilegeService,
                privilegeService);
        if (accountListenerSupport != null) {
            accountListenerSupport.fireAccountPreModified(oldAccount, null);
        }

        accountPO.setEnabled(Boolean.TRUE);
        accountMapper.updateById(accountPO);
        AccountUtils.removeAccountCache(accountId, cacheTemplate);

        if (accountListenerSupport != null) {
            Account newAccount = AccountUtils.assembleAccount(accountPO,
                    accountHistoryPhoneNumberService,
                    accountPrivilegeService,
                    privilegeService);
            accountListenerSupport.fireAccountPostModified(newAccount,
                    oldAccount,
                    null);
        }

    }
}
