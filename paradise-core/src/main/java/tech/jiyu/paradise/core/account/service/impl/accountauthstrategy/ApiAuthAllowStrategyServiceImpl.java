package tech.jiyu.paradise.core.account.service.impl.accountauthstrategy;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import tech.jiyu.paradise.core.account.domain.ApiAccessValidationInfo;
import tech.jiyu.paradise.core.account.enumtype.ApiAuthenticationStrategy;
import tech.jiyu.paradise.core.account.service.ApiAuthenticationStrategyService;

@Component
@Slf4j
public class ApiAuthAllowStrategyServiceImpl implements ApiAuthenticationStrategyService {

    @Override
    public Boolean authenticate(ApiAccessValidationInfo validationInfo) {
        return true;
    }

    @Override
    public ApiAuthenticationStrategy getStrategy() {
        return ApiAuthenticationStrategy.ALLOW;
    }
}
