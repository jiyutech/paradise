package tech.jiyu.paradise.core.account.service;

import org.springframework.validation.annotation.Validated;

import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import tech.jiyu.paradise.core.account.domain.Residence;
import tech.jiyu.utility.cache.CacheStrategy;

/**
 * Created By ResidenceService
 *
 * Date 2017/12/27 20:08
 *
 * @author SimonChan;emailAddress: simonchan@jiyu.tech
 */
@Validated
public interface ResidenceService {

    String CACHE_PREFIX = AccountService.CACHE_PREFIX + "residences:";

    List<Residence> saveOrUpdate(@NotNull Long accountId, @NotNull @Valid List<Residence> residences);

    List<Residence> findByAccountId(Long accountId, CacheStrategy strategy);

    void deleteByAccountId(Long accountId);
}
