package tech.jiyu.paradise.core.account.domain;

import com.google.common.collect.Lists;

import com.fasterxml.jackson.databind.ObjectMapper;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;

import java.io.IOException;
import java.util.Collection;
import java.util.Map;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import tech.jiyu.paradise.core.account.service.AccountService;
import tech.jiyu.paradise.core.util.AccountUtils;
import tech.jiyu.restful.api.domain.KeywordPaginationListQuery;
import tech.jiyu.utility.web.ParameterUtils;

/**
 * @author <a href="mailto:chenshao@jiyu.tech">shawsmith</a>
 */
@NoArgsConstructor
@Getter
@Setter
public class DefaultAccountListQuery extends KeywordPaginationListQuery implements AccountListQuery {

    private Collection<String> roleTypeCodes;
    private Long creatorId;
    private Boolean audited;
    private Boolean enabled;
    private Boolean polyAdminEnabled;
    private Long companyId;
    private Collection<String> excludedRoleTypeCodes;
    private Collection<String> excludedUserNames;

    public static DefaultAccountListQuery parse(Map<String, String> parameters) {
        DefaultAccountListQuery query = new DefaultAccountListQuery();
        query.doParse(parameters,
                "registration_date",
                Boolean.TRUE);
        return query;
    }

    @Override
    public void doParse(Map<String, String> parameters,
                        String defaultSortTypeCode,
                        Boolean defaultAsc) {
        super.doParse(parameters,
                defaultSortTypeCode,
                defaultAsc);

        ObjectMapper objectMapper = new ObjectMapper();

        Collection<String> roleTypeCodes = null;
        try {
            roleTypeCodes = ParameterUtils.parseStringArrayValues(parameters.get("roleTypeCodes"), objectMapper);
        } catch (IOException ignored) {
        }

        if (CollectionUtils.isEmpty(roleTypeCodes)) {
            String roleTypeCode = AccountUtils.parseRoleTypeCode(parameters, null);
            if (StringUtils.isNotBlank(roleTypeCode)) {
                roleTypeCodes = Lists.newArrayList(roleTypeCode);
            }
        }
        setRoleTypeCodes(roleTypeCodes);

        Long creatorId = ParameterUtils.parseLongValue(parameters.get("creatorId"), null);
        setCreatorId(creatorId);

        Boolean audited = ParameterUtils.parseBooleanValue(parameters.get("audited"), null);
        setAudited(audited);

        Boolean enabled = ParameterUtils.parseBooleanValue(parameters.get("enabled"), null);
        setEnabled(enabled);

        Boolean polyAdminEnabled = ParameterUtils.parseBooleanValue(parameters.get("polyAdminEnabled"), null);
        setPolyAdminEnabled(polyAdminEnabled);

        Long companyId = ParameterUtils.parseLongValue(parameters.get("companyId"), AccountService.DEFAULT_COMPANY_ID);
        setCompanyId(companyId);

        try {
            Collection<String> excludedRoleTypeCodes = ParameterUtils.parseStringArrayValues(parameters.get("excludedRoleTypeCodes"), objectMapper);
            setExcludedRoleTypeCodes(excludedRoleTypeCodes);
        } catch (IOException ignored) {
        }

        try {
            Collection<String> excludedUserNames = ParameterUtils.parseStringArrayValues(parameters.get("excludedUserNames"), objectMapper);
            setExcludedUserNames(excludedUserNames);
        } catch (IOException ignored) {
        }
    }
}
