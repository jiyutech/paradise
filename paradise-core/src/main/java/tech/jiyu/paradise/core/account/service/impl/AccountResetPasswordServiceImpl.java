package tech.jiyu.paradise.core.account.service.impl;

import com.google.common.collect.Maps;

import freemarker.template.Configuration;

import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;

import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.regex.Pattern;

import javax.annotation.Resource;

import tech.jiyu.captcha.service.CaptchaCodeMismatchException;
import tech.jiyu.paradise.core.account.dao.mapper.AccountMapper;
import tech.jiyu.paradise.core.account.dao.po.AccountPO;
import tech.jiyu.paradise.core.account.domain.Account;
import tech.jiyu.paradise.core.account.domain.AccountListenerSupport;
import tech.jiyu.paradise.core.account.domain.AccountResetPasswordProperties;
import tech.jiyu.paradise.core.account.domain.MailPasswordResetInfo;
import tech.jiyu.paradise.core.account.exception.AccountPasswordMismatchException;
import tech.jiyu.paradise.core.account.exception.NoSuchAccountException;
import tech.jiyu.paradise.core.account.exception.PasswordFormatException;
import tech.jiyu.paradise.core.account.service.AccountHistoryPhoneNumberService;
import tech.jiyu.paradise.core.account.service.AccountResetPasswordService;
import tech.jiyu.paradise.core.account.service.AccountService;
import tech.jiyu.paradise.core.login.service.LoginService;
import tech.jiyu.paradise.core.privileges.service.AccountPrivilegeService;
import tech.jiyu.paradise.core.privileges.service.PrivilegeService;
import tech.jiyu.paradise.core.util.AccountUtils;
import tech.jiyu.paradise.core.util.LoginUtils;
import tech.jiyu.paradise.core.util.MailUtils;
import tech.jiyu.utility.cache.CacheTemplate;

/**
 * Created By AccountResetPasswordServiceImpl
 *
 * @author Xhh52
 */
@Service
public class AccountResetPasswordServiceImpl implements AccountResetPasswordService {

    @Autowired(required = false)
    private AccountResetPasswordProperties accountResetPasswordProperties;

    @Autowired
    private JavaMailSender javaMailSender;

    @Autowired
    private Configuration freemarkerConfiguration;

    @Resource(name = "stringRedisTemplate",
            type = RedisTemplate.class)
    private RedisTemplate<String, String> resetPasswordCodeTemplate;

    @Autowired
    private LoginService loginService;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private AccountMapper accountMapper;

    @Autowired
    private AccountService accountService;

    @Autowired(required = false)
    private AccountListenerSupport accountListenerSupport;

    @Autowired
    private CacheTemplate cacheTemplate;

    @Autowired
    private AccountHistoryPhoneNumberService accountHistoryPhoneNumberService;

    @Autowired
    private AccountPrivilegeService accountPrivilegeService;

    @Autowired
    private PrivilegeService privilegeService;

    @Value("${paradise.account.password.regx:.*}")
    private String passwordRegx;

    @Value("${paradise.account.password.logout.on.changed:true}")
    private Boolean logout;

    @Override
    public void sendCaptchaCodeToEmail(Account account) {
        ensureResetPasswordPropertiesConfigured();
        MailUtils mailUtils = MailUtils.builder().buildJavaMailSender(javaMailSender)
                .setFreemarkerConfiguration(freemarkerConfiguration)
                .buildMailProperties(accountResetPasswordProperties)
                .buildMailTemplateModel(buildMailTemplateModel(account))
                .buildTo(account.getEmail())
                .build();
        String mailContent = mailUtils.mergeTemplateIntoString("reset-password-email-template.ftl");
        mailUtils.setMailContent(mailContent);
        mailUtils.sendMail();
    }

    private void ensureResetPasswordPropertiesConfigured() {
        if (accountResetPasswordProperties == null) {
            throw new UnsupportedOperationException();
        }
    }

    @Override
    @Transactional
    @Validated
    public Account resetPasswordFromEmail(Long operatorId, MailPasswordResetInfo mailPasswordResetInfo) {
        Long companyId = mailPasswordResetInfo.getCompanyId();
        String mailAddress = mailPasswordResetInfo.getMailAddress();
        AccountPO existed = accountMapper.selectByEmail(companyId, mailAddress);
        if (existed == null) {
            throw new NoSuchAccountException("mailAddress" + mailAddress);
        }

        // 判断新密码是否一致
        String newPassword = mailPasswordResetInfo.getNewPassword();
        if (!StringUtils.equals(newPassword, mailPasswordResetInfo.getConfirmedNewPassword())) {
            throw new AccountPasswordMismatchException();
        }

        // 判断验证码是否一致
        else if (!checkCaptchaCode(mailAddress, mailPasswordResetInfo.getCode())) {
            throw new CaptchaCodeMismatchException();
        }

        Account oldAccount = AccountUtils.assembleAccount(existed,
                accountHistoryPhoneNumberService,
                accountPrivilegeService,
                privilegeService);
        if (accountListenerSupport != null) {
            accountListenerSupport.fireAccountPreModified(oldAccount, null);
        }

        checkPasswordFormat(newPassword);
        Long accountId = existed.getId();
        existed.setPassword(passwordEncoder.encode(newPassword));
        accountMapper.updateById(existed);
        AccountUtils.removeAccountCache(accountId, cacheTemplate);
        if (Boolean.TRUE.equals(logout)) {
            LoginUtils.logout(String.valueOf(accountId), loginService);
        }

        if (accountListenerSupport != null) {
            Account account = AccountUtils.assembleAccount(existed,
                    accountHistoryPhoneNumberService,
                    accountPrivilegeService,
                    privilegeService);
            accountListenerSupport.fireAccountPostModified(account, oldAccount, null);
        }

        return accountService.findById(accountId);
    }

    private boolean checkCaptchaCode(String key, String code) {
        String codeKey = generateResetPasswordCodeKey(key);
        String codeValue = resetPasswordCodeTemplate.opsForValue().get(codeKey);
        return StringUtils.equals(code, codeValue);
    }


    private Map<String, Object> buildMailTemplateModel(Account account) {
        Map<String, Object> templateModel = Maps.newHashMap();

        String userName = account.getUsername();
        if (StringUtils.isBlank(userName)) {
            userName = account.getEmail();
        }

        templateModel.put("userName", userName);
        templateModel.put("captchaCode", generateResetPasswordCode(account));
        templateModel.put("signature", accountResetPasswordProperties.getSignature());

        return templateModel;
    }

    private String generateResetPasswordCode(Account account) {
        String code = RandomStringUtils.randomNumeric(6);
        resetPasswordCodeTemplate.opsForValue().set(generateResetPasswordCodeKey(account.getEmail()), code, accountResetPasswordProperties.getCodeExpireTime(), TimeUnit.MILLISECONDS);
        return code;
    }

    private String generateResetPasswordCodeKey(String key) {
        return key + "_reset_password";
    }

    private void checkPasswordFormat(String password) {
        if (password != null) {
            if (!Pattern.matches(passwordRegx, password)) {
                throw new PasswordFormatException();
            }
        }
    }
}
