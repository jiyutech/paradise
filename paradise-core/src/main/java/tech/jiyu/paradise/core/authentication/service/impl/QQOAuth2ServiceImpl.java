package tech.jiyu.paradise.core.authentication.service.impl;

import com.fasterxml.jackson.core.TreeNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ValueNode;
import com.google.common.collect.Maps;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.io.Charsets;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.HttpEntity;
import org.apache.http.HttpStatus;
import org.apache.http.NameValuePair;
import org.apache.http.StatusLine;
import org.apache.http.client.HttpClient;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.util.EntityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tech.jiyu.paradise.core.account.domain.Account;
import tech.jiyu.paradise.core.account.service.AccountService;
import tech.jiyu.paradise.core.authentication.domain.*;
import tech.jiyu.paradise.core.authentication.exception.CannotAcquireOpenIdException;
import tech.jiyu.paradise.core.authentication.exception.NoProvidePhoneNumberException;
import tech.jiyu.paradise.core.authentication.exception.NoSuchUserInfoException;
import tech.jiyu.paradise.core.authentication.exception.OAuth2KeyExpiredException;
import tech.jiyu.paradise.core.authentication.service.OAuth2AuthenticationService;
import tech.jiyu.paradise.core.authentication.service.OAuth2Service;
import tech.jiyu.parrot.geography.domain.Region;
import tech.jiyu.parrot.geography.service.RegionService;
import tech.jiyu.parrot.preferences.service.PreferenceService;
import tech.jiyu.penguin.wxapi.wechat.common.domain.UserInfo;
import tech.jiyu.penguin.wxapi.wechat.common.exception.CannotAcquireAccessTokenException;
import tech.jiyu.penguin.wxapi.wechat.oauth2.domain.OAuth2AccessToken;
import tech.jiyu.photo.api.PhotoService;
import tech.jiyu.utility.cache.CacheStrategy;
import tech.jiyu.utility.cache.CacheTemplate;
import tech.jiyu.utility.exception.ExceptionNotificationService;

import java.io.IOException;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * @author <a href="mailto:chenshao@dekeapp.com">shawsmith</a>
 */
@Service
public class QQOAuth2ServiceImpl implements OAuth2Service {

    private static final String APP_ID_KEY = "qqOAuth2.appId";
    private static final String APP_SECRET_KEY = "qqOAuth2.appSecret";
    private static final String REDIRECT_URL_KEY = "qqOAuth2.redirectUrl";

    private static final String GET_ACCESS_TOKEN_URL_PATTERN = "https://graph.qq.com/oauth2.0/token?grant_type=authorization_code&client_id=%s&client_secret=%s&code=%s&redirect_uri=%s";

    // 如果需要额外获取 union ID, 需要写邮件向腾讯客服申请对应的权限,
    // 具体详情可以参考以下链接: http://wiki.connect.qq.com/unionid介绍
    // 如果需要unionId, 则在尾部添加"&unionid=1"
    private static final String GET_OPEN_ID_URL_PATTERN = "https://graph.qq.com/oauth2.0/me?access_token=%s";

    private static final String GET_USER_INFO_URL_PATTERN = "https://graph.qq.com/user/get_user_info?access_token=%s&oauth_consumer_key=%s&openid=%s";

    private static final String OPEN_ID_KEY = "open_id";
    private static final String UNION_ID_KEY = "union_id";

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private AccountService accountService;

    @Autowired
    private OAuth2AuthenticationService oAuth2AuthenticationService;

    @Autowired
    private PhotoService photoService;

    @Autowired
    private HttpClient httpClient;

    @Autowired
    private RegionService regionService;

    @Autowired
    private PreferenceService preferenceService;

    @Autowired
    private ExceptionNotificationService exceptionNotificationService;

    @Autowired
    private CacheTemplate cacheTemplate;

    @Value("${paradise.use.union.id:false}")
    private Boolean useUnionId;

    @Override
    public boolean supports(OAuth2Type type) {
        return OAuth2Type.QQ == type;
    }

    @Override
    @Transactional
    public Account createOrUpdate(OAuth2Parameters parameters) throws IOException {

        String authCode = parameters.getAuthCode();
        OAuth2AccessToken accessToken = getAccessToken(authCode);

        UserInfo userInfo = getRequireUserInfo(authCode, accessToken);

        // 3. 如果是新账户则注册，否则就更新账户的微信信息.
        Account account = registerAccountIfNecessary(parameters.getCompanyId(), userInfo, parameters.getRoleTypeCode(), null, parameters.getLoginSource(), parameters.getPrivilegeIds());

        // 3. 创建或者更新OAuth2信息.
        OAuth2Type type = OAuth2Type.fromCode(parameters.getAuthType());
        OAuth2Authentication authentication = buildOAuth2Authentication(account.getId(), accessToken, type);
        oAuth2AuthenticationService.saveOrUpdate(authentication);

        return account;
    }

    private UserInfo getRequireUserInfo(String authCode, OAuth2AccessToken accessToken) throws IOException {
        if (accessToken == null) {
            throw new CannotAcquireAccessTokenException(authCode);
        }

        Map<String, String> openIdAndUnionIdMap = getOpenIdAndUnionId(accessToken);
        String openId = openIdAndUnionIdMap.get(OPEN_ID_KEY);
        if (openId == null) {
            throw new CannotAcquireOpenIdException(accessToken.getAccessToken());
        }

        UserInfo userInfo = getUserInfo(accessToken, openIdAndUnionIdMap);
        if (userInfo == null) {
            throw new NoSuchUserInfoException();
        }
        return userInfo;
    }

    @Override
    public Account createOrUpdate(OAuth2KeyParameters keyParameters) {
        String authKey = keyParameters.getAuthKey();
        OAuth2KeyAuthentication keyAuthentication = Optional.ofNullable(loadKeyAuthenticationFromCache(authKey)).orElseThrow(OAuth2KeyExpiredException::new);
        checkPhoneNumberIfNecessary(keyParameters, keyAuthentication);

        UserInfo userInfo = (UserInfo) keyAuthentication.getOauthUserInfo();
        Account account = registerAccountIfNecessary(keyParameters.getCompanyId(), userInfo, keyParameters.getRoleTypeCode(), keyParameters.getIsoCode(), keyParameters.getLoginSource(), keyParameters.getPrivilegeIds());

        OAuth2Type oAuth2Type = OAuth2Type.fromCode(keyParameters.getAuthType());
        OAuth2Authentication oAuth2Authentication = buildOAuth2Authentication(account.getId(), keyAuthentication.getAccessToken(), oAuth2Type);
        oAuth2AuthenticationService.saveOrUpdate(oAuth2Authentication);

        deleteKeyAuthenticationFromCache(authKey);

        return account;
    }

    private void checkPhoneNumberIfNecessary(OAuth2KeyParameters keyParameters, OAuth2KeyAuthentication keyAuthentication) {
        if (keyAuthentication.getNeedPhoneNumber() && StringUtils.isBlank(keyParameters.getPhoneNumber())) {
            throw new NoProvidePhoneNumberException(keyParameters.getAuthKey());
        }
    }

    @Override
    public OAuth2KeyAuthentication createOAuth2KeyAuthentication(OAuth2Parameters parameters) throws IOException {
        String authCode = parameters.getAuthCode();
        OAuth2AccessToken accessToken = getAccessToken(authCode);

        UserInfo userInfo = getRequireUserInfo(authCode, accessToken);
        OAuth2KeyAuthentication oAuth2KeyAuthentication = buildOAuth2KeyAuthentication(parameters.getCompanyId(), userInfo, accessToken);
        storeKeyAuthenticationToCache(oAuth2KeyAuthentication);
        return oAuth2KeyAuthentication;
    }

    private void storeKeyAuthenticationToCache(OAuth2KeyAuthentication oAuth2KeyAuthentication) {
        UserInfo userInfo = (UserInfo) oAuth2KeyAuthentication.getOauthUserInfo();

        String toBeStoredId;
        if (useUnionId) {
            toBeStoredId = userInfo.getUnionId();
        } else {
            toBeStoredId = userInfo.getOpenId();
        }

        String cacheKeySuffix = String.valueOf(System.currentTimeMillis());
        String cacheKey = OAuth2Service.OAUTH2_KEY_CACHE_PREFIX.concat(toBeStoredId).concat(cacheKeySuffix);
        oAuth2KeyAuthentication.setCacheKey(cacheKey);

        cacheTemplate.set(cacheKey, oAuth2KeyAuthentication);
    }

    private OAuth2KeyAuthentication loadKeyAuthenticationFromCache(String authKey) {
        return cacheTemplate.get(authKey, OAuth2KeyAuthentication.class);
    }

    private void deleteKeyAuthenticationFromCache(String authKey) {
        cacheTemplate.delete(authKey);
    }

    private OAuth2KeyAuthentication buildOAuth2KeyAuthentication(Long companyId, UserInfo userInfo, OAuth2AccessToken accessToken) {
        OAuth2KeyAuthentication oAuth2KeyAuthentication = new OAuth2KeyAuthentication();

        Account account;
        if (useUnionId) {
            account = accountService.findByUnionId(companyId, userInfo.getUnionId());
        } else {
            account = accountService.findByQQOpenId(companyId, userInfo.getOpenId());
        }

        oAuth2KeyAuthentication.setAccessToken(accessToken);
        oAuth2KeyAuthentication.setOauthUserInfo(userInfo);
        if (account == null || StringUtils.isBlank(account.getPhoneNumber())) {
            oAuth2KeyAuthentication.setNeedPhoneNumber(Boolean.TRUE);
        } else {
            oAuth2KeyAuthentication.setNeedPhoneNumber(Boolean.FALSE);
        }

        return oAuth2KeyAuthentication;
    }

    private OAuth2AccessToken getAccessToken(String authCode) throws IOException {
        HttpGet request = buildFetchAccessTokenRequest(authCode);
        try {
            ResponseHandler<OAuth2AccessToken> accessTokenResponseHandler = extractAccessTokenResponseHandler();
            return httpClient.execute(request, accessTokenResponseHandler);
        } finally {
            if (request != null) {
                request.releaseConnection();
            }
        }
    }

    private HttpGet buildFetchAccessTokenRequest(String authCode) {
        String appId = preferenceService.findRequiredPreferenceValue(APP_ID_KEY);
        String appSecret = preferenceService.findRequiredPreferenceValue(APP_SECRET_KEY);
        String redirectUrl = preferenceService.findRequiredPreferenceValue(REDIRECT_URL_KEY);
        String url = String.format(GET_ACCESS_TOKEN_URL_PATTERN, appId, appSecret, authCode, redirectUrl);
        return new HttpGet(url);
    }

    private ResponseHandler<OAuth2AccessToken> extractAccessTokenResponseHandler() {
        return (response) -> {
            if (response == null || response.getStatusLine() == null) {
                return null;
            }

            StatusLine statusLine = response.getStatusLine();
            if (statusLine.getStatusCode() != HttpStatus.SC_OK) {
                return null;
            }

            HttpEntity entity = response.getEntity();
            if (entity == null) {
                return null;
            }

            try {
                String entityContent = EntityUtils.toString(entity);
                if (StringUtils.isBlank(entityContent)) {
                    return null;
                }

                List<NameValuePair> parameters = URLEncodedUtils.parse(entityContent, Charsets.UTF_8);
                if (CollectionUtils.isEmpty(parameters)) {
                    return null;
                }

                Map<String, List<NameValuePair>> parameterGroups = parameters.stream()
                        .collect(Collectors.groupingBy(NameValuePair::getName));


                String accessTokenValue = null;
                List<NameValuePair> accessTokenParameters = parameterGroups.get("access_token");
                if (CollectionUtils.isNotEmpty(accessTokenParameters)) {
                    accessTokenValue = accessTokenParameters.get(0).getValue();
                }

                Long expiresIn = null;
                List<NameValuePair> expiresInParameters = parameterGroups.get("expires_in");
                if (CollectionUtils.isNotEmpty(expiresInParameters)) {
                    expiresIn = Long.parseLong(expiresInParameters.get(0).getValue());
                }

                String refreshToken = null;
                List<NameValuePair> refreshTokenParameters = parameterGroups.get("refresh_token");
                if (CollectionUtils.isNotEmpty(refreshTokenParameters)) {
                    refreshToken = refreshTokenParameters.get(0).getValue();
                }

                if (accessTokenValue == null || refreshToken == null) {
                    return null;
                }

                OAuth2AccessToken accessToken = new OAuth2AccessToken();
                accessToken.setAccessToken(accessTokenValue);
                accessToken.setExpiresIn(expiresIn);
                accessToken.setRefreshToken(refreshToken);
                return accessToken;
            } finally {
                EntityUtils.consumeQuietly(entity);
            }
        };
    }

    private Map<String, String> getOpenIdAndUnionId(OAuth2AccessToken accessToken) throws IOException {
        HttpGet request = buildFetchOpenIdAndOpenIdRequest(accessToken);
        try {
            ResponseHandler<Map<String, String>> openIdAndUionIdResponseHandler = extractOpenIdAndUnionIdResponseHandler();
            return httpClient.execute(request, openIdAndUionIdResponseHandler);
        } finally {
            if (request != null) {
                request.releaseConnection();
            }
        }
    }

    private HttpGet buildFetchOpenIdAndOpenIdRequest(OAuth2AccessToken accessToken) {
        String url = String.format(GET_OPEN_ID_URL_PATTERN, accessToken.getAccessToken());
        return new HttpGet(url);
    }

    private ResponseHandler<Map<String, String>> extractOpenIdAndUnionIdResponseHandler() {
        return (response) -> {
            if (response == null || response.getStatusLine() == null) {
                return Maps.newHashMap();
            }

            StatusLine statusLine = response.getStatusLine();
            if (statusLine.getStatusCode() != HttpStatus.SC_OK) {
                return Maps.newHashMap();
            }

            HttpEntity entity = response.getEntity();
            if (entity == null) {
                return Maps.newHashMap();
            }

            try {
                String entityContent = EntityUtils.toString(entity);
                int startObjectTagIndex = entityContent.indexOf('{');
                int endObjectTagIndex = entityContent.indexOf('}');
                if (startObjectTagIndex < 0 || endObjectTagIndex < 0 || startObjectTagIndex > endObjectTagIndex) {
                    return Maps.newHashMap();
                }

                String jsonContent = entityContent.substring(startObjectTagIndex, endObjectTagIndex + 1);
                TreeNode rootNode = objectMapper.readTree(jsonContent);

                Map<String, String> openIdAndUnionIdMap = Maps.newHashMap();
                ValueNode openIdNode = (ValueNode) rootNode.get("openid");
                if (openIdNode != null) {
                    openIdAndUnionIdMap.put(OPEN_ID_KEY, openIdNode.asText());
                }

                ValueNode unionIdNode = (ValueNode) rootNode.get("unioid");
                if (unionIdNode != null) {
                    openIdAndUnionIdMap.put(UNION_ID_KEY, unionIdNode.asText());
                }
                return openIdAndUnionIdMap;
            } finally {
                EntityUtils.consumeQuietly(entity);
            }
        };
    }

    private UserInfo getUserInfo(OAuth2AccessToken accessToken, Map<String, String> openIdAndUnionIdMap) throws IOException {
        String appId = preferenceService.findRequiredPreferenceValue(APP_ID_KEY);

        String openId = openIdAndUnionIdMap.get(OPEN_ID_KEY);
        HttpGet request = buildFetchUserInfoRequest(accessToken, appId, openId);
        try {
            String unionId = openIdAndUnionIdMap.get(UNION_ID_KEY);
            ResponseHandler<UserInfo> userInfoResponseHandler = extractUserInfoResponseHandler(openId, unionId);
            return httpClient.execute(request, userInfoResponseHandler);
        } finally {
            if (request != null) {
                request.releaseConnection();
            }
        }
    }

    private HttpGet buildFetchUserInfoRequest(OAuth2AccessToken accessToken, String appId, String openId) {
        String url = String.format(GET_USER_INFO_URL_PATTERN, accessToken.getAccessToken(), appId, openId);
        return new HttpGet(url);
    }

    private ResponseHandler<UserInfo> extractUserInfoResponseHandler(String openId, String unionId) {
        return (response) -> {
            if (response == null || response.getStatusLine() == null) {
                return null;
            }

            StatusLine statusLine = response.getStatusLine();
            if (statusLine.getStatusCode() != HttpStatus.SC_OK) {
                return null;
            }

            HttpEntity entity = response.getEntity();
            if (entity == null) {
                return null;
            }

            try {
                String entityContent = EntityUtils.toString(entity);
                TreeNode rootNode = objectMapper.readTree(entityContent);

                ValueNode returnCodeNode = (ValueNode) rootNode.get("ret");
                if (returnCodeNode == null || returnCodeNode.asInt() != 0) {
                    return null;
                }

                UserInfo userInfo = new UserInfo();
                ValueNode nickNameNode = (ValueNode) rootNode.get("nickname");
                if (nickNameNode != null) {
                    userInfo.setNickName(nickNameNode.asText());
                }

                ValueNode genderNode = (ValueNode) rootNode.get("gender");
                if (genderNode != null) {
                    userInfo.setGender(genderNode.asText());
                }

                ValueNode provinceNode = (ValueNode) rootNode.get("province");
                if (provinceNode != null) {
                    String province = provinceNode.asText();
                    if (StringUtils.isNotBlank(province)) {
                        province += "省";
                        userInfo.setProvince(province);
                    }
                }

                ValueNode cityNode = (ValueNode) rootNode.get("city");
                if (cityNode != null) {
                    String city = cityNode.asText();
                    if (StringUtils.isNotBlank(city)) {
                        city += "市";
                        userInfo.setCity(city);
                    }
                }

                ValueNode avatarNode = (ValueNode) rootNode.get("figureurl_qq_2");
                if (avatarNode == null) {
                    avatarNode = (ValueNode) rootNode.get("figureurl_qq_1");
                }

                if (avatarNode != null) {
                    userInfo.setImageUrl(avatarNode.asText());
                }

                userInfo.setOpenId(openId);
                userInfo.setUnionId(unionId);

                return userInfo;
            } finally {
                EntityUtils.consumeQuietly(entity);
            }
        };
    }

    private Account registerAccountIfNecessary(Long companyId, UserInfo userInfo, String roleTypeCode, String isoCode, String loginSource, Collection<Long> privilegeIds) {
        String openId = userInfo.getOpenId();

        Account account;
        if (useUnionId) {
            account = accountService.findByUnionId(companyId, userInfo.getUnionId());
        } else {
            account = accountService.findByQQOpenId(companyId, openId);
        }

        if (account == null) {
            account = accountService.register(buildAccount(companyId, userInfo, roleTypeCode, isoCode, loginSource, privilegeIds), null, null);
        } else {
            updateAccountProperties(userInfo, privilegeIds, account);
            account = accountService.patch(account);
        }
        return account;
    }

    private Account buildAccount(Long companyId, UserInfo userInfo, String roleTypeCode, String isoCode, String loginSource, Collection<Long> privilegeIds) {
        Account account = new Account();

        account.setName(userInfo.getNickName());
        account.setRegistrationDate(System.currentTimeMillis());
        account.setRegistrationSource(loginSource);
        account.setQqOpenId(userInfo.getOpenId());
        account.setUnionId(userInfo.getUnionId());
        account.setGender(userInfo.getGender());
        account.setCompanyId(companyId);
        account.setIsoCode(isoCode);

        Region province = regionService.findProvince(userInfo.getProvince(), CacheStrategy.CACHE_PREFER);
        if (province != null) {
            account.setProvinceId(province.getId());
            Region city = regionService.findCity(userInfo.getCity(), province.getId(), CacheStrategy.CACHE_PREFER);
            if (city != null) {
                account.setCityId(city.getId());
            }
        }

        try {
            String avatar = photoService.upload(userInfo.getImageUrl());
            account.setAvatar(avatar);
        } catch (Exception e) {
            exceptionNotificationService.notify(e);
        }

        account.setRoleTypeCode(roleTypeCode);
        account.setPrivilegeIds(privilegeIds);
        account.setEnabled(Boolean.TRUE);
        account.setVerified(Boolean.FALSE);
        account.setAudited(Boolean.FALSE);
        account.setWorkDayTimeEnabled(Boolean.FALSE);
        account.setPolyAdminEnabled(Boolean.TRUE);
        return account;
    }

    private void updateAccountProperties(UserInfo userInfo, Collection<Long> privilegeIds, Account account) {
        account.setQqOpenId(userInfo.getOpenId());
        account.setUnionId(userInfo.getUnionId());
        account.setName(userInfo.getNickName());
        account.setGender(userInfo.getGender());

        Region province = regionService.findProvince(userInfo.getProvince(), CacheStrategy.CACHE_PREFER);
        if (province != null) {
            account.setProvinceId(province.getId());

            Region city = regionService.findCity(userInfo.getCity(), province.getId(), CacheStrategy.CACHE_PREFER);
            if (city != null) {
                account.setCityId(city.getId());
            }
        }

        try {
            String avatar = photoService.upload(userInfo.getImageUrl());
            account.setAvatar(avatar);
        } catch (Exception e) {
            exceptionNotificationService.notify(e);
        }
        account.setPrivilegeIds(privilegeIds);
    }

    private OAuth2Authentication buildOAuth2Authentication(Long accountId, OAuth2AccessToken accessToken, OAuth2Type authType) {
        OAuth2Authentication authentication = new OAuth2Authentication();
        authentication.setAccountId(accountId);
        authentication.setType(authType);
        authentication.setAccessToken(accessToken.getAccessToken());
        authentication.setRefreshToken(accessToken.getRefreshToken());
        authentication.setExpiresIn(accessToken.getExpiresIn());
        authentication.setScope(accessToken.getScope());
        return authentication;
    }

}
