package tech.jiyu.paradise.core.account.domain;

import org.apache.commons.collections.CollectionUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author <a href="mailto:chenshao@dekeapp.com">shawsmith</a>
 */
public class AccountListenerSupport {

    private List<AccountListener> listeners = new ArrayList<>();

    public void addListener(AccountListener listener) {
        if (listener != null) {
            listeners.add(listener);
        }
    }

    public void removeListener(AccountListener listener) {
        listeners.remove(listener);
    }


    public void fireAccountPreCreated(Account account, Map<String, Object> additionalAttributes) {
        AccountEvent event = new AccountPreCreatedEvent(account, additionalAttributes);
        publishEvent(event);
    }

    public void fireAccountPostCreated(Account account, Map<String, Object> additionalAttributes, ActivationConfig activationConfig, Invitation invitation) {
        AccountEvent event = new AccountPostCreatedEvent(account, additionalAttributes, activationConfig, invitation);
        publishEvent(event);
    }

    public void fireAccountPreModified(Account account, Map<String, Object> additionalAttributes) {
        AccountEvent event = new AccountPrePatchedEvent(account, additionalAttributes);
        publishEvent(event);
    }

    public void fireAccountPostModified(Account account, Account oldAccount, Map<String, Object> additionalAttributes) {
        AccountEvent event = new AccountPostPatchedEvent(account, oldAccount, additionalAttributes);
        publishEvent(event);
    }

    public void fireAccountPreDeleted(Long accountId) {
        AccountPreDeletedEvent event = new AccountPreDeletedEvent(accountId);
        publishEvent(event);
    }

    public void fireAccountPostDeleted(Long accountId) {
        AccountPostDeletedEvent event = new AccountPostDeletedEvent(accountId);
        publishEvent(event);
    }

    public void fireAccountPhoneNumberChange(Long accountId, String oldCountryCode, String oldPhoneNumber, String newCountryCode, String newPhoneNumber) {
        AccountPhoneNumberChangedEvent event = new AccountPhoneNumberChangedEvent(accountId, oldCountryCode, oldPhoneNumber, newCountryCode, newPhoneNumber);
        publishEvent(event);
    }

    public void publishEvent(AccountEvent event) {
        if (event != null && CollectionUtils.isNotEmpty(listeners)) {
            for (AccountListener listener : listeners) {
                listener.onEvent(event);
            }
        }
    }


}
