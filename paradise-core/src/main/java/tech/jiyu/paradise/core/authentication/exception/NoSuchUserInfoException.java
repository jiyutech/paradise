package tech.jiyu.paradise.core.authentication.exception;

import tech.jiyu.utility.api.errors.annotations.ApiErrorKey;

/**
 * @author <a href="mailto:chenshao@dekeapp.com">shawsmith</a>
 */
@ApiErrorKey(group = "oauth2",
             id = "key.expired")
public class NoSuchUserInfoException extends RuntimeException {

    public NoSuchUserInfoException() {
    }

    public NoSuchUserInfoException(String message) {
        super(message);
    }

    public NoSuchUserInfoException(String message, Throwable cause) {
        super(message, cause);
    }

    public NoSuchUserInfoException(Throwable cause) {
        super(cause);
    }
}
