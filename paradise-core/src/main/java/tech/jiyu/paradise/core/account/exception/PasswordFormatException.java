package tech.jiyu.paradise.core.account.exception;

import tech.jiyu.utility.api.errors.annotations.ApiErrorKey;

/**
 * @author Simon [simonchan@jinjie.tech]
 */
@ApiErrorKey(group = "account", id = "password.format")
public class PasswordFormatException extends RuntimeException {

    public PasswordFormatException() {
        super();
    }

    public PasswordFormatException(String message) {
        super(message);
    }

    public PasswordFormatException(String message, Throwable cause) {
        super(message, cause);
    }

    public PasswordFormatException(Throwable cause) {
        super(cause);
    }

    protected PasswordFormatException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
