package tech.jiyu.paradise.core.account.exception;

import tech.jiyu.utility.api.errors.annotations.ApiErrorKey;

/**
 * @author <a href="mailto:chenshao@dekeapp.com">shawsmith</a>
 */
@ApiErrorKey(group = "account",
             id = "unsupported.authentication")
public class UnsupportedAuthenticationException extends RuntimeException {

    public UnsupportedAuthenticationException() {
        super();
    }

    public UnsupportedAuthenticationException(String message) {
        super(message);
    }

    public UnsupportedAuthenticationException(String message, Throwable cause) {
        super(message, cause);
    }

    public UnsupportedAuthenticationException(Throwable cause) {
        super(cause);
    }
}
