package tech.jiyu.paradise.core.privileges.dao.po;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import tech.jiyu.paradise.core.privileges.domain.Privilege;

/**
 * @author <a href="mailto:chenshao@jinjie.tech">shawsmith</a>
 */
@NoArgsConstructor
@Getter
@Setter
public class PrivilegePO {

    private Long id;
    private Long createdDate;
    private Long modifiedDate;
    private String name;
    private String description;
    private Long orderNumber;

    public static PrivilegePO create(Long id, Privilege privilege) {
        PrivilegePO privilegePO = new PrivilegePO();

        privilegePO.setId(id);

        Long now = System.currentTimeMillis();
        privilegePO.setCreatedDate(now);
        privilegePO.setModifiedDate(now);

        if (privilege != null) {
            privilegePO.setName(privilege.getName());
            privilegePO.setDescription(privilege.getDescription());
            privilegePO.setOrderNumber(privilege.getOrderNumber());
        }

        return privilegePO;
    }
}
