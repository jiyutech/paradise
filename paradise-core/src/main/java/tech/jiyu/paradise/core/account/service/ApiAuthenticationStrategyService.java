package tech.jiyu.paradise.core.account.service;

import tech.jiyu.paradise.core.account.domain.ApiAccessValidationInfo;
import tech.jiyu.paradise.core.account.enumtype.ApiAuthenticationStrategy;

public interface ApiAuthenticationStrategyService {

    Boolean authenticate(ApiAccessValidationInfo validationInfo);

    ApiAuthenticationStrategy getStrategy();
}
