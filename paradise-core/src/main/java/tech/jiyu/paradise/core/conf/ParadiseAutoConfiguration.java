package tech.jiyu.paradise.core.conf;

import org.springframework.context.annotation.ComponentScan;

/**
 * @author <a href="mailto:chenshao@jinjie.tech">shawsmith</a>
 */
@ComponentScan(basePackages = {"tech.jiyu.paradise"})
public class ParadiseAutoConfiguration {
}
