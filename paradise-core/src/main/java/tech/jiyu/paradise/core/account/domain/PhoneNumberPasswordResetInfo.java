package tech.jiyu.paradise.core.account.domain;


import javax.validation.constraints.NotEmpty;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import tech.jiyu.captcha.domain.CaptchaCodeKey;
import tech.jiyu.utility.phonenumber.PhoneNumberUtils;

/**
 * @author <a href="mailto:chenshao@jiyu.tech">shawsmith</a>
 */
@NoArgsConstructor
@Getter
@Setter
public class PhoneNumberPasswordResetInfo {

    private Long companyId;

    private String countryCode;

    @NotEmpty(message = "{account.phone.number.reset.password.phone.number}")
    private String phoneNumber;

    @NotEmpty(message = "{account.phone.number.reset.password.captcha.code}")
    private String captchaCode;

    @NotEmpty(message = "{account.phone.number.reset.password.new.password}")
    private String newPassword;

    @NotEmpty(message = "{account.phone.number.reset.password.confirmed.new.password}")
    private String confirmedNewPassword;

    public CaptchaCodeKey buildCaptchaCodeKey() {
        return new CaptchaCodeKey(companyId, PhoneNumberUtils.filterNonDigitCharacters(getCountryCode()), PhoneNumberUtils.filterNonDigitCharacters(getPhoneNumber()));
    }
}
