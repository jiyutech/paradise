package tech.jiyu.paradise.core.account.service.impl;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tech.jiyu.paradise.core.account.dao.mapper.WorkHistoryMapper;
import tech.jiyu.paradise.core.account.dao.po.WorkHistoryPO;
import tech.jiyu.paradise.core.account.domain.WorkHistory;
import tech.jiyu.paradise.core.account.service.WorkHistoryService;
import tech.jiyu.sequences.SequenceService;
import tech.jiyu.utility.cache.CacheCallback;
import tech.jiyu.utility.cache.CacheStrategy;
import tech.jiyu.utility.cache.CacheTemplate;
import tech.jiyu.utility.cache.CacheUtils;

import java.util.*;
import java.util.stream.Collectors;

/**
 * Created By WorkHistoryServiceImpl
 *
 * Date 2017/12/27 20:16
 *
 * @author SimonChan;emailAddress: simonchan@jiyu.tech
 */
@Service
public class WorkHistoryServiceImpl implements WorkHistoryService {

    @Autowired
    private WorkHistoryMapper workHistoryMapper;

    @Autowired
    private SequenceService sequenceService;

    @Autowired
    private CacheTemplate cacheTemplate;

    @Override
    @Transactional
    public List<WorkHistory> saveOrUpdate(Long accountId, List<WorkHistory> histories) {
        if (CollectionUtils.isEmpty(histories)) {
            deleteByAccountId(accountId);
            return Lists.newArrayListWithCapacity(0);
        }
        workHistoryMapper.deleteByAccountId(accountId);
        int order = 0;
        List<WorkHistoryPO> newWorkHistoryPOs = Lists.newArrayList();
        for (WorkHistory workHistory : histories) {
            workHistory.setAccountId(accountId);
            WorkHistoryPO workHistoryPO = WorkHistoryPO.create(sequenceService.nextLongValue(), workHistory);
            workHistoryPO.setOrderNum(order++);
            newWorkHistoryPOs.add(workHistoryPO);
        }
        workHistoryMapper.batchSave(newWorkHistoryPOs);
        removeCache(accountId);
        return findByAccountId(accountId,CacheStrategy.NO_CACHE);
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<WorkHistory> findByAccountId(Long accountId, CacheStrategy strategy) {
        String workHistoryCacheKey = buildCacheKey(accountId);
        Collection<Object> cacheKeys = Collections.singletonList(workHistoryCacheKey);
        List<List<WorkHistory>> workHistoryGroups = cacheTemplate.load(cacheKeys, strategy, new CacheCallback() {
            @Override
            public Map<Object, Object> getKeyValuePairs(Collection<Object> objects) {
                if (CollectionUtils.isEmpty(objects)) {
                    return Maps.newHashMap();
                }

                List<WorkHistory> workHistories = (List<WorkHistory>) objects.iterator().next();
                Map<Object, Object> result = Maps.newHashMap();
                result.put(workHistoryCacheKey, workHistories);
                return result;
            }

            @Override
            public List<Object> loadNewObjects(Collection<Object> keys) {
                List<WorkHistoryPO> workHistoryPOs = workHistoryMapper.selectByAccountId(accountId);
                List<WorkHistory> workHistories = workHistoryPOs.stream()
                        .map(this::transformToWorkHistory)
                        .filter(Objects::nonNull).collect(Collectors.toList());

                List<Object> workHistoryGroups = Lists.newArrayList();
                workHistoryGroups.add(workHistories);
                return workHistoryGroups;
            }

            private WorkHistory transformToWorkHistory(WorkHistoryPO workHistoryPO) {
                if (workHistoryPO == null) {
                    return null;
                }

                WorkHistory workHistory = new WorkHistory();

                workHistory.setId(workHistoryPO.getId());
                workHistory.setAccountId(workHistoryPO.getAccountId());
                workHistory.setCompanyName(workHistoryPO.getCompanyName());
                workHistory.setJobTitle(workHistoryPO.getJobTitle());

                return workHistory;
            }
        });

        return CollectionUtils.isNotEmpty(workHistoryGroups) ? workHistoryGroups.get(0) : null;
    }

    @Override
    @Transactional
    public void deleteByAccountId(Long accountId) {
        workHistoryMapper.deleteByAccountId(accountId);
        removeCache(accountId);
    }

    private String buildCacheKey(Long accountId) {
        return CacheUtils.buildCacheKey(CACHE_PREFIX, accountId);
    }

    private void removeCache(Long accountId) {
        String cacheKey = buildCacheKey(accountId);
        cacheTemplate.delete(cacheKey);
    }
}
