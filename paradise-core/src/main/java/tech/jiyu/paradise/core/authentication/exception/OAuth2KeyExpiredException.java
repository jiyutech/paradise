package tech.jiyu.paradise.core.authentication.exception;

import tech.jiyu.utility.api.errors.annotations.ApiErrorKey;

/**
 * CreatedDate 2018-07-10 上午10:52
 *
 * @author SimonChan;emailAddress: simonchan@jinjie.tech
 */
@ApiErrorKey(group = "oauth2",
             id = "cannot.acquire.facebook.userInfo")
public class OAuth2KeyExpiredException extends RuntimeException {

    public OAuth2KeyExpiredException() {
        super();
    }

    public OAuth2KeyExpiredException(String message) {
        super(message);
    }

    public OAuth2KeyExpiredException(String message, Throwable cause) {
        super(message, cause);
    }

    public OAuth2KeyExpiredException(Throwable cause) {
        super(cause);
    }

    protected OAuth2KeyExpiredException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
