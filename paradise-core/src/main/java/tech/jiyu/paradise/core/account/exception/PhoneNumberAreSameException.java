package tech.jiyu.paradise.core.account.exception;

import tech.jiyu.utility.api.errors.annotations.ApiErrorKey;

/**
 * @author <a href="mailto:chenshao@dekeapp.com">shawsmith</a>
 */
@ApiErrorKey(group = "account",
             id = "same.phone.number")
public class PhoneNumberAreSameException extends RuntimeException {

    public PhoneNumberAreSameException() {
    }

    public PhoneNumberAreSameException(String message) {
        super(message);
    }

    public PhoneNumberAreSameException(String message, Throwable cause) {
        super(message, cause);
    }

    public PhoneNumberAreSameException(Throwable cause) {
        super(cause);
    }
}
