package tech.jiyu.paradise.core.account.service.impl;

import com.google.common.base.Throwables;
import com.google.common.collect.Lists;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Collection;
import java.util.List;
import java.util.Map;

import tech.jiyu.paradise.core.account.domain.Account;
import tech.jiyu.paradise.core.account.domain.AccountEvent;
import tech.jiyu.paradise.core.account.domain.AccountListener;
import tech.jiyu.paradise.core.account.domain.AccountPostCreatedEvent;
import tech.jiyu.paradise.core.account.domain.AccountPostPatchedEvent;
import tech.jiyu.paradise.core.account.domain.EducationExperience;
import tech.jiyu.paradise.core.account.domain.Residence;
import tech.jiyu.paradise.core.account.domain.WorkHistory;
import tech.jiyu.paradise.core.account.service.EducationExperienceService;
import tech.jiyu.paradise.core.account.service.ResidenceService;
import tech.jiyu.paradise.core.account.service.WorkHistoryService;
import tech.jiyu.paradise.core.privileges.service.AccountPrivilegeService;

/**
 * @author <a href="mailto:chenshao@jiyu.tech">shawsmith</a>
 */

@Component
public class AccountBuildInAssociationListener implements AccountListener {

    @Autowired
    private ResidenceService residenceService;

    @Autowired
    private EducationExperienceService educationExperienceService;

    @Autowired
    private WorkHistoryService workHistoryService;

    @Autowired
    private AccountPrivilegeService accountPrivilegeService;

    @Override
    public void onEvent(AccountEvent event) {
        if (event instanceof AccountPostCreatedEvent) {
            AccountPostCreatedEvent postCreatedEvent = (AccountPostCreatedEvent) event;
            Account account = postCreatedEvent.getAccount();
            if (account == null) {
                return;
            }

            Long accountId = account.getId();
            Map<String, Object> additionalProperties = postCreatedEvent.getAdditionalAttributes();
            saveOrUpdateAdditionalProperties(accountId, additionalProperties);
            saveOrUpdatePrivileges(accountId, account.getPrivilegeIds());
        } else if (event instanceof AccountPostPatchedEvent) {
            AccountPostPatchedEvent postPatchedEvent = (AccountPostPatchedEvent) event;
            Account account = postPatchedEvent.getAccount();
            if (account == null) {
                return;
            }

            Long accountId = account.getId();
            Map<String, Object> additionalProperties = postPatchedEvent.getAdditionalAttributes();
            saveOrUpdateAdditionalProperties(accountId, additionalProperties);
            saveOrUpdatePrivileges(accountId, account.getPrivilegeIds());
        }
    }

    @SuppressWarnings("unchecked")
    private void saveOrUpdateAdditionalProperties(Long accountId, Map<String, Object> additionalProperties) {
        if (additionalProperties == null) {
            return;
        }

        List<Map<String, Object>> residenceProperties = (List<Map<String, Object>>) additionalProperties.get("residences");
        if (residenceProperties != null) {
            List<Residence> residences = createTargetObjects(residenceProperties, Residence.class);
            if (residences != null) {
                residenceService.saveOrUpdate(accountId, residences);
            }
        }

        List<Map<String, Object>> workHistoryProperties = (List<Map<String, Object>>) additionalProperties.get("workHistories");
        if (workHistoryProperties != null) {
            List<WorkHistory> histories = createTargetObjects(workHistoryProperties, WorkHistory.class);
            if (histories != null) {
                workHistoryService.saveOrUpdate(accountId, histories);
            }
        }

        List<Map<String, Object>> educationExperienceProperties = (List<Map<String, Object>>) additionalProperties.get("educationExperiences");
        if (educationExperienceProperties != null) {
            List<EducationExperience> experiences = createTargetObjects(educationExperienceProperties, EducationExperience.class);
            if (experiences != null) {
                educationExperienceService.saveOrUpdate(accountId, experiences);
            }
        }
    }

    private <T> List<T> createTargetObjects(List<Map<String, Object>> objects, Class<T> targetClass) {
        if (CollectionUtils.isEmpty(objects)) {
            return Lists.newArrayListWithCapacity(0);
        }

        List<T> targetObjects = Lists.newArrayList();
        objects.forEach(properties -> {
            try {
                T bean = targetClass.newInstance();
                BeanUtils.populate(bean, properties);
                targetObjects.add(bean);
            } catch (Throwable t) {
                throw new RuntimeException(Throwables.getStackTraceAsString(t), t);
            }
        });
        return targetObjects;
    }

    private void saveOrUpdatePrivileges(Long accountId, Collection<Long> privilegeIds) {
        if (accountId == null || privilegeIds == null) {
            return;
        }
        accountPrivilegeService.saveOrUpdate(accountId, privilegeIds);
    }
}
