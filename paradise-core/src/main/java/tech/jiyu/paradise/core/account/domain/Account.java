package tech.jiyu.paradise.core.account.domain;

import com.google.common.collect.Maps;
import com.google.common.collect.Sets;

import com.fasterxml.jackson.databind.ObjectMapper;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;

import java.math.BigDecimal;
import java.time.DayOfWeek;
import java.time.LocalTime;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import tech.jiyu.paradise.core.privileges.domain.Privilege;
import tech.jiyu.paradise.core.util.AccountUtils;
import tech.jiyu.paradise.core.util.SecurityUtils;
import tech.jiyu.restful.api.domain.Create;
import tech.jiyu.restful.api.domain.Edit;
import tech.jiyu.restful.api.domain.Model;
import tech.jiyu.utility.datastructure.Pair;
import tech.jiyu.utility.json.JsonHelper;

/**
 * @author <a href="mailto:chenshao@dekeapp.com">shawsmith</a>
 */

@NoArgsConstructor
@Getter
@Setter
public class Account implements Model {

    private Long id;

    private Boolean deleted;

    private Long deletedTime;

    @Size(max = 128,
            message = "{account.name.limit}")
    private String name;

    @Size(max = 255,
            message = "{account.avatar.limit}")
    private String avatar;

    @Size(max = 16,
            message = "{account.gender.limit}")
    private String gender;

    @NotNull(groups = {Create.class, Edit.class},
            message = "{account.company.id.limit}")
    private Long companyId;

    @Size(max = 128,
            message = "{account.email.limit}")
    @Email(message = "{account.email.limit}")
    private String email;

    @Size(max = 8,
            message = "{account.country.code.limit}")
    private String countryCode;

    @Size(max = 16,
            message = "{account.phone.number.limit}")
    private String phoneNumber;

    @NotNull(groups = {Create.class, Edit.class},
            message = "{account.registration.date.limit}")
    private Long registrationDate;

    @NotNull(groups = {Create.class, Edit.class},
            message = "{account.registration.source.limit}")
    private String registrationSource;

    private String registrationTrafficType;

    private Date birthday;

    private String isoCode;

    private Long provinceId;

    private Long cityId;

    private Long districtId;

    @Size(max = 255,
            message = "{account.address.limit}")
    private String address;

    @Size(max = 64,
            message = "{account.signature.limit}")
    private String signature;

    @NotNull(groups = {Create.class, Edit.class},
            message = "{account.role.type.code.limit}")
    @Size(min = 1,
            max = 16,
            message = "{account.role.type.code.limit}")
    private String roleTypeCode;

    private String accountPermission;

    private Long creatorId;

    @NotNull(groups = {Create.class, Edit.class},
            message = "{account.enabled.limit}")
    private Boolean enabled;

    private Boolean verified;

    @Size(min = 1,
            max = 128,
            message = "{account.username.limit}")
    private String username;

    private String password;

    @Size(max = 32,
            message = "{account.wechat.open.id.limit}")
    private String weChatOpenId;

    @Size(max = 32,
            message = "{account.wechat.tiny.app.open.id.limit}")
    private String weChatTinyAppOpenId;

    @Size(max = 32,
            message = "{account.wechat.h5.app.open.id.limit}")
    private String weChatH5AppOpenId;

    @Size(max = 64,
            message = "{account.qq.open.id.limit}")
    private String qqOpenId;

    @Size(max = 64,
            message = "{account.facebook.user.id.limit}")
    private String facebookUserId;

    @Size(max = 32,
            message = "{account.union.id.limit}")
    private String unionId;

    @Size(max = 64,
            message = "{account.apple.user.id.limit}")
    private String appleUserId;

    @Size(max = 8,
            message = "{account.language.code.limit}")
    private String whatsappCountryCode;

    @Size(max = 16,
            message = "{account.whatsapp.phone.number.limit}")
    private String whatsappPhoneNumber;

    private Long telegramId;

    @Size(max = 32,
            message = "{account.id.card.name.limit}")
    private String idCardName;

    @Size(max = 32,
            message = "{account.id.card.number.limit}")
    private String idCardNumber;

    private String invitationCode;

    @Size(max = 32,
            message = "{account.id.card.timeZone.limit}")
    private String timeZone;

    private Long lastLoginTime;

    private String lastLanguageCode;

    @Size(max = 128,
            message = "{account.profession.limit}")
    private String profession;

    @Size(max = 255,
            message = "{account.individual.resume.limit}")
    private String individualResume;

    private Boolean audited;

    private Boolean workDayTimeEnabled;

    private List<DayOfWeek> allowedWorkDays;

    private List<Pair<LocalTime, LocalTime>> allowedWorkTimes;

    private Boolean polyAdminEnabled;

    @Size(max = 128,
            message = "{account.nickname.limit}")
    private String nickname;

    @Size(max = 128,
            message = "{account.firstName.limit}")
    private String firstName;
    @Size(max = 128,
            message = "{account.lastName.limit}")
    private String lastName;
    private Boolean agreeToReceiveInfo;

    private List<AccountHistoryPhoneNumber> historyPhoneNumbers;

    private Map<String, Object> additionalProperties;

    private Collection<Long> privilegeIds;
    private List<Privilege> privileges;
    private Long accountEstimateDeletingTime;
    private BigDecimal accountDeletingCoolingOffDays; // 用BigDecimal主要是兼容测试场景下，天数可以为小数的情况

    public boolean isActuallyEnabled() {
        return Boolean.TRUE.equals(enabled) && Boolean.TRUE.equals(polyAdminEnabled);
    }

    public boolean isSystem() {
        return StringUtils.equals(SecurityUtils.SYSTEM, roleTypeCode);
    }

    public boolean isIT() {
        return StringUtils.equals(SecurityUtils.IT, roleTypeCode);
    }

    public boolean isCustomer() {
        return StringUtils.equals(SecurityUtils.CUSTOMER, roleTypeCode);
    }

    public boolean isBuyer() {
        return StringUtils.equals(SecurityUtils.BUYER, roleTypeCode);
    }

    public boolean isOperator() {
        return StringUtils.equals(SecurityUtils.OPERATOR, roleTypeCode);
    }

    public boolean isDeveloper() {
        return StringUtils.equals(SecurityUtils.DEVELOPER, roleTypeCode);
    }

    public boolean isSales() {
        return StringUtils.equals(SecurityUtils.SALES, roleTypeCode);
    }

    public boolean isService() {
        return StringUtils.equals(SecurityUtils.SERVICE, roleTypeCode);
    }

    public boolean isCopy() {
        return StringUtils.equals(SecurityUtils.COPY, roleTypeCode);
    }

    public boolean isBoss() {
        return StringUtils.equals(SecurityUtils.BOSS, roleTypeCode);
    }

    public boolean isStaff() {
        return StringUtils.equals(SecurityUtils.STAFF, roleTypeCode);
    }

    public boolean hasPrivilege(String privilegeName) {
        if (StringUtils.isBlank(privilegeName)) {
            return false;
        }
        return hasAllPrivileges(privilegeName);
    }

    public boolean hasAnyPrivileges(String... privilegeNames) {
        if (ArrayUtils.isEmpty(privilegeNames)) {
            return false;
        }
        return hasAnyPrivileges(Sets.newHashSet(privilegeNames));
    }

    public boolean hasAnyPrivileges(Collection<String> privilegeNames) {
        if (CollectionUtils.isEmpty(privilegeNames) || CollectionUtils.isEmpty(privileges)) {
            return false;
        }

        for (String privilegeName : privilegeNames) {
            boolean present = privileges
                    .stream()
                    .anyMatch(privilege -> StringUtils.equalsIgnoreCase(privilege.getName(), privilegeName));
            if (present) {
                return true;
            }
        }
        return false;
    }

    public boolean hasAllPrivileges(String... privilegeNames) {
        if (ArrayUtils.isEmpty(privilegeNames)) {
            return false;
        }
        return hasAllPrivileges(Sets.newHashSet(privilegeNames));
    }

    public boolean hasAllPrivileges(Collection<String> privilegeNames) {
        if (CollectionUtils.isEmpty(privilegeNames) || CollectionUtils.isEmpty(privileges)) {
            return false;
        }

        for (String privilegeName : privilegeNames) {
            boolean present = privileges
                    .stream()
                    .anyMatch(privilege -> StringUtils.equalsIgnoreCase(privilege.getName(), privilegeName));
            if (!present) {
                return false;
            }
        }
        return true;
    }


    @Override
    public Map<String, Object> populatePatchableProperties(ObjectMapper objectMapper) {

        Map<String, Object> properties = Maps.newHashMap();
        if (deletedTime != null) {
            properties.put("deletedTime", deletedTime);
        }

        if (name != null) {
            properties.put("name", name);
        }

        if (avatar != null) {
            properties.put("avatar", avatar);
        }

        if (gender != null) {
            properties.put("gender", gender);
        }

        if (companyId != null) {
            properties.put("companyId", companyId);
        }

        if (email != null) {
            properties.put("email", email);
        }

        if (countryCode != null) {
            properties.put("countryCode", countryCode);
        }

        if (birthday != null) {
            properties.put("birthday", birthday);
        }

        if (isoCode != null) {
            properties.put("isoCode", isoCode);
        }

        if (provinceId != null) {
            properties.put("provinceId", provinceId);
        }

        if (cityId != null) {
            properties.put("cityId", cityId);
        }

        if (districtId != null) {
            properties.put("districtId", districtId);
        }

        if (address != null) {
            properties.put("address", address);
        }

        if (signature != null) {
            properties.put("signature", signature);
        }

        if (roleTypeCode != null) {
            properties.put("roleTypeCode", roleTypeCode);
        }

        if (accountPermission != null) {
            properties.put("accountPermission", accountPermission);
        }

        if (enabled != null) {
            properties.put("enabled", enabled);
        }

        if (verified != null) {
            properties.put("verified", verified);
        }

        if (weChatOpenId != null) {
            properties.put("weChatOpenId", weChatOpenId);
        }

        if (weChatTinyAppOpenId != null) {
            properties.put("weChatTinyAppOpenId", weChatTinyAppOpenId);
        }

        if (weChatH5AppOpenId != null) {
            properties.put("weChatH5AppOpenId", weChatH5AppOpenId);
        }

        if (qqOpenId != null) {
            properties.put("qqOpenId", qqOpenId);
        }

        if (facebookUserId != null) {
            properties.put("facebookUserId", facebookUserId);
        }

        if (appleUserId != null) {
            properties.put("appleUserId", appleUserId);
        }

        if (whatsappCountryCode != null) {
            properties.put("whatsappCountryCode", whatsappCountryCode);
        }

        if (whatsappPhoneNumber != null) {
            properties.put("whatsappPhoneNumber", whatsappPhoneNumber);
        }

        if (telegramId != null) {
            properties.put("telegramId", telegramId);
        }

        if (idCardName != null) {
            properties.put("idCardName", idCardName);
        }

        if (idCardNumber != null) {
            properties.put("idCardNumber", idCardNumber);
        }

        if (invitationCode != null) {
            properties.put("invitationCode", invitationCode);
        }

        if (timeZone != null) {
            properties.put("timeZone", timeZone);
        }

        if (lastLoginTime != null) {
            properties.put("lastLoginTime", lastLoginTime);
        }

        if (lastLanguageCode != null) {
            properties.put("lastLanguageCode", lastLanguageCode);
        }

        if (profession != null) {
            properties.put("profession", profession);
        }

        if (individualResume != null) {
            properties.put("individualResume", individualResume);
        }

        if (audited != null) {
            properties.put("audited", audited);
        }

        if (workDayTimeEnabled != null) {
            properties.put("workDayTimeEnabled", workDayTimeEnabled);
        }

        if (allowedWorkDays != null) {
            String allowedWorkDaysJson = JsonHelper.serialize(AccountUtils.formatWorkDays(allowedWorkDays), objectMapper);
            properties.put("allowedWorkDays", allowedWorkDaysJson);
        }

        if (allowedWorkTimes != null) {
            String allowedWorkTimesJson = JsonHelper.serialize(AccountUtils.formatWorkTimes(allowedWorkTimes, AccountUtils.WORK_TIMES_FORMAT), objectMapper);
            properties.put("allowedWorkTimes", allowedWorkTimesJson);
        }

        if (polyAdminEnabled != null) {
            properties.put("polyAdminEnabled", polyAdminEnabled);
        }

        if (nickname != null) {
            properties.put("nickname", nickname);
        }
        if (firstName != null) {
            properties.put("firstName", firstName);
        }
        if (lastName != null) {
            properties.put("lastName", lastName);
        }
        if (agreeToReceiveInfo != null) {
            properties.put("agreeToReceiveInfo", agreeToReceiveInfo);
        }

        return properties;
    }
}
