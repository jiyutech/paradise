package tech.jiyu.paradise.core.account.domain;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import tech.jiyu.restful.api.domain.Create;
import tech.jiyu.restful.api.domain.Edit;

import javax.validation.constraints.NotNull;

/**
 * Created By InvitationCode
 *
 * Date 2018/2/9 15:03
 *
 * @author SimonChan;emailAddress: simonchan@jiyu.tech
 */
@NoArgsConstructor
@Getter
@Setter
public class InvitationCode {

    @NotNull(groups = {Create.class, Edit.class})
    private String code;

    @NotNull(groups = {Create.class, Edit.class})
    private InvitationCodeStatus status;

    private Long accountId;
}
