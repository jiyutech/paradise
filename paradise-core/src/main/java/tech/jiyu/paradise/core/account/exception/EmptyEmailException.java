package tech.jiyu.paradise.core.account.exception;

import tech.jiyu.utility.api.errors.annotations.ApiErrorKey;

/**
 * Created By EmptyEmailException
 *
 * @author Xhh52
 */
@ApiErrorKey(group = "account",
             id = "empty.email")
public class EmptyEmailException extends RuntimeException {
    public EmptyEmailException() {
        super();
    }

    public EmptyEmailException(String message) {
        super(message);
    }

    public EmptyEmailException(String message, Throwable cause) {
        super(message, cause);
    }

    public EmptyEmailException(Throwable cause) {
        super(cause);
    }

    protected EmptyEmailException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
