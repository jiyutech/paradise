package tech.jiyu.paradise.core.account.service;

import org.apache.commons.lang3.StringUtils;

import tech.jiyu.paradise.core.util.SecurityUtils;

/**
 * @author <a href="mailto:chenshao@jinjie.tech">shawsmith</a>
 */
public interface RolePermissionService {

    default boolean isSystemAccount(String roleTypeCode) {
        return StringUtils.equals(SecurityUtils.SYSTEM, roleTypeCode);
    }

    default boolean isITAccount(String roleTypeCode) {
        return StringUtils.equals(SecurityUtils.IT, roleTypeCode);
    }

    boolean isAdministrationAccount(String roleTypeCode);
}
