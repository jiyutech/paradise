package tech.jiyu.paradise.core.account.service;

import org.springframework.validation.annotation.Validated;

import java.util.Map;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import tech.jiyu.paradise.core.account.domain.Account;
import tech.jiyu.paradise.core.account.domain.AccountListQuery;
import tech.jiyu.utility.pagination.PaginationResult;

/**
 * @author <a href="mailto:chenshao@dekeapp.com">shawsmith</a>
 */
@Validated
public interface AccountListService {

    PaginationResult<Account, Map<String, Long>> getAccounts(@NotNull @Valid AccountListQuery query);
}
