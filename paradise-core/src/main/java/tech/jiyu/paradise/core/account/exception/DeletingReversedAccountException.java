package tech.jiyu.paradise.core.account.exception;

import tech.jiyu.utility.api.errors.annotations.ApiErrorKey;

/**
 * @author <a href="mailto:chenshao@jiyu.tech">shawsmith</a>
 */
@ApiErrorKey(group = "account",
             id = "deleting.reversed.account")
public class DeletingReversedAccountException extends RuntimeException {

    public DeletingReversedAccountException() {
    }

    public DeletingReversedAccountException(String message) {
        super(message);
    }

    public DeletingReversedAccountException(String message, Throwable cause) {
        super(message, cause);
    }

    public DeletingReversedAccountException(Throwable cause) {
        super(cause);
    }
}
